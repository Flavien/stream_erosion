%function to plot a contour comparison of all the flood simulations
%that I did
%
%

load trans_IS_all_flood


FSlab = 28;
FSax  = 28;
FScont = 24;
FS_txt = 30;
fx = 0.5;
fy = 0.5;

fig1 = 1;
fig2 = 2;

%first let's make a matrix
for i=1:length(trans_IS)

    x = mod(i,10);
    if x == 0 x = 10; end

    if i <= 10 y =1;end
    if i >10 && i <= 20 y =2;end
    if i>20 && i <= 30 y =3;end
    if i>30 && i <= 40 y =4;end
    if i>40 && i <= 50 y =5;end
    if i>50 && i <= 60 y =6;end

    flood_mat_depth(x,y) = max(trans_IS(i).sum_e_eff);
    flood_mat_eros(x,y) = trans_IS(i).result_eros;
    flood_mat_Qout(x,y) = trapz(trans_IS(i).Q_out);
    flood_Qtc_term(x,y) = trapz(trans_IS(i).Q_tc(:,end));
end

metre_fact = 1e-3;

%%%%%%%%%%%
f1 = figure(fig1);
f1.Units = 'normalized';
%f1.Position = [0,0.3,fx,fy];

%im = imagesc(flood_mat_depth);
[C,h] = contourf(flood_mat_depth.*metre_fact,15);
imax = gca;

%imax.YDir = 'normal';
imax.YLabel.String = 'Volume factor';
imax.XLabel.String = 'Flood duration (days)';
imax.FontSize = FSax;

cmap = flip(colormap(bone));
colormap(cmap)
cb = colorbar;
caxis([0 60].*metre_fact)
cb.Label.String = 'Max. bedrock channel depth (m)';

h.LineColor = 'none';

imax.YTick = [1:1:10];
imax.XTick = [1:1:6];
imax.XTickLabel = {'10','20','30','40','50','60'};

imax.Position(3) = 0.63;

%%%%%%%%%%%
f2 = figure(fig2);
f1.Units = 'normalized';
%f1.Position = [0,0.3,fx,fy];

%im = imagesc(flood_mat_depth);
[C2,h2] = contourf(flood_mat_eros,15);
imax2 = gca;

%imax.YDir = 'normal';
imax2.YLabel.String = 'Volume factor';
imax2.XLabel.String = 'Flood duration (days)';
imax2.FontSize = FSax;

colormap(cmap)
cb2 = colorbar;
caxis([0 0.3])
cb2.Label.String = 'Averaged erosion rate (mm)';

h2.LineColor = 'none';

imax2.YTick = [1:1:10];
imax2.XTick = [1:1:6];
imax2.XTickLabel = {'10','20','30','40','50','60'};

imax2.Position(3) = 0.63;
