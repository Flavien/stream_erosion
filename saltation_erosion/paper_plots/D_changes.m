%file to plot the changes in patterns of erosion for a given hydrology run
%and different sediment supply rates or different sediment sizes
%
%


%switch to extract stuff from saltation only runs or from total load eros runs
saltation_only = 0;
%presentation formating switch
pres_font = 0;

%sim_struct = load_simulations;
sim_struct = load_simulations(saltation_only);
length_struct = length(sim_struct);

%dx = 200;
%dx = 1000;
%dx = 1 / (length(sim_struct(1).E));
dx = 500 /1000;

x_reg = [0:length(sim_struct(1).E)].*dx;
x_stag = (x_reg(2:end) + x_reg(1:end-1)) ./ 2;
x = x_stag;
MS = zeros(length_struct,1) ;
LW = zeros(length_struct,1) +  3;
text_frac = 0.9;

sub3 = 0;

    Sx = 2;
    Sy = 1;
    p2 = 0;
    p3 = 2;

D_comp = 1;

    ln_spec_col = {rgb('LightSkyBlue'),rgb('SkyBlue'),rgb('DeepSkyBlue'),rgb('DodgerBlue'),'k',...
                rgb('Orange'),rgb('DarkOrange'),rgb('OrangeRed'),rgb('ForestGreen'),rgb('DarkGreen')};
    ln_spec_type = {'-.','-.','-.','-.','-','--','--','--','--','--'};

    %legend_text = {'D=1 mm','2','5','10','60 (REF)','200','300','400','500','550'};
    %legend_text = {'D = 1 mm','2 mm','5 mm','10 mm','60 mm (REF)','200 mm','300 mm','400 mm','500 mm','550 mm'};
    legend_text = {'D = 1 mm','2 mm','5 mm','10 mm','D_{ref} = 60 mm','200 mm','300 mm','400 mm','500 mm','550 mm'};
    LW = [3 1.5 3 1.5 3 1.5 3 2 1.5 3];
    

if pres_font == 1
    %presentation values
    FSlab = 32;
    FSax  = 32;

    fx = 0.55;
    fy = 0.4;
else
    %regular plot values
    FSlab = 26;
    FSax  = 26;
    FS_txt = 28;

    fx = 0.4;
    fy = 0.60;
end


k = 1;

f1 = figure(k);
set(f1,'Units','normalized')
set(f1,'Position',[0,0.5,fx,fy])
S1 = subplot(Sx,Sy,1);
for i=1:length_struct
    width_fact = sqrt(2.*sim_struct(i).S./pi) .* 2;
    %width_fact = width_fact .* 1000; %For mm /a instead of m/a
    %fp1 = fancy_plot(x,sim_struct(i).e_tot.*width_fact,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
    fp1 = semilogy(x,sim_struct(i).e_tot.*width_fact + 1e-6,'Linewidth',LW(i),'Color',ln_spec_col{i},'Linestyle',ln_spec_type{i});
hold on

end
set(legend,'Location','Best')
set(gca,'FontSize',FSax)
%ylabel('Total erosion TLEM, E_{tot} (mm/a*W_{ch})')
ylabel('$E_{\rm tot} \, ({\rm m^2 \, a^{-1}}$)','interpreter','latex')

columnlegend(4,legend_text,'Location','North','boxoff');

grid on
%grid minor

txt_posA = 2.5.*text_frac;
tA = text(1,txt_posA,'A');
set(tA,'FontSize',FS_txt);


S2 = subplot(Sx,Sy,p3);
for i=1:length_struct
    plot_quantity = sim_struct(i).q_tot ./ (sim_struct(i).q_tc./2650);
    %plot_quantity = sim_struct(i).q_b ./ sim_struct(i).q_tc;
    fp2 = fancy_plot(x,plot_quantity,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
hold on
end
set(gca,'FontSize',FSax)
xlabel('Distance from divide (km)')
ylabel('$q_{\rm s}/q_{\rm tc}$','interpreter','latex')
grid on
%grid minor
txt_posC = 2.*text_frac;
tC = text(1,txt_posC,'B');
set(tC,'FontSize',FS_txt);

%Let's make everything look nice

S1.XTickLabel = '';

S1.XLim = [0 50];
S2.XLim = [0 50];
S1.XTick = [0:10:50];
S2.XTick = [0:10:50];

%S1.YLim = [0 2.5];
%S1.YTick = [0:0.5:2.5];
S1.YLim = [1e-2 5];
label1 = linspace(1e-2,1e-1,10);
label2 = linspace(2e-1,1,9);
label = [label1 label2 2 3 4 5];
S1.YTick = label;
S1.YTickLabel = {'10^{-2}','','','','','','','','','10^{-1}','','','','','','','','','1','','','','5'};

S2.YLim = [0 2];
S2.YTick = [0:0.5:2];
S2.YTickLabel = {'0','0.5','1.0','1.5','2.0'};

S1.Position(1) = 0.18;
S2.Position(1) = 0.18;

S1.Position(2) = 0.51;
%S2.Position(2) = 0.18;

S1.Position(3) = 0.775;
S2.Position(3) = 0.775;

S1.Units = 'points';
S2.Units = 'points';
S1.Position(4) = 225;
S2.Position(4) = 225;
