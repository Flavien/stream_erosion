%file to plot the comparison between topo and maybe other runs
%
%
%

loaded_already = 1;

if loaded_already == 1
    %load trans_IS_topo
    load trans_IS_supply
    TR_IS_sup = trans_IS;
else
    TR_IS_sup = load_IS_TR_comp();
end

%plotting stuffs...
x_plot = [1:min(size(trans_IS(1).q_tc_vol))]-0.5;
x_plot = x_plot .* 500./1000;

FSlab = 26;
FSax  = 26;
FS_txt = 28;

text_frac = 0.9;

LW1 = 3;
LW2 = 3.5;

fig1 =1;
fx = 0.4;
fy = 0.3;
sx1 = 2;
sy1 = 3;

L10 = 0;
L50 = 1;

x_start = 40;

cont_nb = 10;

need_sub = 0;

run_nb1 = length(TR_IS_sup);

%run_nb2 = length(TR_IS_geom);

    ln_colA = {rgb('OrangeRed'),rgb('SkyBlue'),'k',rgb('DimGray'),'k'} ;
    ln_typeA = {'-','-','--','-','--'};

    ln_colB = {rgb('OrangeRed'),'k',rgb('SkyBlue'),rgb('DimGray'),'k'} ;


%%%%%%%%%%%
f1 = figure(fig1);
f1.Units = 'normalized';
f1.Position = [0,0.3,fx,fy];

subA = subplot(sx1,sy1,[1,3]);

    for i =1 :run_nb1
        p1(i) = plot(x_plot(x_start:end),TR_IS_sup(i).sum_e_eff(x_start:end),'LineWidth',LW1,'LineStyle','--','Color',ln_colA{i});
        %c1(i,:) = get(p1(i),'Color');
        if i == 1
            hold on
        end
    end

    for i=1:run_nb1
        p2(i) = plot(x_plot(x_start:end),TR_IS_sup(i).sum_E_eff(x_start:end),'Color',ln_colA{i},'LineWidth',LW2);
        %p2(i) = plot(x_plot(x_start:end),TR_IS_sup(i).sum_E_eff(x_start:end),'Color',c1(i,:),'LineWidth',LW2);
    end
    
    ylabel('Integrated erosion')
    xlabel('Distance from divide (km)')
    grid on
    %grid minor

    %l1 = legend('IS \int e_{tot} dt (mm)','IS\_RA','IS\_RB','IS \int E_{tot} dt (mm \times m)');
    %l1.Location = 'West';
    %legend boxoff
    LtxtA = {'','','','','T\_SSP/4','T\_SSP/2','T\_REF','T\_SSPOPT'};

    %clegA = columnlegend(2,LtxtA,'Location','SouthWest','FontSize',FSax-2,'boxoff');

    tly = 440;
    TA1 = text(21,tly,{'$\int \dot{e}_{\rm tot} \mathrm{d}t$','(mm)'},'interpreter','latex');
    TA2 = text(26,tly,{'$\int E_{\rm tot} \mathrm{d}t$','$\mathrm{(mm \times m)}$'},'interpreter','latex');

    TA1.FontSize = FS_txt -4;
    TA2.FontSize = FS_txt -4;

    set(gca,'FontSize',FSax)


    %txt_pos2A = 320.*text_frac;
    %txt_pos2A = 550.*(1 - text_frac);
    %t2A = text(21,txt_pos2A,'A');

    %set(t2A,'FontSize',FS_txt);

        ah13 = gca;
        l13 = legend(ah13,p1, '','','','');
        ah13B = axes('position',get(gca,'position'), 'visible','off');
        l13B = legend(ah13B,p2,' T\_SSP/4',' T\_SSP/2',' T\_REF',' T\_SSPOPT');

        l13.Location = 'West';
        l13B.Location = 'East';
        l13.FontSize = FSax-3;
        l13B.FontSize = FSax-3;
        l13.Box = 'off';
        l13B.Box = 'off';


if need_sub == 1
    subB = subplot(sx1,sy1,[4,6]);

        for i =1 :run_nb2-1
            p3(i) = plot(x_plot(x_start:end),TR_IS_geom(i).sum_e_eff(x_start:end),'LineWidth',LW1,'LineStyle','--','Color',ln_colB{i});
            %c2(i,:) = get(p3(i),'Color');
            if i == 1
                hold on
            end
        end

        for i=1:run_nb2-1
            p4(i) = plot(x_plot(x_start:end),TR_IS_geom(i).sum_E_eff(x_start:end),'Color',ln_colB{i},'LineWidth',LW2);
            %p4(i) = plot(x_plot(x_start:end),TR_IS_geom(i).sum_E_eff(x_start:end),'Color',c2(i,:),'LineWidth',LW2);
        end
        
        ylabel('Integrated erosion')
        xlabel('Distance from divide (km)')
        grid on
        %grid minor

        %l2 = legend('IS1300 \int e_{tot} dt (mm)','IS','IS700','IS1300 \int E_{tot} dt (mm \times m)');
        %l2.Location = 'West';
        %legend boxoff

        TB1 = text(21,tly,{'\int e_{tot}dt','(mm)'});
        TB2 = text(26,tly,{'\int E_{tot}dt','(mm \times m)'});

        TB1.FontSize = FS_txt -4;
        TB2.FontSize = FS_txt -4;

        set(gca,'FontSize',FSax)

        %txt_pos2B = 320.*text_frac;
        txt_pos2B = 320.*(1-text_frac);
        t2B = text(21,txt_pos2B,'B');

        set(t2B,'FontSize',FS_txt);

            ah46 = gca;
            l46 = legend(ah46,p3, '','','');
            ah46B = axes('position',get(gca,'position'), 'visible','off');
            l46B = legend(ah46B,p4,' T\_1300',' T\_REF',' T\_700');

            l46.Location = 'West';
            l46B.Location = 'East';
            l46.FontSize = FSax-3;
            l46B.FontSize = FSax-3;
            l46.Box = 'off';
            l46B.Box = 'off';
end


%Making everything pretty

subA.XLim = [x_start/2 50];
subA.YLim = [0 550];

subA.XTick = [x_start/2:5:50];


%subA.XTickLabel = '';

subA.Position(1) = 0.18;
subB.Position(1) = 0.18;

subA.Position(2) = 0.19;

subA.Position(3) = 0.775;

subA.Units = 'points';
subA.Position(4) = 250;

if need_sub == 1
    subB.XLim = [x_start/2 50];
    subB.YLim = [0 320];

    subB.XTick = [x_start/2:5:50];

    subB.Position(3) = 0.775;

    subB.Units = 'points';
    subB.Position(4) = 250;
end
