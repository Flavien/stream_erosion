%File to plot a the N-channel carved from different simulations

load trans_IS_flood_test.mat

%Runs that I need:
%OPT = 1
%W250 = 2
%IS1300 = 4
%IS_REF = 7
%IS_RB = 8
%IS_SSP/4 = 12
runs_needed = [1 2 3];

%plotting stuffs...
x_plot = [1:min(size(trans_IS(1).q_tc_vol))]-0.5;
x_plot = x_plot .* 500./1000;

%FSlab = 26;
%FSax  = 26;
%FScont = 22;
%FS_txt = 28;

FSlab = 28;
FSax  = 28;
FScont = 24;
FS_txt = 30;

text_frac = 0.9;

LW1 = 1.5;
LW2 = 2.5;

fig1 =1;
fx = 0.7;
fy = 0.45;
sx1 = 1;
sy1 = 2;

L10 = 0;
L50 = 1;

x_start = 1;

cont_nb = 10;

run_nb = length(runs_needed);

%%%%%%%%%%%
f1 = figure(fig1);
f1.Units = 'normalized';
f1.Position = [0,0.3,fx,fy];

%textx1 = 11;
%textx2 = 20;
textx1 = 1;
textx2 = 9;

kk = 1;
S1 = subplot(sx1,sy1,1);
    
    %k = runs_needed(kk);
    k = runs_needed(1);
    vectSA1 = [-200:10:0];
    vectSA2 = [-200:20:0];
    [CA,hA] = contour(x_plot(x_start:end),trans_IS(k).width,trans_IS(k).valley_topo(:,x_start:end),vectSA1);
    hold on
    [CA2,hA2] = contour(x_plot(x_start:end),trans_IS(k).width,trans_IS(k).valley_topo(:,x_start:end),vectSA2);
    hA.LineColor = [0.7 0.7 0.7];
    hA2.LineColor = 'k';

    textyA = max(trans_IS(k).width) .* 0.9;
    TA1 = text(textx1,textyA,'A');
    %TA2 = text(textx2,textyA,'TR\_IS\_REF');
    TA2 = text(textx2,textyA,'Season');
    TA1.FontSize = FSax;
    TA2.FontSize = FSax;

    %t1 = clabel(CA2,'manual');


    xlabel('Distance from divide (km)')
    ylabel('N-Channel radius (m)')
    set(gca,'FontSize',FSax)

kk = 2;
S2 = subplot(sx1,sy1,2);
    
    %k = runs_needed(kk);
    k = runs_needed(2);
    %k = 5;
    %vectSB1 = [-1.2:0.2:0];
    %vectSB2 = [-1.2:0.4:0];
    %vectSB1 = [-35:2.5:0];
    %vectSB2 = [-35:5:0];
    %
    vectSB1 = [-10:1:0];
    vectSB2 = [-10:2:0];
    [CB,hB] = contour(x_plot(x_start:end),trans_IS(k).width,trans_IS(k).valley_topo(:,x_start:end),vectSB1);
    hold on
    [CB2,hB2] = contour(x_plot(x_start:end),trans_IS(k).width,trans_IS(k).valley_topo(:,x_start:end),vectSB2);
    hB.LineColor = [0.7 0.7 0.7];
    hB2.LineColor = 'k';

    textyB = max(trans_IS(k).width) .* 0.9;

    TB1 = text(textx1,textyB,'B');
    %TB2 = text(textx2,textyB,'FLOOD\_Nye\_shape');
    TB2 = text(textx2,textyB,'10-days flood');
    TB1.FontSize = FSax;
    TB2.FontSize = FSax;


    xlabel('Distance from divide (km)')
    %ylabel('N-Channel radius (m)')
    set(gca,'FontSize',FSax)


Nchan_Xlim = [0 50];
S1.XLim = Nchan_Xlim;
S2.XLim = Nchan_Xlim;

Nchan_Xtick = [0:5:50];
S1.XTick = Nchan_Xtick;
S2.XTick = Nchan_Xtick;

Nchan_XTickLab = {'0','','10','','20','','30','','40','','50'};
S1.XTickLabel = Nchan_XTickLab;
S2.XTickLabel = Nchan_XTickLab;

%Making everything pretty again...
%{
Nchan_Ylim = [-2.3 2.3];
S1.YLim = Nchan_Ylim;
S2.YLim = Nchan_Ylim;
S3.YLim = Nchan_Ylim;

Nchan_Ytick = [-2:1:2];
S1.YTick = Nchan_Ytick;
S2.YTick = Nchan_Ytick;
S3.YTick = Nchan_Ytick;
S4.YTick = Nchan_Ytick;
S5.YTick = Nchan_Ytick;
S6.YTick = Nchan_Ytick;

sub_w = 0.29;
S1.Position(3) = sub_w;
S2.Position(3) = sub_w;
S3.Position(3) = sub_w;
S4.Position(3) = sub_w;
S5.Position(3) = sub_w;
S6.Position(3) = sub_w;

sub_h = 0.42;
S1.Position(4) = sub_h;
S2.Position(4) = sub_h;
S3.Position(4) = sub_h;
S4.Position(4) = sub_h;
S5.Position(4) = sub_h;
S6.Position(4) = sub_h;

S1.Position(1) = 0.055;
S4.Position(1) = 0.055;
S2.Position(1) = 0.370;
S5.Position(1) = 0.370;
S3.Position(1) = 0.690;
S6.Position(1) = 0.690;

S1.Position(2) = 0.56; 
S2.Position(2) = 0.56; 
S3.Position(2) = 0.56; 
%}
%And now trying to put some label...

subplot(S1)
t1 = clabel(CA2,'manual');
for j=2:2:length(t1)
    t1(j).FontSize = FScont;
    t1(j).LineWidth = LW1;
end

subplot(S2)
t2 = clabel(CB2,'manual');
for j=2:2:length(t2)
    t2(j).FontSize = FScont;
    t2(j).LineWidth = LW1;
end

%{
subplot(S3)
t3 = clabel(CC2,'manual');
for j=2:2:length(t3)
    t3(j).FontSize = FScont;
    t3(j).LineWidth = LW1;
end
%}
