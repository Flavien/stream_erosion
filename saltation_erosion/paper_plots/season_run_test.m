%Little scrit to help understanding what is happening in the runs
%where I play with the season and input
%
%

%load ../erosion/results/tot_eros/Rchan_SS_surf_ISref1_BC_Atb.mat
%load TR_IS_SD3_01_BenchC_Atb

clf

just_plot = 1;
plot_on = 1;
plot_cont = 0;
fourth_line = 0;

ch_nb = 1;
disp('Number of channels is:')
disp(ch_nb)
disp('And length is:')
L10 = 0
L50 = 1

fig1 = 1;
fig2 = 2;

if just_plot == 0
    Qc_t = 0.01;
    if L10 == 1
        %reg glacier
        %R_t = 0.125;
        R_t = 0.132;
    elseif L50 == 1
        %Ice sheet
        R_t = 0.160;
    end

    q_tc_vol = q_tc_ar./2650.*600;
    q_tc_vol(R < R_t) = 0;

    Q_tc = q_tc_ar./2650.*(2.*R).*ch_nb;
    Q_tc(R < R_t) = 0;

    Q_tc_dt = q_tc_vol.*(2.*R).*ch_nb;
    Q_tot = q_tot_temp.*(2.*R).*ch_nb;
    %Q_tot = Q_tot.*1.262;
    %Q_tot = Q_tot.*0.486;
    %Q_tot = Q_tot.*2;
    %Q_tot = Q_tot.*0.618;

    Q_out = min(Q_tc_dt(:,end),Q_tot(:,end));
    %sum(Q_out)./5e4./1e3

    Q_tc_short = Q_tc(1:6:end,:);
    q_tc_short = q_tc_ar(1:6:end,:);

    R_short = R(1:6:end,:);
    e_eff = e_tot_ar.*1000.*600./constants.secondsinyear;
    e_eff(R < R_t) = 0;
    E_eff = e_eff .* (2.*R).*ch_nb;

    e_tot_short = e_tot_ar(1:6:end,:).*1000;
    e_tot_short(R_short < R_t) = 0;

    E_tot = e_tot_short .* (2.*R_short).*ch_nb;
    rel_supply = q_tot_loc(1:6:end,:) ./ (q_tc_ar(1:6:end,:)./2650);
    %Fe_short = Fe_tot_ar(1:6:end,:);
    Fe_short = Fe_ar(1:6:end,:);
    Fe_short(R_short < R_t) = 0;
    cb_mod = cb_ar;
    cb_mod(Fe_tot_ar == 0) = -0.02;
    cb_short = cb_mod(1:6:end,:);
    w_i_eff_short = w_i_eff_ar(1:6:end,:);
    Qc_short = Qc(1:6:end,:);

    t_stage = tau_ratio;
    %t_stage(R < R_t+0.03) = 0;
    t_stage(R < R_t) = 0;
    t_stage_short = t_stage(1:6:end,:);


    %computing stuff about erosion
    sum_e_eff = sum(e_eff);
    sum_E_eff = sum(E_eff);

    %res_eros = sum(sum_E_eff)./ nb_nodes / width glacier
    result_eros = sum(sum_E_eff)./100./1000;
    
    vol_exit = sum(Q_out);

    %plotting stuffs ...
    t_long = [1:length(q_tc_ar)];
    t_long = t_long ./ 6 ./ 24;
    t_short = [1:length(Q_tc_short)];
    t_short = t_short./24;

    tunnel_valley_plot
end


%s_end = 120*24;
s_end = 150*24;

x_plot = [1:min(size(q_tc_ar))]-0.5;
x_plot = x_plot .* dx./1000;

LW2 = 3.5;
LW1 = 2;
LW3 = 3;
LW4 = 2;

FSlab = 26;
FSax  = 26;
FS_txt = 28;

fx = 0.4;
fy = 0.7;
fx2 = 0.5;
fy2 = 1;
text_frac = 0.87;

input_type = 1;

if input_type == 1
    load ref_input
    b_s_time_short = b_s_time(1:6:end,:);
    b_s_norm_short = b_s_time_norm(1:6:end,end);
elseif input_type == 2
    %load real_input_GPSB_2007_IS1000
    load input_conversion
    b_s_time_short = t_short.*0;
    b_s_time_short(1:length(real_input_2007)) = real_input_2007(:,end) .* fact_2007;
    b_s_norm_short = b_s_time_short ./ 8e-7;
elseif input_type == 3
    load input_conversion
    b_s_time_short = t_short.*0;
    b_s_time_short(1:length(real_input_2008)) = real_input_2008(:,end) .* fact_2008 .* fact_2008_to_2007;
    b_s_norm_short = b_s_time_short ./ 8e-7;
end

if plot_on == 1
    sx1 = 3;
    sy1 = 1;
    if plot_cont == 1
        f1 = figure(fig1);
        f1.Units = 'normalized';
        f1.Position = [0,0.3,fx,fy];
        %figure
        subplot(sx1,sy1,1)
        %contourf(x_plot,t_short(1:s_end),Qc_short(1:s_end,:),[0:0.1:max(max(Qc_short))],'linestyle','none')
        %contourf(x_plot,t_short(1:6:s_end),Qc_short(1:6:s_end,:),[0:0.2:max(max(Qc_short))],'linestyle','none')

        imagesc(x_plot,t_short(1:s_end),Qc_short(1:s_end,:).*ch_nb);
        colorbar
        title('Water discharge, Q_{ch} (m^3/s)')
        set(gca,'YDir','normal')
        set(gca,'FontSize',FSax)
        if L10 == 1
            set(gca,'Xtick',0:2:10)
            txt_posA = s_end/24.*text_frac;
            tA = text(0.25,txt_posA,'A');
        elseif L50 == 1
            set(gca,'Xtick',0:5:50)
            txt_posA = s_end/24.*text_frac;
            tA = text(2,txt_posA,'A');
        end
        %
        set(tA,'FontSize',FS_txt);
        set(tA,'Color',[0.85 0.85 0.85])

        %figure
        subplot(sx1,sy1,2)
        %contourf(x_plot,t_short(1:6:s_end),Q_tc_short(1:6:s_end,:),[0:2e-3:max(max(Q_tc_short))],'linestyle','none')

        imagesc(x_plot,t_short(1:s_end),Q_tc_short(1:s_end,:))
        colorbar
        title('Total transport capacity, Q_{tc} (m^3/s)')
        ylabel('Time (day)')
        set(gca,'YDir','normal')
        set(gca,'FontSize',FSax)
        if L10 == 1
            set(gca,'Xtick',0:2:10)
            txt_posB = s_end/24.*text_frac;
            tB = text(0.25,txt_posB,'B');
        elseif L50 == 1
            set(gca,'Xtick',0:5:50)
            txt_posB = s_end/24.*text_frac;
            tB = text(2,txt_posB,'B');
        end
        %
        set(tB,'FontSize',FS_txt);
        set(tB,'Color',[0.85 0.85 0.85])

        subplot(sx1,sy1,3)
        imagesc(x_plot,t_short(1:s_end),E_tot(1:s_end,:))
        colorbar
        title('Total erosion, E_{tot} (mm/a \times m)')
        xlabel('Distance along the bed (km)')
        set(gca,'YDir','normal')
        set(gca,'FontSize',FSax)
        if L10 == 1
            set(gca,'Xtick',0:2:10)
            txt_posC = s_end/24.*text_frac;
            tC = text(0.25,txt_posC,'C');
        elseif L50 == 1
            set(gca,'Xtick',0:5:50)
            txt_posC = s_end/24.*text_frac;
            tC = text(2,txt_posC,'C');
        end
        %
        set(tC,'FontSize',FS_txt);
        set(tC,'Color',[0.85 0.85 0.85])
    end


    %{
    figure
    contourf(t_stage_short,[0:0.5:max(max(t_stage_short))],'linestyle','none')
    colorbar
    title('transport stage')
    %}

    %figure
    if fourth_line == 1;
        sx2 = 4;
    else
        sx2 = 3;
    end
    sy2 = 2;
    f2 = figure(fig2);
    f2.Units = 'normalized';
    f2.Position = [0,0.3,fx2,fy2];

    SA1 = subplot(sx2,sy2,[1, 2]);

    [AXA H1A H2A] = plotyy(t_short(1:s_end),t_short(1:s_end).*0 + 1,t_short(1:s_end),b_s_norm_short(1:s_end));
    
    set(H1A,'Color','k')
    set(H1A,'Linestyle','--','Linewidth',1.5)
    set(H2A,'Color',[0.85 0.85 0.85])


    if input_type == 2
        AXA(1).YLim = [0 10];
        AXA(1).YTick = [0:2:10];
        AXA(2).YLim = [0 2.5];
        AXA(2).YTick = [0:0.5:2.5];
    elseif input_type == 1
        set(AXA(1),'Ylim',[0 16])
        set(AXA(1),'Ytick',[0:2:16])
        AXA(2).YLim = [0 2];
        AXA(2).YTick = [0:0.5:2];
        AXA(2).YTickLabel = {'0','0.5','1.0','1.5','2.0'};
    elseif input_type == 3
        set(AXA(1),'Ylim',[0 10])
        set(AXA(1),'Ytick',[0:2:10])
        AXA(2).YLim = [0 5];
        AXA(2).YTick = [0:1:5];
    end

    set(AXA(2),'FontSize',FSax)
    %AXA(2).YColor = [0.65 0.65 0.65];
    AXA(2).YColor = [1 1 1].*0.45;
    %AXA(2).YLabel.String = 'b_{s}/b_{s,max}';
    AXA(2).YLabel.Interpreter = 'latex';
    AXA(2).YLabel.String = '$\dot{b}_{\rm ca}/\dot{b}_{\rm ss \, max}$';

    hold on

    if L10 == 1;
        pA1 = plot(t_short(1:s_end),t_stage_short(1:s_end,100),'Linewidth',LW3);
        pA2 = plot(t_short(1:s_end),t_stage_short(1:s_end,75),'Linewidth',LW3);
        pA3 = plot(t_short(1:s_end),t_stage_short(1:s_end,50),'Linewidth',LW3);
        pA4 = plot(t_short(1:s_end),t_stage_short(1:s_end,25),'Linewidth',LW3);
    elseif L50 == 1;
        pA1 = plot(t_short(1:s_end),t_stage_short(1:s_end,100),'Linewidth',LW3);
        pA2 = plot(t_short(1:s_end),t_stage_short(1:s_end,90),'Linewidth',LW3);
        pA3 = plot(t_short(1:s_end),t_stage_short(1:s_end,80),'Linewidth',LW3);
        pA4 = plot(t_short(1:s_end),t_stage_short(1:s_end,70),'Linewidth',LW3);
        pA5 = plot(t_short(1:s_end),t_stage_short(1:s_end,60),'Linewidth',LW3,'LineStyle','--');
        pA6 = plot(t_short(1:s_end),t_stage_short(1:s_end,50),'Linewidth',LW3,'LineStyle','--');
    end
    
    pA1.Color = 'k';
    pA2.Color = rgb('OrangeRed');
    pA3.Color = rgb('DarkOrange');
    pA4.Color = rgb('SkyBlue');
    pA5.Color = rgb('DodgerBlue');
    pA6.Color = rgb('DarkGray');
    %t_stage = 1 line
    plot(t_short(1:s_end),t_short(1:s_end).*0 + 1,'k--','Linewidth',1.5)
    
    SA1.Color = 'none';
    uistack(AXA(2),'bottom')
    set(AXA(2),'Color','w')


    if L10 == 1;
        lA1 = legend([pA1 pA2 pA3 pA4], '10 km','7.5 km','5 km','2.5 km');
    elseif L50 == 1;
        lA1 = legend([pA1 pA2 pA3 pA4 pA5 pA6], '50 km','45 km','40 km','35 km','30 km','25 km');
    end
    lA1.Interpreter = 'latex';
    grid on
    %grid minor
        
    legend boxoff
    ylabel('$\tau^*/\tau^*_{\rm c}$','interpreter','latex')
    set(gca,'FontSize',FSax)
    AXA(1).YColor = 'k';

    if input_type == 1
        txt_pos2A = 16.*text_frac;
    elseif input_type == 2
        txt_pos2A = 10.*text_frac;
    end
    t2A = text(2,txt_pos2A,'A');
    set(t2A,'FontSize',FS_txt);
    
    SA34 = subplot(sx2,sy2,[3, 4]);
    %{
    plot(Q_tc_short(1:s_end,100))
    hold on
    plot(Q_tc_short(1:s_end,75))
    plot(Q_tc_short(1:s_end,50))
    plot(Q_tc_short(1:s_end,25))
    legend('10 km','7.5 km','5 km','2.5 km')
    ylabel('Q_{tc} (m^3/s)')
    %}
    if L10 == 1;
        plot(t_short(1:s_end),1- Fe_short(1:s_end,100),'Linewidth',LW3)
        hold on
        plot(t_short(1:s_end),1 - Fe_short(1:s_end,75),'Linewidth',LW3)
        plot(t_short(1:s_end),1 - Fe_short(1:s_end,50),'Linewidth',LW3)
        plot(t_short(1:s_end),1 - Fe_short(1:s_end,25),'Linewidth',LW3)
    elseif L50 == 1
        %pB1 = plot(t_short(1:s_end),1- Fe_short(1:s_end,100),'Linewidth',LW3);
        %hold on
        %pB2 = plot(t_short(1:s_end),1 - Fe_short(1:s_end,90),'Linewidth',LW3);
        %pB3 = plot(t_short(1:s_end),1 - Fe_short(1:s_end,80),'Linewidth',LW3);
        %pB4 = plot(t_short(1:s_end),1 - Fe_short(1:s_end,70),'Linewidth',LW3);
        %pB5 = plot(t_short(1:s_end),1 - Fe_short(1:s_end,60),'Linewidth',LW3);
        %pB6 = plot(t_short(1:s_end),1 - Fe_short(1:s_end,50),'Linewidth',LW3);
        pB1 = plot(t_short(1:s_end),rel_supply(1:s_end,100),'Linewidth',LW3);
        hold on
        pB2 = plot(t_short(1:s_end),rel_supply(1:s_end,90),'Linewidth',LW3);
        pB3 = plot(t_short(1:s_end),rel_supply(1:s_end,80),'Linewidth',LW3);
        pB4 = plot(t_short(1:s_end),rel_supply(1:s_end,70),'Linewidth',LW3);
        pB5 = plot(t_short(1:s_end),rel_supply(1:s_end,60),'Linewidth',LW3,'LineStyle','--');
        pB6 = plot(t_short(1:s_end),rel_supply(1:s_end,50),'Linewidth',LW3,'LineStyle','--');
    end
    %legend('10 km','7.5 km','5 km','2.5 km')

    pB1.Color = 'k';
    pB2.Color = rgb('OrangeRed');
    pB3.Color = rgb('DarkOrange');
    pB4.Color = rgb('SkyBlue');
    pB5.Color = rgb('DodgerBlue');
    pB6.Color = rgb('DarkGray');

    if L10 == 1;
        lB1 = legend([pB1 pB2 pB3 pB4], '10 km','7.5 km','5 km','2.5 km');
    elseif L50 == 1;
        lB1 = legend([pB1 pB2 pB3 pB4 pB5 pB6], '50 km','45 km','40 km','35 km','30 km','25 km');
    end
    lB1.Interpreter = 'latex';
    lB1.Location = 'West';
    legend boxoff

    grid on
    %grid minor

    ylabel('$q_{\rm s}/q_{\rm tc}$','interpreter','latex')
    set(gca,'FontSize',FSax)

    txt_pos2B = 2.*text_frac;
    t2B = text(2,txt_pos2B,'B');
    set(t2B,'FontSize',FS_txt);

    SA56 = subplot(sx2,sy2,[5, 6]);

    m_fact = 1000;

    if L10 == 1;
        pC1 = plot(t_short(1:s_end),e_tot_short(1:s_end,100),'Linewidth',LW1);
        hold on
        pC2 = plot(t_short(1:s_end),e_tot_short(1:s_end,75),'Linewidth',LW1);
        pC3 = plot(t_short(1:s_end),e_tot_short(1:s_end,50),'Linewidth',LW1);
        pC4 = plot(t_short(1:s_end),e_tot_short(1:s_end,25),'Linewidth',LW1);
    elseif L50 == 1;
        pC1 = plot(t_short(1:s_end),e_tot_short(1:s_end,100)./m_fact,'Linewidth',LW1);
        hold on
        pC2 = plot(t_short(1:s_end),e_tot_short(1:s_end,90)./m_fact,'Linewidth',LW1);
        pC3 = plot(t_short(1:s_end),e_tot_short(1:s_end,80)./m_fact,'Linewidth',LW1);
        pC4 = plot(t_short(1:s_end),e_tot_short(1:s_end,70)./m_fact,'Linewidth',LW1);
        pC5 = plot(t_short(1:s_end),e_tot_short(1:s_end,60)./m_fact,'Linewidth',LW1,'LineStyle','--');
        pC6 = plot(t_short(1:s_end),e_tot_short(1:s_end,50)./m_fact,'Linewidth',LW1,'LineStyle','--');
    end
    
    pC1.Color = 'k';
    pC2.Color = rgb('OrangeRed');
    pC3.Color = rgb('DarkOrange');
    pC4.Color = rgb('SkyBlue');
    pC5.Color = rgb('DodgerBlue');
    pC6.Color = rgb('DarkGray');

    c1 = get(pC1,'Color');
    c2 = get(pC2,'Color');
    c3 = get(pC3,'Color');
    c4 = get(pC4,'Color');
    c5 = get(pC5,'Color');
    c6 = get(pC6,'Color');

    if L10 == 1;
        pCC1 = plot(t_short(1:s_end),E_tot(1:s_end,100),'Color',[c1],'Linewidth',LW2);
        hold on
        pCC2 = plot(t_short(1:s_end),E_tot(1:s_end,75),'Color',c2,'Linewidth',LW2);
        pCC3 = plot(t_short(1:s_end),E_tot(1:s_end,50),'Color',c3,'Linewidth',LW2);
        pCC4 = plot(t_short(1:s_end),E_tot(1:s_end,25),'Color',c4,'Linewidth',LW2);
    elseif L50 == 1;
        pCC1 = plot(t_short(1:s_end),E_tot(1:s_end,100)./m_fact,'Color',[c1],'Linewidth',LW2);
        hold on
        pCC2 = plot(t_short(1:s_end),E_tot(1:s_end,90)./m_fact,'Color',c2,'Linewidth',LW2);
        pCC3 = plot(t_short(1:s_end),E_tot(1:s_end,80)./m_fact,'Color',c3,'Linewidth',LW2);
        pCC4 = plot(t_short(1:s_end),E_tot(1:s_end,70)./m_fact,'Color',c4,'Linewidth',LW2);
        pCC5 = plot(t_short(1:s_end),E_tot(1:s_end,60)./m_fact,'Color',c5,'Linewidth',LW2,'LineStyle','--');
        pCC6 = plot(t_short(1:s_end),E_tot(1:s_end,50)./m_fact,'Color',c6,'Linewidth',LW2,'LineStyle','--');
    end
    %legend('10 km e_{tot}','7.5 km','5 km','2.5 km','10 km E_{tot}','7.5 km','5 km','2.5 km','Location','West')

    if L10 == 1;
        l56 = legend([pC1 pC2 pC3 pC4], '10 km','7.5 km','5 km','2.5 km');
    elseif L50 == 1;
    %SEE LEGEND MAKING AT BOTTOM OF FILE
        %legend_eros = {'e_{tot} @ 50 km','45 km','40 km','35 km','30 km','25 km','E_{tot} @ 50 km','45 km','40 km','35 km','30 km','25 km'};
        %l56 = columnlegend(2,legend_eros,'FontSize',FSax,'boxoff');
    end

    grid on
    %grid minor

    ylabel('Erosion (m/a (\times m))')
    xlabel('Time (day)')
    set(gca,'FontSize',FSax)

    if L10 == 1
        txt_pos2C = 600.*text_frac;
    elseif L50 ==1
        if input_type == 1
            txt_pos2C = 1.8.*text_frac;
        elseif input_type == 2
            txt_pos2C = 2200.*text_frac;
            axis([0 150 0 2200])
        elseif input_type == 3
            txt_pos2C = 1600.*text_frac;
            axis([0 150 0 1600])
        end
    end
    t2C = text(2,txt_pos2C,'C');
    set(t2C,'FontSize',FS_txt);

    %Making sure the colors on the first plot are the proper ones
    %pA1.Color = c1;
    %pA2.Color = c2;
    %pA3.Color = c3;
    %pA4.Color = c4;

    if fourth_line == 1;
        SA7 = subplot(sx2,sy2,7);
        plot(x_plot,sum(e_eff),'Linewidth',LW4)
        hold on
        plot(x_plot,sum(E_eff),'Linewidth',LW4)
        legend('\int e_{tot} dt (mm)','\int E_{tot} dt (mm \times m)','Location','North')
        legend boxoff
        ylabel('Integrated erosion')
        xlabel('Distance along the bed (km)')
        set(gca,'FontSize',FSax)
        if L10 == 1
            set(gca,'Xtick',0:2:10)
            txt_pos2D = 80.*text_frac;
            t2D = text(0.5,txt_pos2D,'D');
        elseif L50 == 1
            max_E_eff = max(sum_E_eff);
            axis([0 50 0 max_E_eff*1.2])
            set(gca,'Xtick',0:5:50)
            txt_pos2D = max_E_eff.*1.2.*text_frac;
            t2D = text(3,txt_pos2D,'D');
        end
        set(t2D,'FontSize',FS_txt);

        SA8 = subplot(sx2,sy2,8);
        %
        %width(1:101) = linspace(1,0,101).*-R_max;
        %width(101:201) = linspace(0,1,101).*R_max;

        %contourf(x_stag,width,valley_topo,'LevelStep',0.5,'linestyle','none')

        %imagesc(x_plot,width,valley_topo)
        contour(x_plot,width,valley_topo,15)
        
        %set(gca,'YDir','normal')
        colormap gray
        %shading flat
        col_b = colorbar;
        col_b.Label.String = 'Depth (mm)';
        %col_b.Ticks = [-25 -20 -15 -10 -5 0];
        %
        if L10 == 1
            col_b.Ticks = [-45:5:0];
            col_b.Label.Color = [0.8 0.8 0.8];
        elseif L50 == 1
            col_min = -round(max(sum_e_eff),2,'significant');
            col_b.Ticks = [col_min:20:0];
        end
        col_b.Location = 'south';

        %
        xlabel('Distance along the bed (km)')
        ylabel('N-Channel radius (m)')
        set(gca,'FontSize',FSax)
        col_b.FontSize = 12;
        
        if L10 == 1
            set(gca,'Xtick',0:2:10)
            txt_pos2E = R_max.*text_frac.*0.9;
            t2E = text(0.75,txt_pos2E,'E');
        elseif L50 == 1
            set(gca,'Xtick',0:5:50)
        txt_pos2E = R_max.*text_frac.*0.9;
        t2E = text(4,txt_pos2E,'E');
        end

        %txt_pos2E = 1.6.*text_frac;
        set(t2E,'FontSize',FS_txt);
    end
    
    


    %Making all the plots look pretty
    
    SA1.XLim = [0 s_end/24];
    SA34.XLim = [0 s_end/24];
    SA56.XLim = [0 s_end/24];

    SA1.XTick = [0:25:s_end/24];
    SA34.XTick = [0:25:s_end/24];
    SA56.XTick = [0:25:s_end/24];

    SA1.XTickLabel = '';
    SA34.XTickLabel = '';

    SA34.YLim = [0 2];
    SA34.YTick = [0:0.5:2];
    SA34.YTickLabel = {'0','0.5','1.0','1.5','2.0'};

    SA56.YLim = [0 1.8];

    SA56.YTick = [0:0.3:1.8];

    SA1.Position(1) = 0.115;
    SA34.Position(1) = 0.115;
    SA56.Position(1) = 0.115;

    SA1.Position(4) = 0.25;
    SA34.Position(4) = 0.25;
    SA56.Position(4) = 0.25;

    if L50 == 1
        ah56 = gca;
        l56 = legend(ah56,[pC1 pC2 pC3 pC4 pC5 pC6], '$\dot{e}_{\rm tot}$ @ 50 km','45 km','40 km','35 km','30 km','25 km');
        l56.Interpreter = 'latex';
        ah56B = axes('position',get(gca,'position'), 'visible','off');
        l56B = legend(ah56B,[pCC1 pCC2 pCC3 pCC4 pCC5 pCC6], '$E_{\rm tot}$ @ 50 km','45 km','40 km','35 km','30 km','25 km');
        l56B.Interpreter = 'latex';

        l56.Location = 'West';
        l56B.Location = 'East';
        l56.FontSize = FSax-2;
        l56B.FontSize = FSax-2;
        l56.Box = 'off';
        l56B.Box = 'off';
    end
end
