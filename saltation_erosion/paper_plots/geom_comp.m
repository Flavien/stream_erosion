%This file is meant to plot the firgure in which I will discuss the effect of the external parameters on the erosion and transport
%
%
%

%switch to extract stuff from saltation only runs or from total load eros runs
saltation_only = 0;
%presentation formating switch
pres_font = 0;
%clf

disp('Erosion and transport capacity multiplied by width!!!!')

%sim_struct = load_simulations;
sim_struct = load_simulations(saltation_only);
length_struct = length(sim_struct);

%dx = 200;
%dx = 1000;
%dx = 1 / (length(sim_struct(1).E));
dx = 500/1000;

x_reg = [0:length(sim_struct(1).E)].*dx;
x_stag = (x_reg(2:end) + x_reg(1:end-1)) ./ 2;
x = x_stag;
MS = zeros(length_struct,1) ;
LW = zeros(length_struct,1) +  3;

input_change = 0;
geom_change = 1;
nb_change = 0;

    %ln_spec_col = {'k',rgb('LightSeaGreen'),rgb('LightSeaGreen'),rgb('OrangeRed'),rgb('OrangeRed'),rgb('DimGray'),'k'} ;
    ln_spec_col = {'k',rgb('SkyBlue'),rgb('SkyBlue'),rgb('OrangeRed'),rgb('OrangeRed'),rgb('DimGray'),'k'} ;
    ln_spec_type = {'-','-','--','-','--'};
    %legend_text = {'SS\_CH\_REF','SS\_CH\_SURF\_WDG','SS\_CH\_SURF\_STP','SS\_CH\_SURF\_IS1300','SS\_CH\_SURF\_IS700'};
    legend_text = {'S\_REF   ','S\_WDG   ','S\_STP   ','S\_1300  ','S\_700  '};

%ln_spec_col = 'k';
%ln_spec_type = '-';

text_frac = 0.9;

sx = 2;
sy = 2;

if pres_font == 1
    %presentation values
    FSlab = 32;
    FSax  = 32;
    FS_txt = 42;

    fx = 0.55;
    fy = 0.4;
else
    %regular plot values
    FSlab = 26;
    FSax  = 26;
    FS_txt = 28;

    fx = 0.75;
    fy = 66;
    %fy = 0.9;
end

%legend_text = {'ref run','b_c x2','Ice sheet','Small glacier'};
%legend_text = {'ref','ref tb','surf','surf tb','moulin','moulin tb'};

k = 1;
f1 = figure(k);
set(f1,'Units','normalized')
set(f1,'Position',[0,0.5,fx,fy])

S11 = subplot(sx,sy,1);
for i=1:length_struct
    fp1 = fancy_plot(x,sim_struct(i).Q_c,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
    hold on
end
set(gca,'FontSize',FSax)
%xlabel('Normalized distance along the bed')
%ylabel('Discharge, Q_{ch} (m^3/s)')
ylabel('$Q_{\rm ch} \, ({\rm m^3 \, s})$','interpreter','latex')
grid on

%legend(legend_text,'Location','West')
%columnlegend(3,legend_text,'Location','northeastoutside','FontSize',FSax)
%legend boxoff

txt_posA = 20.*text_frac;
%legend(legend_text,'Location','West')

tA = text(1,txt_posA,'A');
set(tA,'FontSize',FS_txt);


S1 = subplot(sx,sy,2);
for i=1:length_struct
        fp1 = fancy_plot(x,sim_struct(i).S,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
    hold on
end
%set(legend,'Location','Best')

l1 = legend(legend_text,'Location','North');
l1.Orientation = 'horizontal';
%l1.EdgeColor = [0.9 0.9 0.9];
legend boxoff

set(gca,'FontSize',FSax)
%xlabel('Normalized distance along the bed')
%ylabel('Cross-sectional area, S (m^2)')
ylabel('$S \, ({\rm m^2})$','interpreter','latex')
grid on
%grid minor


    txt_posB = 12.*text_frac;
tB = text(1,txt_posB,'B');
set(tB,'FontSize',FS_txt);

%k = k+1;
%f3 = figure(k);
%set(f3,'Units','normalized')
%set(f3,'Position',[0,0.5,fx,fy])
S2 = subplot(sx,sy,3);
for i=1:length_struct
        fp1 = fancy_plot(x,sim_struct(i).tau_ratio,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
    hold on
end
%plot(x,x.*0 + 2.3,'k-.','LineWidth',1.5)
%set(legend,'Location','Best')
set(gca,'FontSize',FSax)
xlabel('Distance from divide (km)')
%ylabel('Transport stage, \tau^* /\tau^*_c')
ylabel('$\tau^* /\tau^*_{\rm c}$','interpreter','latex')
%legend(legend_text,'Location','Best')
legend boxoff
grid on
%grid minor

    txt_posC = 20.*text_frac;
tC = text(1,txt_posC,'C');
set(tC,'FontSize',FS_txt);

%k = k + 1;
%f4 = figure(k);
%set(f4,'Units','normalized')
%set(f4,'Position',[0,0.5,fx,fy])
S3 = subplot(sx,sy,4);
for i=1:length_struct
    width_fact = sqrt(2.*sim_struct(i).S./pi) .* 2;
    %width_fact = width_fact .* 1000; %For mm /a instead of m/a
        fp1 = fancy_plot(x,sim_struct(i).e_tot.*width_fact,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
        hold on

end
%set(legend,'Location','Best')
set(gca,'FontSize',FSax)
xlabel('Distance from divide (km)')
    %ylabel('Total erosion, E_{tot} (mm/a*W_{ch})')
    ylabel('$E_{\rm tot} \, ({\rm m^2 \, a^{-1}})$','interpreter','latex')
    txt_posD = 1.1*text_frac;
tD = text(1,txt_posD,'D');
set(tD,'FontSize',FS_txt);
%legend(legend_text,'Location','Best')
legend boxoff
grid on
%grid minor


%Making the subplots pretty again...
S11.XLim = [0 50];
S1.XLim = [0 50];
S2.XLim = [0 50];
S3.XLim = [0 50];
S11.XTick = [0:10:50];
S1.XTick = [0:10:50];
S2.XTick = [0:10:50];
S3.XTick = [0:10:50];

S1.YLim = [0 12];
S1.YTick = [0:2:12];

S2.YLim = [0 20];
S2.YTick = [0:5:20];

S3.YLim = [0 1.1];
S3.YTick = [0:0.2:1];

S11.XTickLabel = '';
S1.XTickLabel = '';
%S2.XTickLabel = '';
S3.YTickLabel = {'0','0.2','0.4','0.6','0.8','1.0'};

S11.Position(1) = 0.09;
S1.Position(1) = 0.58;
S2.Position(1) = 0.09;
S3.Position(1) = 0.58;

S11.Position(2) = 0.53;
S1.Position(2) = 0.53;
S2.Position(2) = 0.1;
S3.Position(2) = 0.1;

sub_w = 0.395;
S11.Position(3) = sub_w;
S1.Position(3) = sub_w;
S2.Position(3) = sub_w;
S3.Position(3) = sub_w;

sub_h = 0.39;
S11.Position(4) = sub_h;
S1.Position(4) = sub_h;
S2.Position(4) = sub_h;
S3.Position(4) = sub_h;
