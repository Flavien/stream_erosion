%This file is meant to plot the firgure in which I will discuss the effect of the external parameters on the erosion and transport
%
%
%

%switch to extract stuff from saltation only runs or from total load eros runs
saltation_only = 0;
%presentation formating switch
pres_font = 0;
clf

disp('Erosion and transport capacity multiplied by width!!!!')

sim_struct = load_cav_sim;
length_struct = length(sim_struct);

%dx = 200;
%dx = 1000;
%dx = 1 / (length(sim_struct(1).E));
dx = 500 / 1000;

x_reg = [0:length(sim_struct(1).E)].*dx;
x_stag = (x_reg(2:end) + x_reg(1:end-1)) ./ 2;
x = x_stag;
MS = zeros(length_struct,1) ;
LW = zeros(length_struct,1) +  3;

input_change = 0;
geom_change = 1;
nb_change = 0;

    %ln_spec_col = {'k',rgb('DimGray'),rgb('Silver'),'k',rgb('DimGray'),rgb('Silver'),'k'} ;
    ln_spec_col = {'k',rgb('OrangeRed'),rgb('SkyBlue'),rgb('OrangeRed'),rgb('OrangeRed'),rgb('DimGray'),'k'} ;
    ln_spec_type = {'-','-','-','--','--','--'};
    %legend_text = {'SS\_CV\_Ub10','SS\_CV\_Ub20','SS\_CV\_Ub50'};
    legend_text = {'S\_CAV10','S\_CAV20','S\_CAV50'};
    %legend_text = {'CAV\_Ub5','CAV\_Ub10','CAV\_Ub30','ub = 5 m/a; lam','ub = 10 m/a; lam','ub = 30 m/a; lam'};

text_frac = 0.9;

if pres_font == 1
    %presentation values
    FSlab = 32;
    FSax  = 32;
    FS_txt = 42;

    fx = 0.55;
    fy = 0.4;
else
    %regular plot values
    FSlab = 26;
    FSax  = 26;
    FS_txt = 28;

    fx = 0.75;
    %fy = 1;
    fy = 0.66;
end

%legend_text = {'ref run','b_c x2','Ice sheet','Small glacier'};
%legend_text = {'ref','ref tb','surf','surf tb','moulin','moulin tb'};

k = 1;

width_fact = 1000; %to get the total erosion over the glacier bed
width_fact_eros = 1e3; %to get the total erosion over the glacier bed and in m/a

f1 = figure(k);
%f1 = figure;
set(f1,'Units','normalized')
set(f1,'Position',[0,0.5,fx,fy])
S1 = subplot(2,2,1);
for i=1:length_struct
    flow_speed = sim_struct(i).q_w ./ sim_struct(i).h;
    fp1 = fancy_plot(x,flow_speed,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
    %fp1 = fancy_plot(x,sim_struct(i).q_w.*width_fact,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
    if i == 1
        hold on
    end
end
l1 = legend(legend_text);
l1.Location = 'northoutside';
l1.Orientation = 'horizontal';
l1.Box = 'off';
set(gca,'FontSize',FSax)

%ylabel('Depth averaged flow speed, u (m/s)')
%ylabel('u (m/s)')
ylabel('$u \, ({\rm m \,s^{-1} })$','interpreter','latex')
    grid on
    %grid minor

    txt_posA = 0.8.*text_frac;
tA = text(2,txt_posA,'A');
set(tA,'FontSize',FS_txt);

S2 = subplot(2,2,2);
for i=1:length_struct
    fp1 = fancy_plot(x,sim_struct(i).h,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
    if i == 1
        hold on
    end
end
set(gca,'FontSize',FSax)

%ylabel('Cavity height, h_{ca} (m)')
ylabel('$h_{\rm ca} \, ({\rm m})$','interpreter','latex')
    grid on
    %grid minor

    txt_posB = 0.12.*text_frac;

tB = text(2,txt_posB,'B');
set(tB,'FontSize',FS_txt);

S3 = subplot(2,2,3);
for i=1:length_struct
    fp1 = fancy_plot(x,sim_struct(i).tau_ratio,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
    if i == 1
        hold on
    end
end

set(gca,'FontSize',FSax)

xlabel('Distance from divide (km)')
%ylabel('Transport stage, \tau^* /\tau^*_c')
ylabel('$\tau^* /\tau^*_{\rm c}$','interpreter','latex')
    grid on
    %grid minor

    txt_posC = 170.*text_frac;

tC = text(2,txt_posC,'C');
set(tC,'FontSize',FS_txt);

S4 = subplot(2,2,4);
for i=1:length_struct

    fp1 = fancy_plot(x,sim_struct(i).e_tot.*width_fact_eros,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
    if i == 1
        hold on
    end

end
set(gca,'FontSize',FSax)
%xlabel('Normalized distance along the bed')
xlabel('Distance from divide (km)')

    %ylabel('Total erosion, E_{tot} (mm/a*W)')
    ylabel('$E_{\rm tot} \, ({\rm m^2 \, a^{-1}})$','interpreter','latex')
    grid on
    %grid minor

    txt_posD = 1.*text_frac;
tD = text(2,txt_posD,'D');
set(tD,'FontSize',FS_txt);

%Making all the plots look pretty:

S1.XLim = [0 50];
S2.XLim = [0 50];
S3.XLim = [0 50];
S4.XLim = [0 50];

S1.XTick = [0:10:50];
S1.XTickLabel = '';
S2.XTick = [0:10:50];
S2.XTickLabel = '';
S3.XTick = [0:10:50];
S4.XTick = [0:10:50];

S1.YLim = [0 0.8];
S1.YTick = [0:0.2:0.8];

S2.YLim = [0 0.12];
S2.YTick = [0:0.03:0.12];

S3.YLim = [0 170];
S3.YTick = [0:50:150];

S4.YLim = [0 1];
S4.YTick = [0:0.2:1];
S4.YTickLabel = {'0','0.2','0.4','0.6','0.8','1.0'};

sub_w = 0.395;
S1.Position(3) = sub_w;
S2.Position(3) = sub_w;
S3.Position(3) = sub_w;
S4.Position(3) = sub_w;

sub_h = 0.39;
S1.Position(4) = sub_h;
S2.Position(4) = sub_h;
S3.Position(4) = sub_h;
S4.Position(4) = sub_h;

S1.Position(1) = 0.09;
S2.Position(1) = 0.58;
S3.Position(1) = 0.09;
S4.Position(1) = 0.58;

S1.Position(2) = 0.54;
S2.Position(2) = 0.54;
