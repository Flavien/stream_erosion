%Plotting file to produce a plot for the summary comparison of the paper
%All the TR simulations of the paper are in there
%Hope it's not gonna be too much of a mess...
%we'll see


loaded_already = 1;

if loaded_already == 1
    %load trans_IS_final
    load trans_IS_ALL
    %load trans_IS_all_short
else
    trans_IS = load_IS_TR_comp();
end

%plotting stuffs...
x_plot = [1:min(size(trans_IS(1).q_tc_vol))]-0.5;
x_plot = x_plot .* 500./1000;

FSlab = 26;
FSax  = 26;
FS_txt = 28;

text_frac = 0.10;
X_frac = 0.85;

BW = 0.7;

fig1 =1;
fx = 1;
fy = 0.5;
sx1 = 1;
sy1 = 3;

sub4_on = 0;

L10 = 0;
L50 = 1;

x_start = 30;

run_nb = length(trans_IS);
%%%%%%%%%%%

f1 = figure(fig1);
f1.Units = 'normalized';
f1.Position = [0,0.3,fx,fy];

for i = 1:run_nb
    avg_eros(i) = trans_IS(i).result_eros;
    Sed_out(i) = trans_IS(i).vol_exit;
    A_ratio(i) = max(trans_IS(i).sum_e_eff) ./ (max(trans_IS(i).width) .* 2);
    Nch_length(i) = 50 - max(find(trans_IS(i).valley_topo(100,1:end-10) == 0)).*500./1000;
    eros_depth(i) = max(trans_IS(i).sum_e_eff);
    if i == 2
        avg_eros(i) = trans_IS(i).result_eros .* 4;
        Sed_out(i) = trans_IS(i).vol_exit .*4;
    elseif i == 4
        avg_eros(i) = trans_IS(i).result_eros .* 3;
        Sed_out(i) = trans_IS(i).vol_exit .*3;
    elseif i == 6
        avg_eros(i) = trans_IS(i).result_eros .* 2;
        Sed_out(i) = trans_IS(i).vol_exit .*2;
    end
end

avg_eros = flip(avg_eros);
Sed_out = flip(Sed_out);
A_ratio = flip(A_ratio);
Nch_length = flip(Nch_length);
eros_depth = flip(eros_depth);

equivalent_eros = Sed_out ./ 50000;

%run_names = {'IS\_OPT','IS\_W250','IS\_W333','IS1300','IS\_W500',...
%            'IS\_RA','IS\_REF','IS\_RB','IS\_SSP/2','IS700','IS700B','IS\_SSP/4'};
%run_names = {'TR\_IS\_SSPOPT','TR\_IS\_W250','TR\_IS\_W333','TR\_IS1300','TR\_IS\_W500',...
%            'TR\_IS\_RA','TR\_IS\_REF','TR\_IS\_RB','TR\_IS\_SSP/2','TR\_IS700','TR\_IS\_SSP/4'};

%run_names = {'T\_SSPOPT','T\_W250','T\_W333','T\_1300','T\_W500',...
%            'T\_2007','T\_REF','T\_2008','T\_SSP/2','T\_700','T\_SSP/4'};
run_names = {'T\_SSPOPT','T\_W250','T\_1.5Q','T\_W333','T\_1300','T\_W500',...
            'T\_2007','T\_REF','T\_2008','T\_SSP/2','T\_700','T\_SSP/4'};
run_names = flip(run_names);

%useful_runs = [1 3 4 5 6 7 8 9 10 11 12];
useful_runs = [1 2 3 4 5 6 7 8 9 10 11 12];
%useful_runs = [1 2 3 4 5 6 7 8 9 10 11];

%run_sup = [1 3 11];
%run_W = [7 9 10];
%run_geom = [2 8];
%run_input = [4 6];
%run_ref = 5;

run_sup = [1 3 12];
run_W = [7 9 11];
run_geom = [2 8];
run_input = [4 6 10];
run_ref = 5;


%%%%%%%%%%%%%%%%%%
s1 = subplot(sx1,sy1,1);

%b1 = barh(avg_eros);
for i=useful_runs
    yy = i;
    %yy = 12-i;
    %if i == 12
      %  yy = 11;
        %yy = 1;
    %end
    b1(i) = barh(yy,avg_eros(i));
    if i == 1
        hold on
    end
    b1(i).BarWidth = BW;
end

%axis([0 0.13 0.5 11.5])
axis([0 0.13 0.5 12.5])

s1.XMinorTick = 'on';
s1.XGrid = 'on';
s1.XMinorGrid = 'on';

%s1.YTick = [1:run_nb-1];
s1.YTick = [1:run_nb];
s1.YGrid = 'on';
s1.YTickLabel = run_names;

txt_pos = 11.5.*text_frac;
t2A = text(0.13*X_frac,txt_pos,'A');
set(t2A,'FontSize',FS_txt);


xlabel('Averaged erosion rate (mm/a)')
set(gca,'FontSize',FSax)

%%%%%%%%%%%%%%%%%%
s2 = subplot(sx1,sy1,2);

%b2 = barh(Sed_out);
for i=useful_runs
    yy = i;
    b2(i) = barh(yy,equivalent_eros(i));
    if i == 1
        hold on
    end
    b2(i).BarWidth = BW;
end


%axis([0 2.6e5 0.5 11.5])
%axis([0 4.5 0.5 11.5])
axis([0 4.5 0.5 12.5])

s2.XMinorTick = 'on';
s2.XGrid = 'on';
s2.XMinorGrid = 'on';
s2.XTick = [0:1:4];

%s2.YTick = [1:run_nb-1];
s2.YTick = [1:run_nb];
s2.YGrid = 'on';

t2B = text(4.5*X_frac,txt_pos,'B');
set(t2B,'FontSize',FS_txt);

set(gca,'YTickLabel','')
%xlabel('Sediment flushed (m^3/a)')
xlabel('Apparent erosion rate (mm/a)')
set(gca,'FontSize',FSax)

%%%%%%%%%%%%%%%%%%
s3 = subplot(sx1,sy1,3);

for i=useful_runs
    yy = i;
    %b3(i) = barh(yy,A_ratio(i));
    b3(i) = barh(yy,eros_depth(i));
    if i == 1
        hold on
    end
    b3(i).BarWidth = BW;
end


%axis([0 210 0.5 11.5])
axis([0 210 0.5 12.5])

s3.XMinorTick = 'on';
s3.XGrid = 'on';
s3.XMinorGrid = 'on';
s3.XTick = [0:50:200];

%s3.YTick = [1:run_nb-1];
s3.YTick = [1:run_nb];
s3.YGrid = 'on';

t2C = text(210*X_frac,txt_pos,'C');
set(t2C,'FontSize',FS_txt);

set(gca,'YTickLabel','')
%xlabel('N-channel aspect ratio')
xlabel('Maximum incision depth (mm)')
set(gca,'FontSize',FSax)

%%%%%%%%%%%%%%%%%%
if sub4_on == 1
    s4 = subplot(sx1,sy1,4);

    for i=useful_runs
        yy = i;
        if i == 12
            yy = 11;
        end
        b4(i) = barh(yy,Nch_length(i));
        if i == 1
            hold on
        end
        b4(i).BarWidth = BW;
    end

    axis([0 40 0.5 11.5])

    s4.XMinorTick = 'on';
    s4.XGrid = 'on';
    s4.XMinorGrid = 'on';
    s4.XTick = [0:10:40];

    %s4.YTick = [1:run_nb-1];
    s4.YTick = [1:run_nb];
    s4.YGrid = 'on';

    t2D = text(40*X_frac,txt_pos,'D');
    set(t2D,'FontSize',FS_txt);

    set(gca,'YTickLabel','')
    xlabel('N-channel length (km)')
    set(gca,'FontSize',FSax)
end

%Making everythin look nice

%run_sup = [1 9 12];
%run_W = [2 3 5];
%run_geom = [4 10];
%run_input = [6 8];
%run_ref = 7;
b1(run_ref).FaceColor = 'k';
b2(run_ref).FaceColor = 'k';
b3(run_ref).FaceColor = 'k';
%b4(run_ref).FaceColor = 'k';

col_run_sup = rgb('DarkOrange');
for jj=run_sup
    b1(jj).FaceColor = col_run_sup;
    b2(jj).FaceColor = col_run_sup;
    b3(jj).FaceColor = col_run_sup;
    %b4(jj).FaceColor = col_run_sup;
end

col_run_W = rgb('SkyBlue');
for jj=run_W
    b1(jj).FaceColor = col_run_W;
    b2(jj).FaceColor = col_run_W;
    b3(jj).FaceColor = col_run_W;
    %b4(jj).FaceColor = col_run_W;
end

col_run_geom = rgb('OrangeRed');
for jj=run_W
    b1(jj).FaceColor = col_run_geom;
    b2(jj).FaceColor = col_run_geom;
    b3(jj).FaceColor = col_run_geom;
    %b4(jj).FaceColor = col_run_geom;
end

col_run_input = rgb('DodgerBlue');
for jj=run_input
    b1(jj).FaceColor = col_run_input;
    b2(jj).FaceColor = col_run_input;
    b3(jj).FaceColor = col_run_input;
    %b4(jj).FaceColor = col_run_input;
end

s1.Position(1) = 0.10;
s2.Position(1) = 0.39;
s3.Position(1) = 0.69;
%s4.Position(1) = 0.775;

ypos = 0.13;
s1.Position(2) = ypos;
s2.Position(2) = ypos;
s3.Position(2) = ypos;
%s4.Position(2) = ypos;

sub_w = 0.25;
s1.Position(3) = sub_w;
s2.Position(3) = sub_w;
s3.Position(3) = sub_w;
%s4.Position(3) = sub_w;

