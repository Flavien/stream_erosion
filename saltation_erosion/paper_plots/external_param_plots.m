%This file is meant to plot the firgure in which I will discuss the effect of the external parameters on the erosion and transport
%
%
%

%switch to extract stuff from saltation only runs or from total load eros runs
saltation_only = 0;
%presentation formating switch
pres_font = 0;

disp('Erosion and transport capacity multiplied by width!!!!')

%sim_struct = load_simulations;
sim_struct = load_simulations(saltation_only);
length_struct = length(sim_struct);

%dx = 200;
%dx = 1000;
dx = 500/1000;
%To normalize the plots
%dx = 1 / (length(sim_struct(1).E));

x_reg = [0:length(sim_struct(1).E)].*dx;
x_stag = (x_reg(2:end) + x_reg(1:end-1)) ./ 2;
x = x_stag;
MS = zeros(length_struct,1) ;
LW = zeros(length_struct,1) +  3;

input_change = 1;

%ln_spec_col = {rgb('Silver'),'k',rgb('DimGray'),'k',rgb('DimGray'),'k','k','k'} ;
ln_spec_col = {rgb('OrangeRed'),'k',rgb('SkyBlue'),'k',rgb('SkyBlue'),'k','k','k'} ;
ln_spec_type = {'-','--','--','-','-','--','-.'};
%legend_text = {'MOULIN','CST\_Bc','CST\_Bcx2','REF','REF\_Bcx2'};
%legend_text = {'SS\_CH\_MO','SS\_CH\_CST','SS\_CH\_CSTx2','SS\_CH\_REF','SS\_CH\_SURFx2'};
legend_text = {'S\_MOULIN','S\_CST','S\_CSTx2','S\_REF','S\_SURFx2'};

%ln_spec_col = 'k';
%ln_spec_type = '-';

text_frac = 0.9;

sx = 2;
sy = 3;
if pres_font == 1
    %presentation values
    FSlab = 32;
    FSax  = 32;
    FS_txt = 42;

    fx = 0.55;
    fy = 0.4;
else
    %regular plot values
    FSlab = 26;
    FSax  = 26;
    FS_txt = 28;

    %fx = 0.55;
    %fy = 1;
    fx = 1;
    fy = 0.6;
end

%legend_text = {'ref run','b_c x2','Ice sheet','Small glacier'};
%legend_text = {'ref','ref tb','surf','surf tb','moulin','moulin tb'};

k = 1;

%f1 = figure(k);
f1 = figure;
set(f1,'Units','normalized')
set(f1,'Position',[0,0.5,fx,fy])
S1 = subplot(sx,sy,1);
hold on
for i=1:length_struct
    %fancy_plot(x,sim_struct(i).E,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
    fp1 = fancy_plot(x,sim_struct(i).Q_c,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
    %fp1 = plot(x,sim_struct(i).Q_c,'Linewidth',LW(i),'Linestyle',ln_spec_type{i});
end
set(gca,'FontSize',FSax)
S1.XTick = [0:10:50];
%xlabel('Normalized distance along the bed')
%ylabel('Discharge, Q_{ch} (m^3/s)')
ylabel('$Q_{\rm ch} \, ({\rm m^3 \, s^{-1}})$','interpreter','latex')

%legend(legend_text,'Location','West')
columnlegend(3,legend_text,'Location','northeastoutside','FontSize',FSax)
legend boxoff

txt_posA = 40.*text_frac;
%legend(legend_text,'Location','West')

tA = text(1,txt_posA,'A');
set(tA,'FontSize',FS_txt);

%k = k + 1;
%f2 = figure(k);
%set(f2,'Units','normalized')
%set(f2,'Position',[0,0.5,fx,fy])
S2 = subplot(sx,sy,2);
hold on
for i=1:length_struct
    fp1 = fancy_plot(x,sim_struct(i).S,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
    %fp1 = plot(x,sim_struct(i).S,'Linewidth',LW(i),'Linestyle',ln_spec_type{i});
end

%set(legend,'Location','Best')

set(gca,'FontSize',FSax)
set(gca,'XTick',0:10:50)
%xlabel('Normalized distance along the bed')
%ylabel('Cross-sectional area, S (m^2)')
ylabel('$S \, ({\rm m^2})$','interpreter','latex')
    txt_posB = 15.*text_frac;
tB = text(1,txt_posB,'B');
set(tB,'FontSize',FS_txt);

%k = k+1;
%f3 = figure(k);
%set(f3,'Units','normalized')
%set(f3,'Position',[0,0.5,fx,fy])
S3 = subplot(sx,sy,3);
hold on
for i=1:length_struct
    fp1 = fancy_plot(x,sim_struct(i).tau_ratio,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
    %fp1 = plot(x,sim_struct(i).tau_ratio,'Linewidth',LW(i),'Linestyle',ln_spec_type{i});
    %add_legend
end
%plot(x,x.*0 + 2.3,'k-.','LineWidth',1.5)
%set(legend,'Location','Best')
set(gca,'FontSize',FSax)
S3.XTick = [0:10:50];
%xlabel('Normalized distance along the bed')
%ylabel('Transport stage, \tau^* /\tau^*_c')
ylabel('$\tau^* /\tau^*_{\rm c}$','interpreter','latex')

    txt_posC = 14.*text_frac;

tC = text(1,txt_posC,'C');
set(tC,'FontSize',FS_txt);

%k = k + 1;
%f4 = figure(k);
%set(f4,'Units','normalized')
%set(f4,'Position',[0,0.5,fx,fy])
S4 = subplot(sx,sy,6);
hold on
for i=1:length_struct
    width_fact = sqrt(2.*sim_struct(i).S./pi) .* 2;
    %width_fact = width_fact .* 1000; %For mm /a instead of m/a
    fp1 = fancy_plot(x,sim_struct(i).e_tot.*width_fact,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
    %fp1 = plot(x,sim_struct(i).e_tot.*width_fact,'Linewidth',LW(i),'Linestyle',ln_spec_type{i});
end
%set(legend,'Location','Best')
set(gca,'FontSize',FSax)
S4.XTick = [0:10:50];
xlabel('Distance from divide (km)')
%ylabel('Total erosion, E_{tot} (mm/a*W_{ch})')
ylabel('$E_{\rm tot} \, ({\rm m^2 \, a^{-1}}$)','interpreter','latex')
txt_posD = 1.3.*text_frac;
tD = text(1,txt_posD,'F');
set(tD,'FontSize',FS_txt);

%k = k + 1;
%f5 = figure(k);
%set(f5,'Units','normalized')
%set(f5,'Position',[0,0.5,fx,fy])
S5 = subplot(sx,sy,5);
hold on
for i=1:length_struct;
    %width_fact = 1000; %For mm /a instead of m/a
    width_fact = 1; %For m /a 
    fp1 = fancy_plot(x,sim_struct(i).e_tot.*width_fact,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
    %fp1 = plot(x,sim_struct(i).e_tot.*width_fact,'Linewidth',LW(i),'Linestyle',ln_spec_type{i});

    %add_legend
end
%set(legend,'Location','Best')
set(gca,'FontSize',FSax)
S5.XTick = [0:10:50];
xlabel('Distance from divide (km)')
%ylabel('Erosion rate, e_{tot} (mm/a)')
%ylabel('e_{tot} (m/a)')
ylabel('$\dot{e}_{\rm tot} \, ({\rm m \, a^{-1}}$)','interpreter','latex')

txt_posE = 0.25.*text_frac;
tE = text(1,txt_posE,'E');
set(tE,'FontSize',FS_txt);

%k = k + 1;
%f6 = figure(k);
%set(f6,'Units','normalized')
%set(f6,'Position',[0,0.5,fx,fy])
S6 = subplot(sx,sy,4);
hold on
for i=1:length_struct
    width_fact = sqrt(2.*sim_struct(i).S./pi) .* 2 ./ 2650;
    fp1 = fancy_plot(x,sim_struct(i).q_tc.*width_fact,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
    %fp1 = plot(x,sim_struct(i).q_tc.*width_fact,'Linewidth',LW(i),'Linestyle',ln_spec_type{i});
end
%set(legend,'Location','Best')
set(gca,'FontSize',FSax)
S6.XTick = [0:10:50];
%ylabel('Total transport capacity, Q_{tc} (m^3/s)')
ylabel('$Q_{\rm tc} \, ({\rm m^3 \, s^{-1}})$','interpreter','latex')
xlabel('Distance from divide (km)')

txt_posF = 0.35.*text_frac;
tF = text(1,txt_posF,'D');
set(tF,'FontSize',FS_txt);


%Making the subplots pretty
S1.XTickLabel = '';
S2.XTickLabel = '';
S3.XTickLabel = '';
%S6.XTickLabel = '';

sub_w = 0.26;
S1.Position(3) = sub_w;
S2.Position(3) = sub_w;
S3.Position(3) = sub_w;
S4.Position(3) = sub_w;
S5.Position(3) = sub_w;
S6.Position(3) = sub_w;

sub_h = 0.365;
S1.Position(4) = sub_h;
S2.Position(4) = sub_h;
S3.Position(4) = sub_h;
S4.Position(4) = sub_h;
S5.Position(4) = sub_h;
S6.Position(4) = sub_h;

S1.XLim = [0 50];
S2.XLim = [0 50];
S3.XLim = [0 50];
S4.XLim = [0 50];
S5.XLim = [0 50];
S6.XLim = [0 50];

S1.YLim = [0 40];
S1.YTick = [0:10:40];
S2.YLim = [0 15];
S2.YTick = [0:5:15];
S3.YLim = [0 14];
S3.YTick = [0:2:14];
S4.YLim = [0 1.3];
S4.YTick = [0:0.3:1.5];
S5.YLim = [0 0.25];
S5.YTick = [0:0.05:0.25];
S5.YTickLabel = {'0','0.05','0.10','0.15','0.20','0.25'};
S6.YLim = [0 0.35];
S6.YTick = [0:0.05:0.35];
S6.YTickLabel = {'0','0.05','0.10','0.15','0.20','0.25','0.30','0.35'};;

S1.XGrid = 'on';
%S1.XMinorGrid = 'on';
S1.YGrid = 'on';
%S1.YMinorGrid = 'on';
S2.XGrid = 'on';
%S2.XMinorGrid = 'on';
S2.YGrid = 'on';
%S2.YMinorGrid = 'on';
S3.XGrid = 'on';
%S3.XMinorGrid = 'on';
S3.YGrid = 'on';
%S3.YMinorGrid = 'on';
S4.XGrid = 'on';
%S4.XMinorGrid = 'on';
S4.YGrid = 'on';
%S4.YMinorGrid = 'on';
S5.XGrid = 'on';
%S5.XMinorGrid = 'on';
S5.YGrid = 'on';
%S5.YMinorGrid = 'on';
S6.XGrid = 'on';
%S6.XMinorGrid = 'on';
S6.YGrid = 'on';
%S6.YMinorGrid = 'on';

S1.Position(1) = 0.070;
S6.Position(1) = 0.070;
S2.Position(1) = 0.400;
S5.Position(1) = 0.400;
S3.Position(1) = 0.725;
S4.Position(1) = 0.725;

S1.Position(2) = 0.53; 
S2.Position(2) = 0.53; 
S3.Position(2) = 0.53; 
