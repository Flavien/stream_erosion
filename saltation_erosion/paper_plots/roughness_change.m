%This file is meant to plot the firgure in which I will discuss the effect of the external parameters on the erosion and transport
%
%
%

%switch to extract stuff from saltation only runs or from total load eros runs
saltation_only = 0;
%presentation formating switch
pres_font = 0;

disp('Erosion and transport capacity multiplied by width!!!!')

%sim_struct = load_simulations;
sim_struct = load_simulations(saltation_only);
length_struct = length(sim_struct);

%dx = 200;
%dx = 1000;
dx = 1 / (length(sim_struct(1).E));

x_reg = [0:length(sim_struct(1).E)].*dx;
x_stag = (x_reg(2:end) + x_reg(1:end-1)) ./ 2;
x_stag = x_stag .*50;
x = x_stag;

MS = zeros(length_struct,1) ;
LW = zeros(length_struct,1) +  3;

input_change = 0;
geom_change = 0;
nb_change = 1;

    ln_spec_col = {rgb('Silver'),'k','k',rgb('Gray'),rgb('SlateGray'),rgb('SlateGray')} ;
    ln_spec_type = {'-','-','--','-','-','--'};
    %legend_text = {'SS\_CH\_MR35','SS\_CH\_REF','SS\_CH\_CHSP','SS\_CH\_MR35','SS\_CH\_MR38','SS\_CH\_MR46'};
    legend_text = {'S\_MR23 ','S\_REF ','S\_MR35 ','S\_MR38 ','S\_MR46 '};

text_frac = 0.9;

if pres_font == 1
    %presentation values
    FSlab = 32;
    FSax  = 32;
    FS_txt = 42;

    fx = 0.75;
    fy = 0.66;
else
    %regular plot values
    FSlab = 26;
    FSax  = 26;
    FS_txt = 28;

    fx = 0.75;
    fy = 0.66;
end


k = 1;

%f1 = figure(k);
f1 = figure;
%k = k + 1;
%f2 = figure(k);
set(f1,'Units','normalized')
set(f1,'Position',[0,0.5,fx,fy])
S1 = subplot(2,2,1);
for i=1:length_struct
hold on
    fp1 = fancy_plot(x,sim_struct(i).S,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});

end
set(gca,'FontSize',FSax)
%ylabel('Cross-sectional area, S (m^2)')
ylabel('$S \, ({\rm m^2})$','interpreter','latex')
grid on

    txt_posB = 12.*text_frac;
tB = text(2,txt_posB,'A');
set(tB,'FontSize',FS_txt);

cl1 = columnlegend(3,legend_text,'boxoff');

S2 = subplot(2,2,2);
for i=1:length_struct
hold on
    fp1 = fancy_plot(x,sim_struct(i).tau_ratio,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
end
%plot(x,x.*0 + 2.3,'k-.','LineWidth',1.5)
%set(legend,'Location','Best')
set(gca,'FontSize',FSax)
%ylabel('Transport stage, \tau^* /\tau^*_c')
ylabel('$\tau^* /\tau^*_{\rm c}$','interpreter','latex')
grid on

    txt_posC = 12.*text_frac;
tC = text(2,txt_posC,'B');
set(tC,'FontSize',FS_txt);

%k = k + 1;
%f4 = figure(k);
%set(f4,'Units','normalized')
%set(f4,'Position',[0,0.5,fx,fy])
S4 = subplot(2,2,4);
for i=1:length_struct
hold on
    width_fact = sqrt(2.*sim_struct(i).S./pi) .* 2;
    %width_fact = width_fact .* 1000; %For mm /a instead of m/a
        fp1 = fancy_plot(x,sim_struct(i).e_tot.*width_fact,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});

    %add_legend
end
%set(legend,'Location','Best')
set(gca,'FontSize',FSax)
xlabel('Distance from divide (km)')
    %ylabel('Total erosion, E_{tot} (mm/a*W_{ch})')
    ylabel('$E_{\rm tot} \, ({\rm m^2 \, a^{-1}}$)','interpreter','latex')

grid on
    txt_posD = 1.4.*text_frac;
tD = text(2,txt_posD,'D');
set(tD,'FontSize',FS_txt);

%k = k + 1;
%f6 = figure(k);
%set(f6,'Units','normalized')
%set(f6,'Position',[0,0.5,fx,fy])
S3 = subplot(2,2,3);
for i=1:length_struct
hold on
    width_fact = sqrt(2.*sim_struct(i).S./pi) .* 2 ./ 2650;
    %width_fact = width_fact.*0 + 1;
        fp1 = fancy_plot(x,sim_struct(i).q_tc.*width_fact,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
end
%set(legend,'Location','Best')
set(gca,'FontSize',FSax)
xlabel('Distance from divide (km)')
    %ylabel('Total transport capacity, Q_{tc} (m^3/s)')
    ylabel('$Q_{\rm tc} \, ({\rm m^3 \, s^{-1}})$','interpreter','latex')
grid on
    
    txt_posF = 0.28.*text_frac;
tF = text(2,txt_posF,'C');
set(tF,'FontSize',FS_txt);


%%% Making everything look pretty
S1.XLim = [0 50];
S2.XLim = [0 50];
S3.XLim = [0 50];
S4.XLim = [0 50];

S1.YLim = [0 12];
S2.YLim = [0 12];
S3.YLim = [0 0.28];
S4.YLim = [0 1.4];

S1.XTick = [0:10:50];
S2.XTick = [0:10:50];
S3.XTick = [0:10:50];
S4.XTick = [0:10:50];

S1.XTickLabel = '';
S2.XTickLabel = '';

S4.YTick = [0:0.3:1.2];

S1.Position(1) = 0.095;
S3.Position(1) = 0.095;
S2.Position(1) = 0.585;
S4.Position(1) = 0.585;

S1.Position(2) = 0.52;
S2.Position(2) = 0.52;
S3.Position(2) = 0.1;
S4.Position(2) = 0.1;

sub_w = 0.395;
S1.Position(3) = sub_w;
S2.Position(3) = sub_w;
S3.Position(3) = sub_w;
S4.Position(3) = sub_w;

sub_h = 0.39;
S1.Position(4) = sub_h;
S2.Position(4) = sub_h;
S3.Position(4) = sub_h;
S4.Position(4) = sub_h;

