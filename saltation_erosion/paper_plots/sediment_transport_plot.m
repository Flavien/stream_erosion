%This script is to make figures in which I show how sediment is transported subglacially
%This is based on a bunch of imagesc plots
%
%

%close all
%clear all

%load Rchan_SS_ref_testA

n_nodes = min(size(eta_ar));
x = [0:dx:(n_nodes).*dx]./1000;
x_plot = (x(2:end) + x(1:end-1)) ./ 2;
t_plot = [1:length(eta_ar)].*dt./3600;

MS  = 0;
LW  = 2;

fx = 0.75;
fy = 0.66;

FSlab = 16;
FSax  = 16;
FS_txt = 18;

text_frac = 0.9;

Sx = 2;
Sy = 2;

%ln_spec_col = {'k','b','r','m','k','b','r','m'} ;
ln_spec_col = {rgb('Red'),rgb('Green'),rgb('Blue'),rgb('Purple'),...
rgb('DeepPink'),rgb('Brown'),rgb('Orange'),rgb('Black'),rgb('DimGray'),rgb('DarkGreen')};
ln_spec_type = {'-','-','-','-','-','--','--','--','--','-.','-.','-.','-.'};
size_color = length(ln_spec_col);
size_type = length(ln_spec_type);

time_to_plot = [1 10 20 30 40 50 60 70 80 90 100 150 200 250 300 350 400]; % D = 60 mm
%time_to_plot = [1 20 40 60 80 100 120 140 150 160 170 180 200 250 300 350 400]; % D = 1mm
for i=1:length(time_to_plot)
    legend_entry{i} = strcat(num2str(time_to_plot(i)*dt./3600),' hrs');
end

plot_resolution = 25;


%f1 = figure('NumberTitle','off','Name','Sediment transport explanation');
%f1 = figure(1);
f1 = figure;
clf
set(f1,'Units','normalized');
set(f1,'Position',[0.8,0.2,fx,fy]);

width_fact = 2.*R;
needed = 1;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           FIGURE

S1 = subplot(Sx,Sy,1);
%v_plot1 = linspace(0,1.2,plot_resolution); % testA D=1 / channel W
v_plot1 = linspace(0,2,plot_resolution); % testA D=1 / channel W
clim1 = [v_plot1(1) v_plot1(end)];


im_eta_cont = logspace(log10(0.01),log10(5),plot_resolution);
log_im_eta = log10(im_eta_cont);
%log_data_eta = log10(eta_ar);
log_data_eta = log10(eta_int_ar);
log_data_eta(log_data_eta == -Inf) = NaN;

im_colorbar_eta = [0.01 0.1 1 5];
log_im_col_eta = log10(im_colorbar_eta);


%IM1 = imagesc(x_plot,t_plot,log_data_eta,[log_im_eta(1) log_im_eta(end)]);
%IM1 = contourf(x_map_avg.*200./1000,t_plot,log_data_eta,[log_im_eta(1) log_im_eta(end)]);
IM1 = contourf(x_map_avg.*200./1000,t_plot,log_data_eta,log_im_eta);

colorbar('YTick',log_im_col_eta,'YTickLabel',im_colorbar_eta);

%set(gca,'YDir','normal');

%set(IM1,'alphadata',eta_ar ~= 0)
%set(IM1,'alphadata',~isnan(log_data_eta))
shading flat
set(gca,'Xtick',0:2:10)
set(gca,'FontSize',FSax)
%xlabel('Distance along the bed (km)')
ylabel('Time during simulation (hr)')
title('\eta (m)')
txt_posA = t_plot(end).*text_frac;
tA = text(0.5,txt_posA,'A');
tA2 = text(4,txt_posA,'Sediment-free bed');
set(tA,'FontSize',FS_txt);
set(tA2,'FontSize',FS_txt);


if needed == 1
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %           FIGURE
    %f9 = figure('NumberTitle','off','Name','Near bed concentration');
    %clf
    %set(f9,'Units','normalized');
    %set(f9,'Position',[0.2,0.4,fx,fy]);
    S2 = subplot(Sx,Sy,2);

    max_cb = max(max(cb_ar));
    %v_plot9 = linspace(0,1,25); % testA D=1
    %v_plot9 = linspace(0,0.025,25); % testB D=1
    v_plot9 = linspace(0,max_cb,plot_resolution); % test C D=1
    clim9 = [v_plot9(1) v_plot9(end)];

    IM2 = imagesc(x_plot,t_plot,cb_ar,clim9);
    %imagesc(x_plot,t_plot,cb_ar,v_plot9)
    set(gca,'YDir','normal');
    %imagesc(x_plot,t_plot,cb_ar)

    colorbar
    shading flat

    set(IM2,'alphadata',eta_ar ~= 0)

    set(gca,'Xtick',0:2:10)
    set(gca,'FontSize',FSax)
    %xlabel('Distance along the bed (km)')
    %ylabel('Time during simulation (hr)')
    title('c_b')
    txt_posB = t_plot(end).*text_frac;
    tB = text(0.5,txt_posB,'B');
    tB2 = text(4,txt_posB,'Sediment-free bed');
    set(tB,'FontSize',FS_txt);
    set(tB2,'FontSize',FS_txt);
    %set(tB,'Color','w');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           FIGURE
%f4 = figure('NumberTitle','off','Name','Fraction exposed');
%clf
%set(f4,'Units','normalized');
%set(f4,'Position',[0.2,0.4,fx,fy]);
S3 = subplot(Sx,Sy,3);

%v_plot4 = linspace(0,1,25);

%contourf(x_plot,t_plot,Fe_ar,v_plot4)
IM3 = imagesc(x_plot,t_plot,Fe_tot_ar);
set(gca,'YDir','normal');

colorbar
shading flat

set(IM3,'alphadata',eta_ar ~= 0)

set(gca,'Xtick',0:2:10)
set(gca,'FontSize',FSax)
xlabel('Distance along the bed (km)')
ylabel('Time during simulation (hr)')
title('F_e (TLEM)')
txt_posC = t_plot(end).*text_frac;
tC = text(0.5,txt_posC,'C');
tC2 = text(4,txt_posC,'Sediment-free bed');
set(tC,'FontSize',FS_txt);
set(tC2,'FontSize',FS_txt);
%set(tC,'Color','w');

if needed == 0
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %           FIGURE
    %f7 = figure('NumberTitle','off','Name','Total volumetric transport capacity');
    %clf
    %set(f7,'Units','normalized');
    %set(f7,'Position',[0.2,0.4,fx,fy]);
    S4 = subplot(Sx,Sy,1);


    fp7 = fancy_plot(x_plot,q_tc_ar(end,:)./constants.rho_s.*width_fact(end,:),MS,LW,ln_spec_col{3},ln_spec_type{3});

    %l1 = legend(legend_entry);
    %set(l1,'Location','NorthWest')
    set(gca,'FontSize',FSax)
    set(gca,'Xtick',0:2:10)
    set(gca,'Xtick',0:2:10)
    %xlabel('Distance along the bed (km)')
    ylabel('Total transport capacity (m^3/s)')
    txt_posD = 0.04.*text_frac;
    tD = text(0.5,txt_posD,'A');
    set(tD,'FontSize',FS_txt);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           FIGURE
%f3 = figure('NumberTitle','off','Name','Total load erosion');
%clf
%set(f3,'Units','normalized');
%set(f3,'Position',[0.2,0.4,fx,fy]);
S5 = subplot(Sx,Sy,4);


v_plot3 = linspace(0,max(max(e_tot_ar.*width_fact)).*1e3,plot_resolution);
clim3 = [v_plot3(1) v_plot3(end)];

if clim3(2) == 0
    clim3(2) = 1e-6;
end

im_etot_cont = logspace(log10(0.1),log10(500),plot_resolution);
log_im_etot = log10(im_etot_cont);
log_data = log10(e_tot_ar.*1e3.*width_fact);
log_data(log_data == -Inf) = NaN;

im_colorbar = [0.1 1 10 100 500];
log_im_col = log10(im_colorbar);

IM5 = imagesc(x_plot,t_plot,log_data,[log_im_etot(1) log_im_etot(end)]);

colorbar('YTick',log_im_col,'YTickLabel',im_colorbar);

%imagesc(x_plot,t_plot,e_tot_ar.*1e3.*width_fact,clim3)
%contourf(x_plot,t_plot,e_tot_ar.*1e3,v_plot3)
set(gca,'YDir','normal');

set(IM5,'alphadata',~isnan(log_data))
%colorbar
shading flat
set(gca,'Xtick',0:2:10)
set(gca,'FontSize',FSax)
xlabel('Distance along the bed (km)')
%ylabel('Time during simulation (hr)')
%title('Total erosion (total load) (m/a)')
title('E_{tot} (mm/a*W_{ch})')
txt_posE = t_plot(end).*text_frac;
tE = text(0.5,txt_posE,'D');
tE2 = text(4,txt_posE,'Sediment-free bed');
set(tE,'FontSize',FS_txt);
set(tE2,'FontSize',FS_txt);


if needed == 0
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %           FIGURE
    %f5 = figure('NumberTitle','off','Name','Saltation abrasion');
    %clf
    %set(f5,'Units','normalized');
    %set(f5,'Position',[0.2,0.4,fx,fy]);
    S6 = subplot(Sx,Sy,6);

    v_plot5 = linspace(0,max(max(e_salt_ar.*width_fact)).*1e3,plot_resolution);
    clim5 = [v_plot5(1) v_plot5(end)];

    if clim5(2) == 0
        clim5(2) = 1e-6;
    end

    %im_etot_cont = logspace(log10(500),log10(3e5),plot_resolution);
    %log_im_etot = log10(im_etot_cont);
    log_data2 = log10(e_salt_ar.*1e3.*width_fact);
    %log_data2 = log10(e_salt_ar.*width_fact);
    log_data2(log_data2 == -Inf) = NaN;

    IM6 = imagesc(x_plot,t_plot,log_data2,[log_im_etot(1) log_im_etot(end)]);
    %colorbar('YTick',log_im_etot,'YTickLabel',im_etot_cont);
    colorbar('YTick',log_im_col,'YTickLabel',im_colorbar);

    %imagesc(x_plot,t_plot,e_salt_ar.*1e3.*width_fact,clim5)
    %contourf(x_plot,t_plot,e_salt_ar.*1e3,v_plot5)
    set(gca,'YDir','normal');
    set(IM6,'alphadata',~isnan(log_data))

    %colorbar
    shading flat
    set(gca,'Xtick',0:2:10)
    set(gca,'FontSize',FSax)
    xlabel('Distance along the bed (km)')
    %ylabel('Time during simulation (hr)')
    title('Total e_{salt} (mm/a)')
    txt_posF = t_plot(end).*text_frac;
    tF = text(0.5,txt_posF,'F');
    set(tF,'FontSize',FS_txt);
end
