%file to plot the comparison between topo and maybe other runs
%
%
%

loaded_already = 1;

if loaded_already == 1
    load misc_trans_IS/trans_IS_real
    %load trans_IS_topo
else
    trans_IS = load_IS_TR_comp();
end

%plotting stuffs...
x_plot = [1:min(size(trans_IS(1).q_tc_vol))]-0.5;
x_plot = x_plot .* 500./1000;

t_short = [1:length(trans_IS(1).E_tot)];
t_short = t_short./24;

s_end = 150*24;
text_xpos = 115;

FSlab = 26;
FSax  = 26;
FS_txt = 28;

text_frac = 0.85;

LW2 = 3;
LW1 = 1.5;
LW3 = 2.5;
LW4 = 2;

fig1 =1;
fx = 0.5;
fy = 1;
sx1 = 4;
sy1 = 2;

L10 = 0;
L50 = 1;

x_start = 30;

run_nb = length(trans_IS);

%PREPARING INPUT PLOT
    load input_conversion
    b_s_time_short_2007 = t_short.*0;
    b_s_time_short_2008 = t_short.*0;

    b_s_time_short_2007(1:length(real_input_2007)) = real_input_2007(:,end) .* fact_2007;
    b_s_norm_short_2007 = b_s_time_short_2007 ./ 8e-7;

    b_s_time_short_2008(1:length(real_input_2008)) = real_input_2008(:,end) .* fact_2008 .* fact_2008_to_2007;
    b_s_norm_short_2008 = b_s_time_short_2008 ./ 8e-7;



%keyboard
%%%%%%%%%%%
f1 = figure(fig1);
f1.Units = 'normalized';
f1.Position = [0,0,fx,fy];
%%%%%%%%
    S12 = subplot(sx1,sy1,[1, 2]);

    [AXA H1A H2A] = plotyy(t_short(1:s_end),t_short(1:s_end).*0 + 1,t_short(1:s_end),b_s_norm_short_2007(1:s_end));
    
    set(H1A,'Color','k')
    set(H1A,'Linestyle','--','Linewidth',1.5)
    set(H2A,'Color',[0.80 0.80 0.80])
    set(H2A,'Linewidth',1.8)


    AXA(1).YLim = [0 12];
    AXA(1).YTick = [0:3:12];
    %AXA(2).YLim = [0 2.5];
    %AXA(2).YTick = [0:0.5:2.5];
    AXA(2).YLim = [0 4];
    AXA(2).YTick = [0:1:4];

    set(AXA(2),'FontSize',FSax)
    AXA(2).YColor = [0.45 0.45 0.45];
    %AXA(2).YLabel.String = 'b_{s}/b_{s,max}';
    AXA(2).YLabel.Interpreter = 'latex';
    AXA(2).YLabel.String = '$\dot{b}_{\rm ca}/\dot{b}_{\rm ss \, max}$';

    hold on
    
    k = 1;
    plot_qtA = trans_IS(k).t_stage;

    c1 = 'k';
    c2 = rgb('OrangeRed');
    c3 = rgb('DarkOrange');
    c4 = rgb('SkyBlue');
    c5 = rgb('DodgerBlue');
    %c6 = rgb('DarkGray');

    %pA1 = plot(t_short(1:s_end),trans_IS(k).t_stage(1:s_end,100),'Linewidth',LW3);
    %pA2 = plot(t_short(1:s_end),trans_IS(k).t_stage(1:s_end,90),'Linewidth',LW3);
    %pA3 = plot(t_short(1:s_end),trans_IS(k).t_stage(1:s_end,80),'Linewidth',LW3);
    %pA4 = plot(t_short(1:s_end),trans_IS(k).t_stage(1:s_end,70),'Linewidth',LW3);
    %pA5 = plot(t_short(1:s_end),trans_IS(k).t_stage(1:s_end,60),'Linewidth',LW3);
    %pA6 = plot(t_short(1:s_end),trans_IS(k).t_stage(1:s_end,50),'Linewidth',LW3);

    pA1 = plot(t_short(1:s_end),plot_qtA(1:s_end,100),'Color',c1,'Linewidth',LW3);
    pA2 = plot(t_short(1:s_end),plot_qtA(1:s_end,85),'Color',c2,'Linewidth',LW3);
    pA3 = plot(t_short(1:s_end),plot_qtA(1:s_end,70),'Color',c3,'Linewidth',LW3);
    pA4 = plot(t_short(1:s_end),plot_qtA(1:s_end,55),'Color',c4,'Linewidth',LW3);
    pA5 = plot(t_short(1:s_end),plot_qtA(1:s_end,40),'Color',c5,'Linewidth',LW3);
    %pA6 = plot(t_short(1:s_end),plot_qtA(1:s_end,50),'Linewidth',LW3);
    %t_stage = 1 line
    %plot(t_short(1:s_end),t_short(1:s_end).*0 + 1,'k--','Linewidth',1.5)
    grid on
    
    S12.Color = 'none';
    uistack(AXA(2),'bottom')
    set(AXA(2),'Color','w')


    lA1 = legend([pA1 pA2 pA3 pA4 pA5], '50 km','42.5 km','35 km','27.5 km','20 km');
    lA1.Location = 'NorthWest';
    lA1.Box = 'on';
    lA1.Color = [0.95 0.95 0.95];
    lA1.EdgeColor = 'none';

    ylabel('$\tau^*/\tau^*_{\rm c}$','interpreter','latex')
    set(gca,'FontSize',FSax)
    AXA(1).YColor = 'k';


    txt_pos2A = 12.*text_frac;
    t2A = text(text_xpos,txt_pos2A,'T\_2007   A');
    set(t2A,'FontSize',FS_txt);
%%%%%%%%
    S34 = subplot(sx1,sy1,[3, 4]);
    m_fact = 1000;

    pBB1 = plot(t_short(1:s_end),trans_IS(k).E_tot(1:s_end,100)./m_fact,'Color',c1,'Linewidth',LW2);
    hold on
    pBB2 = plot(t_short(1:s_end),trans_IS(k).E_tot(1:s_end,85)./m_fact,'Color',c2,'Linewidth',LW2);
    pBB3 = plot(t_short(1:s_end),trans_IS(k).E_tot(1:s_end,70)./m_fact,'Color',c3,'Linewidth',LW2);
    pBB4 = plot(t_short(1:s_end),trans_IS(k).E_tot(1:s_end,55)./m_fact,'Color',c4,'Linewidth',LW2);
    pBB5 = plot(t_short(1:s_end),trans_IS(k).E_tot(1:s_end,40)./m_fact,'Color',c5,'Linewidth',LW2);

    pB1 = plot(t_short(1:s_end),trans_IS(k).e_tot(1:s_end,100)./m_fact,'Color',c1,'Linewidth',LW1);
    hold on
    pB2 = plot(t_short(1:s_end),trans_IS(k).e_tot(1:s_end,85)./m_fact,'Color',c2,'Linewidth',LW1);
    pB3 = plot(t_short(1:s_end),trans_IS(k).e_tot(1:s_end,70)./m_fact,'Color',c3,'Linewidth',LW1);
    pB4 = plot(t_short(1:s_end),trans_IS(k).e_tot(1:s_end,55)./m_fact,'Color',c4,'Linewidth',LW1);
    pB5 = plot(t_short(1:s_end),trans_IS(k).e_tot(1:s_end,40)./m_fact,'Color',c5,'Linewidth',LW1);

    %legend('10 km e_{tot}','7.5 km','5 km','2.5 km','10 km E_{tot}','7.5 km','5 km','2.5 km','Location','West')
    %l34 = legend('50 km e_{tot}','37.5 km','25 km','12.5 km','50 km E_{tot}');
    %l34 = legend([pBB1 pB1],'E_{tot} (m^2/a)','e_{tot} (m/a)');
    l34 = legend([pBB1 pB1],'$E_{\rm tot}$ (${\rm m^2 \, a^{-1}}$)','$\dot{e}_{\rm tot}$ $({\rm m \, a^{-1}})$');
    l34.Interpreter = 'latex';
    l34.Location = 'NorthWest';
    legend boxoff
    ylabel('Erosion')
    %xlabel('Time (day)')
    set(gca,'FontSize',FSax)

    grid on
    %grid minor

    txt_pos2B = 1.6.*text_frac;

    t2B = text(text_xpos,txt_pos2B,'T\_2007   B');
    set(t2B,'FontSize',FS_txt);

    %Making sure the colors on the first plot are the proper ones
    %pA1.Color = c1;
    %pA2.Color = c2;
    %pA3.Color = c3;
    %pA4.Color = c4;

%%%%%%%%%%%%%% New half of the subplots
    S56 = subplot(sx1,sy1,[5, 6]);

    [AXB H1B H2B] = plotyy(t_short(1:s_end),t_short(1:s_end).*0 + 1,t_short(1:s_end),b_s_norm_short_2008(1:s_end));
    
    set(H1B,'Color','k')
    set(H1B,'Linestyle','--','Linewidth',1.5)
    set(H2B,'Color',[0.80 0.80 0.80])
    set(H2B,'Linewidth',1.8)


    set(AXB(1),'Ylim',[0 12])
    set(AXB(1),'Ytick',[0:3:12])
    %AXB(2).YLim = [0 5];
    %AXB(2).YTick = [0:1:5];
    AXB(2).YLim = [0 4];
    AXB(2).YTick = [0:1:4];

    set(AXB(2),'FontSize',FSax)
    AXB(2).YColor = [0.45 0.45 0.45];
    %AXB(2).YLabel.String = 'b_{s}/b_{s,max}';
    AXB(2).YLabel.Interpreter = 'latex';
    AXB(2).YLabel.String = '$\dot{b}_{\rm ca}/\dot{b}_{\rm ss \, max}$';

    hold on
    
    k = 2;

    pC1 = plot(t_short(1:s_end),trans_IS(k).t_stage(1:s_end,100),'Color',c1,'Linewidth',LW3);
    pC2 = plot(t_short(1:s_end),trans_IS(k).t_stage(1:s_end,85),'Color',c2,'Linewidth',LW3);
    pC3 = plot(t_short(1:s_end),trans_IS(k).t_stage(1:s_end,70),'Color',c3,'Linewidth',LW3);
    pC4 = plot(t_short(1:s_end),trans_IS(k).t_stage(1:s_end,55),'Color',c4,'Linewidth',LW3);
    pC5 = plot(t_short(1:s_end),trans_IS(k).t_stage(1:s_end,40),'Color',c5,'Linewidth',LW3);
    %t_stage = 1 line
    %plot(t_short(1:s_end),t_short(1:s_end).*0 + 1,'k--','Linewidth',1.5)
    
    S56.Color = 'none';
    uistack(AXB(2),'bottom')
    set(AXB(2),'Color','w')


    lC1 = legend([pC1 pC2 pC3 pC4 pC5], '50 km','42.5 km','35 km','27.5 km','20 km');
    lC1.Location = 'NorthWest';
    lC1.Box = 'on';
    lC1.Color = [0.95 0.95 0.95];
    lC1.EdgeColor = 'none';

    ylabel('$\tau^*/\tau^*_{\rm c}$','interpreter','latex')
    set(gca,'FontSize',FSax)
    AXB(1).YColor = 'k';

    grid on

    txt_pos2C = 12.*text_frac;
    t2C = text(text_xpos,txt_pos2C,'T\_2008   C');
    set(t2C,'FontSize',FS_txt);
%%%%%%%%
    S78 = subplot(sx1,sy1,[7, 8]);

    pDD1 = plot(t_short(1:s_end),trans_IS(k).E_tot(1:s_end,100)./m_fact,'Color',c1,'Linewidth',LW2);
    hold on
    pDD2 = plot(t_short(1:s_end),trans_IS(k).E_tot(1:s_end,85)./m_fact,'Color',c2,'Linewidth',LW2);
    pDD3 = plot(t_short(1:s_end),trans_IS(k).E_tot(1:s_end,70)./m_fact,'Color',c3,'Linewidth',LW2);
    pDD4 = plot(t_short(1:s_end),trans_IS(k).E_tot(1:s_end,55)./m_fact,'Color',c4,'Linewidth',LW2);
    pDD5 = plot(t_short(1:s_end),trans_IS(k).E_tot(1:s_end,40)./m_fact,'Color',c5,'Linewidth',LW2);

    pD1 = plot(t_short(1:s_end),trans_IS(k).e_tot(1:s_end,100)./m_fact,'Color',c1,'Linewidth',LW1);
    hold on
    pD2 = plot(t_short(1:s_end),trans_IS(k).e_tot(1:s_end,85)./m_fact,'Color',c2,'Linewidth',LW1);
    pD3 = plot(t_short(1:s_end),trans_IS(k).e_tot(1:s_end,70)./m_fact,'Color',c3,'Linewidth',LW1);
    pD4 = plot(t_short(1:s_end),trans_IS(k).e_tot(1:s_end,55)./m_fact,'Color',c4,'Linewidth',LW1);
    pD5 = plot(t_short(1:s_end),trans_IS(k).e_tot(1:s_end,40),'Color',c5,'Linewidth',LW1);

    %legend('10 km e_{tot}','7.5 km','5 km','2.5 km','10 km E_{tot}','7.5 km','5 km','2.5 km','Location','West')
    %l78 = legend('50 km e_{tot}','37.5 km','25 km','12.5 km','50 km E_{tot}');
    l78 = legend([pDD1 pD1],'$E_{\rm tot}$ (${\rm m^2 \, a^{-1}}$)','$\dot{e}_{\rm tot}$ $({\rm m \, a^{-1}})$');
    l78.Interpreter = 'latex';
    l78.Location = 'NorthWest';
    l78.Box = 'off';
    ylabel('Erosion')
    xlabel('Time (day)')
    set(gca,'FontSize',FSax)

    grid on
    %grid minor

    txt_pos2D = 1.6.*text_frac;
    t2D = text(text_xpos,txt_pos2D,'T\_2008   D');
    set(t2D,'FontSize',FS_txt);

    %Making sure the colors on the first plot are the proper ones
    %pB1.Color = c1;
    %pB2.Color = c2;
    %pB3.Color = c3;
    %pB4.Color = c4;


%Making sure all the plots are pretty

S34.YLim = [0 1.6];
S78.YLim = [0 1.6];
S34.YTick = [0:0.3:1.5];
S78.YTick = [0:0.3:1.5];

S12.XLim = [0 s_end/24];
S34.XLim = [0 s_end/24];
S56.XLim = [0 s_end/24];
S78.XLim = [0 s_end/24];

S12.XTick = [0:15:s_end/24];
S34.XTick = [0:15:s_end/24];
S56.XTick = [0:15:s_end/24];
S78.XTick = [0:15:s_end/24];

S12.XTickLabel = '';
S34.XTickLabel = '';
S56.XTickLabel = '';

S12.Position(2) = 0.76;
S34.Position(2) = 0.535;
S56.Position(2) = 0.30;
S78.Position(2) = 0.075;

sub_h = 0.21;
S12.Position(4) = sub_h;
S34.Position(4) = sub_h;
S56.Position(4) = sub_h;
S78.Position(4) = sub_h;
