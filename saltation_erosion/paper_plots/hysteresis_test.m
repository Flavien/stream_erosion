%Little piece of code to sort out the direction of hysteresis between the
%discharge and the transport stage 
%
%

function [hyst_clock] = hysteresis_test(Qc_var,t_stage_var,x_loc)

%Qc_var = Qc_short;
%t_stage_var = t_stage_short;

%Qc_var = Qc;
%t_stage_var = t_stage;

%x_loc = 100;
%day = 24;
%s_end_lg = 150*24+1;

day = 24*6;
s_end_lg = 150*24*6+1;
%s_start = 23*24+1;
s_start = 1;

%hyst_clock = zeros(1,s_end_lg) -1;

for i=s_start:day:s_end_lg
    if i == s_start
        ii = 1;
    end
    max_Qc(ii) = max(Qc_var(i:i+day,x_loc));
    max_tstage(ii) = max(t_stage_var(i:i+day,x_loc));
    pos_Qc_t = find(Qc_var(i:i+day,x_loc) == max_Qc(ii));
    pos_tst_t = find(t_stage_var(i:i+day,x_loc) == max_tstage(ii));
    pos_Qc(ii) = max(pos_Qc_t);
    pos_tst(ii) = max(pos_tst_t);

    if max_tstage == 0
        hyst_clock(ii) = -1;
    elseif pos_Qc(ii) == pos_tst(ii)
        hyst_clock(ii) = 0.5;
    elseif pos_Qc(ii) > pos_tst(ii)
        hyst_clock(ii) = 1;
    elseif pos_Qc(ii) < pos_tst(ii)
        hyst_clock(ii) = 0;
    end

    ii = ii +1;
    %keyboard;
end
