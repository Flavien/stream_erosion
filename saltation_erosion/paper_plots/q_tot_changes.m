%file to plot the changes in patterns of erosion for a given hydrology run
%and different sediment supply rates or different sediment sizes
%
%




%switch to extract stuff from saltation only runs or from total load eros runs
saltation_only = 0;
%presentation formating switch
pres_font = 0;

%sim_struct = load_simulations;
sim_struct = load_simulations(saltation_only);
length_struct = length(sim_struct);

%dx = 200;
%dx = 1000;
%dx = 1 / (length(sim_struct(1).E));
dx = 500/1000;

x_reg = [0:length(sim_struct(1).E)].*dx;
x_stag = (x_reg(2:end) + x_reg(1:end-1)) ./ 2;
x = x_stag;
MS = zeros(length_struct,1) ;
LW = zeros(length_struct,1) +  3;
text_frac = 0.9;

qtot_comp = 1;

    %ln_spec_col = {rgb('Gainsboro'),rgb('LightGray'),rgb('Silver'),rgb('LightSlateGray'),'k',...
     %           rgb('SlateGray'),rgb('DarkSlateGray'),rgb('Gray'),rgb('DimGray'),rgb('DimGray')};
    ln_spec_col = {rgb('LightSkyBlue'),rgb('SkyBlue'),rgb('DeepSkyBlue'),rgb('DodgerBlue'),'k',...
                rgb('Orange'),rgb('DarkOrange'),rgb('OrangeRed'),rgb('ForestGreen'),rgb('DarkGreen')};
    ln_spec_type = {'-.','-.','-.','-.','-','--','--','--','--','--'};
    %legend_text = {'q_{s,ref}/20','/10','/5','/2','REF, q_{s,ref}','x2','x5','x10','x20','x25'};
    legend_text = {'q_{s,ref}/20','/10','/5','/2','q_{s,ref}','x2','x5','x10','x20','x25'};
    LW = [3 1.5 3 1.5 3 1.5 3 1.5 3 1.5];
    
    sx = 3;
    sy = 1;

if pres_font == 1
    %presentation values
    FSlab = 32;
    FSax  = 32;

    fx = 0.55;
    fy = 0.4;
else
    %regular plot values
    FSlab = 26;
    FSax  = 26;
    FS_txt = 28;

    fx = 0.4;
    fy = 1;
end


k = 1;

f1 = figure(k);
set(f1,'Units','normalized')
set(f1,'Position',[0,0.5,fx,fy])
S1 = subplot(sx,sy,2);
for i=1:length_struct
    width_fact = sqrt(2.*sim_struct(i).S./pi) .* 2;
    %width_fact = width_fact .* 1000; %For mm /a instead of m/a
    %%%%%fp1 = fancy_plot(x,sim_struct(i).e_tot.*width_fact,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
    fp1 = semilogy(x,sim_struct(i).e_tot.*width_fact + 1e-6,'Linewidth',LW(i),'Color',ln_spec_col{i},'Linestyle',ln_spec_type{i});
    hold on
    %pause

    if i== 1
        max_etot = max(sim_struct(i).e_tot.*width_fact);
    else
        max_etot = max(max_etot,max(sim_struct(i).e_tot.*width_fact));
    end
   
end
%set(legend,'Location','Best')
set(gca,'FontSize',FSax)
%ylabel('Total erosion TLEM, E_{tot} (mm/a*W_{ch})')
ylabel('$E_{\rm tot} \, ({\rm m^2 \, a^{-1}})$','interpreter','latex')

%columnlegend(3,legend_text,'Location','North','boxoff');
cl1 = columnlegend(5,legend_text,'Location','North','boxoff');
grid on
%grid minor

txt_posA = 3.*text_frac;
tA = text(1,txt_posA,'B');
set(tA,'FontSize',FS_txt);

S2 = subplot(sx,sy,3);
for i=1:length_struct
    width_fact = sqrt(2.*sim_struct(i).S./pi) .* 2;
    %width_fact = width_fact .* 1000; %For mm /a instead of m/a
    %fp2 = fancy_plot(x,sim_struct(i).E.*width_fact,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
    fp2 = semilogy(x,sim_struct(i).E.*width_fact + 1e-6,'Linewidth',LW(i),'Color',ln_spec_col{i},'Linestyle',ln_spec_type{i});
hold on
    if i== 1
        max_E = max(sim_struct(i).E);
    else
        max_E = max(max_E,max(sim_struct(i).E));
    end
end
set(legend,'Location','Best')
set(gca,'FontSize',FSax)
xlabel('Distance from divide (km)')
%ylabel('Total erosion SEM, E_{salt} (mm/a*W_{ch})')
ylabel('$E_{\rm salt} \, ({\rm m^2 \, a^{-1}})$','interpreter','latex')
%txt_posB = max_E*1.02.*text_frac;

%columnlegend(3,legend_text,'Location','West','boxoff');
grid on
%grid minor

txt_posB = 3.*text_frac;
tB = text(1,txt_posB,'C');
set(tB,'FontSize',FS_txt);

S3 = subplot(sx,sy,1);
for i=1:length_struct
    plot_quantity = sim_struct(i).q_tot ./ (sim_struct(i).q_tc./2650);
    %plot_quantity = sim_struct(i).q_b ./ (sim_struct(i).q_tc.*600);
    fp3 = fancy_plot(x,plot_quantity,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
hold on
    if i== 1
        max_qrel = max(plot_quantity);
    else
        max_qrel = max(max_qrel,max(plot_quantity));
    end
end
%set(legend,'Location','Best')
set(gca,'FontSize',FSax)
%xlabel('Normalized distance along the bed')
%ylabel('Relative sediment supply TLEM, q_{b}/q_{tc}')
ylabel('$q_{\rm s}/q_{\rm tc}$','interpreter','latex')
grid on
%grid minor
txt_posC = 2*text_frac;
tC = text(1,txt_posC,'A');
set(tC,'FontSize',FS_txt);


%Making the plots pretty

S3.YLim = [0 2];

S1.XLim = [0 50];
S2.XLim = [0 50];
S3.XLim = [0 50];
S1.XTick = [0:10:50];
S2.XTick = [0:10:50];
S3.XTick = [0:10:50];

S3.XTickLabel = '';
S1.XTickLabel = '';

%S1.YLim = [0 5];
%S1.YTick = [0:0.5:3];
%S1.YLim = [2e-2 5];
S1.YLim = [1e-2 5];
label1 = linspace(1e-2,1e-1,10);
label2 = linspace(2e-1,1,9);
label = [label1 label2 2 3 4 5];
S1.YTick = label;
S1.YTickLabel = {'10^{-2}','','','','','','','','','10^{-1}','','','','','','','','','1','','','','5'};
%S1.YTickLabel = {'','2x10^{-2}','','','','','','','','10^{-1}','','','','','','','','','1','','','','5'};

%S2.YLim = [0 1.5];
%S2.YTick = [0:0.5:1.5];
S2.YLim = [1e-2 5];
S2.YTick = label;
S2.YTickLabel = {'10^{-2}','','','','','','','','','10^{-1}','','','','','','','','','1','','','','5'};

S3.YLim = [0 2];
S3.YTick = [0:0.5:2];
S3.YTickLabel = {'0','0.5','1.0','1.5','2.0'};

S3.Position(2) = 0.65;
S1.Position(2) = 0.37;
S2.Position(2) = 0.085;

S1.Position(1) = 0.17;
S2.Position(1) = 0.17;
S3.Position(1) = 0.17;

S1.Position(4) = 0.25;
S2.Position(4) = 0.25;
S3.Position(4) = 0.25;

S1.Position(3) = 0.775;
S2.Position(3) = 0.775;
S3.Position(3) = 0.775;
