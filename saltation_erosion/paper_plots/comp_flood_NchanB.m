%File to plot a the N-channel carved from different simulations

load_first = 1;

if load_first == 1
    load trans_IS_all_flood.mat
    tIS_all_floods = trans_IS;
    clearvars trans_IS;
    load trans_IS_flood_test
    trans_IS(2) = tIS_all_floods(20);
end

%Runs that I need:
runs_needed = [1 2];
plot_Xsect = 1;
%flag = 0 1 2
AGU_pres = 2;

%plotting stuffs...
x_plot = [1:min(size(trans_IS(1).q_tc_vol))]-0.5;
x_plot = x_plot .* 500./1000;

%FSlab = 26;
%FSax  = 26;
%FScont = 22;
%FS_txt = 28;

FSlab = 28;
FSax  = 28;
FScont = 24;
FS_txt = 30;

text_frac = 0.9;

LW1 = 1.5;
LW2 = 3.5;
LW3 = 5;

fig1 =1;
fig2 =2;
fx = 0.7;
fy = 0.45;
fx2 = 0.7;
fy2 = 0.35;
sx1 = 1;
sy1 = 2;

L10 = 0;
L50 = 1;

x_start = 1;

cont_nb = 10;

run_nb = length(runs_needed);

%%%%%%%%%%%
f1 = figure(fig1);
f1.Units = 'normalized';
f1.Position = [0,0.3,fx,fy];

%textx1 = 11;
%textx2 = 20;
textx1 = 1;
textx2 = 9;
metre_fact = 1e-3;

kk = 1;
S1 = subplot(sx1,sy1,1);
    
    %k = runs_needed(kk);
    k = runs_needed(1);
    vectSA1 = [-120:2:0].*metre_fact;
    vectSA2 = [-10 0].*metre_fact;
    vectSA3 = [-100 0].*metre_fact;

    width_coordA = trans_IS(k).width + max(trans_IS(k).width);
    [CA,hA] = contourf(x_plot(x_start:end),width_coordA,trans_IS(k).valley_topo(:,x_start:end).*metre_fact,vectSA1);
    hold on

    hA.LineColor = 'none';
    [CA3,hA3] = contour(x_plot(x_start:end),width_coordA,trans_IS(k).valley_topo(:,x_start:end).*metre_fact,vectSA3);
    [CA2,hA2] = contour(x_plot(x_start:end),width_coordA,trans_IS(k).valley_topo(:,x_start:end).*metre_fact,vectSA2);
    hA2.LineColor = 'k';
    hA3.LineColor = 'w';
    
    colormap('gray')
    cb = colorbar;
    %cb.Limits = XXXX
    cb.Location = 'south';

if AGU_pres == 0;
    textyA = max(trans_IS(k).width) .* 0.9;
    textyA = max(width_coordA) .* 0.9;
    TA1 = text(textx1,textyA,'A');
    %TA2 = text(textx2,textyA,'TR\_IS\_REF');
    TA2 = text(textx2,textyA,'Season');
    TA1.FontSize = FSax;
    TA2.FontSize = FSax;
end

if AGU_pres == 2
    S1.YLim = [-25 25];
    S1.YTick = [-25 -15 -5 5 15];
    S1.YTickLabel = {'0','10','20','30','40'};
end

    %t1 = clabel(CA2,'manual');


    xlabel('Distance from ice divide (km)')
    ylabel('Channel width (m)')
    set(gca,'FontSize',FSax)

kk = 2;
S2 = subplot(sx1,sy1,2);
    
    %k = runs_needed(kk);
    k = runs_needed(2);
    %vectSB1 = [-120:5:0];
    %vectSB2 = [-120:20:0];
    width_coordB = trans_IS(k).width + max(trans_IS(k).width);
    [CB,hB] = contourf(x_plot(x_start:end),width_coordB,trans_IS(k).valley_topo(:,x_start:end).*metre_fact,vectSA1);
    hold on
    [CB2,hB2] = contour(x_plot(x_start:end),width_coordB,trans_IS(k).valley_topo(:,x_start:end).*metre_fact,vectSA2);
    %hB.LineColor = [0.7 0.7 0.7];
    hB2.LineColor = 'k';
    hB.LineColor = 'none';

    %textyB = max(trans_IS(k).width) .* 0.9;
    textyB = max(width_coordB) .* 0.9;

if AGU_pres == 0
    TB1 = text(textx1,textyB,'B');
    %TB2 = text(textx2,textyB,'FLOOD\_Nye\_shape');
    TB2 = text(textx2,textyB,'20-days flood, V_{water} x10');
    TB1.FontSize = FSax;
    TB2.FontSize = FSax;
end

    cb2 = colorbar;
    cb2.Location = 'south';
    caxis([-120 0].*metre_fact)
    cb2.Visible = 'off';

    xlabel('Distance from ice divide (km)')
    %ylabel('N-Channel radius (m)')
    set(gca,'FontSize',FSax)

if plot_Xsect == 1
    f2 = figure(fig2);
    f2.Units = 'normalized';
    f2.Position = [0,0.3,fx2,fy2];

        hold off
        p2a = plot(width_coordB,trans_IS(2).valley_topo(:,40).*metre_fact);
        hold on
        p2b = plot(width_coordA+22.5,trans_IS(1).valley_topo(:,60).*metre_fact);
        p2c = plot(width_coordA+22.5,trans_IS(1).valley_topo(:,95).*metre_fact);

        eros_2mm = (trans_IS(2).valley_topo(:,40).*0 - 2).*metre_fact;
        eros_10mm = eros_2mm *5;
        
        p2d = plot(width_coordB,eros_2mm,'--');
        p2e = plot(width_coordB,eros_10mm,':');

        ylabel('Depth (m)','FontSize',FSax)
        xlabel('Width (m)','FontSize',FSax)

        l2 = legend('Flood','Annual processes (upstream)','Annual processes (downstream)');
        %l2 = legend('Flood @ 20 km','Annual processes @ 30 km','Annual processes @ 47.5 km','2 mm of erosion','10 mm of erosion');
        l2.FontSize = FSax;
        l2.Location = 'SouthEast';
        l2.Box = 'off';

        ax2 = gca;
        
        ax2.FontSize = FSax;
        ax2.YLim = [-120 0].*metre_fact;

        p2a.LineWidth = LW3;
        p2b.LineWidth = LW3;
        p2c.LineWidth = LW3;
        p2d.LineWidth = LW2;
        p2e.LineWidth = LW2;

        p2a.Color = 'k';
        p2b.Color = rgb('Gray');
        p2c.Color = rgb('Maroon');
        p2d.Color = rgb('MediumBlue');
        p2e.Color = rgb('MediumBlue');
end


Nchan_Xlim = [0 50];
S1.XLim = Nchan_Xlim;
S2.XLim = Nchan_Xlim;

Nchan_Xtick = [0:5:50];
S1.XTick = Nchan_Xtick;
S2.XTick = Nchan_Xtick;

Nchan_XTickLab = {'0','','10','','20','','30','','40','','50'};
S1.XTickLabel = Nchan_XTickLab;
S2.XTickLabel = Nchan_XTickLab;

%Making everything pretty again...

sub_h = 0.70;
S1.Position(4) = sub_h;
S2.Position(4) = sub_h;

S1.Position(2) = 0.16; 
S2.Position(2) = 0.16; 

cb.Position(1) = 0.35;
cb.Position(2) = 0.95;

cb.Label.String = 'Depth (m)';
cb.Label.Position(1) = -140.*metre_fact;
cb.Label.Position(2) = 0.9;

cb.Ticks = [-120:20:0].*metre_fact;

if AGU_pres < 2
    clabel(CA3,hA3,'Color','w','FontSize',FScont)
    clabel(CA2,hA2,'Color','k','FontSize',FScont,'LabelSpacing',550)
end

clabel(CB2,hB2,'Color','k','FontSize',FScont,'LabelSpacing',1000)

%sub_w = 0.29;
%S1.Position(3) = sub_w;
%S2.Position(3) = sub_w;


%S1.Position(1) = 0.055;
%S2.Position(1) = 0.370;

