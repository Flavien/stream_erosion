%file to plot the comparison between topo and maybe other runs
%
%
%

loaded_already = 1;

if loaded_already == 1
    load trans_IS_input
else
    trans_IS = load_IS_TR_comp();
end

%plotting stuffs...
x_plot = [1:min(size(trans_IS(1).q_tc_vol))]-0.5;
x_plot = x_plot .* 500./1000;

FSlab = 16;
FSax  = 16;
FS_txt = 18;

text_frac = 0.9;

LW1 = 1.5;
LW2 = 2.5;

fig1 =1;
fx = 0.66;
fy = 0.5;
sx1 = 2;
sy1 = 3;

L10 = 0;
L50 = 1;

x_start = 30;

cont_nb = 10;

run_nb = length(trans_IS);
%%%%%%%%%%%
f1 = figure(fig1);
f1.Units = 'normalized';
f1.Position = [0,0.3,fx,fy];

subA = subplot(sx1,sy1,[1,3]);

    for i =1 :run_nb
        p1(i) = plot(x_plot(x_start:end),trans_IS(i).sum_e_eff(x_start:end),'LineWidth',LW1,'LineStyle','--');
        c1(i,:) = get(p1(i),'Color');
        if i == 1
            hold on
        end
    end

    for i=1:run_nb
        p2(i) = plot(x_plot(x_start:end),trans_IS(i).sum_E_eff(x_start:end),'Color',c1(i,:),'LineWidth',LW2);
    end
    
    ylabel('Integrated erosion')
    xlabel('Distance along the bed (km)')

    l1 = legend('IS \int e_{tot} dt (mm)','IS\_RA','IS\_RB','IS \int E_{tot} dt (mm \times m)');
    l1.Location = 'West';
    legend boxoff

    set(gca,'FontSize',FSax)

    set(gca,'Xtick',x_start/2:5:50)
    txt_pos2A = 320.*text_frac;
    t2A = text(16,txt_pos2A,'A');

    set(t2A,'FontSize',FS_txt);
    axis([x_start/2 50 0 320])

subplot(sx1,sy1,4)
    
    k = 1;
    %imagesc(x_plot(x_start:end),trans_IS(k).width,trans_IS(k).valley_topo(:,15:end))
    contour(x_plot(x_start:end),trans_IS(k).width,trans_IS(k).valley_topo(:,x_start:end),cont_nb)
    
    %set(gca,'YDir','normal')
    colormap gray
    %shading flat
    col_b = colorbar;
    col_b.Label.String = 'Depth (mm)';
    %col_b.Ticks = [-25 -20 -15 -10 -5 0];
    %
        %col_min = -round(max(trans_IS(k).sum_e_eff),1,'significant');
        %col_b.Ticks = [col_min:10:0];
        %col_b.Ticks = [col_min:25:0];
        col_b.Ticks = [-125:25:0];

    col_b.Location = 'south';

    %
    %xlabel('Distance along the bed (km)')
    ylabel('N-Channel radius (m)')
    set(gca,'FontSize',FSax)
    col_b.FontSize = 12;

    R_max = max(trans_IS(k).width);
    axis([x_start/2 50 -R_max R_max])
    
    if L10 == 1
        set(gca,'Xtick',0:2:10)
        txt_pos2B = R_max.*text_frac.*0.9;
        t2B = text(0.75,txt_pos2B,'B');
    elseif L50 == 1
        set(gca,'Xtick',x_start/2:5:50)
        txt_pos2B = R_max.*text_frac.*0.9;
        t2B = text(17.5,txt_pos2B,'B');
        titleB = text(28,txt_pos2B,'IS');
    end
    
    set(t2B,'FontSize',FS_txt);
    set(titleB,'FontSize',FS_txt);


subplot(sx1,sy1,5)
    
    k = 2;
    %imagesc(x_plot(x_start:end),trans_IS(k).width,trans_IS(k).valley_topo(:,15:end))
    contour(x_plot(x_start:end),trans_IS(k).width,trans_IS(k).valley_topo(:,x_start:end),cont_nb)
    
    %set(gca,'YDir','normal')
    colormap gray
    %shading flat
    col_b = colorbar;
    col_b.Label.String = 'Depth (mm)';
    %col_b.Ticks = [-25 -20 -15 -10 -5 0];
    %
    if L10 == 1
        col_b.Ticks = [-45:5:0];
        col_b.Label.Color = [0.8 0.8 0.8];
    elseif L50 == 1
        col_min = -round(max(trans_IS(k).sum_e_eff),1,'significant');
        %col_b.Ticks = [col_min:10:0];
        %col_b.Ticks = [col_min:25:0];
        col_b.Ticks = [-125:25:0];
    end
    col_b.Location = 'south';

    %
    xlabel('Distance along the bed (km)')
    %ylabel('N-Channel radius (m)')
    set(gca,'FontSize',FSax)
    col_b.FontSize = 12;

    R_max = max(trans_IS(k).width);
    axis([x_start/2 50 -R_max R_max])
    
    if L10 == 1
        set(gca,'Xtick',0:2:10)
        txt_pos2C = R_max.*text_frac.*0.9;
        t2C = text(0.75,txt_pos2C,'C');
    elseif L50 == 1
        set(gca,'Xtick',x_start/2:5:50)
        txt_pos2C = R_max.*text_frac.*0.9;
        t2C = text(17.5,txt_pos2C,'C');
        titleC = text(31,txt_pos2C,'IS\_RA');
    end
    
    set(t2C,'FontSize',FS_txt);
    set(titleC,'FontSize',FS_txt);

subplot(sx1,sy1,6)
    
    k = 3;
    %imagesc(x_plot(x_start:end),trans_IS(k).width,trans_IS(k).valley_topo(:,15:end))
    contour(x_plot(x_start:end),trans_IS(k).width,trans_IS(k).valley_topo(:,x_start:end),cont_nb)
    
    %set(gca,'YDir','normal')
    colormap gray
    %shading flat
    col_b = colorbar;
    col_b.Label.String = 'Depth (mm)';
    %col_b.Ticks = [-25 -20 -15 -10 -5 0];
    %
    if L10 == 1
        col_b.Ticks = [-45:5:0];
        col_b.Label.Color = [0.8 0.8 0.8];
    elseif L50 == 1
        col_min = -round(max(trans_IS(k).sum_e_eff),1,'significant');
        %col_b.Ticks = [col_min:10:0];
        %col_b.Ticks = [col_min:25:0];
        col_b.Ticks = [-125:25:0];
    end
    col_b.Location = 'south';

    %
    %xlabel('Distance along the bed (km)')
    ylabel('N-Channel radius (m)')
    set(gca,'FontSize',FSax)
    col_b.FontSize = 12;

    R_max = max(trans_IS(k).width);
    axis([x_start/2 50 -R_max R_max])
    
    if L10 == 1
        set(gca,'Xtick',0:2:10)
        txt_pos2D = R_max.*text_frac.*0.9;
        t2D = text(0.75,txt_pos2D,'D');
    elseif L50 == 1
        set(gca,'Xtick',x_start/2:5:50)
        txt_pos2D = R_max.*text_frac.*0.9;
        t2D = text(17.5,txt_pos2D,'D');
        titleD = text(29,txt_pos2D,'IS\_RB');
    end
    
    set(t2D,'FontSize',FS_txt);
    set(titleD,'FontSize',FS_txt);
