%Script to compute hysteresis at different points along the bed of one simulation
%
%
%

%load TR_RB_seas_run_test

for i=80:1:100
    if i == 80
        ii = 1;
    end

    hyst_season(ii,:) = hysteresis_test(Qc,t_stage,i);
    %hyst_clock{ii,:} = find(hyst_season(ii,:) == 1);
    %hyst_Cclock{ii,:} = find(hyst_season(ii,:) == 0);
    %hyst_undef{ii,:} = find(hyst_season(ii,:) == 0.5);

    ii = ii + 1;
end

dRdt = (R_short(2:end,:) - R_short(1:end-1,:))./3600;
S = (pi.*R_short.^2)./2;
dSdt = (S(2:end,:) - S(1:end-1,:))./3600;
dRdt = dRdt./max(max(dRdt));

dSdt_on = 0;
%sx = 4;
sx = 3;
sy = 1;

fx = 0.5;
fy = 1;
s_start = 25*24;
s_end_short = 120*24;

FS_ax = 26;
text_frac = 0.1;

LW1 = 3;
LW2 = 2;

%PREPARING INPUT PLOT
    load input_conversion
    b_s_time_short_2007 = t_short.*0;
    b_s_time_short_2008 = t_short.*0;

    b_s_time_short_2007(1:length(real_input_2007)) = real_input_2007(:,end) .* fact_2007;
    b_s_norm_short_2007 = b_s_time_short_2007 ./ 8e-7;

    b_s_time_short_2008(1:length(real_input_2008)) = real_input_2008(:,end) .* fact_2008 .* fact_2008_to_2007;
    b_s_norm_short_2008 = b_s_time_short_2008 ./ 8e-7;

input_style = 3;

f1 = figure;
f1.Units = 'normalized';
f1.Position = [0,0.3,fx,fy];

x_locs = [80 86 94 100];

%legend_text = {'40 km','42 km','44 km','46 km','48 km','50 km'};
legend_text = {'40 km','43 km','46 km','50 km'};

%for i=1:2:21
pos = 1;
S1 = subplot(sx,sy,pos);
%plot(t_short(1:s_end_short),b_s_norm_short(1:s_end_short));

    x_loc = 80;
    if input_style == 1
        [AXA H1A H2A] = plotyy(t_short(s_start:s_end_short),t_stage_short(s_start:s_end_short,x_loc),t_short(s_start:s_end_short),b_s_norm_short(s_start:s_end_short));
    elseif input_style == 2
        [AXA H1A H2A] = plotyy(t_short(s_start:s_end_short),t_stage_short(s_start:s_end_short,x_loc),t_short(s_start:s_end_short),b_s_norm_short_2007(s_start:s_end_short));
    elseif input_style ==3
        [AXA H1A H2A] = plotyy(t_short(s_start:s_end_short),t_stage_short(s_start:s_end_short,x_loc),t_short(s_start:s_end_short),b_s_norm_short_2008(s_start:s_end_short));
    end
    hold on
    H1A.LineWidth = LW1;
    H2A.LineWidth = LW2;
    H2A.Color = [0.85 0.85 0.85];
    AXA(2).YColor = [0.6 0.6 0.6];
    AXA(2).FontSize = FS_ax;

    %AXA(2).YLabel.String = 'b_{s}/b_{s,max}';
    AXA(2).YLabel.Interpreter = 'latex';
    AXA(2).YLabel.String = '$\dot{b}_{\rm ca}/\dot{b}_{\rm ss \, max}$';
    AXA(1).YLabel.Interpreter = 'latex';
    AXA(1).YLabel.String = '$\tau^* / \tau^*_{\rm c}$';
    AXA(1).YLabel.Color = 'k';
    AXA(1).YColor = 'k';

    AXA(1).YLim = [0 12];
    AXA(1).YTick = [0:3:12];
    AXA(2).YLim = [0 4];
    AXA(2).YTick = [0:1:4];

    S1.Color = 'none';
    uistack(AXA(2),'bottom')
    set(AXA(2),'Color','w')

    for ii = x_locs(2:end)
        if ii == x_locs(2);
            k = 1;
        end
        pA(k) = plot(t_short(s_start:s_end_short),t_stage_short(s_start:s_end_short,ii),'LineWidth',LW1);
        k = k+1;
    end

    pA(3).Color = 'k';
    pA(2).Color = rgb('OrangeRed');
    pA(1).Color = rgb('SkyBlue');
    H1A.Color = rgb('DarkGray');
    
        %lb = columnlegend(2,legend_text,'boxoff');
        %lb = legend([H1A pA],legend_text,'Location','North','Orientation','Horizontal');
        lb = legend([flip(pA) H1A],flip(legend_text),'Location','North','Orientation','Horizontal');
        lb.FontSize = 24;
        lb.Box = 'off';
    grid on

    tA = text(26,12*text_frac,'A','FontSize',FS_ax);

if dSdt_on == 1
    pos = pos +1;
    S2 = subplot(sx,sy,pos);
        x_locB = 100;
        %pB = plot(t_short(s_start:s_end_short),dRdt(s_start:s_end_short,x_locB));

        for ii = x_locs
            if ii == 80;
                k = 1;
            end
            pB(k) = plot(t_short(s_start:s_end_short),dSdt(s_start:s_end_short,ii),'LineWidth',LW1);
        hold on
            k = k+1;
        end

        pB(4).Color = 'k';
        pB(3).Color = rgb('OrangeRed');
        pB(2).Color = rgb('SkyBlue');
        pB(1).Color = rgb('DarkGray');

        yb = ylabel('${\rm d} S / {\rm d}t \, ( {\rm m^2 \, s^{-1}})$');
        yb.Interpreter = 'latex';
        grid on
        S2.FontSize = FS_ax;

        lb = columnlegend(2,legend_text,'boxoff');
        lb.FontSize = 24;
end

pos = pos +1;
S3 = subplot(sx,sy,pos);
    x_locC = 100;
    %pC = plot(t_short(s_start:s_end_short),Qc_short(s_start:s_end_short,x_locC));

    for ii = x_locs
        if ii == 80;
            k = 1;
        end
        pC(k) = plot(t_short(s_start:s_end_short),Qc_short(s_start:s_end_short,ii),'LineWidth',LW1);
    hold on
        k = k+1;
    end

        pC(4).Color = 'k';
        pC(3).Color = rgb('OrangeRed');
        pC(2).Color = rgb('SkyBlue');
        pC(1).Color = rgb('DarkGray');

    yc = ylabel('$Q_{\rm ch} \, ({\rm m^3 \, s^{-1}})$','interpreter','latex');
    grid on
    S3.FontSize = FS_ax;
    tB = text(26,15*text_frac,'B','FontSize',FS_ax);

pos = pos +1;
S4 = subplot(sx,sy,pos);
%for i=1:4:21
for i=[1 7 15 21]
    if i == 1
        ii = 1;
    end
    hyst_clock = find(hyst_season(i,:) == 1);
    hyst_Cclock = find(hyst_season(i,:) == 0);
    hyst_undef = find(hyst_season(i,:) == 0.5);

    %if i < 10
        %p1(ii) = plot(hyst_season(i,:) + 2.5*i,'x');
        p1(ii) = plot(hyst_clock,hyst_clock.*0 + 0.6*ii,'o');
        hold on
        p2(ii) = plot(hyst_Cclock,hyst_Cclock.*0 + 0.6*ii,'o','MarkerFaceColor','k');
        p3(ii) = plot(hyst_undef,hyst_undef.*0 + 0.6*ii,'x');
    %else
        %p1(ii) = plot(hyst_season(i,:) + 2.5*i,'o');
    %end

    ii = ii + 1;
end

    tC = text(26,3*text_frac,'C','FontSize',FS_ax);

%ax1 = gca;
ax1 = S4;
grid on
%ax1.YTick = [0.3:0.6:6.3];
%ax1.YLim = [0 6.9];
ax1.YTick = [0.6:0.6:2.4];
ax1.YLim = [0 3];
%ax1.YTickLabel = [40:1:50];
ax1.YTickLabel = [40 43 46 50];
ax1.FontSize = FS_ax;
ax1.XLim = [25 120];
ax1.XTick = [30:15:120];

%Making a stupid legend
pl1 = plot(28,2.7,'kx');
pl1.MarkerSize = 10;
tl1 = text(29,2.7,'Undefined events');
tl1.FontSize = FS_ax;

pl2 = plot(63,2.7,'ko');
pl2.MarkerSize = 10;
tl2 = text(64,2.7,'Clockwise');
tl2.FontSize = FS_ax;


pl3 = plot(87,2.7,'ko');
pl3.MarkerSize = 12;
pl3.MarkerFaceColor = 'k';
tl3 = text(88,2.7,'Counter clockwise');
tl3.FontSize = FS_ax;

for ii = 1:length(p1)
    p1(ii).MarkerEdgeColor = 'k';
    p2(ii).MarkerEdgeColor = 'k';
    p3(ii).MarkerEdgeColor = 'k';
    p1(ii).MarkerSize = 12;
    p2(ii).MarkerSize = 12;
    p3(ii).MarkerSize = 9;
end
yd = ylabel('Distance (km)');
%xd = xlabel('Days after simulation start');
xd = xlabel('Time (day)');

%Making everything pretty all over again!
S1.FontSize = FS_ax;
%S2.FontSize = FS_ax;
S3.FontSize = FS_ax;

S1.XLim = [25 120];
%S2.XLim = [25 120];
S3.XLim = [25 120];

S3.YLim = [0 15];
S3.YTick = [0:3:15];

S1.XTick = [30:15:120];
%S2.XTick = [30:15:120];
S3.XTick = [30:15:120];

S1.XTickLabel = '';
%S2.XTickLabel = '';
S3.XTickLabel = '';

S1.Position(2) = 0.68;
S3.Position(2) = 0.38;
S4.Position(2) = 0.09;

sub_h = 0.25;
S1.Position(4) = sub_h;
S3.Position(4) = sub_h;
S4.Position(4) = sub_h + 0.01;

%%%%%%%%%%%%%%%%%%%%%%%%%
%NEW FIGURE

f2 = figure;
f2.Units = 'normalized';
f2.Position = [0,0.3,0.5,0.5];
if input_style == 1
    switch_day = 76;
    x_hyst = 100;
elseif input_style == 2
    switch_day = 105;
    x_hyst = 96;
elseif input_style == 3
    switch_day = 119;
    x_hyst = 92;
end

p5 = plot(Qc_short(1:switch_day*24,x_hyst),t_stage_short(1:switch_day*24,x_hyst),'LineWidth',LW1);
hold on
p6 = plot(Qc_short(switch_day*24:s_end,x_hyst),t_stage_short(switch_day*24:s_end,x_hyst),'LineWidth',LW1);
p7 = plot(Qc_short(65*24:67*24,x_hyst),t_stage_short(65*24:67*24,x_hyst),'LineWidth',LW1);

ax2 = gca;

xlabel('$Q_{\rm ch} \, ({\rm m^3 \, s^{-1}})$','interpreter','latex')
ylabel('$\tau^* / \tau^*_{\rm c}$','interpreter','latex')

grid on

ax2.FontSize = FS_ax;
