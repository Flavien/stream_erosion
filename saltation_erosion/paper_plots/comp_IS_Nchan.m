%File to plot a the N-channel carved from different simulations

load trans_IS_final
%load trans_IS_3floods.mat
%Runs that I need:
%OPT = 1
%W250 = 2
%IS1300 = 4
%IS_REF = 7
%IS_RB = 8
%IS_SSP/4 = 12
runs_needed = [1 2 4 7 8 12];

%plotting stuffs...
x_plot = [1:min(size(trans_IS(1).q_tc_vol))]-0.5;
x_plot = x_plot .* 500./1000;

FSlab = 26;
FSax  = 26;
FScont = 22;
FS_txt = 28;

text_frac = 0.9;

LW1 = 1.5;
LW2 = 2.5;

fig1 =1;
fx = 1;
fy = 0.6;
sx1 = 2;
sy1 = 3;

L10 = 0;
L50 = 1;

x_start = 20;

cont_nb = 10;

run_nb = length(runs_needed);

%%%%%%%%%%%
f1 = figure(fig1);
f1.Units = 'normalized';
f1.Position = [0,0.3,fx,fy];

texty = 1.9;
textx1 = 11;
textx2 = 20;

kk = 1;
S1 = subplot(sx1,sy1,1);
    
    %k = runs_needed(kk);
    k = runs_needed(1);
    vectSA1 = [-200:10:0];
    vectSA2 = [-200:20:0];
    [CA,hA] = contour(x_plot(x_start:end),trans_IS(k).width,trans_IS(k).valley_topo(:,x_start:end),vectSA1);
    hold on
    [CA2,hA2] = contour(x_plot(x_start:end),trans_IS(k).width,trans_IS(k).valley_topo(:,x_start:end),vectSA2);
    hA.LineColor = [0.7 0.7 0.7];
    hA2.LineColor = 'k';

    TA1 = text(textx1,texty,'A');
    TA2 = text(textx2,texty,'TR\_IS\_SSPOPT');
    TA1.FontSize = FSax;
    TA2.FontSize = FSax;

    %t1 = clabel(CA2,'manual');


    %xlabel('Distance from divide (km)')
    %ylabel('N-Channel radius (m)')
    set(gca,'FontSize',FSax)

kk = 2;
S2 = subplot(sx1,sy1,2);
    
    %k = runs_needed(kk);
    k = runs_needed(2);
    vectSB1 = [-80:10:0];
    vectSB2 = [-80:20:0];
    [CB,hB] = contour(x_plot(x_start:end),trans_IS(k).width,trans_IS(k).valley_topo(:,x_start:end),vectSB1);
    hold on
    [CB2,hB2] = contour(x_plot(x_start:end),trans_IS(k).width,trans_IS(k).valley_topo(:,x_start:end),vectSB2);
    hB.LineColor = [0.7 0.7 0.7];
    hB2.LineColor = 'k';

    TB1 = text(textx1,texty,'B');
    TB2 = text(textx2,texty,'TR\_IS\_W250');
    TB1.FontSize = FSax;
    TB2.FontSize = FSax;


    %xlabel('Distance from divide (km)')
    %ylabel('N-Channel radius (m)')
    set(gca,'FontSize',FSax)

kk = 3;
S3 = subplot(sx1,sy1,3);
    
    %k = runs_needed(kk);
    k = runs_needed(3);
    vectSC1 = [-120:10:0];
    vectSC2 = [-120:20:0];
    [CC,hC] = contour(x_plot(x_start:end),trans_IS(k).width,trans_IS(k).valley_topo(:,x_start:end),vectSC1);
    hold on
    [CC2,hC2] = contour(x_plot(x_start:end),trans_IS(k).width,trans_IS(k).valley_topo(:,x_start:end),vectSC2);
    hC.LineColor = [0.7 0.7 0.7];
    hC2.LineColor = 'k';

    TC1 = text(textx1,texty,'C');
    TC2 = text(textx2,texty,'TR\_IS1300');
    TC1.FontSize = FSax;
    TC2.FontSize = FSax;


    %xlabel('Distance from divide (km)')
    %ylabel('N-Channel radius (m)')
    set(gca,'FontSize',FSax)

kk = 4;
S4 = subplot(sx1,sy1,4);
    
    %k = runs_needed(kk);
    k = runs_needed(4);
    vectSD1 = [-110:10:0];
    vectSD2 = [-100:20:0];
    [CD,hD] = contour(x_plot(x_start:end),trans_IS(k).width,trans_IS(k).valley_topo(:,x_start:end),vectSD1);
    hold on
    [CD2,hD2] = contour(x_plot(x_start:end),trans_IS(k).width,trans_IS(k).valley_topo(:,x_start:end),vectSD2);
    hD.LineColor = [0.7 0.7 0.7];
    hD2.LineColor = 'k';

    TD1 = text(textx1,texty,'D');
    TD2 = text(textx2,texty,'TR\_IS\_REF');
    TD1.FontSize = FSax;
    TD2.FontSize = FSax;


    xlabel('Distance from divide (km)')
    ylabel('N-Channel radius (m)')
    set(gca,'FontSize',FSax)

kk = 5;
S5 = subplot(sx1,sy1,5);
    
    %k = runs_needed(kk);
    k = runs_needed(5);
    vectSE1 = [-120:10:0];
    vectSE2 = [-120:20:0];
    [CE,hE] = contour(x_plot(x_start:end),trans_IS(k).width,trans_IS(k).valley_topo(:,x_start:end),vectSE1);
    hold on
    [CE2,hE2] = contour(x_plot(x_start:end),trans_IS(k).width,trans_IS(k).valley_topo(:,x_start:end),vectSE2);
    hE.LineColor = [0.7 0.7 0.7];
    hE2.LineColor = 'k';

    TE1 = text(textx1,texty,'E');
    TE2 = text(textx2,texty,'TR\_IS\_RB');
    TE1.FontSize = FSax;
    TE2.FontSize = FSax;


    xlabel('Distance from divide (km)')
    %ylabel('N-Channel radius (m)')
    set(gca,'FontSize',FSax)

kk = 6;
S6 = subplot(sx1,sy1,6);
    
    %k = runs_needed(kk);
    k = runs_needed(6);
    vectSF1 = [-50:10:0];
    vectSF2 = [-40:20:0];
    [CF,hF] = contour(x_plot(x_start:end),trans_IS(k).width,trans_IS(k).valley_topo(:,x_start:end),vectSF1);
    hold on
    [CF2,hF2] = contour(x_plot(x_start:end),trans_IS(k).width,trans_IS(k).valley_topo(:,x_start:end),vectSF2);
    hF.LineColor = [0.7 0.7 0.7];
    hF2.LineColor = 'k';

    TF1 = text(textx1,texty,'F');
    TF2 = text(textx2,texty,'TR\_IS\_SSP/4');
    TF1.FontSize = FSax;
    TF2.FontSize = FSax;


    xlabel('Distance from divide (km)')
    %ylabel('N-Channel radius (m)')
    set(gca,'FontSize',FSax)


%Making everything pretty again...
Nchan_Ylim = [-2.3 2.3];
S1.YLim = Nchan_Ylim;
S2.YLim = Nchan_Ylim;
S3.YLim = Nchan_Ylim;
S4.YLim = Nchan_Ylim;
S5.YLim = Nchan_Ylim;
S6.YLim = Nchan_Ylim;

Nchan_Ytick = [-2:1:2];
S1.YTick = Nchan_Ytick;
S2.YTick = Nchan_Ytick;
S3.YTick = Nchan_Ytick;
S4.YTick = Nchan_Ytick;
S5.YTick = Nchan_Ytick;
S6.YTick = Nchan_Ytick;

Nchan_Xlim = [10 50];
S1.XLim = Nchan_Xlim;
S2.XLim = Nchan_Xlim;
S3.XLim = Nchan_Xlim;
S4.XLim = Nchan_Xlim;
S5.XLim = Nchan_Xlim;
S6.XLim = Nchan_Xlim;

Nchan_Xtick = [10:5:50];
S1.XTick = Nchan_Xtick;
S2.XTick = Nchan_Xtick;
S3.XTick = Nchan_Xtick;
S4.XTick = Nchan_Xtick;
S5.XTick = Nchan_Xtick;
S6.XTick = Nchan_Xtick;

S1.XTickLabel = '';
S2.XTickLabel = '';
S3.XTickLabel = '';

S2.YTickLabel = '';
S3.YTickLabel = '';
S5.YTickLabel = '';
S6.YTickLabel = '';

sub_w = 0.29;
S1.Position(3) = sub_w;
S2.Position(3) = sub_w;
S3.Position(3) = sub_w;
S4.Position(3) = sub_w;
S5.Position(3) = sub_w;
S6.Position(3) = sub_w;

sub_h = 0.42;
S1.Position(4) = sub_h;
S2.Position(4) = sub_h;
S3.Position(4) = sub_h;
S4.Position(4) = sub_h;
S5.Position(4) = sub_h;
S6.Position(4) = sub_h;

S1.Position(1) = 0.055;
S4.Position(1) = 0.055;
S2.Position(1) = 0.370;
S5.Position(1) = 0.370;
S3.Position(1) = 0.690;
S6.Position(1) = 0.690;

S1.Position(2) = 0.56; 
S2.Position(2) = 0.56; 
S3.Position(2) = 0.56; 

%And now trying to put some label...

subplot(S1)
t1 = clabel(CA2,'manual');
for j=2:2:length(t1)
    t1(j).FontSize = FScont;
    t1(j).LineWidth = LW1;
end

subplot(S2)
t2 = clabel(CB2,'manual');
for j=2:2:length(t2)
    t2(j).FontSize = FScont;
    t2(j).LineWidth = LW1;
end

subplot(S3)
t3 = clabel(CC2,'manual');
for j=2:2:length(t3)
    t3(j).FontSize = FScont;
    t3(j).LineWidth = LW1;
end

subplot(S4)
t4 = clabel(CD2,'manual');
for j=2:2:length(t4)
    t4(j).FontSize = FScont;
    t4(j).LineWidth = LW1;
end

subplot(S5)
t5 = clabel(CE2,'manual');
for j=2:2:length(t5)
    t5(j).FontSize = FScont;
    t5(j).LineWidth = LW1;
end

subplot(S6)
t6 = clabel(CF2,'manual');
for j=2:2:length(t6)
    t6(j).FontSize = FScont;
    t6(j).LineWidth = LW1;
end

