%Script to try to compute the erosion from the two models: saltation-erosion and total load
%This script also integrate the formulation of sediment transport if enabled.
%This part is to try to compute transport / erosion in the network of cavities
%
%

clc
clear all

disp('Make sure you have a matlabpool running before doing anything!')
disp('Try something like matlabpool(4) if on the mac, more on desktop machine')

%disp('not parallel for debugging')
%pause
matlabpool

%transport_sed = input('if sed transport: 1 / 0 otherwise: ');

transport_sed = 0;
%disp('Sediment is being transported, hopefully!')
%shear_avg = input('avg shear stress = 1; partition on bed = 0: ');
shear_avg = 0;

%Call file to load the constants
[constants,tau_c,D,W,q_s,slope,Qw,Hw,q_tot_init,theta] = init_season_run;

mac_path = '../hydro/runs/';
ubuntu_path = '../../subglacial_water_flow/runs/';

%LoaLd file from previous run
%file_name = 'TR_SD3_01.mat';
file_name = 'TR_IS_SD3_W250.mat'

file_to_load = strcat(ubuntu_path,file_name);
%file_to_load = strcat(mac_path,file_name);

%t_slice = [55 0];
%t_slice = [350 0];
%t_slice = [70 0];

%For an actual seasonal simulation
t_slice = [99999 0];

replicate = 1;

%% NOTE h substituted by R for cavities
[q_w RH R p_s tau_b_sub tau_tot u_shear_sub u_mean_sub slope_eq dt dx] = extract_cav_run(file_to_load,constants,t_slice);

if replicate > 1
    q_w = repmat(q_w,replicate,1);
    RH = repmat(RH,replicate,1);
    R = repmat(R,replicate,1);
    tau_b_sub = repmat(tau_b_sub,replicate,1);
    tau_tot = repmat(tau_tot,replicate,1);
    u_shear_sub = repmat(u_shear_sub,replicate,1);
    u_mean_sub = repmat(u_mean_sub,replicate,1);
    slope_eq = repmat(slope_eq,replicate,1);
end



%Make an array of q_s to reproduce figure 10 in S&D 2004
Qs_array = [0:0.5:50];
q_s_array = Qs_array./W;

%disp('Shields stress computed with u_shear, see compute_Shields')
%disp('Shields stress computed with tau_b, see compute_Shields')

%YOU ALSO NEED TO ACCOUNT FOR THE WIDTH OF THE SYSTEM THAT IS CHANGING SOMEHOW

testA = 0;
testB = 0;
testC = 0;
std_source = 1;
tot_load_width_test = 0;

if testA == 1
    q_tot_temp = q_w.*0;
    q_tot_loc = q_w.*0 + q_tot_init;
    eta_ar     = q_w .* 0;
    eta_ar(1,1:20) = eta_ar(1,1:20)+0.1*20;
    %eta_ar(1,end-2:end) = eta_ar(1,end-2:end)+0.1;
    %eta_ar(1,end) = eta_ar(1,end)+0.2;
    %NOTE to check sediment conservation:
    %plot(sum((eta_int_ar.*2.*repmat(R_interp,1120,1))'))
    %with 1120 being the number of timesteps actually needed in the simulation
    disp('Mass conservation test A')
elseif testB == 1
    eta_ar     = q_w .* 0;
    q_tot_temp = q_w.*0;
    q_tot_loc = q_w.*0;
    q_tot_loc(:,1) = q_tot_loc(:,1) + q_tot_init;
    disp('Mass conservation test B')
elseif testC == 1
    eta_ar     = q_w .* 0;
    q_tot_temp = q_w.*0;
    q_tot_loc = q_w.*0 + q_tot_init/10;
    disp('Mass conservation test C')
elseif std_source == 1
    eta_ar     = q_w .* 0;
    %q_tot_temp = q_w.*0 + q_tot_init*4;
    q_tot_temp = q_w.*0;
    q_tot_loc = q_w.*0 + q_tot_init;
    disp('regular source term for sediment input')
elseif tot_load_width_test == 1
    eta_ar     = q_w .* 0;
    %q_tot_temp = q_w.*0 + q_tot_init;
    q_tot_temp = q_w.*0;
    q_tot_loc(:,6) = q_tot_loc(:,6) + q_tot_init./4;
    disp('tot load and width test')
end


%Little bit of cheating to smooth out u_shear and tau_b so that the sediment transport is smoother
u_sh_smooth = u_shear_sub;
u_sh_smooth(:,2:end-1) = (u_shear_sub(:,1:end-2) + 2.*u_shear_sub(:,2:end-1) + u_shear_sub(:,3:end)) ./ 4;
u_sh_smooth(u_shear_sub == 0) = 0;
u_shear_sub = u_sh_smooth;

u_sh_smooth = u_mean_sub;
u_sh_smooth(:,2:end-1) = (u_mean_sub(:,1:end-2) + 2.*u_mean_sub(:,2:end-1) + u_mean_sub(:,3:end)) ./ 4;
u_mean_smooth(u_mean_sub == 0) = 0;
u_mean_sub = u_sh_smooth;

tau_b_sub_smooth = tau_b_sub;
tau_b_sub_smooth(:,2:end-1) = (tau_b_sub(:,1:end-2) + 2.*tau_b_sub(:,2:end-1) + tau_b_sub(:,3:end)) ./ 4;
tau_b_sub_smooth(tau_b_sub == 0) = 0;
tau_b_sub = tau_b_sub_smooth;

for i=1:length(q_w)
    disp(i)
        detadt = eta_ar(i,2:end) .* 0 + 100;
        %k = 1;
        %for k=1:20
        %while min(abs(detadt > D./10))
         %   k = k + 1;
            [q_sup] = availability_update(eta_ar(i,:),constants,dt,dx);
            q_tot_temp(i,:) = q_tot_loc(i,:).*dt + q_sup;

            %for j=1:min(size(q_w))
            parfor j=1:min(size(q_w))

            %Set up the good values for this round:
            tau_b = tau_b_sub(i,j);
            u_shear = u_shear_sub(i,j);
            u_mean = u_mean_sub(i,j);
            R_temp = R(i,j);
            q_tot = q_tot_temp(i,j);



            
            %for k=1:length(q_s_array)
            k=1;

                %q_s = q_s_array(k);

                %Call function to compute total load erosion 
                [e_salt,E,e_tot,e_tot_nd,Vi,Ir,Fe,q_tc,tau,Dmax,tot_load_stuff,salt_stuff] =...
                                 total_eros_R_channel(constants,tau_c,D,q_tot.*constants.rho_s,...
                                                        tau_b,u_shear,u_mean,R_temp,q_tot,theta,dt);

                %Storing everything in arrays
                e_salt_ar(i,j)    = e_salt;
                E_ar(i,j)         = E;
                Vi_ar(i,j)        = Vi;
                Ir_ar(i,j)        = Ir;
                Fe_ar(i,j)        = Fe;
                q_tc_ar(i,j)      = q_tc;
                q_b_ar(i,j)       = tot_load_stuff.q_b;
                q_sus_ar(i,j)     = tot_load_stuff.q_sus;
                w_i_eff_ar(i,j)   = tot_load_stuff.w_i_eff;
                cb_ar(i,j)        = tot_load_stuff.c_b;
                Fe_tot_ar(i,j)    = tot_load_stuff.Fe_tot;
                tau_ar(i,j)       = tau;
                tau_b_ar(i,j)     = tau_b;
                u_shear_ar(i,j)   = u_shear;
                w_f_ar(i,j)       = salt_stuff.w_f;
                w_si_ar(i,j)      = salt_stuff.w_si;
                Ls_ar(i,j)        = salt_stuff.Ls;
                Hs_ar(i,j)        = salt_stuff.Hs;
                Us_ar(i,j)        = salt_stuff.Us;
                Hf_ar(i,j)        = tot_load_stuff.Hf;
                Dmax_ar(i,j)      = Dmax;
                e_tot_ar(i,j)     = e_tot;
                e_tot_nd_ar(i,j)  = e_tot_nd;
                chi_ar(i,j)       = tot_load_stuff.chi;
            %end
            end

            if transport_sed == 1

                if i < length(q_w)
                    
                    %Making the interpolation to solve the sediment transport on an adaptative grid
                        %dt_temp = 10;
                        dt_temp = 5;

                    if i > 1
                        eta_interp = eta_int_ar(i-1,:);
                    end
                    grid_interp;
                    %keyboard
                    if i == 1
                        eta_int_ar = zeros((max(t_slice)+1)*replicate,sum(cpc_round));
                    end

                    for jj = 1:dt/dt_temp
                        %dt_temp = 600;
                        %dt_temp = dt;
                        
                        %eta_temp = eta_ar(i,:);
                        %NOTE: q_sup is a function of the timestep. eta * (1 - lambda) ./dt
                        %SO if the time step changes, q_sup will have to be adjusted consequently

                        if jj > 1
                            eta_interp = eta_int_ar(i,:);
                            %q_tot_interp = eta_interp.*(1-constants.lambda_s)./dt_temp;
                            q_tot_interp = q_sup_loc_int.*dt + eta_interp.*(1-constants.lambda_s);
                        end

                    [detadt,eta_interp,q_adv_w_ar(i,:),detadt_w q_in_ar(i,:) q_out_ar(i,:)] = ...
                            Exners_eq(constants,q_tc_interp,q_tot_interp,eta_interp,detadt,dx_interp,dt_temp,R_interp,Us_ar(i,:),i);


                        eta_int_ar(i,:) = eta_interp;
                    %keyboard

                    end

                    %Interpolating back
                    %eta_interp_back = interp1(x_map_avg,eta_interp,x_reg,'spline');
                    eta_interp_back = interp1(x_map_avg,eta_interp,x_reg,'pchip');
                    eta_ar(i+1,:) = eta_interp_back;
                    detadt_ar(i,:) = detadt;
                end
            end

        %end
end
tau_ratio = tau_ar ./ tau_c;
excess_tau = tau_ratio - 1;

save
