%Little function to extract the width a valley in a format that can be plotted along the 
%longitudinal profile
%
%

function [width_prof] =  extract_width(valley_topo,width)

valley_size = size(valley_topo);

width_nb1 = zeros(1,valley_size(2));
width_nb2 = zeros(1,valley_size(2));

%keyboard

for i=1:valley_size(2)-1
    width_nb1(i) = max(find(valley_topo(1:100,i) == 0))./100;
    width_nb2(i) = min(find(valley_topo(100:end,i) == 0))./100;
end

%width_nb1(end) = 1./100;
%width_nb2(end) = 201./100;

width_nb = ((1-width_nb1) + width_nb2);

width_prof = width_nb .* max(width);
width_prof(end) = max(width.*2);
%keyboard
