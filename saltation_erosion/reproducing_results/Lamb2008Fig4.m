%Script in which I compute the test scenario in both the Sklar and Dietrich 2004 paper
%and in Lamb et al 2008.
%
%Both the saltation erosion model alone and the total load erosion are computed.
%This script can also be used to reproduce some of the figures in these papers.

%profile on
plotting = 1;
computation = 1;

%Make an array of q_s to reproduce figure 10 in S&D 2004
if computation == 1

    clearvars -except plotting computation
    clc

    %Call file to load the constants
    [constants tau_c D W q_s slope Qw Hw q_tot theta] = init_tot_load;

    array_length = 500;
    slope_array_1 = logspace(-5,0,array_length); % for D = 0.001
    slope_array_60 = logspace(-3,1.5,array_length); % for D = 0.06
    Hw_array_60 = logspace(-2,4,array_length); % for D = 0.06
    Hw_array_1 = logspace(-2,2,array_length); % for D = 0.001
    D_array = logspace(-4,-1,array_length); % for D = 0.001
    qs_array_1 = logspace(-4.5,-2.0,array_length); % for D = 0.001
    qs_array_60 = logspace(-4.5,-1.5,array_length); % for D = 0.060
    D_choice = [0.001 0.06];

    %for i=1:length(slope_array)
%In order to reproduce Lamb 2008 Fig. 4-8 I need to play with the transport stage with either a constant slope
%or with a constants Hw.
%The range of tau_b needed is basically up to 3e5 Pa
%given that tau_b = constants.rho_w * constants.g * Hw * slope; and that ref is H = 0.95 and slope = 0.0053
for i = 1:7;
        %for j=1:length(slope_array)
        for j=1:array_length
            
            if i == 1
                D = D_choice(1);
                slope = slope_array_1(j);
                theta = slope;
            elseif i == 2
                if j ==1
                    [constants tau_c D W q_s slope Qw Hw q_tot theta] = init_tot_load;
                end
                D = D_choice(2);
                slope = slope_array_60(j);
                theta = slope;
            elseif i == 3 
                if j ==1
                    [constants tau_c D W q_s slope Qw Hw q_tot theta] = init_tot_load;
                end
                D = D_choice(1);
                Hw = Hw_array_1(j);
            elseif i == 4
                if j ==1
                    [constants tau_c D W q_s slope Qw Hw q_tot theta] = init_tot_load;
                end
                D = D_choice(2);
                Hw = Hw_array_60(j);
            elseif i == 5
                if j ==1
                    [constants tau_c D W q_s slope Qw Hw q_tot theta] = init_tot_load;
                end
                D = D_choice(1);
                q_tot = qs_array_1(j);
                q_s = q_tot .* constants.rho_s;
            elseif i == 6
                if j ==1
                    [constants tau_c D W q_s slope Qw Hw q_tot theta] = init_tot_load;
                end
                D = D_choice(2);
                q_tot = qs_array_60(j);
                q_s = q_tot .* constants.rho_s;
            elseif i == 7
                if j ==1
                    [constants tau_c D W q_s slope Qw Hw q_tot theta] = init_tot_load;
                end
                D = D_array(j);
            end
                    

            [e_salt E e_tot_load Vi Ir Fe q_tc tau tau_b u_shear u_mean tot_load_stuff salt_stuff] =...
                                                    total_eros(constants,tau_c,D,W,q_s,slope,Qw,Hw,q_tot,theta);

            %Storing everything in arrays
            storage = 1;
            if storage == 1
                e_salt_ar(i,j)    = e_salt;
                e_tot_ar(i,j)     = e_tot_load;
                E_ar(i,j)         = E;
                Vi_ar(i,j)        = Vi;
                Ir_ar(i,j)        = Ir;
                Fe_ar(i,j)        = Fe;
                q_tc_ar(i,j)      = q_tc;
                tau_ar(i,j)       = tau;
                tau_b_ar(i,j)     = tau_b;
                u_shear_ar(i,j)   = u_shear;
                u_mean_ar(i,j)    = u_mean;
                cb_ar(i,j)        = tot_load_stuff.c_b;
                chi_ar(i,j)       = tot_load_stuff.chi;
                Hs_ar(i,j)        = salt_stuff.Hs;
                Us_ar(i,j)        = salt_stuff.Us;
                w_i_eff(i,j)      = tot_load_stuff.w_i_eff;
                Hf_ar(i,j)        = tot_load_stuff.Hf;
                w_f_ar(i,j)       = salt_stuff.w_f;
                w_s_ar(i,j)       = tot_load_stuff.w_s;
                w_st_ar(i,j)       = tot_load_stuff.w_st;
                w_si_ar(i,j)      = salt_stuff.w_si;
                dz_ar(i,j)      = salt_stuff.dz;
                P_rouse_ar(i,j)      = salt_stuff.P_rouse;
                chi_int_ar(i,j)      = salt_stuff.chi_int;
                %zeta_b_ar(i,j)      = salt_stuff.zeta_b;
            end

        end
        %parfor end
    end % for i loop end
end %computation end

if plotting == 1
    LW = 1.5;
    LW_t = 3.5;
    FSax = 18;
    FSleg = 16;
    mm_fact = 1e3;

    t_stage = tau_ar./tau_c;

    %%%%%%%%%%%%%%%%%%%%%%%%%%% FIGURE %%%%%%%%%%%%%%%%%%%%%%%%%%%
    f1 = figure(1);
    k = 2;
    semilogx(t_stage(k,:),w_s_ar(k,:)./w_st_ar(k,:),'k','LineWidth',LW)
    hold on
    semilogx(t_stage(k,:),w_si_ar(k,:)./w_st_ar(k,:),'b.-','LineWidth',LW_t)
    semilogx(t_stage(k,:),(w_s_ar(k,:) + u_shear_ar(k,:))./w_st_ar(k,:),'k--','LineWidth',LW)
    semilogx(t_stage(k,:),(w_s_ar(k,:) - u_shear_ar(k,:))./w_st_ar(k,:),'k--','LineWidth',LW)
    semilogx(t_stage(k,:),w_i_eff(k,:)./w_st_ar(k,:),'k','LineWidth',LW_t)
    axis([1 1e3 -6 6])
    l1 = legend('w_s','S and D 2004','w_s+u_*','w_s-u_*','w_{i,eff}');
    set(l1,'Location','SouthWest','FontSize',FSleg)
    xlabel('Transport stage (\tau^* / \tau^*_c)','FontSize',FSax)
    ylabel('Velocity / w_{st}','FontSize',FSax)
    set(gca,'FontSize',FSax)
    grid on

    %%%%%%%%%%%%%%%%%%%%%%%%%%% FIGURE %%%%%%%%%%%%%%%%%%%%%%%%%%%
    f2 = figure(2);
    k = 3;
    loglog(t_stage(k,:),e_tot_ar(k,:).*mm_fact,'m','LineWidth',LW_t)
    hold on
    k = 1;
    loglog(t_stage(k,:),e_tot_ar(k,:).*mm_fact,'m--','LineWidth',LW_t)
    k = 4;
    loglog(t_stage(k,:),e_tot_ar(k,:).*mm_fact,'k','LineWidth',LW)
    k = 2;
    loglog(t_stage(k,:),e_tot_ar(k,:).*mm_fact,'k--','LineWidth',LW)

    loglog(t_stage(k,:),E_ar(k,:).*mm_fact,'b.-','LineWidth',LW)

    axis([1 1e4 1e-1 1e2])
    l2 = legend('Tot load (D = 1 mm; S = 0.0053)','Tot load (D = 1 mm; H = 0.95 m)','Tot load (D = 60 mm; S = 0.0053)','Tot load (D = 60 mm; H = 0.95 m)','Salt abrasion (D = 60 mm; H = 0.95 m)');
    set(l2,'Location','SouthWest','FontSize',FSleg)
    xlabel('Transport stage (\tau^* / \tau^*_c)','FontSize',FSax)
    ylabel('Erosion rate (mm/yr)','FontSize',FSax)
    set(gca,'FontSize',FSax)
    grid on

    %%%%%%%%%%%%%%%%%%%%%%%%%%% FIGURE %%%%%%%%%%%%%%%%%%%%%%%%%%%
    f3 = figure(3);
    k = 3;
    loglog(t_stage(k,:),cb_ar(k,:),'m','LineWidth',LW_t)
    hold on
    k = 1;
    loglog(t_stage(k,:),cb_ar(k,:),'m--','LineWidth',LW_t)
    k = 4;
    loglog(t_stage(k,:),cb_ar(k,:),'k','LineWidth',LW)
    k = 2;
    loglog(t_stage(k,:),cb_ar(k,:),'k--','LineWidth',LW)

    axis([1 1e4 1e-10 1e2])
    l3 = legend('Tot load (D = 1 mm; S = 0.0053)','Tot load (D = 1 mm; H = 0.95 m)','Tot load (D = 60 mm; S = 0.0053)','Tot load (D = 60 mm; H = 0.95 m)');
    set(l3,'Location','SouthWest','FontSize',FSleg)
    xlabel('Near bed sed conc (c_{b})','FontSize',FSax)
    ylabel('Erosion rate (mm/yr)','FontSize',FSax)
    set(gca,'FontSize',FSax)
    set(gca,'Ytick',[1e-10 1e-8 1e-6 1e-4 1e-2 1e0 1e2])
    grid on

    %%%%%%%%%%%%%%%%%%%%%%%%%%% FIGURE %%%%%%%%%%%%%%%%%%%%%%%%%%%
    f4 = figure(4);
    k = 5;
    plot(qs_array_1.*constants.rho_s./q_tc_ar(k,:),e_tot_ar(k,:).*mm_fact,'k','LineWidth',LW_t)
    hold on
    k = 6;
    plot(qs_array_60.*constants.rho_s./q_tc_ar(k,:),e_tot_ar(k,:).*mm_fact,'k','LineWidth',LW)
    plot(qs_array_60.*constants.rho_s./q_tc_ar(k,:),E_ar(k,:).*mm_fact,'b.-','LineWidth',LW)
    
    axis([0 2 0 55])
    l4 = legend('Tot load (D = 1 mm)','Tot load (D = 60 mm)','Salt abrasion (D = 60 mm)');
    set(l4,'Location','NorthEast','FontSize',FSleg)
    xlabel('Relative sediment supply (q_{s}/q_{tc})','FontSize',FSax)
    ylabel('Erosion rate (mm/yr)','FontSize',FSax)
    set(gca,'FontSize',FSax)
    grid on

    %%%%%%%%%%%%%%%%%%%%%%%%%%% FIGURE %%%%%%%%%%%%%%%%%%%%%%%%%%%
    f5 = figure(5);
    k = 7;
    loglog(D_array.*mm_fact,e_tot_ar(k,:).*mm_fact,'k','LineWidth',LW_t)
    hold on
    loglog(D_array.*mm_fact,E_ar(k,:).*mm_fact,'b.-','LineWidth',LW)
    
    axis([4e-1 1e2 1e-1 1e2])
    l5 = legend('Tot load (D = 60 mm)','Salt abrasion (D = 60 mm)');
    set(l5,'Location','SouthEast','FontSize',FSleg)
    xlabel('Grain size (mm)','FontSize',FSax)
    ylabel('Erosion rate (mm/yr)','FontSize',FSax)
    set(gca,'FontSize',FSax)
    grid on

end
