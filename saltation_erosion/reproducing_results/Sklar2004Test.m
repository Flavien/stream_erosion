%function in which I call whatever I want to try to reproduce either
%Sklar and Dietrich 2004 or Lamb et al 2008 figures
%
%

%profile on
just_plot = 1;

%Make an array of q_s to reproduce figure 10 in S&D 2004
if just_plot == 0

clear all
clc
    %Call file to load the constants
    [constants tau_c D W q_s slope Qw Hw] = init_test;
    Qs_array = [0:1:3000];
    q_s_array = Qs_array./W;

    slope_array = [0.0053 0.015 0.03 0.06 0.085 0.12];
    Hw_array = [1.0737 0.7765 0.6268 0.5066 0.4554 0.4098];

    for i=1:length(slope_array)

        slope = slope_array(i);
        Hw      = Hw_array(i);

        parfor j=1:length(q_s_array)

            q_s = q_s_array(j);
            
            %Call function to compute saltation erosion 
            [e_salt E Vi Ir Fe q_tc tau tau_b u_shear w_f w_si] = salt_eros(constants,tau_c,D,W,q_s,slope,Qw,Hw);

            %Storing everything in arrays
            e_salt_ar(i,j)    = e_salt;
            E_ar(i,j)         = E;
            Vi_ar(i,j)        = Vi;
            Ir_ar(i,j)        = Ir;
            Fe_ar(i,j)        = Fe;
            q_tc_ar(i,j)      = q_tc;
            tau_ar(i,j)       = tau;
            tau_b_ar(i,j)     = tau_b;
            u_shear_ar(i,j)   = u_shear;
            w_f_ar(i,j)       = w_f;
            w_si_ar(i,j)      = w_si;

        end
    end
    %profile viewer
    %profile off
end
LW = 2;
FSax = 18;
FSleg = 20;
mm_fact = 1e-3;

figure(1)
plot(Qs_array,E_ar(1,:)./mm_fact,'Color',rgb('Black'),'Linewidth',LW,'LineStyle','-')
hold on
plot(Qs_array,E_ar(2,:)./mm_fact,'Color',rgb('SlateGray'),'Linewidth',LW,'LineStyle','-')
plot(Qs_array,E_ar(3,:)./mm_fact,'Color',rgb('DarkGray'),'Linewidth',LW,'LineStyle','-')
plot(Qs_array,E_ar(4,:)./mm_fact,'Color',rgb('Black'),'Linewidth',LW,'LineStyle','--')
plot(Qs_array,E_ar(5,:)./mm_fact,'Color',rgb('SlateGray'),'Linewidth',LW,'LineStyle','--')
plot(Qs_array,E_ar(6,:)./mm_fact,'Color',rgb('DarkGray'),'Linewidth',LW,'LineStyle','--')
l1 = legend('0.0053','0.015','0.03','0.06','0.085','0.12');
v = get(l1,'title');
set(l1,'Location','SouthEast','FontSize',FSleg)
set(v,'string','Channel slope','FontSize',FSleg);
xlabel('Sediment supply (kg/s)','FontSize',FSax)
ylabel('Erosion rate (mm/a)','FontSize',FSax)
set(gca,'FontSize',FSax)
grid on
