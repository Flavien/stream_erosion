%Script in which I compute the test scenario in both the Sklar and Dietrich 2004 paper
%and in Lamb et al 2008.
%
%Both the saltation erosion model alone and the total load erosion are computed.
%This script can also be used to reproduce some of the figures in these papers.

%profile on
plotting = 0;
computation = 1;

%Make an array of q_s to reproduce figure 10 in S&D 2004
if computation == 1

    clearvars -except plotting computation
    clc

    %Call file to load the constants
    [constants tau_c D W q_s slope Qw Hw q_tot theta] = init_tot_load;
    Qs_array = [0:1:3000];
    q_s_array = Qs_array./W;

    slope_array = [0.0053 0.015 0.03 0.06 0.085 0.12];
    Hw_array = [1.0737 0.7765 0.6268 0.5066 0.4554 0.4098];

    %for i=1:length(slope_array)

        %slope = slope_array(i);
        %Hw      = Hw_array(i);

        %parfor j=1:length(q_s_array)

            %q_s = q_s_array(j);
            
            %Call function to compute saltation erosion 
            %[e_salt E Vi Ir Fe q_tc tau tau_b u_shear w_f w_si] = salt_eros(constants,tau_c,D,W,q_s,slope,Qw,Hw);
            [e_salt E e_tot_load Vi Ir Fe q_tc tau tau_b u_shear u_mean tot_load_stuff salt_stuff] =...
                                                    total_eros(constants,tau_c,D,W,q_s,slope,Qw,Hw,q_tot,theta);

            %Storing everything in arrays
            storage = 0;
            if storage == 1
                e_salt_ar(i,j)    = e_salt;
                E_ar(i,j)         = E;
                Vi_ar(i,j)        = Vi;
                Ir_ar(i,j)        = Ir;
                Fe_ar(i,j)        = Fe;
                q_tc_ar(i,j)      = q_tc;
                tau_ar(i,j)       = tau;
                tau_b_ar(i,j)     = tau_b;
                u_shear_ar(i,j)   = u_shear;
                w_f_ar(i,j)       = w_f;
                w_si_ar(i,j)      = w_si;
            end

        %end
        %parfor end
    %end
    %running through slope end
    %profile viewer
    %profile off
end

if plotting == 1
    LW = 2;
    FSax = 18;
    FSleg = 20;
    mm_fact = 1e-3;
    px = 0.4;
    py = 0.3;
    sx = 0.3;
    sy = 0.7;

    %f1 = figure('NumberTitle','off','Name','Concentration plots');
    f1 = figure(1);
    set(f1,'Units','Normalized')
    set(f1,'Position',[px py sx sy])

    if D == 0.06
        subplot(2,1,1)
    elseif D == 0.001;
        subplot(2,1,2)
    end
    z_end = length(tot_load_stuff.c_sed);
    z_start = length(tot_load_stuff.z) - z_end + 1;
    semilogy(tot_load_stuff.c_sed,tot_load_stuff.z(z_start:end),'Linewidth',LW)
    hold on
    y_coord = linspace(0,0.017,100);
    plot(y_coord,y_coord.*0 + salt_stuff.Hs,'-.','Linewidth',LW)
    plot(y_coord,y_coord.*0 + tot_load_stuff.Hf,'k-.','Linewidth',LW)
    axis([0 0.017 5e-3 1])
    plot(linspace(6e-3,salt_stuff.Hs,20).* 0 + tot_load_stuff.c_b,linspace(6e-3,salt_stuff.Hs,20),'LineStyle','-','Linewidth',LW)
    l1 = legend('Sediment concentration','Hs','Hf');
    set(l1,'Location','NorthEast','FontSize',FSleg)
    xlabel('Sediment concentration (c)','FontSize',FSax)
    ylabel('Height above bed (m)','FontSize',FSax)
    set(gca,'FontSize',FSax)
    set(gca,'Xtick',[0 0.004 0.008 0.012 0.016])
    grid on
end
