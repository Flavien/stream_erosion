%File to produce a bunch of test to see how much topographical changes
%can affect the hyd pot grad and see what we can infer about
%changes in water flow paths
%

%load gotta_save_soomething
%load long_term_sims/cheat_profs_TV_1yr

%setting up some constants
g = 9.81;
rho_i = 910;
rho_w = 1000;
dx = 2000; %2km
dwx = 1; %1 metre
Wm = 201; %width in metres

mid_topo = resulting_topo(10000,:)./1000;
coarse_mid_topo = mid_topo(1:4:end);
coarse_mid_topo(137) = 0;

bed_cut = ice_profs_1yr(1,10:146);
max_extent_prof = ice_profs_1yr(4470,10:146);

hi_orig = max_extent_prof - bed_cut;
hi_TV = max_extent_prof + coarse_mid_topo; 
%just as a test
phi_hi = hi_orig.*g.*rho_w;
dphi_hi = (phi_hi(2:end) - phi_hi(1:end-1))./dx;

%%%%%%%%%%%%%%%%%%%%%%%%%
%YOU ARE A MORRON... JUST CALCULATE A 2D FIELD AND FUCK OFF...
coarse_topo = zeros(Wm,137);

ice_surf_max_ext = repmat(max_extent_prof,[Wm,1]);
bed_surf = repmat(bed_cut,[Wm,1]);
coarse_topo(:,1:136) = resulting_topo(1:100:end,1:4:end)./1000;
topo_surf = bed_surf + coarse_topo;
hi_surf = ice_surf_max_ext - bed_surf;
hi_TV_surf = ice_surf_max_ext - topo_surf;


phi_hi_surf = hi_surf .*g.*rho_w;
phi_hi_TV_surf = hi_TV_surf .*g.*rho_w;

%And now trying to calculate some gradients
%I just hope that I am putting the proper dx and dy
[dphi_hi_pdx dphi_hi_pdy] = gradient(phi_hi_surf,dx,dwx);
[dphi_hi_TV_pdx dphi_hi_TV_pdy] = gradient(phi_hi_TV_surf,dx,dwx);

dphi_diff_pdx_ratio = (dphi_hi_pdx - dphi_hi_TV_pdx)./dphi_hi_pdx;

%Now let's try to plot something half decent

x_grid = [0:136].*dx ./1e3+820;
y_grid = [0:200];
x_select = [137-20:137];
%y_select = [50:1:150];
%y_select_coar = [50:10:150];
y_select = [1:1:201];
y_select_coar = [1:10:201];
x_plot = x_select.*dx ./1e3+820; %want that on in km
y_plot = y_select.*dwx;
y_plot_coar = y_select_coar.*dwx;

%If you want to plot something you can kinda look at
figure
[C,h] = contour(x_plot,y_plot,coarse_topo(y_select,x_select));
cb1 = colorbar;
%cb1.
hold on
%q_plot = quiver(x_plot,y_plot_coar,-dphi_hi_TV_pdx(y_select_coar,x_select),-dphi_hi_TV_pdy(y_select_coar,x_select));
q_plot = quiver(x_plot,y_plot_coar,-dphi_hi_TV_pdx(y_select_coar,x_select),dphi_hi_TV_pdy(y_select_coar,x_select));

%now adjusting quiver plot
q_plot.LineWidth = 1.5;
q_plot.Color = 'k';

%now adjusting contour plot
h.LevelStep = 0.5;

ylabel('Width (m)')
xlabel('Distance from ice divide (km)')
cb1.Label.String = 'Bed elevation (m)';

%Now plotting the relative difference in x hyd gradient. No y grad in flat bed so useless to check y
figure
[cs1,hs1] = contour(x_grid,y_grid,dphi_diff_pdx_ratio);
cb2 = colorbar;
hs1.LevelStep = 0.01;

ylabel('Width (m)')
xlabel('Distance from ice divide (km)')
cb2.Label.String = 'Normalized difference in hyd. pot. grad in the x direction';
