%File to plot the maximum particle size moving at different times of the simulation
%
%
%

s_end = 120*24;
s_start = 20*24;

x_plot = [1:min(size(q_tc_ar))]-0.5;
x_plot = x_plot .* dx./1000;

Dmax_short = Dmax_ar(1:6:end,:);

t_short = [1:length(Dmax_short)];
t_short = t_short./24;

LW2 = 3.5;
LW1 = 2;
LW3 = 3;
LW4 = 2;

FSlab = 26;
FSax  = 26;
FS_txt = 28;
FScont = 22;

fx = 0.41;
fy = 0.4;


f1 = figure(1);
f1.Units = 'normalized';
f1.Position = [0,0.3,fx,fy];

%vectC1 = [0:0.05:0.8];
vectC1 = [0:0.05:0.7];
%[C h] = contour(x_plot,t_short(s_start:s_end),Dmax_short(s_start:s_end,:),vectC1);
[C h] = contourf(x_plot,t_short(s_start:s_end),Dmax_short(s_start:s_end,:),vectC1);

h.LineColor = 'none';

xlabel('Distance from divide (km)')
ylabel('Time (day)')

gray_inv = flip(gray);
colormap(gray_inv)

text(51,125,'D (m)','FontSize',24)

cb = colorbar;
cb.Ticks = [0:0.05:0.7];
cb.TickLabels = {'0','','0.1','','0.2','','0.3','','0.4','','0.5','','0.6','','0.7'};
cb.Position(4) = 0.75;

f1ax = gca;

f1ax.FontSize = FSax;

f1ax.XLim = [0 50];
f1ax.XTick = [0:10:50];

f1ax.Position(1) = 0.127;

%t1 = clabel(C,h,'manual');
%for j=1:1:length(t1)
%    t1(j).FontSize = FScont;
%    t1(j).LineWidth = LW1;
%end
