%file to make some quick temporary plots of the flood simulations
%
%
%

%load trans_IS_ref_vs_Nye20
%load trans_IS_flood_test
%load trans_IS_3floods
%load trans_IS_TRvsFL30.mat
%load trans_IS_TRvsFL30.mat

trans_IS_flood_test = trans_IS;

space = size(trans_IS_flood_test(1).Qc);
space2 = size(trans_IS_flood_test(2).Qc);

dt1 = 600 .* 6;
dt2 = 600 .* 6;
dx = 500;

t_plot1 = [1:space(1)].*dt1 ./ 3600 ./ 24; %days
t_plot2 = [1:space2(1)].*dt2 ./ 3600 ./ 24; %days
x_plot = [1:100] .* dx ./ 1000; %km

sx = 2;
sy = 3;
figure
subplot(sx,sy,1)
[C1,h1] = contourf(x_plot,t_plot1,trans_IS_flood_test(1).E_tot./1000,30);
colorbar
h1.LineColor = 'none';
title('Total Erosion')
ylabel('Time (day), season')

subplot(sx,sy,2)
[C2,h2] = contourf(x_plot,t_plot1,trans_IS_flood_test(1).Q_tc,30);
colorbar
h2.LineColor = 'none';
title('Total transport capacity')

subplot(sx,sy,3)
[C3,h3] = contourf(x_plot,t_plot1,trans_IS_flood_test(1).Qc,30);
colorbar
h3.LineColor = 'none';
title('Discharge')

subplot(sx,sy,4)
[Cb1,hb1] = contourf(x_plot,t_plot2,trans_IS_flood_test(2).E_tot./1000,30);
colorbar
hb1.LineColor = 'none';
title('Total Erosion')
ylabel('Time (day), 10 days flood')

subplot(sx,sy,5)
[Cb2,hb2] = contourf(x_plot,t_plot2,trans_IS_flood_test(2).Q_tc,30);
colorbar
hb2.LineColor = 'none';
title('Total transport capacity')

subplot(sx,sy,6)
[Cb3,hb3] = contourf(x_plot,t_plot2,trans_IS_flood_test(2).Qc,30);
colorbar
hb3.LineColor = 'none';
title('Discharge')

figure
plot(x_plot,trans_IS_flood_test(1).sum_E_eff)
hold on
plot(x_plot,trans_IS_flood_test(1).sum_e_eff,'--')
plot(x_plot,trans_IS_flood_test(2).sum_E_eff,'x-')
plot(x_plot,trans_IS_flood_test(2).sum_e_eff,'--')

l2 = legend('\int E_{eff} dt, ref','\int e_{eff} dt, ref','\int E_{eff} dt, flood','\int e_{eff} dt, flood');
l2.Location = 'NorthWest';
title('Comparison btwn season and 10-days flood')
