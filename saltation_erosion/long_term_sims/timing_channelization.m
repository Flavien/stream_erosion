%Script to calcute for how long a part of the bedrock is exposed
%to a channelized system. Or more exactly, for how long erosion
%or transport can happen at a given point
%
%This script is meant to be run after the TV_geol plotting function

TV = 0;
IG = 1;

if TV == 1
    eros_check = E_eff_cheat .* 0;
    trans_check = Qtc_cheat.*0;

    for k = 1:length(E_eff_cheat)
        tp_eros_check = find(E_eff_cheat(k,:) > 0);
        tp_qtc_check = find(Qtc_cheat(k,:) > 0);

        eros_check(k,tp_eros_check) = 1;
        trans_check(k,tp_qtc_check) = 1;
    end

    cum_eros_yrs = sum(eros_check);
    cum_trans_yrs = sum(trans_check); 

    %Now make an ugly figure for now.
    figure
    plot(x_tot,cum_trans_yrs)
    hold on
    plot(x_tot,cum_eros_yrs)

    xlabel('Distance from the ice divide (km)')
    ylabel('Number of years of activity')
    legend('Transport happens','Erosion happens')
elseif IG == 1
    sz1 = length(trans_IS);
    sz2 = length(trans_IS(1).sum_e_eff);
    eros_check = zeros(sz1,sz2);
    trans_check = zeros(sz1,sz2);

    for k =1:sz1
        tp_eros_check = find(trans_IS(k).sum_e_eff > 0);
        tp_qtc_check = find(sum_Qtc(k,:) > 0);

        eros_check(k,tp_eros_check) = 1;
        trans_check(k,tp_qtc_check) = 1;
    end

    cum_eros_yrs = sum(eros_check);
    cum_trans_yrs = sum(trans_check); 

    %Now make an ugly figure for now.
    dt_IG = 300; %in years
    figure
    plot(x_tot,cum_trans_yrs.*dt_IG)
    hold on
    plot(x_tot,cum_eros_yrs.*dt_IG)

    xlabel('Distance from the ice divide (km)')
    ylabel('Number of years of activity')
    legend('Transport happens','Erosion happens')
end
