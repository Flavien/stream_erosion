%Mostly plot for fun to show the shape of a tunnel valley carved by the model.
%
%
%

%E_eff_tv = e_tot_ar.*600./constants.secondsinyear.*1000;
%E_eff_tv = e_eff./1000;
E_eff_tv = e_eff;
%E_eff_tv(R < 0.2) = 0;

%R = R.*5;

R_max = max(max(R));

x_nodes = round(R_max.*100);
x_no_tot = x_nodes*2 +1;

R_discrete = round(R./(R_max/x_nodes));


time_tot = length(E_eff);

plot_only = 0;

%E_eff = E_eff.*200;
grd_length = min(size(e_tot_ar));

if plot_only == 0
valley_topo = zeros(x_no_tot,grd_length);
    for i =1:time_tot
        for j = 1:grd_length
            valley_topo(x_nodes+1-R_discrete(i,j):x_nodes+1+R_discrete(i,j),j) = ...
                    valley_topo(x_nodes+1-R_discrete(i,j):x_nodes+1+R_discrete(i,j),j) - E_eff_tv(i,j);
        end
    end
end


max_depth = min(min(valley_topo));
%And now plotting it
x_stag = linspace(0.5,9.5,grd_length);
width(1:x_nodes+1) = linspace(1,0,x_nodes+1).*-R_max;
width(x_nodes+1:x_no_tot) = linspace(0,1,x_nodes+1).*R_max;

