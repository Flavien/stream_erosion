%File to make a figure that show the details of the inner gorge that is formed.
%This is kindda of an attempt at making a figure for the paper
%
%
%
%It might be good to run the making topo script first to at least compute the
%topo that you might plot

%Load all the different profiles used for the different stages


%load long_term_sims/ELA1262_T11570_hyd_input.mat
%load_ice_profiles

%load ENTL_coarse.mat
%load trans_IS_ENTL_HR_reg_topo.mat
%load ENTL_coarse_Ice_profs.mat

%if not everything is loaded
% for ENTL HR
%load trans_IS_ENTL_HR_bs3_D256.mat

%load IG_topo_D256_W100
%y_tot = y_tot_IG;
%resulting_topo = resulting_topo_IG_meters.*1e3;



dx = 0.215;
x_st = 26;
x_end = 138;

%putting units
x_tot = linspace(x_st*dx,(x_end+x_st)*dx,x_end);
%Turning it into half width in meters...
y_tot = y_tot .* 1e3 -100;

x_t_l = [min(x_tot)-dx x_tot];

total_e_eff = zeros(1,x_end);
total_e_eff_adv = zeros(1,x_end);
total_e_eff_rtr = zeros(1,x_end);
total_E_eff = zeros(1,x_end);
total_E_eff_adv = zeros(1,x_end);
total_E_eff_rtr = zeros(1,x_end);

Q_tc_int = zeros(1,x_end);
Q_tc_int_adv = zeros(1,x_end);
Q_tc_int_rtr = zeros(1,x_end);
sum_Qtc = zeros(length(trans_IS),x_end);

for i=1:length(trans_IS)
    total_e_eff = total_e_eff + trans_IS(i).sum_e_eff.*300.* (dx*1e3);
    total_E_eff = total_E_eff + trans_IS(i).sum_E_eff.*300.* (dx*1e3);

%SORT OUT THE UNITS HERE
%Qtc is in m^3/s
%Here I am computing total volume
    sum_Qtc(i,:) = trapz(trans_IS(i).Q_tc) .* trans_IS(i).dt;

    Q_tc_int = Q_tc_int + sum_Qtc(i,:) .* 300 .* (dx*1e3);
end

for i=1:11
    total_e_eff_adv = total_e_eff_adv + trans_IS(i).sum_e_eff.*300.* (dx*1e3);
    total_E_eff_adv = total_E_eff_adv + trans_IS(i).sum_E_eff.*300.* (dx*1e3);

    Q_tc_int_adv = Q_tc_int_adv + sum_Qtc(i,:) .* 300.* (dx*1e3);
end

for i=12:15
    total_e_eff_rtr = total_e_eff_rtr + trans_IS(i).sum_e_eff.*300.* (dx*1e3);
    total_E_eff_rtr = total_E_eff_rtr + trans_IS(i).sum_E_eff.*300.* (dx*1e3);
    
    Q_tc_int_rtr = Q_tc_int_rtr + sum_Qtc(i,:) .* 300.* (dx*1e3);
end

%fx = 0.7;
%fy = 0.9;
fx = 8.9*2;
fy = 12;
sx = 3;
sy = 2;

LW1 = 2;
LW2 = 1.2;
FSax = 11;

col_advance = rgb('SkyBlue');
col_retreat = rgb('SteelBlue');
col_max =  rgb('RoyalBlue');

f1 = figure;
f1.Units = 'centimeters';
f1.Position(3) = fx;
f1.Position(4) = fy;
f1.Units = 'normalized';
%%%%%%%%%%%%%%%%%%%%%%
%   1ST subplot
S1 = subplot(sx,sy,[1 2]);

%pa1 = plot(x_t_l,z_b);
plot_ice_profiles;

ylabel('Elevation (m)')
S1.XTickLabel = '';

%%%%%%%%%%%%%%%%%%%%%%
%   2ND subplot
S2 = subplot(sx,sy,[3 4]);

[C,h] = contourf(x_tot,y_tot(1:100:end),resulting_topo(1:100:end,:)./1000,30);
h.LineColor = 'none';
cb = colorbar;
cb.Location = 'west';
%cb.Label.String = 'Erosion depth (m)';
cb.Label.String = 'Depth (m)';
axis([5 35 -100 100])

%ylabel({'Bedrock channel';'width (km)'})
ylabel('Width (km)')
xlabel('Distance from ice divide (km)')

%%%%%%%%%%%%%%%%%%%%%%
%   3RD subplot
S3 = subplot(sx,sy,5);

%pc1 = plot(x_tot,total_e_eff./1000,'--');
pc2 = plot(x_tot,total_E_eff./1000);
%hold on

%pc3 = plot(x_tot,total_E_eff_adv);
%pc4 = plot(x_tot,total_E_eff_rtr,'--');

%l2 = legend('Total','Advance','Retreat');
%l2.Location = 'NorthWest';
%l2.Box = 'off';
ylabel({'Volume eroded';'per cell (m^3)'})
xlabel('Distance from divide (km)')

pc2.LineWidth = LW1;
%pc3.LineWidth = LW2;
%pc4.LineWidth = LW2;
pc2.Color = 'k';
%pc3.Color = col_advance;
%pc4.Color = col_retreat;
max_vol = max(total_E_eff./1000).*1.2;
axis([5 35 0 max_vol])

%%%%%%%%%%%%%%%%%%%%%%
%   3RD subplot
S4 = subplot(sx,sy,6);

pd3 = plot(x_tot,Q_tc_int);
%pd1 = plot(x_tot,Q_tc_int_adv);
%hold on
%pd2 = plot(x_tot,Q_tc_int_rtr,'--');

%l3 = legend('Advance','Retreat');
%l3.Location = 'NorthWest';
%l3.Box = 'off';
ylabel({'Transport';'capacity (m^3)'})
xlabel('Distance from divide (km)')

%pd1.LineWidth = LW1;
%pd2.LineWidth = LW1;
pd3.LineWidth = LW1;

pd3.Color = 'k';
%pd1.Color = col_advance;
%pd2.Color = col_retreat;

%max_tc_adv = max(Q_tc_int_adv);
%max_tc_rtr = max(Q_tc_int_adv);
%max_tc_ax = max(max_tc_adv,max_tc_rtr) * 1.2;
max_tc_ax = max(Q_tc_int) * 1.2;

axis([5 35 0 max_tc_ax])

%%%%%%%%%
%making everything neat

S1.FontSize = FSax;
S2.FontSize = FSax;
S3.FontSize = FSax;
S4.FontSize = FSax;

S1.Position(2) = 0.75;
S2.Position(2) = 0.48;

%%%%%%%%%%%%%
%Adding some text in corners
subplot(S1)
tA = text(34,1070,'A');

subplot(S2)
tB = text(34,0.165*1e3 -100,'B');

subplot(S3)
tC = text(33,max_vol.*0.85,'C');

subplot(S4)
tD = text(33,max_tc_ax*0.85,'D');
