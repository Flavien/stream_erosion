%File to plot the ice profiles nicely following the way they were loaded
%
%Loading through the load_ice_profiles.m file
%

function plot_ice_profiles()

load ENTL_coarse_Ice_profs.mat
%Setting up all the flags
own_fig = 0;            
plot_sliding = 0;

dx = 0.43;
x_st = 13;
x_end = 68;

%putting units
x_tot = linspace(x_st*dx,(x_end+x_st)*dx,x_end);

x_t_l = [min(x_tot)-dx x_tot];


growing_profs = [1 2 3 4 5 6 7 8 9 10];
max_ext = 19;
retreating_profs = [11 12 13 18];

E_series = [14 15 16 17 18];

legend_yrs = {'bed','0','300','600','900','1200','1500','1800',...
            '2100','2400','2700','3000','3300','3600','3900','4200'};

%LW2 = 3;
%LW3 = 3.5;
%LW4 = 5;
%For sup mat of paper
LW2 = 1.5;
LW3 = 1.5;
LW4 = 2;

if own_fig == 1
    figure
end
plot(x_t_l,bed_topo,'k','LineWidth',1)
hold on

col_advance = rgb('SkyBlue');
col_retreat = rgb('SteelBlue');
%col_max =  rgb('RoyalBlue');
col_max =  rgb('SkyBlue');

%for i=1:length(file_names)
for i=growing_profs
    p1 = plot(x_t_l,ice_topos(i,:),'-','LineWidth',LW2,'Color',col_advance);
end

p2 = plot(x_t_l,ice_topos(max_ext,:),'-','LineWidth',LW4,'Color',col_max);

for i=retreating_profs
    p3 = plot(x_t_l,ice_topos(i,:),'--','LineWidth',LW3,'Color',col_retreat);
end

%l1 = legend([p1 p2 p3],'Advance','Maximum','Retreat');
%l1.Box = 'off';

ax = gca;

ax.YLim = [0 1300];

%l1 = legend(legend_yrs);

%for i=E_series
%    plot(x_t_l,ice_topos(i,:),'s-');
%end

if own_fig == 1
    if plot_sliding == 1
        fnb = gcf;
        figure(fnb.Number + 1)

        for i=growing_profs
            p1 = plot(x_t_l,ub_profs(i,:),'-','LineWidth',LW2);
            hold on
        end

        p2 = plot(x_t_l,ub_profs(max_ext,:),'x-','LineWidth',LW2);

        for i=retreating_profs
            p3 = plot(x_t_l,ub_profs(i,:),'o-','LineWidth',LW2);
        end

        legend_ub = legend_yrs(2:end);
        
        l2 = legend(legend_ub);
    end
end
