%File to use the different time slices
%and put them together to end up with the final topography
%
%

ENTL_HR = 0;
TV_E_HR = 0;
TV_K = 1;

%dx = 0.43;
%x_st = 13;
%x_end = 68;
if ENTL_HR ==1
    % for ENTL HR
    dx = 0.215;
    x_st = 26;
    x_end = 138;
end

%for TV_E
if TV_E_HR == 1
    dx = 1000;
    x_st = 1;
    x_end = 290;
end

%fot TV_K
if TV_K == 1
    dx = 500; %m
    x_st = 1;
    x_end = 541;
    load long_term_sims/IS_K_ice_profs_25.mat
    x_st_from_div = 820; %km
end

%putting units
x_tot = linspace(x_st*dx,(x_end+x_st)*dx,x_end)./1e3 + x_st_from_div;

%Span of the valley in meters
valley_span = 200; %Aiming at a 1:10 ratio for depth / width
%valley_span = 200;

%Cross-section coordinate, a point each centimetre
%actually, just making sure that there is a hundred times more points than meters in the width
y_tot = [0:1:valley_span*100]./100; %I actually want the width in metres now...
y_mid = valley_span/2/(valley_span/200)*100 + 1; %/4 and not /2 because it's based on a 200m width and not 400...
y_end = valley_span*100 + 1;

%resulting_topo = trans_IS(1).valley_topo.*0;
resulting_topo = zeros(y_end,length(x_tot));

%Defining future variables to be stored
size_ice_profs = size(ice_profs_25);
Qtc_cheat = zeros(size_ice_profs(1),length(trans_IS(1).sum_E_eff));
E_eff_cheat = zeros(size_ice_profs(1),length(trans_IS(1).sum_E_eff));
e_eff_cheat = zeros(size_ice_profs(1),length(trans_IS(1).sum_E_eff));

for i = 1:length(trans_IS)
    %Check maximum width of N-channel
    temp_width = round(max(trans_IS(i).width).*100);    %temp_width is in centimetres
    %Sampling the domain to isolate the N-channel
    current_temp_topo = trans_IS(i).valley_topo(y_mid-temp_width:y_mid+temp_width,:);
    %keyboard;
    cu_blc_topo = current_temp_topo.*0;

    current_E_eff = trans_IS(i).sum_E_eff;
    current_e_eff = trans_IS(i).sum_e_eff;
    current_Q_tc = sum(trans_IS(i).Q_tc);
    cu_blc_Qtc = current_Q_tc.*0;

    if i<length(trans_IS)
        next_temp_width = round(max(trans_IS(i+1).width).*100);
        next_temp_topo = trans_IS(i+1).valley_topo(y_mid-temp_width:y_mid+temp_width,:);
        next_blc_topo = next_temp_topo .*0;


        next_E_eff = trans_IS(i+1).sum_E_eff;
        next_e_eff = trans_IS(i+1).sum_e_eff;
        next_Q_tc = sum(trans_IS(i+1).Q_tc);
    end

    %Making sure erosion won't be applied outside the domain
    possible_span = y_mid -temp_width-1;

    %Defining a distribution along which to apply the erosion
    pd = makedist('Normal','mu',3,'sigma',1.0);
    

    %possible_span = round(possible_span ./ 4);
    %Apparently 300 years between sims in that simulation...
    if ENTL_HR == 1 tt_dt = 300; end
    if TV_E_HR == 1 tt_dt = 500; end
    if TV_K == 1 
        tt_dt = 250; 
        %if i == 1 tt_dt = 125; end
        %if i == length(trans_IS) tt_dt = 125; end
    end

    for tt = 1:tt_dt
       %keyboard; 
        full_on_cheat_alg;
        %cheating_algorythm;
        %if mod(tt,25) == 5
        %    plot(temp_topo(500,:))
        %    hold on
        %    pause(0.5)
        %end

        %position of modified variable: (i-1)*250+tt_dt
        %Now let's store these variables
        Qtc_cheat(tt + (i-1)*250,:) = temp_Qtc;
        E_eff_cheat(tt + (i-1)*250,:) = temp_E_eff;
        e_eff_cheat(tt + (i-1)*250,:) = temp_e_eff;
        %keyboard

        %rand_pos = randi([y_mid-possible_span y_mid+possible_span],1,1);
        %along a normal distribution...
        rand_pos = round(pd.random./6.*y_end);
        %rand_pos = round(pd.random./6.*y_mid);
        temp_width_new = ceil(length(int_slice(:,1))./2);
        temp_width_new2 = floor(length(int_slice(:,1))./2);
        if rand_pos >= y_end - temp_width_new
        %if rand_pos >= possible_span
            rand_pos = possible_span;
        elseif rand_pos <= temp_width_new
            rand_pos = temp_width_new +1;
        end
        %y1_t = rand_pos-temp_width;
        %y2_t = rand_pos+temp_width;
        y1_t = rand_pos-temp_width_new;
        y2_t = rand_pos+temp_width_new2-1;
        resulting_topo(y1_t:y2_t,:) = resulting_topo(y1_t:y2_t,:) + temp_topo;
    end
end

%In case you actually want to save something
%save('gotta_save_soomething.mat','resulting_topo','Qtc_cheat','E_eff_cheat','e_eff_cheat','y_tot',...
%                                    'x_tot','dx','x_end','x_st_from_div')

%% TO MAKE IT USEFUL FOR FIG 3 OF PAPER
resulting_topo_TV_meters = resulting_topo ./ 1e3;
x_tot_TV = x_tot;
y_tot_TV = y_tot;

%Loading the longest bed
if ENTL_HR == 1
    %load ELA1262_T11570_hyd_input
    %MAC
    load entlebuch_bed43km_200pts.mat
    %UBUNTU
    %load /media/yeti/B6DEEFA4DEEF5B5F/temp_hydro_backup/Sims_backup/hydro/runs/ENTL_E/ELA1262_initial_test_hyd_input/entlebuch_bed43km_200pts.mat
    bed_prof(152) = 630; %Removing a spiky bit...
    z_b = bed_prof(x_st:x_st +x_end);

    bed_interp = (z_b(2:end) + z_b(1:end-1)) ./ 2;
    bed_2d = repmat(bed_interp,y_end,1);

    %Calculating the real resulting topo
    real_topo = resulting_topo./1000 + bed_2d;
end

%%%%%%%%%%%%%%%%%
% NOW PLOTTING THE RESULTS
%figure(1)
%contour(x_tot,y_tot,resulting_topo./1000,30)
%colorbar

%figure
%surf(x_tot,y_tot,resulting_topo./1000)
%shading interp
%axis equal

%figure
%surf(x_tot,y_tot,real_topo)
%shading interp

plotting = 0;

if plotting == 1
    figure
    y_slice = [14:3:68];
    w1 = waterfall(resulting_topo(5000:15000,y_slice)'./1000);
    w1.FaceColor = 'none';
    w1.LineWidth = 2;
    w1.CData(1,:) = nan;
    w1.CData(end-2:end,:) = nan;

    figure
    x_slice = [7500 9000 10000 11000 12500];
    w2 = waterfall(resulting_topo(x_slice,:)./1000);
    w2.FaceColor = 'none';
    w2.LineWidth = 2;
    w2.CData(1,:) = nan;
    w2.CData(end-2:end,:) = nan;
end
