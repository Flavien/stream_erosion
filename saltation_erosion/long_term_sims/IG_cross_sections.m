%file to plot the cross section of the inner gorge landform that I create
%
%
%

%Profiles needed:
% km 15.1 => 46
%km 19.5 => 66
%km 22.5 => 80
%km 26.14 => 97
%km 28 => 105
%km 29.6 => 113
%km 32.8 => 128
%km 35 => 138

Xsect2d = 0;
Xsect3d = 1;
waterfall_plot = 0;
IG = 1;
TV = 0;

if IG == 1
    x_tot = x_tot_IG;
    y_tot = y_tot_IG;
    resulting_topo = resulting_topo_IG_meters;
elseif TV == 1
    x_tot = x_tot_TV;
    y_tot = y_tot_TV;
    resulting_topo = resulting_topo_TV_meters;
end

f_cr = figure;
sx_cr = 2;
sy_cr = 4;

mm_to_m_fact = 1;

LW1 = 2;
FSax = 12;

%cut_piece = 7500;
cut_piece = 5000;
y_tot_cut = y_tot(cut_piece:end-cut_piece);

k = 1;
if Xsect2d == 1
    section_slices = [46 66 80 97 105 113 128 138];
    for i = section_slices
        S_cr{k} = subplot(sx_cr,sy_cr,k);
        p_cross{k} = plot(y_tot,resulting_topo(:,i)./mm_to_m_fact,'LineWidth',LW1);
        ax_cr{k} = gca;
        axis([0 0.2 -35 0])
        %hold on
        k = k+1;
    end
elseif Xsect3d == 1
    %section_slices = [25 40 62 65 97 105 113 128 135 138];
    if IG == 1
        section_slices = [32 65 85 105 128 138];
    elseif TV == 1
        section_slices = [137 250 345 470 524 541];
    end
    for i = section_slices
        %S_cr{k} = subplot(sx_cr,sy_cr,k);
        %p_cross{k} = plot3(y_tot.*0 + i,y_tot,resulting_topo(:,i)./mm_to_m_fact,'LineWidth',LW1);
        p_cross{k} = plot3(y_tot_cut.*0 + x_tot(i),y_tot_cut,resulting_topo(cut_piece:end-cut_piece,i)./mm_to_m_fact,'LineWidth',LW1);
        %ax_cr{k} = gca;
        %axis([0 0.2 -35 0])
        hold on
        k = k+1;
        p_long = plot3(x_tot(section_slices(1):end),x_tot(section_slices(1):end).*0+0.1,resulting_topo(10000,section_slices(1):end));
    end
end
p_long.LineStyle = '--';
p_long.Color = 'k';

if Xsect2d == 1
    for kk = 1:length(section_slices)
        ax_cr{kk}.FontSize = FSax;
    end

    for kk = [2 3 4 6 7 8]
    ax_cr{kk}.YTickLabel = '';
    end

    for kk = [1 2 3 4]
        ax_cr{kk}.XTickLabel = '';
    end
end

%ax_cr{1}.YLabel = 'Depth (m)';
%ax_cr{6}.XLabel = 'Width (km)';


if waterfall_plot == 1
    figure
        x_spac = 10;
        w1 = waterfall(y_tot(5000:100:end-5000)-0.05,x_tot(1:x_spac:end),resulting_topo(5000:100:end-5000,1:x_spac:end)'./mm_to_m_fact);
        w1.FaceColor = 'none';
        w1.LineWidth = 2;
        w1.CData(1,:) = nan;
        w1.CData(end-2:end,:) = nan;
        colorbar

        ax2 = gca;
        ax2.FontSize = FSax;
        xlabel('Width (km)')
        ylabel('Distance from divide (km)')
        zlabel('Depth (m)')
end


