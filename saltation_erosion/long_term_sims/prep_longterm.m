%Function to compute the what I want out of the run
%to load in a structure and plot a comparison of the plots
%
%



function [valley_topoB,sum_e_effB,sum_E_effB,Qc_shortB,Q_tc_shortB,E_totB,t_stage_shortB,...
            Fe_shortB,e_tot_shortB,max_E_eff,result_eros,vol_exit,Q_out,q_tc_vol_shB,width,...
            dt,gl_W,R,u_mean_sub,u_shear_ar] =...
                                            prep_longterm(file_to_load,ch_nb,node_tot)

load(file_to_load)
%node_tot = 68
node_temp = min(size(e_tot_ar));
dt_step = 1;

%ch_nb = 1;

Qc_t = 0.01;

R_t = 0.250;

q_tc_vol = q_tc_ar./2650.*dt;
q_tc_vol(R < R_t) = 0;
q_tc_vol_sh = q_tc_vol(1:dt_step:end,:);

Q_tc = q_tc_ar./2650.*(2.*R).*ch_nb;
Q_tc(R < R_t) = 0;

%Transforming into volume of sediment
Q_tc_dt = q_tc_vol.*(2.*R).*ch_nb; %[m^3]
Q_tot = q_tot_temp.*(2.*R).*ch_nb; %[m^3], q_tot_temp already *dt in main script
%Q_tot = Q_tot.*1.262;
%Q_tot = Q_tot.*0.486;
%Q_tot = Q_tot.*2;
%Q_tot = Q_tot.*0.618;

%Volume
Q_out = min(Q_tc_dt(:,end),Q_tot(:,end));

Q_tc_short = Q_tc(1:dt_step:end,:);
q_tc_short = q_tc_ar(1:dt_step:end,:);

R_short = R(1:dt_step:end,:);
%Converting e_eff in mm per time slot 
e_eff = e_tot_ar.*1000.*dt./constants.secondsinyear;
e_eff(R < R_t) = 0;
E_eff = e_eff .* (2.*R).*ch_nb;

%e_tot_short in mm /a
e_tot_short = e_tot_ar(1:dt_step:end,:).*1000;
e_tot_short(R_short < R_t) = 0;

E_tot = e_tot_short .* (2.*R_short).*ch_nb;
Fe_short = Fe_tot_ar(1:dt_step:end,:);
Fe_short(R_short < R_t) = 0;
cb_mod = cb_ar;
cb_mod(Fe_tot_ar == 0) = -0.02;
cb_short = cb_mod(1:dt_step:end,:);
w_i_eff_short = w_i_eff_ar(1:dt_step:end,:);
Qc_short = Qc(1:dt_step:end,:);

t_stage = tau_ratio;
%t_stage(R < R_t+0.03) = 0;
t_stage(R < R_t) = 0;
t_stage_short = t_stage(1:dt_step:end,:);


%computing stuff about erosion
%sum_e_eff is in mm per simulation
sum_e_eff = sum(e_eff);
sum_E_eff = sum(E_eff);

max_E_eff = max(sum_E_eff);

%res_eros = sum(sum_E_eff)./ nb_nodes / width glacier
%result_eros = sum(sum_E_eff)./100./1000;
result_eros = sum(sum_E_eff)./min(size(e_tot_ar))./gl_W;

%sum of total volume transported at the last node
vol_exit = sum(Q_out);

%plotting stuffs ...
t_long = [1:length(q_tc_ar)];
t_long = t_long ./ 6 ./ 24;
t_short = [1:length(Q_tc_short)];
t_short = t_short./24;

tunnel_valley_long

tot_valley_width = 100*100; % 100m * 100 cm/m;

valley_topoB    = zeros(tot_valley_width*2+1,node_tot);
sum_e_effB      = zeros(1,node_tot);
sum_E_effB      = zeros(1,node_tot);
Qc_shortB       = zeros(max(size(Qc_short(:,1))),node_tot);
Q_tc_shortB     = zeros(max(size(Qc_short(:,1))),node_tot);
E_totB          = zeros(max(size(Qc_short(:,1))),node_tot);
t_stage_shortB  = zeros(max(size(Qc_short(:,1))),node_tot);
Fe_shortB       = zeros(max(size(Qc_short(:,1))),node_tot);
e_tot_shortB    = zeros(max(size(Qc_short(:,1))),node_tot);
q_tc_vol_shB    = zeros(max(size(Qc_short(:,1))),node_tot);

valley_size = size(valley_topo);
y_val_coord = (valley_size-1)./2;

valley_topoB(tot_valley_width-y_val_coord:tot_valley_width+y_val_coord,1:node_temp) = valley_topo; 
sum_e_effB(1:node_temp) = sum_e_eff;
sum_E_effB(1:node_temp) = sum_E_eff;
Qc_shortB(:,1:node_temp) = Qc_short;
%Q_tc_short in [m^3/s]
Q_tc_shortB(:,1:node_temp) = Q_tc_short;
E_totB(:,1:node_temp) = E_tot;
t_stage_shortB(:,1:node_temp) = t_stage_short;
Fe_shortB(:,1:node_temp) = Fe_short;
e_tot_shortB(:,1:node_temp) = e_tot_short;
q_tc_vol_shB(:,1:node_temp) = q_tc_vol_sh;
