%Script to retrieve the sediment yield out of simulations that were made with the IG
%scenario.
%
%
ENTL = 1;
TV = 0;

if ENTL == 1
    %load trans_IS_ENTL_HR_bs3_D256
    load trans_IS_ENTL_HR_bs3_D60
    dx = 215; %[m] for ENTL sim
    extra_length = 5000; %[m] for ENTL sim
elseif TV == 1;
    %load trans_IS_TR_K_vHR_D256.mat
    load trans_IS_TR_K_vHR_D60.mat
    dx = 500; %[m] for TV_vHR sim
    extra_length = 820000; %[m] for ENTL sim
end


n_slice = length(trans_IS);
gl_W = trans_IS(1).gl_W;

%First step is to find out about the ice extent at each time slice.
for i = 1:n_slice
    %testing where sediment motion happened during run
    motion_test = find(sum(trans_IS(i).t_stage) > 0);
    motion_range(i,:) = [min(motion_test) max(motion_test)];

    %calculating total glacier length and surface
    gl_length_shown(i) = motion_range(i,2) .* dx;
    gl_length(i) = motion_range(i,2) .* dx + extra_length;
    gl_surf_shown(i) = gl_length_shown(i) .* gl_W;
    gl_surf(i) = gl_length(i) .* gl_W;

    %calculating length of bed over which transport happened and surf concerned
    gl_motion_length(i) = (motion_range(i,2) - motion_range(i,1)) .* dx;
    gl_motion_surf(i) = gl_motion_length(i) .* gl_W;

    %Calculating erosion averaged over the bed [m]
    bed_avg_eros(i) = trans_IS(i).vol_exit ./ gl_surf(i);
    bed_avg_eros_shown(i) = trans_IS(i).vol_exit ./ gl_surf_shown(i);

    %Calculating erosion averaged over channelized area [m]
    channel_avg_eros(i) = trans_IS(i).vol_exit ./ gl_motion_surf(i);
end

