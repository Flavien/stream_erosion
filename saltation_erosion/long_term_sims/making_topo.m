%File to use the different time slices
%and put them together to end up with the final topography
%
%
% Before running this file you have to go back to the load_longterm and choose the set of sims you want to use
% MAKE SURE the list is loaded properly in sim_list_longterm first

ENTL_HR = 0;
TV_E_HR = 0;
TV_K = 1;

%dx = 0.43;
%x_st = 13;
%x_end = 68;
if ENTL_HR ==1
    % for ENTL HR
    dx = 0.215;
    x_st = 26;
    x_end = 138;
end

%for TV_E
if TV_E_HR == 1
    dx = 1000;
    x_st = 1;
    x_end = 290;
end

%fot TV_K
if TV_K == 1
    dx = 1000;
    x_st = 1;
    x_end = 541;
end

%putting units
x_tot = linspace(x_st*dx,(x_end+x_st)*dx,x_end);

%Span of the valley in meters
valley_span = 200;

%Cross-section coordinate, a point each centimetre
y_tot = [0:1:200*100]./100./1000;
y_mid = valley_span/2*100 + 1;
y_end = valley_span*100 + 1;

resulting_topo = trans_IS(1).valley_topo.*0;

for i = 1:length(trans_IS)
    %Check maximum width of N-channel
    temp_width = round(max(trans_IS(i).width).*100);
    %Sampling the domain to isolate the N-channel
    temp_topo = trans_IS(i).valley_topo(y_mid-temp_width:y_mid+temp_width,:);
    %Making sure erosion won't be applied outside the domain
    possible_span = y_mid -temp_width-1;

    %Defining a distribution along which to apply the erosion
    %pd = makedist('Normal','mu',3);
    %over 200 metres of width
    pd = makedist('Normal','mu',3,'sigma',0.6);
    %over 100 metres of width
    %pd = makedist('Normal','mu',3,'sigma',0.3);
    

    %possible_span = round(possible_span ./ 4);
    %Apparently 300 years between sims in that simulation...
    if ENTL_HR == 1 tt_dt = 300; end
    if TV_E_HR == 1 tt_dt = 500; end
    if TV_K == 1 tt_dt = 250; end
    for tt = 1:tt_dt
        %rand_pos = randi([y_mid-possible_span y_mid+possible_span],1,1);
        %along a normal distribution...
        rand_pos = round(pd.random./6.*y_end);
        %rand_pos = round(pd.random./6.*y_mid);
        if rand_pos >= y_end - temp_width
        %if rand_pos >= possible_span
            rand_pos = possible_span;
        elseif rand_pos <= temp_width
            rand_pos = temp_width +1;
        end
        y1_t = rand_pos-temp_width;
        y2_t = rand_pos+temp_width;
        resulting_topo(y1_t:y2_t,:) = resulting_topo(y1_t:y2_t,:) + temp_topo;
    end
end

%Loading the longest bed
if ENTL_HR == 1
    %load ELA1262_T11570_hyd_input
    %MAC
    load entlebuch_bed43km_200pts.mat
    %UBUNTU
    %load /media/yeti/B6DEEFA4DEEF5B5F/temp_hydro_backup/Sims_backup/hydro/runs/ENTL_E/ELA1262_initial_test_hyd_input/entlebuch_bed43km_200pts.mat
    bed_prof(152) = 630; %Removing a spiky bit...
    z_b = bed_prof(x_st:x_st +x_end);

    bed_interp = (z_b(2:end) + z_b(1:end-1)) ./ 2;
    bed_2d = repmat(bed_interp,y_end,1);

    %Calculating the real resulting topo
    real_topo = resulting_topo./1000 + bed_2d;
end

%%%%%%%%%%%%%%%%%
% NOW PLOTTING THE RESULTS
%figure(1)
%contour(x_tot,y_tot,resulting_topo./1000,30)
%colorbar

%figure
%surf(x_tot,y_tot,resulting_topo./1000)
%shading interp
%axis equal

%figure
%surf(x_tot,y_tot,real_topo)
%shading interp

plotting = 0;

if plotting == 1
    figure
    y_slice = [14:3:68];
    w1 = waterfall(resulting_topo(5000:15000,y_slice)'./1000);
    w1.FaceColor = 'none';
    w1.LineWidth = 2;
    w1.CData(1,:) = nan;
    w1.CData(end-2:end,:) = nan;

    figure
    x_slice = [7500 9000 10000 11000 12500];
    w2 = waterfall(resulting_topo(x_slice,:)./1000);
    w2.FaceColor = 'none';
    w2.LineWidth = 2;
    w2.CData(1,:) = nan;
    w2.CData(end-2:end,:) = nan;
end
