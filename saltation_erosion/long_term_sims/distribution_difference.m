%File to plot the different landforms resulting from different distributions
%used in the making_diff_topos.m file
%
%

%load diff_topos_from_ENTL_HRbs3_D256.mat

fx = 0.5;
fy = 1;
sx = 5;
sy = 1;

LW1 = 2;

f1 = figure;
f1.Units = 'normalized';
f1.Position = [0,0.0,fx,fy];

y_tot = y_tot .* 1e3 - 100;

%%%%%%%%%%%%%%%%%%%%%%
%   1ST subplot
S1 = subplot(sx,sy,1);

[CA,hA] = contourf(x_tot,y_tot(1:20:end),resulting_topoA(1:20:end,:),30);
hA.LineColor = 'none';
cbA = colorbar;
cbA.Location = 'west';
cbA.Label.String = 'Erosion depth (m)';
axis([5 35 -100 100])

ylabel('Half width (m)')
title('Normal distribution (\mu = 3, \sigma =1)')

%%%%%%%%%%%%%%%%%%%%%%
%   2nd subplot
S2 = subplot(sx,sy,2);

[CB,hB] = contourf(x_tot,y_tot(1:20:end),resulting_topoB(1:20:end,:),30);
hB.LineColor = 'none';
cbB = colorbar;
cbB.Location = 'west';
cbB.Label.String = 'Erosion depth (m)';
axis([5 35 -100 100])

ylabel('Half width (m)')
title('Random distribution (200 m span)')

%%%%%%%%%%%%%%%%%%%%%%
%   3rd subplot
S3 = subplot(sx,sy,3);

[CC,hC] = contourf(x_tot,y_tot(1:20:end),resulting_topoC(1:20:end,:),30);
hC.LineColor = 'none';
cbC = colorbar;
cbC.Location = 'west';
cbC.Label.String = 'Erosion depth (m)';
axis([5 35 -100 100])

ylabel('Half width (m)')
title('Normal distribution (\mu = 3, \sigma = 1.4)')

%%%%%%%%%%%%%%%%%%%%%%
%   4th subplot
S4 = subplot(sx,sy,4);

[CD,hD] = contourf(x_tot,y_tot(1:20:end),resulting_topoD(1:20:end,:),30);
hD.LineColor = 'none';
cbD = colorbar;
cbD.Location = 'west';
cbD.Label.String = 'Erosion depth (m)';
axis([5 35 -100 100])

ylabel('Half width (m)')
title('Normal distribution (\mu =3, \sigma = 0.6)')

%%%%%%%%%%%%%%%%%%%%%%
%   1ST subplot
S5 = subplot(sx,sy,5);

[CE,hE] = contourf(x_tot,y_tot(1:20:end),resulting_topoE(1:20:end,:),30);
hE.LineColor = 'none';
cbE = colorbar;
cbE.Location = 'west';
cbE.Label.String = 'Erosion depth (m)';
axis([5 35 -100 100])

ylabel('Half width (m)')
title('Random distribution (100 m span)')
xlabel('Distance from the ice divide (km)')
