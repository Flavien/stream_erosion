%Ugly fuckign piece of code to do the cheating for the
%creation of TV topography.
%
%

%Stuff about prev erosion profile
p_eros_prof = current_temp_topo(round(temp_width),:);
p_eros_span = find(p_eros_prof < 0);
if max(p_eros_span) == length(p_eros_prof(1,:))
    ii = 0;
else
    ii = 1;
end
prev_slice = current_temp_topo(:,min(p_eros_span)-1:max(p_eros_span)+ii);
prev_E_slice = current_E_eff(min(p_eros_span)-1:max(p_eros_span)+ii);
prev_e_slice = current_e_eff(min(p_eros_span)-1:max(p_eros_span)+ii);
p_size = size(prev_slice);
%now finding the slice for Qtc
p_Qtc_span = find(current_Q_tc > 0);
if max(p_Qtc_span) == length(current_Q_tc)
    iii = 0;
else
    iii = 1;
end
prev_Qtc_slice = current_Q_tc(min(p_Qtc_span)-1:max(p_Qtc_span)+iii);
p_Qtc_size = size(prev_Qtc_slice);

%Stuff about next erosion profile
n_eros_prof = next_temp_topo(round(temp_width),:);
n_eros_span = find(n_eros_prof < 0);
if max(n_eros_span) == length(n_eros_prof(1,:))
    jj = 0;
else
    jj = 1;
end
next_slice = next_temp_topo(:,min(n_eros_span)-1:max(n_eros_span)+jj);
next_E_slice = next_E_eff(min(n_eros_span)-1:max(n_eros_span)+jj);
next_e_slice = next_e_eff(min(n_eros_span)-1:max(n_eros_span)+jj);
n_size = size(next_slice);

%now finding the slice for Qtc
n_Qtc_span = find(next_Q_tc > 0);
if max(n_Qtc_span) == length(next_Q_tc)
    jjj = 0;
else
    jjj = 1;
end
next_Qtc_slice = next_Q_tc(min(n_Qtc_span)-1:max(n_Qtc_span)+jjj);
n_Qtc_size = size(next_Qtc_slice);


%finding about the future size
int_slice_size_n = max(length(prev_slice(:,1)),length(next_slice(:,1)));
int_slice_size_m = max(length(prev_slice(1,:)),length(next_slice(1,:)));
int_slice_size_Qtc = max(length(prev_Qtc_slice),length(next_Qtc_slice));
%setting the size before interpolation
int_slice_prev = zeros(int_slice_size_n,int_slice_size_m);
int_slice_E_prev = zeros(1,int_slice_size_m);
int_slice_e_prev = zeros(1,int_slice_size_m);
int_slice_Qtc_prev = zeros(1,int_slice_size_Qtc);

int_slice_next = zeros(int_slice_size_n,int_slice_size_m);
int_slice_E_next = zeros(1,int_slice_size_m);
int_slice_e_next = zeros(1,int_slice_size_m);
int_slice_Qtc_next = zeros(1,int_slice_size_Qtc);

%now putting some numbers in there
%first for prev slice
if p_size(1) < int_slice_size_n
    p_n_diff = int_slice_size_n - p_size(1);
    %checking if number is even
    if mod(p_n_diff,2) == 0
        p_n_pos = p_n_diff./2;

        int_slice_prev(p_n_pos+1:end-p_n_pos,end-p_size(2)+1:end) = prev_slice;
    else
        p_n_pos1 = ceil(p_n_diff./2);
        p_n_pos2 = floor(p_n_diff./2);

        int_slice_prev(p_n_pos1+1:end-p_n_pos2,end-p_size(2)+1:end) = prev_slice;
    end
else
    int_slice_prev(:,end-p_size(2)+1:end) = prev_slice;
end
int_slice_E_prev(end-p_size(2)+1:end) = prev_E_slice;
int_slice_e_prev(end-p_size(2)+1:end) = prev_e_slice;
int_slice_Qtc_prev(end-p_Qtc_size(2)+1:end) = prev_Qtc_slice;

%then for next slice
if n_size(1) < int_slice_size_n
    n_n_diff = int_slice_size_n - n_size(1);
    %checking if number is even
    if mod(n_n_diff,2) == 0
        n_n_pos = n_n_diff./2;

        int_slice_next(n_n_pos+1:end-n_n_pos,end-n_size(2)+1:end) = next_slice;
    else
        n_n_pos1 = ceil(n_n_diff./2);
        n_n_pos2 = floor(n_n_diff./2);

        int_slice_next(n_n_pos1+1:end-n_n_pos2,end-n_size(2)+1:end) = next_slice;
    end
else
    int_slice_next(:,end-n_size(2)+1:end) = next_slice;
end
int_slice_E_next(end-n_size(2)+1:end) = next_E_slice;
int_slice_e_next(end-n_size(2)+1:end) = next_e_slice;
int_slice_Qtc_next(end-n_Qtc_size(2)+1:end) = next_Qtc_slice;

%Now computing the actually interpolated slices
int_slice = ((int_slice_prev .* (250 -tt)) + (int_slice_next .* tt)) ./ 250;
int_slice_E = ((int_slice_E_prev .* (250 -tt)) + (int_slice_E_next .* tt)) ./ 250;
int_slice_e = ((int_slice_e_prev .* (250 -tt)) + (int_slice_e_next .* tt)) ./ 250;
int_slice_Qtc = ((int_slice_Qtc_prev .* (250 -tt)) + (int_slice_Qtc_next .* tt)) ./ 250;

%Let see what happens with that but it's kindda uggly
E_eff_slice = int_slice_E;
e_eff_slice = int_slice_e;
Qtc_slice = int_slice_Qtc;

%Now put the slice at the right position along x
load('cheat_profs_TV_1yr.mat','int_terminus_pos');
%The -41 is because I'm pretty sure I cut at 410 and not 400 for the hydro stuff.
new_x_end_pos = int_terminus_pos(tt + (i-1)*250) -41;
%initializing the vectors and matrices to 0
temp_topo = zeros(int_slice_size_n,x_end);
temp_Qtc = cu_blc_topo(1,:); 
temp_E_eff = cu_blc_topo(1,:); 
temp_e_eff = cu_blc_topo(1,:); 

%Now putting everything back together
temp_topo(:,new_x_end_pos-length(int_slice(1,:))+1:new_x_end_pos) = int_slice;

temp_E_eff(new_x_end_pos-length(int_slice_E)+1:new_x_end_pos) = int_slice_E;
temp_e_eff(new_x_end_pos-length(int_slice_e)+1:new_x_end_pos) = int_slice_e;

temp_Qtc(new_x_end_pos-length(int_slice_Qtc)+1:new_x_end_pos) = int_slice_Qtc;

%keyboard;
