%File to plot the Nchannel of every simulations to understand a little more what is haapening
%
%
%

load ENTL_coarse

dx = 0.43;
x_st = 13;
x_end = 68;

%putting units
x_tot = linspace(x_st*dx,(x_end+x_st)*dx,x_end);

%Span of the valley in meters
valley_span = 200;

%Cross-section coordinate, a point each centimetre
y_tot = [0:1:200*100]./100./1000;
y_mid = valley_span/2*100 + 1;
y_end = valley_span*100 + 1;

for i=1:length(trans_IS)
    max_eros = 0;
    temp_max_eros = max(trans_IS(i).sum_e_eff);
    max_eros = max(max_eros,temp_max_eros);
    min_cont = round(max_eros,-1);
end

f1 = figure;
for i=1:length(trans_IS)
    %Check maximum width of N-channel
    temp_width = round(max(trans_IS(i).width).*100);
    %Sampling the domain to isolate the N-channel
    temp_topo = trans_IS(i).valley_topo(y_mid-temp_width:y_mid+temp_width,:);

    S{i} = subplot(3,5,i);
    %IM = imagesc(x_tot,trans_IS(i).width,temp_topo);
    [C,h] = contourf(x_tot,trans_IS(i).width,temp_topo);
    h.LineColor = 'none';
    h.LevelList = [-min_cont:10:0];
    %keyboard
    caxis([-min_cont 0]);
    cb = colorbar;
    cb.Limits = [-min_cont 0];
end

f2 = figure;
for i=1:length(trans_IS)
    S2{i} = subplot(3,5,i);
    plot(x_tot,trans_IS(i).sum_e_eff);
    hold on
    plot(x_tot,trans_IS(i).sum_E_eff);
    axis([0 max(x_tot) 0 450])
end

