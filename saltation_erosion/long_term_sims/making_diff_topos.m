%File to use the different time slices
%and put them together to end up with the final topography
%
%and then compare the difference depending on the lateral distribution used
%
%Step 1: Load a run series to construct the feature with
%Step 2: Run this script
%Step 3: Enjoy the plot

%dx = 0.43;
%x_st = 13;
%x_end = 68;

%% THAT WORKS FOR IG
%dx = 0.215;
%x_st = 26;
%x_end = 138;

%% THAT WORKS FOR IG
dx = 1000;
x_st = 1;
x_end = 541;

%putting units
x_tot = linspace(x_st*dx,(x_end+x_st)*dx,x_end);

%Span of the valley in meters
valley_span = 200;

%Cross-section coordinate, a point each centimetre
y_tot = [0:1:200*100]./100./1000;
y_mid = valley_span/2*100 + 1;
y_end = valley_span*100 + 1;

resulting_topoA = trans_IS(1).valley_topo.*0;
resulting_topoB = trans_IS(1).valley_topo.*0;
resulting_topoC = trans_IS(1).valley_topo.*0;
resulting_topoD = trans_IS(1).valley_topo.*0;
resulting_topoE = trans_IS(1).valley_topo.*0;

% APPARENTLY 250yrs between sims
dt_sims = 250;

for i = 1:length(trans_IS)
    %Check maximum width of N-channel
    temp_width = round(max(trans_IS(i).width).*100);
    %Sampling the domain to isolate the N-channel
    temp_topo = trans_IS(i).valley_topo(y_mid-temp_width:y_mid+temp_width,:);
    %Making sure erosion won't be applied outside the domain
    possible_span = y_mid -temp_width-1;

    %Defining a distribution along which to apply the erosion
    pd = makedist('Normal','mu',3,'sigma',1);

    %Apparently 300 years between sims in that simulation...
    for tt = 1:dt_sims
        %rand_pos = randi([y_mid-possible_span y_mid+possible_span],1,1);
        %along a normal distribution...
        rand_pos = round(pd.random./6.*y_end);
        if rand_pos >= y_end - temp_width
        %if rand_pos >= possible_span
            rand_pos = possible_span;
        elseif rand_pos <= temp_width
            rand_pos = temp_width +1;
        end
        y1_t = rand_pos-temp_width;
        y2_t = rand_pos+temp_width;
        resulting_topoA(y1_t:y2_t,:) = resulting_topoA(y1_t:y2_t,:) + temp_topo;
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% JUST A RANDOM DISTRIBUTION ACROSS THE WIDTH
for i = 1:length(trans_IS)
    %Check maximum width of N-channel
    temp_width = round(max(trans_IS(i).width).*100);
    %Sampling the domain to isolate the N-channel
    temp_topo = trans_IS(i).valley_topo(y_mid-temp_width:y_mid+temp_width,:);
    %Making sure erosion won't be applied outside the domain
    possible_span = y_mid -temp_width-1;

    %Defining a distribution along which to apply the erosion
    %pd = makedist('Normal','mu',3,'sigma',0.6);

    %Apparently 300 years between sims in that simulation...
    for tt = 1:dt_sims
        rand_pos = randi([y_mid-possible_span y_mid+possible_span],1,1);
        %along a normal distribution...
        %rand_pos = round(pd.random./6.*y_end);
        %rand_pos = round(pd.random./6.*y_mid);
        if rand_pos >= y_end - temp_width
        %if rand_pos >= possible_span
            rand_pos = possible_span;
        elseif rand_pos <= temp_width
            rand_pos = temp_width +1;
        end
        y1_t = rand_pos-temp_width;
        y2_t = rand_pos+temp_width;
        resulting_topoB(y1_t:y2_t,:) = resulting_topoB(y1_t:y2_t,:) + temp_topo;
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Normal distribution with a wider span, sigma = 1.5
for i = 1:length(trans_IS)
    %Check maximum width of N-channel
    temp_width = round(max(trans_IS(i).width).*100);
    %Sampling the domain to isolate the N-channel
    temp_topo = trans_IS(i).valley_topo(y_mid-temp_width:y_mid+temp_width,:);
    %Making sure erosion won't be applied outside the domain
    possible_span = y_mid -temp_width-1;

    %Defining a distribution along which to apply the erosion
    pd = makedist('Normal','mu',3,'sigma',1.4);

    %Apparently 300 years between sims in that simulation...
    for tt = 1:dt_sims
        %rand_pos = randi([y_mid-possible_span y_mid+possible_span],1,1);
        %along a normal distribution...
        rand_pos = round(pd.random./6.*y_end);
        if rand_pos >= y_end - temp_width
        %if rand_pos >= possible_span
            rand_pos = possible_span;
        elseif rand_pos <= temp_width
            rand_pos = temp_width +1;
        end
        y1_t = rand_pos-temp_width;
        y2_t = rand_pos+temp_width;
        resulting_topoC(y1_t:y2_t,:) = resulting_topoC(y1_t:y2_t,:) + temp_topo;
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Normal distribution with a narrower span, sigma = 0.5
for i = 1:length(trans_IS)
    %Check maximum width of N-channel
    temp_width = round(max(trans_IS(i).width).*100);
    %Sampling the domain to isolate the N-channel
    temp_topo = trans_IS(i).valley_topo(y_mid-temp_width:y_mid+temp_width,:);
    %Making sure erosion won't be applied outside the domain
    possible_span = y_mid -temp_width-1;

    %Defining a distribution along which to apply the erosion
    pd = makedist('Normal','mu',3,'sigma',0.6);

    for tt = 1:dt_sims
        %rand_pos = randi([y_mid-possible_span y_mid+possible_span],1,1);
        %along a normal distribution...
        rand_pos = round(pd.random./6.*y_end);
        if rand_pos >= y_end - temp_width
        %if rand_pos >= possible_span
            rand_pos = possible_span;
        elseif rand_pos <= temp_width
            rand_pos = temp_width +1;
        end
        y1_t = rand_pos-temp_width;
        y2_t = rand_pos+temp_width;
        resulting_topoD(y1_t:y2_t,:) = resulting_topoD(y1_t:y2_t,:) + temp_topo;
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% JUST A RANDOM DISTRIBUTION ACROSS THE WIDTH but only 100 metres
for i = 1:length(trans_IS)
    %Check maximum width of N-channel
    temp_width = round(max(trans_IS(i).width).*100);
    %Sampling the domain to isolate the N-channel
    temp_topo = trans_IS(i).valley_topo(y_mid-temp_width:y_mid+temp_width,:);
    %Making sure erosion won't be applied outside the domain
    possible_span = y_mid -temp_width-1;
    possible_span = round(possible_span/2);

    %Defining a distribution along which to apply the erosion
    %pd = makedist('Normal','mu',3,'sigma',0.6);

    %Apparently 250 years between sims in that simulation...
    for tt = 1:dt_sims
        rand_pos = randi([y_mid-possible_span y_mid+possible_span],1,1);
        %along a normal distribution...
        %rand_pos = round(pd.random./6.*y_end);
        %rand_pos = round(pd.random./6.*y_mid);
        if rand_pos >= y_end - temp_width
        %if rand_pos >= possible_span
            rand_pos = possible_span;
        elseif rand_pos <= temp_width
            rand_pos = temp_width +1;
        end
        y1_t = rand_pos-temp_width;
        y2_t = rand_pos+temp_width;
        resulting_topoE(y1_t:y2_t,:) = resulting_topoE(y1_t:y2_t,:) + temp_topo;
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Loading the longest bed
%load ELA1262_T11570_hyd_input
%load ../../subglacial_water_flow/coupled/ELA1262_initial_test_hyd_input/entlebuch_bed43km_200pts.mat

%That will work only for IG scenario... NO BED INTERP FOR TV
%load entlebuch_bed43km_200pts.mat
%bed_prof(152) = 630; %Removing a spiky bit...
%z_b = bed_prof(x_st:x_st +x_end);
%bed_interp = (z_b(2:end) + z_b(1:end-1)) ./ 2;
%bed_2d = repmat(bed_interp,y_end,1);

%Calculating the real resulting topo
resulting_topoA = resulting_topoA./1000;
resulting_topoB = resulting_topoB./1000;
resulting_topoC = resulting_topoC./1000;
resulting_topoD = resulting_topoD./1000;
resulting_topoE = resulting_topoE./1000;

%real_topoA = resulting_topoA + bed_2d;
%real_topoB = resulting_topoB + bed_2d;
%real_topoC = resulting_topoC + bed_2d;
%real_topoD = resulting_topoD + bed_2d;
%real_topoE = resulting_topoE + bed_2d;


plotting = 0;

if plotting == 1
    figure
    y_slice = [14:3:68];
    w1 = waterfall(resulting_topoA(5000:15000,y_slice)');
    w1.FaceColor = 'none';
    w1.LineWidth = 2;
    w1.CData(1,:) = nan;
    w1.CData(end-2:end,:) = nan;

    figure
    x_slice = [7500 9000 10000 11000 12500];
    w2 = waterfall(resulting_topoA(x_slice,:));
    w2.FaceColor = 'none';
    w2.LineWidth = 2;
    w2.CData(1,:) = nan;
    w2.CData(end-2:end,:) = nan;
end
