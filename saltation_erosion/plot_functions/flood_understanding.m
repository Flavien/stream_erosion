%Little file to plot and understand what is going on during the floods...
%
%
%

load trans_IS_flood_test

figure(1)
sx =2; sy = 5;

subplot(sx,sy,1)
k=1;
contourf(trans_IS(k).E_tot)
colorbar

subplot(sx,sy,2)
k=k+1;
contourf(trans_IS(k).E_tot)
colorbar

subplot(sx,sy,3)
k=k+1;
contourf(trans_IS(k).E_tot)
colorbar

subplot(sx,sy,4)
k=k+1;
contourf(trans_IS(k).E_tot)
colorbar

subplot(sx,sy,5)
k=k+1;
contourf(trans_IS(k).E_tot)
colorbar

subplot(sx,sy,7)
k=k+1;
contourf(trans_IS(k).E_tot)
colorbar

subplot(sx,sy,8)
k=k+1;
contourf(trans_IS(k).E_tot)
colorbar

subplot(sx,sy,9)
k=k+1;
contourf(trans_IS(k).E_tot)
colorbar

subplot(sx,sy,10)
k=k+1;
contourf(trans_IS(k).E_tot)
colorbar


subplot(sx,sy,6)
plot(trans_IS(2).Qc(:,1))
hold on
plot(trans_IS(3).Qc(:,1))
plot(trans_IS(4).Qc(:,1))
plot(trans_IS(5).Qc(:,1))


%%%%%%%%%%%%%%%%%%%%%
figure(2)
sx =2; sy = 5;

subplot(sx,sy,1)
k=1;
contourf(trans_IS(k).e_tot)
colorbar

subplot(sx,sy,2)
k=k+1;
contourf(trans_IS(k).e_tot)
colorbar

subplot(sx,sy,3)
k=k+1;
contourf(trans_IS(k).e_tot)
colorbar

subplot(sx,sy,4)
k=k+1;
contourf(trans_IS(k).e_tot)
colorbar

subplot(sx,sy,5)
k=k+1;
contourf(trans_IS(k).e_tot)
colorbar

subplot(sx,sy,7)
k=k+1;
contourf(trans_IS(k).e_tot)
colorbar

subplot(sx,sy,8)
k=k+1;
contourf(trans_IS(k).e_tot)
colorbar

subplot(sx,sy,9)
k=k+1;
contourf(trans_IS(k).e_tot)
colorbar

subplot(sx,sy,10)
k=k+1;
contourf(trans_IS(k).e_tot)
colorbar


%%%%%%%%%%%%%%%%%%%%%
figure(3)
sx =2; sy = 5;

subplot(sx,sy,1)
k=1;
contourf(trans_IS(k).R.*2)
colorbar

subplot(sx,sy,2)
k=k+1;
contourf(trans_IS(k).R.*2)
colorbar

subplot(sx,sy,3)
k=k+1;
contourf(trans_IS(k).R.*2)
colorbar

subplot(sx,sy,4)
k=k+1;
contourf(trans_IS(k).R.*2)
colorbar

subplot(sx,sy,5)
k=k+1;
contourf(trans_IS(k).R.*2)
colorbar

subplot(sx,sy,7)
k=k+1;
contourf(trans_IS(k).R.*2)
colorbar

subplot(sx,sy,8)
k=k+1;
contourf(trans_IS(k).R.*2)
colorbar

subplot(sx,sy,9)
k=k+1;
contourf(trans_IS(k).R.*2)
colorbar

subplot(sx,sy,10)
k=k+1;
contourf(trans_IS(k).R.*2)
colorbar
