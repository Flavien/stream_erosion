%This script is used to the results of the simulations targetted toward
%the testing of mass conservation after sediment transport is introduced.

n_nodes = min(size(eta_ar));
x_plot = [0:dx:(n_nodes-1).*dx];
t_plot = [1:length(eta_ar)].*dt./3600;

MS  = 0;
LW  = 2;
fx = 0.55;
fy = 0.45;
FSlab = 14;
FSax  = 14;

%ln_spec_col = {'k','b','r','m','k','b','r','m'} ;
ln_spec_col = {rgb('Red'),rgb('Green'),rgb('Blue'),rgb('Purple'),...
rgb('DeepPink'),rgb('Brown'),rgb('Orange'),rgb('Black'),rgb('DimGray'),rgb('DarkGreen')};
ln_spec_type = {'-','-','-','-','-','--','--','--','--','-.','-.','-.','-.'};
size_color = length(ln_spec_col);
size_type = length(ln_spec_type);

time_to_plot = [1 10 20 30 40 50 60 70 80 90 100 150 200 250 300 350 400]; % D = 60 mm
%time_to_plot = [1 20 40 60 80 100 120 140 150 160 170 180 200 250 300 350 400]; % D = 1mm
for i=1:length(time_to_plot)
    legend_entry{i} = strcat(num2str(time_to_plot(i)*dt./3600),' hrs');
end

plot_resolution = 25;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           FIGURE
f1 = figure('NumberTitle','off','Name','Sediment thickness');
clf
set(f1,'Units','normalized');
%set(f1,'Position',[0.2,0.4,fx,fy]);

%v_plot1 = linspace(0,0.25,25); % testA D=1
%v_plot1 = linspace(0,1e-3,25); % testB D=1
%v_plot1 = linspace(0,0.05,25); % testC D=1
v_plot1 = linspace(0,1.2,plot_resolution); % testA D=1 / channel W
clim1 = [v_plot1(1) v_plot1(end)];

imagesc(x_plot,t_plot,eta_ar,clim1)
%imagesc(cumsum(dx_interp),t_plot,eta_int_ar,clim1)
%imagesc(x_map_avg,t_plot,eta_int_ar,clim1)
%imagesc(x_plot,t_plot,eta_ar.*2.*R,clim1)
%contourf(x_plot,t_plot,eta_ar,v_plot1)
set(gca,'YDir','normal');

colorbar
shading flat
%set(gca,'Xtick',0:0.2:1)
set(gca,'FontSize',FSax)
xlabel('Distance x (m)')
ylabel('Time during simulation (hr)')
title('\eta (m)')


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           FIGURE
f2 = figure('NumberTitle','off','Name','Total sediment supply');
clf
set(f2,'Units','normalized');
%set(f2,'Position',[0.2,0.4,fx,fy]);

v_plot2 = linspace(0,max(max(q_tot_temp)),plot_resolution); % testA D=1
%v_plot2 = linspace(0,2.2e-4,25); % testB D=1
%v_plot2 = linspace(0,0.01,25); % testC D=1
clim2 = [v_plot2(1) v_plot2(end)];

imagesc(x_plot,t_plot,q_tot_temp,clim2)
%contourf(x_plot,t_plot,q_tot_temp,v_plot2)
set(gca,'YDir','normal');

colorbar
shading flat
set(gca,'FontSize',FSax)
xlabel('Distance x (m)')
ylabel('Time during simulation (hr)')
title('q_{tot} (m^2/s)')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           FIGURE
f3 = figure('NumberTitle','off','Name','Total load erosion');
clf
set(f3,'Units','normalized');
%set(f3,'Position',[0.2,0.4,fx,fy]);
width_fact = 2.*R;

v_plot3 = linspace(0,max(max(e_tot_ar.*width_fact)).*1e3,plot_resolution);
clim3 = [v_plot3(1) v_plot3(end)];

if clim3(2) == 0
    clim3(2) = 1e-6;
end

im_etot_cont = logspace(log10(0.1),log10(500),plot_resolution);
log_im_etot = log10(im_etot_cont);
log_data = log10(e_tot_ar.*1e3.*width_fact);

imagesc(x_plot,t_plot,log_data,[log_im_etot(1) log_im_etot(end)])
colorbar('YTick',log_im_etot,'YTickLabel',im_etot_cont);
%imagesc(x_plot,t_plot,e_tot_ar.*1e3.*width_fact,clim3)
%contourf(x_plot,t_plot,e_tot_ar.*1e3,v_plot3)
set(gca,'YDir','normal');

%colorbar
shading flat
set(gca,'FontSize',FSax)
xlabel('Distance x (m)')
ylabel('Time during simulation (hr)')
title('Total erosion with total load model (mm/a)')


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           FIGURE
f4 = figure('NumberTitle','off','Name','Fraction exposed');
clf
set(f4,'Units','normalized');
%set(f4,'Position',[0.2,0.4,fx,fy]);

%v_plot4 = linspace(0,1,25);

%contourf(x_plot,t_plot,Fe_ar,v_plot4)
imagesc(x_plot,t_plot,Fe_tot_ar)
set(gca,'YDir','normal');

colorbar
shading flat
set(gca,'FontSize',FSax)
xlabel('Distance x (m)')
ylabel('Time during simulation (hr)')
title('Fraction exposed (total load model)')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           FIGURE
f5 = figure('NumberTitle','off','Name','Saltation abrasion');
clf
set(f5,'Units','normalized');
%set(f5,'Position',[0.2,0.4,fx,fy]);

v_plot5 = linspace(0,max(max(e_salt_ar.*width_fact)).*1e3,plot_resolution);
clim5 = [v_plot5(1) v_plot5(end)];

if clim5(2) == 0
    clim5(2) = 1e-6;
end

%im_etot_cont = logspace(log10(500),log10(3e5),plot_resolution);
%log_im_etot = log10(im_etot_cont);
log_data2 = log10(e_salt_ar.*1e3.*width_fact);

imagesc(x_plot,t_plot,log_data2,[log_im_etot(1) log_im_etot(end)])
colorbar('YTick',log_im_etot,'YTickLabel',im_etot_cont);

%imagesc(x_plot,t_plot,e_salt_ar.*1e3.*width_fact,clim5)
%contourf(x_plot,t_plot,e_salt_ar.*1e3,v_plot5)
set(gca,'YDir','normal');

%colorbar
shading flat
set(gca,'FontSize',FSax)
xlabel('Distance x (m)')
ylabel('Time during simulation (hr)')
title('Total erosion with saltation-erosion model (mm/a)')


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           FIGURE
f6 = figure('NumberTitle','off','Name','spatialy integrated eta');
clf
set(f6,'Units','normalized');
set(f6,'Position',[0.2,0.4,fx,fy]);


%int_eta_dx = sum(eta_ar') ./dx;
%int_eta_dx = sum(eta_ar')./51;
%int_eta_dx = sum((eta_ar.*2.*R)')./51;
int_eta_dx = sum((eta_int_ar.*2.*repmat(R_interp,length(eta_ar),1))')./length(eta_int_ar);

fp6 = fancy_plot(t_plot,int_eta_dx,MS,LW,ln_spec_col{3},ln_spec_type{3});

%l1 = legend(legend_entry);
%set(l1,'Location','NorthWest')
%axis([0 80 0 0.012]) % Most of the time!
%axis([0 80 0 0.034]) % Most of the time!
set(gca,'FontSize',FSax)
xlabel('Time during the simulation (hrs)')
ylabel('Spatially integrated avg sediment thickness (m)')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           FIGURE
f7 = figure('NumberTitle','off','Name','Total volumetric transport capacity');
clf
set(f7,'Units','normalized');
set(f7,'Position',[0.2,0.4,fx,fy]);

fp7 = fancy_plot(x_plot,q_tc_ar(end,:)./constants.rho_s.*width_fact(end,:),MS,LW,ln_spec_col{3},ln_spec_type{3});

%l1 = legend(legend_entry);
%set(l1,'Location','NorthWest')
set(gca,'FontSize',FSax)
xlabel('Distance x (m)')
ylabel('Total volumetric transport capacity (m^3/s)')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           FIGURE
f8 = figure('NumberTitle','off','Name','Impact vel');
clf
set(f8,'Units','normalized');
%set(f8,'Position',[0.2,0.4,fx,fy]);

v_plot8 = linspace(min(min(w_i_eff_ar)),max(max(w_i_eff_ar)),plot_resolution);
clim8 = [v_plot8(1) v_plot8(end)];

imagesc(x_plot,t_plot,w_i_eff_ar,clim8)
%contourf(x_plot,t_plot,w_i_eff_ar,v_plot8)
set(gca,'YDir','normal');
%imagesc(x_plot,t_plot,w_i_eff_ar)

colorbar
shading flat
set(gca,'FontSize',FSax)
xlabel('Distance x (m)')
ylabel('Time during simulation (hr)')
title('Effective impact velocity (m/s)')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           FIGURE
f9 = figure('NumberTitle','off','Name','Near bed concentration');
clf
set(f9,'Units','normalized');
%set(f9,'Position',[0.2,0.4,fx,fy]);

%v_plot9 = linspace(0,1,25); % testA D=1
%v_plot9 = linspace(0,0.025,25); % testB D=1
max_cb = max(max(cb_ar));
v_plot9 = linspace(0,max_cb,plot_resolution); % test C D=1
clim9 = [v_plot9(1) v_plot9(end)];

imagesc(x_plot,t_plot,cb_ar,clim9)
%imagesc(x_plot,t_plot,cb_ar,v_plot9)
set(gca,'YDir','normal');
%imagesc(x_plot,t_plot,cb_ar)

colorbar
shading flat
set(gca,'FontSize',FSax)
xlabel('Distance x (m)')
ylabel('Time during simulation (hr)')
title('Near-bed sed concentration')

