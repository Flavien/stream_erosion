%Function to plot surf plot of interesting quantities to understand changes
%how the R-channel and saltation erosion model interact
%
%

t_start = 3600;
t_last = length(E_ar);

f_start = 1;

figure(f_start)
surf(E_ar(t_start:t_last,:))
shading flat
colorbar
xlabel('Distance x')
ylabel('Time (nb tstp)')
title('Eros rate, simplified expression (m/a)')

k = 1;
figure(f_start+k)
surf(e_salt_ar(t_start:t_last,:))
shading flat
colorbar
xlabel('Distance x')
ylabel('Time (nb tstp)')
title('Eros rate, composite expression (m/a)')

k = k+1;
figure(f_start + k)
surf(tau_ratio(t_start:t_last,:) -1)
shading flat
colorbar
xlabel('Distance x')
ylabel('Time (nb tstp)')
title('\tau^* / \tau^*_c -1')

k = k + 1;
figure(f_start + k)
surf(Fe_ar(t_start:t_last,:))
shading flat
colorbar
xlabel('Distance x')
ylabel('Time (nb tstp)')
title('Fraction exposed')

k = k + 1;
figure(f_start + k)
surf(Ir_ar(t_start:t_last,:))
shading flat
colorbar
xlabel('Distance x')
ylabel('Time (nb tstp)')
title('Impact rate')

k = k + 1;
figure(f_start + k)
surf(Vi_ar(t_start:t_last,:))
shading flat
colorbar
xlabel('Distance x')
ylabel('Time (nb tstp)')
title('Volume removed / impact')

k = k + 1;
figure(f_start + k)
surf(q_tc_ar(t_start:t_last,:))
shading flat
colorbar
xlabel('Distance x')
ylabel('Time (nb tstp)')
title('Transport capacity')

k = k + 1;
figure(f_start + k)
surf(u_shear_ar(t_start:t_last,:))
shading flat
colorbar
xlabel('Distance x')
ylabel('Time (nb tstp)')
title('Shear velocity')
