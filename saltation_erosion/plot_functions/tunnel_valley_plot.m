%Mostly plot for fun to show the shape of a tunnel valley carved by the model.
%
%
%

%E_eff_tv = e_tot_ar.*600./constants.secondsinyear.*1000;
%E_eff_tv = e_eff./1000;
E_eff_tv = e_eff;
%E_eff_tv(R < 0.2) = 0;

%R = R.*5;

R_max = max(max(R));

R_discrete = round(R./(R_max/100));


time_tot = length(E_eff);

plot_only = 0;

%E_eff = E_eff.*200;

if plot_only == 0
    j_end = min(size(R));
    valley_topo = zeros(201,j_end);
        for i =1:time_tot
            for j = 1:j_end
                valley_topo(101-R_discrete(i,j):101+R_discrete(i,j),j) = ...
                        valley_topo(101-R_discrete(i,j):101+R_discrete(i,j),j) - E_eff_tv(i,j);
            end
        end
end


max_depth = min(min(valley_topo));
%And now plotting it
x_stag = linspace(0.5,9.5,100);
width(1:101) = linspace(1,0,101).*-R_max;
width(101:201) = linspace(0,1,101).*R_max;

%{
%figure
%surf(x_stag,width,valley_topo)
contourf(x_stag,width,valley_topo,'LevelStep',0.001,'linestyle','none')
%contour(x_stag,width,valley_topo,[0:0.005:min(min(valley_topo))],'textlist',[0 0.01 0.02 0.03])
colormap gray
shading flat
col_b = colorbar;
col_b.Label.String = 'Depth (mm)';
col_b.Label.Color = [0.8 0.8 0.8];
%col_b.Ticks = [-25 -20 -15 -10 -5 0];
col_b.Location = 'south';

xlabel('Distance along the bed (km)')
ylabel('Valley radius (m)')
%zlabel('Depth of the valley (m)')
%}
