%Little piece of script that is not even a function to add a legend
%within my for loops while plotting stuffs

if i == 1
    legend(legend_text{i});
    legend('hide')
    [LEGH,OBJH,OUTH,OUTM] = legend;
    %legend([OUTH;fp1],OUTM{:},legend_text{i})
    legend([fp1],OUTM{:},legend_text{i})

else
    [LEGH,OBJH,OUTH,OUTM] = legend;
    legend([OUTH;fp1],OUTM{:},legend_text{i})
end
