figure(1)
surf(eta_ar)
shading interp
title('eta')

figure(2)
surf(q_tot_temp.*constants.rho_s)
shading interp
title('q_{tot}')

figure(3)
surf(e_salt_ar)
shading interp
title('saltation abrasion')

figure(4)
surf(e_tot_ar)
shading interp
title('total load abrasion')

figure(5)
surf(q_tc_ar)
shading interp
title('q_{tc}')

figure(6)
surf(Fe_ar)
shading interp
title('Fe')

figure(7)
surf(q_sus_ar)
shading interp
title('suspended flux')

figure(8)
surf(w_i_eff_ar)
shading interp
title('Effective impact velocity')

figure(9)
surf(cb_ar)
shading interp
colorbar
title('Near bed concentration')
