%This script is used to the results of the simulations targetted toward
%the testing of mass conservation after sediment transport is introduced.

n_nodes = min(size(eta_ar));
x_plot = [0:dx:(n_nodes-1).*dx];

MS  = 0;
LW  = 4;
fx = 0.55;
fy = 0.4;
FSlab = 32;
FSax  = 32;

%ln_spec_col = {'k','b','r','m','k','b','r','m'} ;
ln_spec_col = {rgb('Red'),rgb('Green'),rgb('Blue'),rgb('Purple'),...
                rgb('DeepPink'),rgb('Brown'),rgb('Orange'),rgb('Black'),rgb('DimGray'),rgb('DarkGreen')};
ln_spec_type = {'-','-','-','-','-','--','--','--','--','-.','-.','-.','-.'};
size_color = length(ln_spec_col);
size_type = length(ln_spec_type);

time_to_plot = [1 10 20 30 50 70]; % D = 60 mm
%time_to_plot = [1 20 40 60 80 100 120 140 150 160 170 180 200 250 300 350 400]; % D = 1mm
for i=1:length(time_to_plot)
    legend_entry{i} = strcat(num2str(time_to_plot(i)*dt./3600),' hrs');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           FIGURE
f1 = figure('NumberTitle','off','Name','Sediment thickness');
clf
set(f1,'Units','normalized');
set(f1,'Position',[0.2,0.4,fx,fy]);

k=1;
kk=1;
for i=time_to_plot
    fp1(k) = fancy_plot(x_plot,eta_ar(i,:),MS,LW,ln_spec_col{k},ln_spec_type{kk});
    hold on
    k=k+1;
    kk = kk + 1;
    k=mod(k,size_color-1);
    kk=mod(kk,size_type-1);
    if k==0
        k=size_color;
    end
    if kk==0
        kk=size_type;
    end
end
l1 = legend(legend_entry);
set(l1,'Location','NorthWest')
set(gca,'FontSize',FSax)
xlabel('Distance x (m)')
ylabel('\eta (m)')



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           FIGURE
f3 = figure('NumberTitle','off','Name','Total load erosion');
clf
set(f3,'Units','normalized');
set(f3,'Position',[0.2,0.4,fx,fy]);

k=1;
kk=1;
for i=time_to_plot
    fp3(k) = fancy_plot(x_plot,e_tot_ar(i,:),MS,LW,ln_spec_col{k},ln_spec_type{kk});
    hold on
    k=k+1;
    kk = kk + 1;
    k=mod(k,size_color-1);
    kk=mod(kk,size_type-1);
    if k==0
        k=size_color;
    end
    if kk==0
        kk=size_type;
    end
end
l1 = legend(legend_entry);
set(l1,'Location','NorthWest')
set(gca,'FontSize',FSax)
xlabel('Distance x (m)')
ylabel('Total load erosion rate (m/a)')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           FIGURE
f4 = figure('NumberTitle','off','Name','Fraction exposed');
clf
set(f4,'Units','normalized');
set(f4,'Position',[0.2,0.4,fx,fy]);

k=1;
kk=1;
for i=time_to_plot
    fp3(k) = fancy_plot(x_plot,Fe_ar(i,:),MS,LW,ln_spec_col{k},ln_spec_type{kk});
    hold on
    k=k+1;
    kk = kk + 1;
    k=mod(k,size_color-1);
    kk=mod(kk,size_type-1);
    if k==0
        k=size_color;
    end
    if kk==0
        kk=size_type;
    end
end
l1 = legend(legend_entry);
set(l1,'Location','NorthWest')
set(gca,'FontSize',FSax)
xlabel('Distance x (m)')
ylabel('Fe')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           FIGURE
f5 = figure('NumberTitle','off','Name','Saltation abrasion');
clf
set(f5,'Units','normalized');
set(f5,'Position',[0.2,0.4,fx,fy]);

k=1;
kk=1;
for i=time_to_plot
    fp3(k) = fancy_plot(x_plot,e_salt_ar(i,:),MS,LW,ln_spec_col{k},ln_spec_type{kk});
    hold on
    k=k+1;
    kk = kk + 1;
    k=mod(k,size_color-1);
    kk=mod(kk,size_type-1);
    if k==0
        k=size_color;
    end
    if kk==0
        kk=size_type;
    end
end
l1 = legend(legend_entry);
set(l1,'Location','NorthWest')
set(gca,'FontSize',FSax)
xlabel('Distance x (m)')
ylabel('Saltation load erosion (m/a)')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           FIGURE
f6 = figure('NumberTitle','off','Name','spatialy integrated eta');
clf
set(f6,'Units','normalized');
set(f6,'Position',[0.2,0.4,fx,fy]);

t_plot = [1:length(eta_ar)].*dt./3600;

int_eta_dx = sum(eta_ar') ./dx;

fp6 = fancy_plot(t_plot,int_eta_dx,MS,LW,ln_spec_col{3},ln_spec_type{3});

%l1 = legend(legend_entry);
%set(l1,'Location','NorthWest')
axis([0 80 0 0.012])
set(gca,'FontSize',FSax)
xlabel('Time during the simulation (hrs)')
ylabel('Spatially integrated avg sediment thickness (m)')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           FIGURE
f7 = figure('NumberTitle','off','Name','spatialy integrated eta');
clf
set(f7,'Units','normalized');
set(f7,'Position',[0.2,0.4,fx,fy]);

fp7 = fancy_plot(x_plot,q_tc_ar(200,:),MS,LW,ln_spec_col{3},ln_spec_type{3});

%l1 = legend(legend_entry);
%set(l1,'Location','NorthWest')
set(gca,'FontSize',FSax)
xlabel('Distance x (m)')
ylabel('Transport capacity (kg/s)')

