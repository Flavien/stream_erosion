%File to load the TR simulations 
%Calling the function to compute what I need
%Storing it in a structure
%Ready to send for plotting

function [trans_IS] = load_IS_TR_comp()

list_flood_comp = {'ANALYSIS_ref_IS_BenchC_Atb.mat',...
                    'AN_FL_Nye_10days_BenchC_Atb.mat',...
                    'AN_FL_Nye_20days_BenchC_Atb.mat',...
                    'AN_FL_Nye_30days_BenchC_Atb.mat',...
                    'AN_FL_Nye_40days_BenchC_Atb.mat',...
                    'AN_FL_Nye_10days_Vx2_BenchC_Atb.mat',...
                    'AN_FL_Nye_20days_Vx2_BenchC_Atb.mat',...
                    'AN_FL_Nye_30days_Vx2_BenchC_Atb.mat',...
                    'AN_FL_Nye_40days_Vx2_BenchC_Atb.mat'};

list_all_floods = {'FL_Nye_10days_Vx01_BenchC_Atb.mat',...
                    'FL_Nye_10days_BenchC_Atb.mat',...
                    'FL_Nye_10days_Vx2_BenchC_Atb.mat',...
                    'FL_Nye_10days_Vx3_BenchC_Atb.mat',...
                    'FL_Nye_10days_Vx4_BenchC_Atb.mat',...
                    'FL_Nye_10days_Vx5_BenchC_Atb.mat',...
                    'FL_Nye_10days_Vx6_BenchC_Atb.mat',...
                    'FL_Nye_10days_Vx7_BenchC_Atb.mat',...
                    'FL_Nye_10days_Vx8_BenchC_Atb.mat',...
                    'FL_Nye_10days_Vx9_BenchC_Atb.mat',...
                    'FL_Nye_10days_Vx10_BenchC_Atb.mat',...
                    'FL_Nye_20days_Vx01_BenchC_Atb.mat',...
                    'FL_Nye_20days_BenchC_Atb.mat',...
                    'FL_Nye_20days_Vx2_BenchC_Atb.mat',...
                    'FL_Nye_20days_Vx3_BenchC_Atb.mat',...
                    'FL_Nye_20days_Vx4_BenchC_Atb.mat',...
                    'FL_Nye_20days_Vx5_BenchC_Atb.mat',...
                    'FL_Nye_20days_Vx6_BenchC_Atb.mat',...
                    'FL_Nye_20days_Vx7_BenchC_Atb.mat',...
                    'FL_Nye_20days_Vx8_BenchC_Atb.mat',...
                    'FL_Nye_20days_Vx9_BenchC_Atb.mat',...
                    'FL_Nye_20days_Vx10_BenchC_Atb.mat',...
                    'FL_Nye_30days_Vx01_BenchC_Atb.mat',...
                    'FL_Nye_30days_BenchC_Atb.mat',...
                    'FL_Nye_30days_Vx2_BenchC_Atb.mat',...
                    'FL_Nye_30days_Vx3_BenchC_Atb.mat',...
                    'FL_Nye_30days_Vx4_BenchC_Atb.mat',...
                    'FL_Nye_30days_Vx5_BenchC_Atb.mat',...
                    'FL_Nye_30days_Vx6_BenchC_Atb.mat',...
                    'FL_Nye_30days_Vx7_BenchC_Atb.mat',...
                    'FL_Nye_30days_Vx8_BenchC_Atb.mat',...
                    'FL_Nye_30days_Vx9_BenchC_Atb.mat',...
                    'FL_Nye_30days_Vx10_BenchC_Atb.mat',...
                    'FL_Nye_40days_Vx01_BenchC_Atb.mat',...
                    'FL_Nye_40days_BenchC_Atb.mat',...
                    'FL_Nye_40days_Vx2_BenchC_Atb.mat',...
                    'FL_Nye_40days_Vx3_BenchC_Atb.mat',...
                    'FL_Nye_40days_Vx4_BenchC_Atb.mat',...
                    'FL_Nye_40days_Vx5_BenchC_Atb.mat',...
                    'FL_Nye_40days_Vx6_BenchC_Atb.mat',...
                    'FL_Nye_40days_Vx7_BenchC_Atb.mat',...
                    'FL_Nye_40days_Vx8_BenchC_Atb.mat',...
                    'FL_Nye_40days_Vx9_BenchC_Atb.mat',...
                    'FL_Nye_40days_Vx10_BenchC_Atb.mat',...
                    'FL_Nye_50days_Vx01_BenchC_Atb.mat',...
                    'FL_Nye_50days_BenchC_Atb.mat',...
                    'FL_Nye_50days_Vx2_BenchC_Atb.mat',...
                    'FL_Nye_50days_Vx3_BenchC_Atb.mat',...
                    'FL_Nye_50days_Vx4_BenchC_Atb.mat',...
                    'FL_Nye_50days_Vx5_BenchC_Atb.mat',...
                    'FL_Nye_50days_Vx6_BenchC_Atb.mat',...
                    'FL_Nye_50days_Vx7_BenchC_Atb.mat',...
                    'FL_Nye_50days_Vx8_BenchC_Atb.mat',...
                    'FL_Nye_50days_Vx9_BenchC_Atb.mat',...
                    'FL_Nye_50days_Vx10_BenchC_Atb.mat',...
                    'FL_Nye_60days_Vx01_BenchC_Atb.mat',...
                    'FL_Nye_60days_BenchC_Atb.mat',...
                    'FL_Nye_60days_Vx2_BenchC_Atb.mat',...
                    'FL_Nye_60days_Vx3_BenchC_Atb.mat',...
                    'FL_Nye_60days_Vx4_BenchC_Atb.mat',...
                    'FL_Nye_60days_Vx5_BenchC_Atb.mat',...
                    'FL_Nye_60days_Vx6_BenchC_Atb.mat',...
                    'FL_Nye_60days_Vx7_BenchC_Atb.mat',...
                    'FL_Nye_60days_Vx8_BenchC_Atb.mat',...
                    'FL_Nye_60days_Vx9_BenchC_Atb.mat',...
                    'FL_Nye_60days_Vx10_BenchC_Atb.mat'};
                    

list_all_fl_D256 = {'FL_Nye_10days_Vx01_D256.mat',...
                    'FL_Nye_10days_D256.mat',...
                    'FL_Nye_10days_Vx2_D256.mat',...
                    'FL_Nye_10days_Vx3_D256.mat',...
                    'FL_Nye_10days_Vx4_D256.mat',...
                    'FL_Nye_10days_Vx5_D256.mat',...
                    'FL_Nye_10days_Vx6_D256.mat',...
                    'FL_Nye_10days_Vx7_D256.mat',...
                    'FL_Nye_10days_Vx8_D256.mat',...
                    'FL_Nye_10days_Vx9_D256.mat',...
                    'FL_Nye_10days_Vx10_D256.mat',...
                    'FL_Nye_20days_Vx01_D256.mat',...
                    'FL_Nye_20days_D256.mat',...
                    'FL_Nye_20days_Vx2_D256.mat',...
                    'FL_Nye_20days_Vx3_D256.mat',...
                    'FL_Nye_20days_Vx4_D256.mat',...
                    'FL_Nye_20days_Vx5_D256.mat',...
                    'FL_Nye_20days_Vx6_D256.mat',...
                    'FL_Nye_20days_Vx7_D256.mat',...
                    'FL_Nye_20days_Vx8_D256.mat',...
                    'FL_Nye_20days_Vx9_D256.mat',...
                    'FL_Nye_20days_Vx10_D256.mat',...
                    'FL_Nye_30days_Vx01_D256.mat',...
                    'FL_Nye_30days_D256.mat',...
                    'FL_Nye_30days_Vx2_D256.mat',...
                    'FL_Nye_30days_Vx3_D256.mat',...
                    'FL_Nye_30days_Vx4_D256.mat',...
                    'FL_Nye_30days_Vx5_D256.mat',...
                    'FL_Nye_30days_Vx6_D256.mat',...
                    'FL_Nye_30days_Vx7_D256.mat',...
                    'FL_Nye_30days_Vx8_D256.mat',...
                    'FL_Nye_30days_Vx9_D256.mat',...
                    'FL_Nye_30days_Vx10_D256.mat',...
                    'FL_Nye_40days_Vx01_D256.mat',...
                    'FL_Nye_40days_D256.mat',...
                    'FL_Nye_40days_Vx2_D256.mat',...
                    'FL_Nye_40days_Vx3_D256.mat',...
                    'FL_Nye_40days_Vx4_D256.mat',...
                    'FL_Nye_40days_Vx5_D256.mat',...
                    'FL_Nye_40days_Vx6_D256.mat',...
                    'FL_Nye_40days_Vx7_D256.mat',...
                    'FL_Nye_40days_Vx8_D256.mat',...
                    'FL_Nye_40days_Vx9_D256.mat',...
                    'FL_Nye_40days_Vx10_D256.mat',...
                    'FL_Nye_50days_Vx01_D256.mat',...
                    'FL_Nye_50days_D256.mat',...
                    'FL_Nye_50days_Vx2_D256.mat',...
                    'FL_Nye_50days_Vx3_D256.mat',...
                    'FL_Nye_50days_Vx4_D256.mat',...
                    'FL_Nye_50days_Vx5_D256.mat',...
                    'FL_Nye_50days_Vx6_D256.mat',...
                    'FL_Nye_50days_Vx7_D256.mat',...
                    'FL_Nye_50days_Vx8_D256.mat',...
                    'FL_Nye_50days_Vx9_D256.mat',...
                    'FL_Nye_50days_Vx10_D256.mat',...
                    'FL_Nye_60days_Vx01_D256.mat',...
                    'FL_Nye_60days_D256.mat',...
                    'FL_Nye_60days_Vx2_D256.mat',...
                    'FL_Nye_60days_Vx3_D256.mat',...
                    'FL_Nye_60days_Vx4_D256.mat',...
                    'FL_Nye_60days_Vx5_D256.mat',...
                    'FL_Nye_60days_Vx6_D256.mat',...
                    'FL_Nye_60days_Vx7_D256.mat',...
                    'FL_Nye_60days_Vx8_D256.mat',...
                    'FL_Nye_60days_Vx9_D256.mat',...
                    'FL_Nye_60days_Vx10_D256.mat'};
                    

list_fl_D256 = {'FL_Nye_30days_D256.mat',...
                'FL_Nye_30days_Vx2_D256.mat',...
                'FL_Nye_30days_Vx3_D256.mat',...
                'FL_Nye_30days_Vx4_D256.mat',...
                'FL_Nye_30days_Vx5_D256.mat',...
                'FL_Nye_30days_Vx6_D256.mat',...
                'FL_Nye_30days_Vx7_D256.mat',...
                'FL_Nye_30days_Vx8_D256.mat',...
                'FL_Nye_30days_Vx9_D256.mat',...
                'FL_Nye_30days_Vx10_D256.mat'};

list_fining = {'ANALYSIS_ref_IS_BenchC_Atb.mat',...
                'AN_TR_IS_SD3_01_DwnFinA.mat',...
                'AN_TR_IS_SD3_01_DwnFinB.mat',...
                'AN_TR_IS_SD3_01_DwnFinC.mat',...
                'AN_TR_IS_SD3_01_DwnFinD.mat'};

list_Seas_D256 = {'TR_IS_SD3_BenchC_D256.mat',...
                'TR_IS_SD3_BenchC_D256.mat'};

spin_test = {'TR_ENTL_B2_HR_Rub_BenchC_Atb.mat',...
            'spin_TR_ENTL_B2_HR_Rub_BenchC_Atb.mat'};

%run_list = list_topo
%run_list = list_opt
%run_list = list_supply
%run_list = list_width
%run_list = list_real_input
%run_list = list_input_comp
%run_list = list_input_short
%run_list = list_ALL
%run_list = list_ALL_short
%run_list = list_extra_comp
%run_list = list_flood_comp
%run_list = list_fining
%run_list = list_all_floods
%run_list = list_all_fl_D256
run_list = list_Seas_D256
%run_list = spin_test
%run_list = list_fl_D256

path_to_file = '';
%path_to_file = '../results/tot_eros/FL/';
%path_to_file = '/media/yeti/B6DEEFA4DEEF5B5F/temp_hydro_backup/Sims_backup/erosion/FLOODS/';
%path_to_file = '../results/tot_eros/ENTL_HR/';
%path_to_store = '/media/yeti/B6DEEFA4DEEF5B5F/temp_hydro_backup/Total_erosion_simulations/FLOODS/';

run_nb = length(run_list);

already_prep = 0;
for i = 1:run_nb

    ch_nb = 1;
    L10 = 0;
    L50 = 1;
%keyboard
    if already_prep == 0
        file_to_load = strcat(path_to_file,run_list{i});
        %file_to_load = strcat(path_to_store,run_list{i});
        [valley_topo,sum_e_eff,sum_E_eff,Qc_short,Q_tc_short,E_tot,t_stage_short,R_short,...
                Fe_short,e_tot_short,max_E_eff,result_eros,vol_exit,Q_out,q_tc_vol_sh,width] =...
                                                prep_IS_TR_comp(file_to_load,ch_nb,L10,L50);
    elseif already_prep == 1
        load(run_list{i})
    end

    trans_IS(i) = struct('valley_topo',valley_topo,...
                    'sum_e_eff',sum_e_eff,...
                    'sum_E_eff',sum_E_eff,...
                    'Qc',Qc_short,...
                    'R',R_short,...
                    'Q_tc',Q_tc_short,...
                    'E_tot',E_tot,...
                    't_stage',t_stage_short,...
                    'Fe',Fe_short,...
                    'e_tot',e_tot_short,...
                    'max_E_eff',max_E_eff,...
                    'result_eros',result_eros,...
                    'vol_exit',vol_exit,...
                    'Q_out',Q_out,...
                    'q_tc_vol',q_tc_vol_sh,...
                    'width',width);

end
