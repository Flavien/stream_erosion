%Function to plot the last long profile of interesting quantities to understand changes
%how the R-channel and saltation erosion model interact
%
%

%for fi = 1:9
%    figure(fi)
%    clf
%end
close all

%sim_struct = load_simulations;
sim_struct = load_simulations;
length_struct = length(sim_struct);

%dx = 200;
%dx = 1000;
dx = 1 / (length(sim_struct(1).E)-1);

x = [0:length(sim_struct(1).E)-1].*dx;
MS = zeros(length_struct,1) ;
LW = zeros(length_struct,1) +  4;
ln_spec_col = {'k','b','k','b','k','b','r','m'} ;
ln_spec_type = {'-','-','--','--','--','--','--','--'};
%ln_spec_col = 'k';
%ln_spec_type = '-';

FSlab = 32;
FSax  = 32;

fx = 0.55;
fy = 0.4;

legend_text = {'ref run','b_c x2','Ice sheet','Small glacier'};
%legend_text = {'ref run','SG 300','SG 250','SG 500'};
%legend_text = {'ref run','bc x2','bc /2','bc x4'};
%legend_text = {'ref run','bc x2','W/2'};
%legend_text = {'IS 1000','IS 500','IS 750','IS 1250','IS 1500'};
%legend_text = {'<n> = 0.032','<n> = 0.038','<n> = 0.0464'};
%legend_text = {'ref run','cav ref'};

k = 1;

f1 = figure(k);
set(f1,'Units','normalized')
set(f1,'Position',[0,0.5,fx,fy])
hold on
for i=1:length_struct
    %fancy_plot(x,sim_struct(i).E,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
    fp1 = fancy_plot(x,sim_struct(i).E,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
    if i == 1
        legend(legend_text{i});
        legend('hide')
        [LEGH,OBJH,OUTH,OUTM] = legend;
        %legend([OUTH;fp1],OUTM{:},legend_text{i})
        legend([fp1],OUTM{:},legend_text{i})

    else
        [LEGH,OBJH,OUTH,OUTM] = legend;
        legend([OUTH;fp1],OUTM{:},legend_text{i})
    end
end
set(legend,'Location','Best')
set(gca,'FontSize',FSax)
xlabel('Distance x')
ylabel('Eros rate, simplified expression (m/a)')

k = k + 1;
f2 = figure(k);
set(f2,'Units','normalized')
set(f2,'Position',[0,0.5,fx,fy])
hold on
for i=1:length_struct
    fp1 = fancy_plot(x,sim_struct(i).e_salt,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
    add_legend
end
set(legend,'Location','Best')
set(gca,'FontSize',FSax)
xlabel('Distance x')
ylabel('Eros rate, saltation only (m/a)')

k = k+1;
f3 = figure(k);
set(f3,'Units','normalized')
set(f3,'Position',[0,0.5,fx,fy])
hold on
for i=1:length_struct
    fp1 = fancy_plot(x,sim_struct(i).excess_tau,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
    add_legend
end
set(legend,'Location','Best')
set(gca,'FontSize',FSax)
xlabel('Distance x')
ylabel('Excess Shields stress')

k = k + 1;
f4 = figure(k);
set(f4,'Units','normalized')
set(f4,'Position',[0,0.5,fx,fy])
hold on
for i=1:length_struct
    fp1 = fancy_plot(x,sim_struct(i).Fe,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
    add_legend
end
set(legend,'Location','Best')
set(gca,'FontSize',FSax)
xlabel('Distance x')
ylabel('Fraction exposed')

k = k + 1;
f5 = figure(k);
set(f5,'Units','normalized')
set(f5,'Position',[0,0.5,fx,fy])
hold on
for i=1:length_struct
    fp1 = fancy_plot(x,sim_struct(i).Ir,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
    add_legend
end
set(legend,'Location','Best')
set(gca,'FontSize',FSax)
xlabel('Distance x')
ylabel('Impact rate')

k = k + 1;
f6 = figure(k);
set(f6,'Units','normalized')
set(f6,'Position',[0,0.5,fx,fy])
hold on
for i=1:length_struct
    fp1 = fancy_plot(x,sim_struct(i).Vi,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
    add_legend
end
set(legend,'Location','Best')
set(gca,'FontSize',FSax)
xlabel('Distance x')
ylabel('Volume removed / impact')

k = k + 1;
f7 = figure(k);
set(f7,'Units','normalized')
set(f7,'Position',[0,0.5,fx,fy])
hold on
for i=1:length_struct
    fp1 = fancy_plot(x,sim_struct(i).q_tc,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
    add_legend
end
set(legend,'Location','Best')
set(gca,'FontSize',FSax)
xlabel('Distance x')
ylabel('Transport capacity (kg/s)')

k = k + 1;
f8 = figure(k);
set(f8,'Units','normalized')
set(f8,'Position',[0,0.5,fx,fy])
hold on
for i=1:length_struct
    fp1 = fancy_plot(x,sim_struct(i).u_shear,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
    add_legend
end
set(legend,'Location','Best')
set(gca,'FontSize',FSax)
xlabel('Distance x')
ylabel('Shear velocity')

k = k + 1;
f9 = figure(k);
set(f9,'Units','normalized')
set(f9,'Position',[0,0.5,fx,fy])
hold on
for i=1:length_struct
    fp1 = fancy_plot(x,sim_struct(i).Dmax,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
    add_legend
end
set(legend,'Location','Best')
set(gca,'FontSize',FSax)
xlabel('Distance x')
ylabel('Max particle D moving (m)')
