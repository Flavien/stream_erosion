%Function to plot the last long profile of interesting quantities to understand changes
%how the R-channel and saltation erosion model interact
%
%

for fi = 1:9
    figure(fi)
    clf
end

%sim_struct = load_simulations;
sim_struct = load_simulations;
length_struct = length(sim_struct);

%dx = 200;
%dx = 1000;
dx = 1 / (length(sim_struct(1).E)-1);

x = [0:length(sim_struct(1).E)-1].*dx;
MS = zeros(length_struct,1) ;
LW = zeros(length_struct,1) +  3;
ln_spec_col = {'k','b','r','m','k','b','r','m'} ;
ln_spec_type = {'-','-','-','-','--','--','--','--'};
%ln_spec_col = 'k';
%ln_spec_type = '-';

FSlab = 14;
FSax  = 14;

%legend_text = {'ref run','Discharge x2','Ice sheet','Small glacier'};
%legend_text = {'ref run','SG 300','SG 250','SG 500'};
%legend_text = {'ref run','bc x2','bc /2','bc x4'};
%legend_text = {'ref run','bc x2','W/2'};
%legend_text = {'IS 1000','IS 500','IS 750','IS 1250','IS 1500'};
%legend_text = {'<n> = 0.032','<n> = 0.038','<n> = 0.0464'};
%legend_text = {'ref run','cav ref'};
legend_text = {'ref run','bc x2','bc /2','IS 1000','IS 500'};

k = 1;

figure(k)
hold on
for i=1:length_struct
    %fancy_plot(x,sim_struct(i).E,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
    fp1 = fancy_plot(x,sim_struct(i).E,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
    if i == 1
        legend(legend_text{i});
        legend('hide')
        [LEGH,OBJH,OUTH,OUTM] = legend;
        %legend([OUTH;fp1],OUTM{:},legend_text{i})
        legend([fp1],OUTM{:},legend_text{i})

    else
        [LEGH,OBJH,OUTH,OUTM] = legend;
        legend([OUTH;fp1],OUTM{:},legend_text{i})
    end
end
set(legend,'Location','Best')
set(gca,'FontSize',FSax)
xlabel('Distance x')
ylabel('Eros rate, simplified expression (m/a)')

k = k + 1;
figure(k)
hold on
for i=1:length_struct
    fp1 = fancy_plot(x,sim_struct(i).e_tot,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
    add_legend
end
set(legend,'Location','Best')
set(gca,'FontSize',FSax)
xlabel('Distance x')
ylabel('Eros rate total load (m/a)')

k = k+1;
figure(k)
hold on
for i=1:length_struct
    fp1 = fancy_plot(x,sim_struct(i).excess_tau,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
    add_legend
end
set(legend,'Location','Best')
set(gca,'FontSize',FSax)
xlabel('Distance x')
ylabel('\tau^* / \tau^*_c -1')

k = k + 1;
figure(k)
hold on
for i=1:length_struct
    fp1 = fancy_plot(x,sim_struct(i).Fe,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
    add_legend
end
set(legend,'Location','Best')
set(gca,'FontSize',FSax)
xlabel('Distance x')
ylabel('Fraction exposed')

k = k + 1;
figure(k)
hold on
for i=1:length_struct
    fp1 = fancy_plot(x,sim_struct(i).w_i_eff,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
    add_legend
end
set(legend,'Location','Best')
set(gca,'FontSize',FSax)
xlabel('Distance x')
ylabel('Effective impact velocity')

k = k + 1;
figure(k)
hold on
for i=1:length_struct
    fp1 = fancy_plot(x,sim_struct(i).cb,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
    add_legend
end
set(legend,'Location','Best')
set(gca,'FontSize',FSax)
xlabel('Distance x')
ylabel('Near bed sediment concentration')

k = k + 1;
figure(k)
hold on
for i=1:length_struct
    fp1 = fancy_plot(x,sim_struct(i).q_tc,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
    add_legend
end
set(legend,'Location','Best')
set(gca,'FontSize',FSax)
xlabel('Distance x')
ylabel('Transport capacity')

k = k + 1;
figure(k)
hold on
for i=1:length_struct
    fp1 = fancy_plot(x,sim_struct(i).q_tot,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
    add_legend
end
set(legend,'Location','Best')
set(gca,'FontSize',FSax)
xlabel('Distance x')
ylabel('Total volumetric supply rate')

k = k + 1;
figure(k)
hold on
for i=1:length_struct
    fp1 = fancy_plot(x,sim_struct(i).Dmax,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
    add_legend
end
set(legend,'Location','Best')
set(gca,'FontSize',FSax)
xlabel('Distance x')
ylabel('Max particle D moving')


disp('ADD PLOTTING OF SEDIMENT TRACKING!')
