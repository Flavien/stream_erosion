%File to make a plot of the comparison of the total erosion produced during a flood or during a season
%
%
%o

%load trans_IS_flood_test
%load trans_IS_all_flood

%season
%10
%20
%30
%x2

dx = 500; %meters
gl_surf = 500*100 * 4000;
k = 1;
i  = 1;

FSax = 36;
LWmark = 4;
MS = 30;

f1 = figure;
f1.Units = 'Normalized';
f1.Position = [0 0 0.7 0.6];

p1 = plot(k,max(trans_IS(i).sum_e_eff),'x');
hold on

k = 2;
i= i +1;
p2 = plot(k,max(trans_IS(i).sum_e_eff),'d');

k = 3;
i= i +1;
p3 = plot(k,max(trans_IS(i).sum_e_eff),'<');

k = 4;
i= i +1;
p4 = plot(k,max(trans_IS(i).sum_e_eff),'o');

k = 5;
i= i +1;
p5 = plot(k,max(trans_IS(i).sum_e_eff),'s');


k = 2;
i= i +1;
p6 = plot(k,max(trans_IS(i).sum_e_eff),'d');

k = 3;
i= i +1;
p7 = plot(k,max(trans_IS(i).sum_e_eff),'<');

k = 4;
i= i +1;
p8 = plot(k,max(trans_IS(i).sum_e_eff),'o');

k = 5;
i= i +1;
p9 = plot(k,max(trans_IS(i).sum_e_eff),'s');

axis([0.5 5.5 0 120])
ylabel('Maximum erosion depth (mm)')

axes = gca;
axes.XTick = [1 2 3 4 5];
axes.XTickLabel = {'Season','10 days','20 days','30 days','40 days'};
axes.FontSize = FSax;

p1.MarkerSize = MS;
p2.MarkerSize = MS;
p3.MarkerSize = MS;
p4.MarkerSize = MS;
p5.MarkerSize = MS;
p6.MarkerSize = MS;
p7.MarkerSize = MS;
p8.MarkerSize = MS;
p9.MarkerSize = MS;

p2.MarkerFaceColor = axes.ColorOrder(2,:);
p3.MarkerFaceColor = axes.ColorOrder(3,:);
p4.MarkerFaceColor = axes.ColorOrder(4,:);
p5.MarkerFaceColor = axes.ColorOrder(5,:);

p1.LineWidth = LWmark;
p2.LineWidth = LWmark;
p3.LineWidth = LWmark;
p4.LineWidth = LWmark;
p5.LineWidth = LWmark;
p6.LineWidth = LWmark;
p7.LineWidth = LWmark;
p8.LineWidth = LWmark;
p9.LineWidth = LWmark;
