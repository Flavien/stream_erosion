%File to automatically save figures
%
%for the last part of the path to be a folder, finish line with /
%

%path = '../figures/Sediment_transport/ref_MB_testA_1m/'
%path = '../erosion/figures/saltation/nagra_pres_testA/'
%path = '../erosion/figures/nagra_pres_comp/'
%path = '../erosion/figures/tot_eros_width/Rchan_SS_SG3_D60_E/'
%path = '../erosion/figures/mass_conservation_test/testD_D60/'
%path = '../erosion/figures/Lamb2008_check/'
%path = '../erosion/figures/tot_eros/Bench_like_season/1channel/'
%path = '../erosion/figures/tot_eros/Bench_like_season/2channels/'
%path = '../erosion/figures/tot_eros/real_input/B2007/'
path = '../erosion/figures/tot_eros/real_input/B2008/'
%path = '../erosion/figures/tot_eros/IS_2mm/'
disp('if path is right type enter, otherwise ctrl+c; note: dont forget / in the end')
pause

%After plot_eros_surf
%fign = {'D_salt_tot_comp','q_tc_comp'};

%fign = {'E','e_salt','tau_excess','Fe','Ir','Vi','q_tc','u_shear','Dmax'};
%fign = {'eta','q_supply','e_tot','Fe','e_salt','sum_eta','q_tc'};

%fign = {'eta_ctf','q_supply_ctf','e_tot_ctf','Fe_ctf','e_salt_ctf','sum_eta','q_tc','w_i_eff_ctf','cb_ctf'};

%fign = {'velocity_vs_tstage','eros_vs_tstage','eros_vs_Csed','eros_vs_sed_supply','eros_vs_grain_size'};

%fign = {'Q_Qtc_Etot_cont','eros_explanation'};
fign = {'2008_eros_cont','2008_eros_explanation'};

a = 1;
b = 2;
for i=a:b
    f = figure(i);
    name = strcat(path,fign{i-a+1});
    hgexport(f,name)
    saveas(f,name,'fig')
    %saveas(f,name,'eps')
    %print(f,'-depsc','-painters',name)
    %print(f,'-dpdf','-painters',name)
    %print(f,'-dpng',name)
    %print(f,'-dbmp',name)
    
end
