%Function to compute the what I want out of the run
%to load in a structure and plot a comparison of the plots
%
%

function [valley_topo,sum_e_eff,sum_E_eff,Qc_short,Q_tc_short,E_tot,t_stage_short,...
            Fe_short,e_tot_short,max_E_eff,result_eros,vol_exit,Q_out,q_tc_vol_sh,width] =...
                                            prep_IS_TR_comp(file_to_load,ch_nb,L10,L50)

load(file_to_load)

Qc_t = 0.01;
if L10 == 1
    %reg glacier
    %R_t = 0.125;
    R_t = 0.132;
elseif L50 == 1
    %Ice sheet
    R_t = 0.160;
end

q_tc_vol = q_tc_ar./2650.*600;
q_tc_vol(R < R_t) = 0;
q_tc_vol_sh = q_tc_vol(1:6:end,:);

Q_tc = q_tc_ar./2650.*(2.*R).*ch_nb;
Q_tc(R < R_t) = 0;

Q_tc_dt = q_tc_vol.*(2.*R).*ch_nb;
Q_tot = q_tot_temp.*(2.*R).*ch_nb;
%Q_tot = Q_tot.*1.262;
%Q_tot = Q_tot.*0.486;
%Q_tot = Q_tot.*2;
%Q_tot = Q_tot.*0.618;

Q_out = min(Q_tc_dt(:,end),Q_tot(:,end));
%sum(Q_out)./5e4./1e3

Q_tc_short = Q_tc(1:6:end,:);
q_tc_short = q_tc_ar(1:6:end,:);

R_short = R(1:6:end,:);
e_eff = e_tot_ar.*1000.*600./constants.secondsinyear;
e_eff(R < R_t) = 0;
E_eff = e_eff .* (2.*R).*ch_nb;

e_tot_short = e_tot_ar(1:6:end,:).*1000;
e_tot_short(R_short < R_t) = 0;

E_tot = e_tot_short .* (2.*R_short).*ch_nb;
Fe_short = Fe_tot_ar(1:6:end,:);
Fe_short(R_short < R_t) = 0;
cb_mod = cb_ar;
cb_mod(Fe_tot_ar == 0) = -0.02;
cb_short = cb_mod(1:6:end,:);
w_i_eff_short = w_i_eff_ar(1:6:end,:);
Qc_short = Qc(1:6:end,:);

t_stage = tau_ratio;
%t_stage(R < R_t+0.03) = 0;
t_stage(R < R_t) = 0;
t_stage_short = t_stage(1:6:end,:);


%computing stuff about erosion
sum_e_eff = sum(e_eff);
sum_E_eff = sum(E_eff);

max_E_eff = max(sum_E_eff);

%res_eros = sum(sum_E_eff)./ nb_nodes / width glacier
result_eros = sum(sum_E_eff)./100./1000;

vol_exit = sum(Q_out);

%plotting stuffs ...
t_long = [1:length(q_tc_ar)];
t_long = t_long ./ 6 ./ 24;
t_short = [1:length(Q_tc_short)];
t_short = t_short./24;

tunnel_valley_plot
