%Script to plot the contour of the results of the paper run ch_D81
%This is temporary and meant to try to understand what is happening.
%
%
%close all

f=1;
%%%%%%%%%%%%%%%%%%%%
figure(f)
contourf(E_ar)
colorbar
shading flat
title('E rate computed with simplified expression')
f = f +1;

%%%%%%%%%%%%%%%%%%%%
figure(f)
contourf(Qc)
colorbar
shading flat
title('Discharge in conduits m3/s')
f = f +1;

%%%%%%%%%%%%%%%%%%%%
figure(f)
contourf(p_c./1e6)
colorbar
shading flat
title('Water pressure in channels MPa')
f = f +1;

%%%%%%%%%%%%%%%%%%%%
figure(f)
contourf(tau_ratio)
colorbar
shading flat
title('\tau / \tau_c')
f = f +1;

%%%%%%%%%%%%%%%%%%%%
figure(f)
contourf(u_mean_sub)
colorbar
shading flat
title('Mean flow velocity m/s')
f = f +1;

%%%%%%%%%%%%%%%%%%%%
figure(f)
contourf(u_shear_sub)
colorbar
shading flat
title('Shear flow velocity m/s')
f = f +1;

%%%%%%%%%%%%%%%%%%%%
figure(f)
contourf(R)
colorbar
shading flat
title('radius of channel m')
f = f +1;

%%%%%%%%%%%%%%%%%%%%
figure(f)
contourf(tau_tot)
colorbar
shading flat
title('Total shear stress computed for channel growth Pa')
f = f +1;


f=0;
