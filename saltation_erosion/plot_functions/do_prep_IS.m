%Script to run the prep_IS_TR_comp function
%
%It allows me to get the interesting metrics for a single simulation only
%

ch_nb = 1;
L10 = 0;
L50 = 1;

%path_to_file = '../results/tot_eros/FL/';
path_to_file = '';
%file_to_load = 'TR_IS_SD3_01_DwnFinD.mat'
%file_to_load = 'TR_IS_SD3_01_DwnFinD.mat';
file_to_load = 'TR_IS_SD3_BenchC_D256.mat';
%file_to_load = 'FL_SIN_120days_BenchC_Atb.mat';
file_to_save = strcat('AN_',file_to_load);

[valley_topo,sum_e_eff,sum_E_eff,Qc_short,Q_tc_short,E_tot,t_stage_short,R_short,...
            Fe_short,e_tot_short,max_E_eff,result_eros,vol_exit,Q_out,q_tc_vol_sh,width] =...
                                            prep_IS_TR_comp(strcat(path_to_file,file_to_load),ch_nb,L10,L50);

save(file_to_save)
