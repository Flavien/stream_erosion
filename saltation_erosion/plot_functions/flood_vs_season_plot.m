%File to simply plot the shape of the inputs and the difference that there is between flood and seasons
%
%
%

LW = 2;
LW2 = 4;
fx = 850;
fy = 200;

f1 = figure(1);
load ref_input.mat
p1 = plot(b_s_time_norm(1:17300,1));
f1.Position(3) = fx;
f1.Position(4) = fy;
ax1 = gca;
ax1.XTickLabel = '';
ax1.YTickLabel = '';

p1.LineWidth = LW;
p1.Color = 'k';



load trans_IS_flood_test

f2 = figure(2);
p2 = plot(trans_IS(2).Qc(:,1));
hold on
p3 = plot(trans_IS(3).Qc(:,1));
p4 = plot(trans_IS(4).Qc(:,1));
p5 = plot(trans_IS(5).Qc(:,1));

p2.Color = 'k';
p3.Color = [0.4 0.4 0.4];
p4.Color = [0.6 0.6 0.6];
p5.Color = [0.8 0.8 0.8];

f2.Position(3) = fx;
f2.Position(4) = fy;
ax2 = gca;
ax2.XLim = [1 850];
ax2.XTickLabel = '';
ax2.YTickLabel = '';

p2.LineWidth = LW2;
p3.LineWidth = LW2;
p4.LineWidth = LW2;
p5.LineWidth = LW2;

f3 = figure(3);
p6 = plot(trans_IS(2).Qc(:,1));
f3.Position(3) = fx;
f3.Position(4) = fy;
ax3 = gca;
ax3.XLim = [1 250];
ax3.XTickLabel = '';
ax3.YTickLabel = '';

p6.LineWidth = LW2;
p6.Color = 'k';
