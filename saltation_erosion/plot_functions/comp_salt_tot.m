%Function to plot the last long profile of interesting quantities to understand changes
%how the R-channel and saltation erosion model interact
%
%

%for fi = 1:9
%    figure(fi)
%    clf
%end
close all

%sim_struct = load_simulations;
sim_struct = load_simulations;
length_struct = length(sim_struct);

%dx = 200;
%dx = 1000;
dx = 1 / (length(sim_struct(1).E)-1);

x = [0:length(sim_struct(1).E)-1].*dx;
MS = zeros(5,1) ;
LW = zeros(5,1) +  4;
ln_spec_col = {'k','b','m','k','b'} ;
ln_spec_type = {'-','-','--','-.','-.'};
%ln_spec_col = 'k';
%ln_spec_type = '-';

FSlab = 32;
FSax  = 32;

fx = 0.65;
fy = 0.4;

legend_text = {'e_{tot} D = 60 mm','e_{salt} D = 60 mm','e_{tot} D = 60 mm; q_{tot}/10','e_{tot} D = 1 mm','e_{salt} D = 1mm'};

k = 1;

f2 = figure(k);
set(f2,'Units','normalized')
set(f2,'Position',[0,0.5,fx,fy*1.3])
hold on

i=1;
    fp1 = fancy_plot(x,sim_struct(1).e_tot,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
i=2;
    fp1 = fancy_plot(x,sim_struct(1).E,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
i=3;
    
    fp1 = fancy_plot(x,sim_struct(2).e_tot,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
i=4;

    fp1 = fancy_plot(x,sim_struct(3).e_tot,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
i=5;
    fp1 = fancy_plot(x,sim_struct(3).E,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});

legend(legend_text)
set(legend,'Location','Best')
set(gca,'FontSize',FSax)
xlabel('Distance x')
ylabel('Eros rate, saltation only (m/a)')

k = k + 1;
f7 = figure(k);
set(f7,'Units','normalized')
set(f7,'Position',[0,0.5,fx,fy.*0.85])
hold on

    fp1 = fancy_plot(x,sim_struct(1).q_tc,MS(1),LW(1),ln_spec_col{1},ln_spec_type{1});
    i=5;
    fp1 = fancy_plot(x,sim_struct(3).q_tc,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
    i=3;
    fp1 = fancy_plot(x,sim_struct(1).q_tot.*2650,MS(i),LW(i),ln_spec_col{i},ln_spec_type{1});

legend('D = 60 mm','D = 1 mm','Sediment supply (q_{tot})')
set(legend,'Location','Best')
set(gca,'FontSize',FSax)
xlabel('Distance x')
ylabel('Transport capacity (kg/s)')

%{
k = k + 1;
f9 = figure(k);
set(f9,'Units','normalized')
set(f9,'Position',[0,0.5,fx,fy])
hold on
    fp1 = fancy_plot(x,sim_struct(i).Dmax,MS(i),LW(i),ln_spec_col{i},ln_spec_type{i});
    legend('leg')
set(legend,'Location','Best')
set(gca,'FontSize',FSax)
xlabel('Distance x')
ylabel('Max particle D moving (m)')

k = k+1;
f3 = figure(k);
set(f3,'Units','normalized')
set(f3,'Position',[0,0.5,fx,fy])
hold on
    fp1 = fancy_plot(x,sim_struct(1).excess_tau,MS(1),LW(1),ln_spec_col{1},ln_spec_type{1});
    legend('leg')
set(legend,'Location','Best')
set(gca,'FontSize',FSax)
xlabel('Distance x')
ylabel('Excess Shields stress')
%}
