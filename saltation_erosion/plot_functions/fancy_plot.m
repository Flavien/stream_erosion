%function to reuse the matlab plot with all the specs needed included directly
%MarkerSize, LineWidth color, type and marker shape can be defined
%
%

function [fp] = fancy_plot(x,var,MS,LW,ln_spec_col,ln_spec_type)

if MS ==0 
    %ln_spec_type = ln_spec_type(1:end-1);
    fp = plot(x,var,ln_spec_type,'Color',ln_spec_col,'LineWidth',LW);
else
    fp = plot(x,var,ln_spec_type,'Color',ln_spec_col,'MarkerSize',MS,'LineWidth',LW);
end
