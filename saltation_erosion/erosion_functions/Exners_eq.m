%Function to compute changes in sediment thickness at the base of the channel.
%This sediment conservation is usually referred to as Exner's equation
%
%The amount of sediment tranported is a function of the channel width as well

function [detadt eta q_adv_w detadt_w q_in q_out] = Exners_eq(constants,q_tc,q_sup,eta,detadt,dx,dt,R,Us,i_300)


%NOTE:
%q_sup = q_tot_temp
%q_tot = q_tot_init

cell_nb = length(q_tc);

q_adv = zeros(1,cell_nb);
q_adv_w = zeros(1,cell_nb);

%The values used so far are width averaged.
%Here I compute the total sediment transport that happens as a function of the channel width.
q_sup_w = q_sup.* 2 .* R;
%q_tc_w  = q_tc.* 2 .* R;
q_tc_w  = q_tc.* 2 .* R .* dt;

qtc_avg = (q_tc_w(2:end) + q_tc_w(1:end-1)) ./2;
qsup_avg = (q_sup_w(2:end) + q_sup_w(1:end-1)) ./2;

test_val = q_tc_w - q_sup_w.*constants.rho_s;

%Fluxes here are mass fluxes.
%Hence the division by sediment density to get a volume
%NOTE: q_adv now defined on the edges
%q_adv_w(test_val >= 0) = q_sup_w(test_val >= 0);
%q_adv_w(test_val < 0) = q_tc_w(test_val < 0)./constants.rho_s;
q_adv_w = min(q_tc_w./constants.rho_s,q_sup_w);

qadv_avg = (q_adv_w(2:end) + q_adv_w(1:end-1)) ./2;

%Conversion factor for volume flux to height
volume_fact = 1 ./ ( (1-constants.lambda_s).*2.*R);

%computing the rate of change of eta
detadt_w = zeros(1,cell_nb);

%Definition of Input flux in each cell
q_in = zeros(1,cell_nb);
q_in(1) = q_sup_w(1);
q_in(2:end) = (q_adv_w(1:end-1) + q_sup_w(2:end));

%ADD SOURCE TO q_in WHEN THAT APPLY BECAUSE FOR NOW IT IS 0 AND IT IS 
%HIDDEN INTO THE q_sup_w

%Definition of output flux from each cell
q_out = zeros(1,cell_nb);
q_out = q_adv_w;

%Change in sediment height for each cell
detadt_w = volume_fact .* (q_in - q_out);

if i_300 >= 1e10
    keyboard
end

%keyboard

%Calculating next time sediment height
%The formula is not eta = eta_prev + detatdt_w *dt because eta is included in q_sup_w
eta = detadt_w;
%eta = detadt_w .* dt;
eta(eta < 0) = 0;
