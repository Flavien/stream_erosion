%This funtion computes the impact rate in the total load model from Lamb & al 2008.
%The impact rate is different from that of the saltating load only because it accounts
%for the eddys triggering bed-ward motion of the sediment
%I here compute it following eq. (15).
%Note that it could also be computed from (12)

function [Ir_tot] = impact_rate_tot_load(c_b,w_s,R)

A2 = 1/3;

Vp = 4/3 * pi * R^3;

Ir_tot = (A2 * c_b * w_s) / Vp; 
