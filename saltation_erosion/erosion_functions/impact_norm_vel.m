%Formula to compute the vertical impact velocity of a particle on the bed
%Based on Sklar & Dietrich 2004
%
%Formulas 3.11 - 3.15 in the lit review

function [w_si Ls w_f Us Hs] = impact_norm_vel(constants,tau,tau_c,D,u_shear,u_mean);

%Shear velocity of the flow:
%u_shear = (tau_b * constants.rho_w)^(1/2);

%w_f always a scalar so it makes vectorization easier
w_f = settling_vel(constants,D);

tau_ex = tau ./ tau_c;

%Computation of mean horizontal sed velocity u_s
Us = 1.56 .* (tau_ex -1).^(0.56) .* (constants.r.*constants.g.*D).^(1/2);
Us(imag(Us)~=0) = 0;

%disp('sediment is moving faster than water! Us = u_mean')
Us = min(Us,u_mean);

%Saltation hop height
Hs = 1.44 .* (tau_ex - 1).^(1/2) .* D;
Hs(imag(Hs)~=0) = 0;

%Saltation hop length
Ls_A = 8.0 .* (tau_ex - 1).^(0.88);
Ls_B = (1 - (u_shear./w_f)^2).^(1/2);
Ls_B(imag(Ls_B)~=0) = 0;

if min(size(tau_ex)) > 1
        Ls = D * Ls_A ./ Ls_B;
        Ls(imag(Ls)~=0) = 0;

        Lsd = 2/3 .* Ls; 
        %Compute avg fall velocity
        w_sd = Hs .* Us ./ Lsd;
        w_sd(imag(w_sd) ~= 0) = 0;
        w_si = 2.* w_sd;

   %making sure everything stays within reason 
    Ls(Ls_B <= 0) = 0;
    Lsd(Ls_B <= 0) = 0;
    w_sd(Ls_B <= 0) = 0;
    w_si(Ls_B <= 0) = 0;
else
    if Ls_B > 0
        Ls = D * Ls_A ./ Ls_B;
        Ls(imag(Ls)~=0) = 0;

        Lsd = 2/3 .* Ls; 

        %Compute avg fall velocity
        w_sd = Hs .* Us ./ Lsd;
        w_sd(imag(w_sd) ~= 0) = 0;

        w_si = 2.* w_sd;
    else
        %disp('Hop length going to infinite in saltations model, set to 0')
        %disp('Hence w_sd and w_si set to 0 too')
        Ls = 0;
        Lsd = 0;
        w_sd = 0;
        w_si = 0;
    end
end

%%%%%%%%%%%%%%%%%
%   SUB-FUNCTION FOR SETTLING VELOCITY OF PARTICLE

function [w_f] = settling_vel(constants,D);
%The settling velocity is computed after the formula given in Dietrich 1982
%as suggested in S & D 2004 paper.
%Because the angularity of subglacial sediment might vary from that of fluvial sediment
%this is probably worth a bit of discussion.
%However actually tuning it or playing with it is probably out of the scope of what I'm
%interested in.

CSF = 0.8;
Powers = 3.5;

%Dimentionless particle size (Eq. 6)
Dstar = ((constants.rho_s - constants.rho_w)*constants.g*D^3) / (constants.rho_w*constants.nu^2);


%Components of dimless settling velocity
%R1 (right-hand side of Eq. 9)
R1 = -3.76715 +...
        + 1.92944*log10(Dstar)... 
        - 0.09815*(log10(Dstar))^(2)... 
        - 0.00575*(log10(Dstar))^(3)... 
        + 0.00056*(log10(Dstar))^(4);

%R2 (Eq. 16)
R2_A = log10(1- (1-CSF)/0.85);
R2_B = (1-CSF)^2.3 * tanh(log10(Dstar) - 4.6);
R2_C = 0.3 * (0.5 -CSF) * (1-CSF)^2 * (log10(Dstar) - 4.6);

R2 = R2_A - R2_B + R2_C;

%R3 (Eq. 18)
exponent = 1 + (3.5 - Powers)/2.5;
R3_mid = (CSF/2.83) * tanh(log10(Dstar) - 4.6);

R3 = (0.65 - R3_mid)^exponent;

%Dimless settling velocity (Eq. 19)
Wstar = R3 * 10^(R1 + R2);
%Wstar = 10^(R1 + R2);

%Inverting Wstar to get w_f (Eq. 5)
w_f = ((Wstar * (constants.rho_s - constants.rho_w) * constants.g * constants.nu) / constants.rho_w )^(1/3);
