%Function to compute chi in Lamb et al 2008.
%It is a term to relate suspended sediment fluc to the bulk parameters of the flow.
%chi from eq. (28)
%

%function [chi w_st P_rouse z dz] = chi_param(constants,D,Cd,u_mean,Hs,u_shear,R)
function [chi w_st P_rouse z z_Hb dz zeta_a chi_int_trapz] = chi_param(constants,D,Cd,u_mean,Hs,u_shear,R)

%R substituted for H.
%z_size = 50000;
z_size = 8000;
z = linspace(1e-5,R,z_size);
dz = (R-1e-5)./z_size;
%z_Hb = Hs.*0;

if Hs < 0.95.*R
    z_Hb = find(z > Hs);
else
    z_Hb = [z_size];
end


%Terminal settling velocity
w_st = (4./3 .* (constants.r .* constants.g .* D)./ Cd).^(0.5);

%Rouse number
P_rouse = w_st ./ (constants.kappa .* u_shear);

if Hs >= 0.95*R
    chi = 0;
    chi_int_trapz = 1;
    zeta_b = 1;
    zeta_a = 1;
    %Not too sure about that one here...
else
    %Explaination about z_0 in Lamb et al. 2008, following equation 22
    %z_0 = 3*D./30;
    z_0 = 3*0.06./30;

    %zeta_z = z./R;
    zeta_z = z(z_Hb(1):end)./R;
    zeta_b = Hs./R;

    zeta_a = ((1-zeta_z)./zeta_z) ./ ((1-zeta_b)./zeta_b);

    %Interior of integral to compute chi
    chi_int = (zeta_a.^P_rouse) .* (u_shear./constants.kappa) .* log(z(z_Hb(1):end)./z_0);

chi_int_trapz = trapz(chi_int);

    if max(chi_int) == 0
        chi = 0;
    else
        chi = 1./(u_mean.*R) * trapz(chi_int) * dz;
        %chi = 1./(u_mean.*R) * sum(chi_int) * dz;
        if chi < 0
            chi = 0;
        end
    end
end
