%Function to compute the rate of particle impact on the bed
%
%Part of the saltation abrasion model from Sklar and Dietrich 2004
%
%See formulas 3.7 in lit. review

function [Ir] = impact_rate(q_s,constants,D,Ls);

if min(size(Ls)) > 1
    Ir = (6.*q_s) ./ (pi .* constants.rho_s .* D.^3 .* Ls);
    %Ir(Ir <= D) = 0;
else
    %if Ls > D
        Ir = (6*q_s) ./ (pi * constants.rho_s * D^3 * Ls);
    %else
     %   Ir = 0;
    %end
end

