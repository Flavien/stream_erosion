%Function to compute the volume of rock eroded when a particle impact the bed
%
%Part of the saltation abrasion model from Sklar and Dietrich 2004
%
%See formulas 3.4 - 3.6 in lit. review

function [Vi] = vol_impact(constants,D,w_si);

%From formula 3.6
ViA = pi .* constants.rho_s .* D.^3 .* w_si.^2 .* constants.Y;
ViB = 6 .* constants.k_v .* constants.sigma_T.^2;

Vi = ViA ./ ViB;
