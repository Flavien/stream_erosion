%This function is to calculate the volumetric bedload flux
%as part of the total load erosion model (Lamb et al. 2008)
%
%q_b from eq. 20; q_sus from eq. 17

function [q_b q_sus] = vol_bedload_flux(constants,c_b,q_tot,R,Us,Hs,u_mean,chi,dt)

%bed_part = Us.*Hs;
%Account for timestepping...
bed_part = Us.*Hs.*dt;

%water_part = u_mean .* R .* chi; 
water_part = u_mean .* R .* chi.*dt; 

%bedload transport
if Us == 0
    q_b = 0;
    q_sus = 0;
else
    q_b = q_tot .* bed_part ./ (water_part + bed_part);
    %Suspended load transport
    q_sus = c_b .* u_mean .* R .* chi;
end


%Matching up units with transport capacity
q_b = q_b .* constants.rho_s;
q_sus = q_sus .* constants.rho_s;
