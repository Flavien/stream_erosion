%Function to recompute how avaiable the sediment is at the bed.
%The idea is to add the whatever volume of sediment there is stored at the bed to the 
%supply rate. That way I don't even need to remodify Fe as I go, if q_tot > q_tc, erosion will
%be 0.
%It also means that I assume instant mobilitzation, but should be fine.

function [q_sup] = availability_update(eta,constants,dt,dx)

%compute the volume of sediment per unit width:
%Vsed = eta .* (1-constants.lambda_s) .* dx;

%q_sup = Vsed./dt;

%q_sup = eta.*(1-constants.lambda_s)./dt;
%pause
%disp('Come back here and check the mess with the sed availability!')
q_sup = eta.*(1-constants.lambda_s);
