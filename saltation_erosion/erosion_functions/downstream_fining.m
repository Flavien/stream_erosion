%Function to process the downstream fining of clasts
%
%
%

function [Ds] = downstream_fining(W,x,D0) 

x0 = W ./2;
%alpha = 0.25;
%alpha = 0.3;
alpha = 0.5;

only_lower_half = 0;
whole_bed = 0;
two_sources = 0;
two_sourcesB = 1;

if only_lower_half == 1
    if x < 25000
        Ds = 0.06;
    else
        x_tp = x-25000;
        Ds = D0.* ((x_tp + x0)./x0).^(-alpha);
    end
elseif whole_bed == 1
    Ds = D0.* ((x + x0)./x0).^(-alpha);
elseif two_sources == 1
    if x < 25000
        Ds = 0.06;
    elseif x >= 25000 && x < 37500
        x_tp = x-25000;
        Ds = D0.* ((x_tp + x0)./x0).^(-alpha);
    elseif x >= 37500
        x_tp = x-37500;
        Ds = D0.* ((x_tp + x0)./x0).^(-alpha);
    end
elseif two_sourcesB == 1
    if x < 25000
        Ds = 0.06;
    elseif x >= 25000 && x < 42500
        x_tp = x-25000;
        Ds = D0.* ((x_tp + x0)./x0).^(-alpha);
    elseif x >= 42500
        x_tp = x-42500;
        Ds = D0.* ((x_tp + x0)./x0).^(-alpha);
    end
end
