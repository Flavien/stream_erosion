%Function to compute the fall height of particles in the supsended and bedload model
%The fall height is a function of the concentration sed in the water column.
%Hf from eq.(32) in Lamb et al. (2008)
%

function [Hf] = fall_height(constants,z,z_Hb,dz,c_b,c_sed,R,Hs)

dcdz = z(z_Hb(1):end).*0;
dcdz2 = z(z_Hb(1):end).*0;

%SETTING IT THAT WAY FOR NOW BUT NOT TOO SURE ABOUT THE ACTUAL DIRECTION OF z FOR NOW
%Turns out it seems reasonable to set the dcdz at 0 at the Hd boundary
%See also paragraph 34, dcdz = 0 for z < Hb
%In the end what is really necessary is to have a large number of points, a few hundred is not enough, probably due to
%the logarythmic nature of the surve

dcdz(end) = 0;
dcdz(1:end-1) = (c_sed(2:end) - c_sed(1:end-1))./dz;

%Note that the (-1) is here to invert the bounds of the integral form H -> Hs to Hs -> H
if c_b == 0 || isnan(c_b) == 1
    Hf = 0;
elseif Hs >= 0.95*R
    %Hf = R;
    Hf = Hs;
else
    Hf = 1./c_b .* (-1) .* trapz(z(z_Hb(1):end).*dcdz) .* dz;
end
