%This script is meant to compute stream erosion following the approach of
%Sklar and Dietrich 2004 at first.
%Then some plugins should be added or modifications done to add Lamb et al 2008
%After that some principles tied to steep streams probably.
%
%The reasoning behind this script is based on the literature review on river erosion that I wrote.

function [e_salt E e_tot_load Vi Ir Fe q_tc tau tau_b u_shear u_mean tot_load_stuff salt_stuff] =...
                                         total_eros(constants,tau_c,D,W,q_s,slope,Qw,Hw,q_tot,theta);

dt = 600;

%Just to make it so that I don't have to change it between model test and simulations
R = Hw;
%Following Eq. 3.3 in the lit review
%   e_stream = Vi * Ir * Fe

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       CALLING FUNCTION TO COMPUTE ALL I NEED
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Call functions to compute all of that

%CALL function to recover flow speed OR shear stress, or BOTH...
%call [tau_b u_shear] = open_flow_tau_b
[tau_b u_shear u_mean] = open_flow_tau_b(Hw,W,slope,theta,constants,Qw);
slope = 0.0053;
theta = 0.0053;

%call [tau] = compute_Shields
tau = compute_Shields(u_shear,constants,D,tau_b);

%call function for transport capacity [q_tc] = Qtc_FLvB
[q_tc] = Qtc_FLv(constants,D,tau,tau_c);

%call [w_si Ls] = impact_norm_vel()
[w_si Ls w_f Us Hs] = impact_norm_vel(constants,tau,tau_c,D,u_shear,u_mean);

%And making sure the clasts are not flying out of the water
if Hs > R
    Hs = R;
end

%call Vi = vol_impact
[Vi] = vol_impact(constants,D,w_si);

%call Ir = impact_rate
[Ir] = impact_rate(q_s,constants,D,Ls);

%call Fe = fraction_exposed
%[Fe] = fraction_exposed(q_s,q_tc);
[Fe] = fraction_exposed(q_s,q_tc.*dt);

%Now calling functions for the total load erosion model

[Cd] = drag_coeff(constants,D,w_f);

%[chi w_st P_rouse z dz] = chi_param(constants,D,Cd,u_mean,Hs,u_shear,R);
%[chi w_st P_rouse z z_Hb dz zeta_a chi_int_trapz] = chi_param(constants,D,Cd,u_mean,Hs,u_shear,R);
[chi,w_st,P_rouse,z,z_Hb,dz,zeta_a,chi_int_trapz] = chi_param(constants,D,Cd,u_mean,Hs,u_shear,R);

%[c_sed c_b] = sed_concentration(constants,chi,u_mean,R,Hs,Us,z,z_Hb,zeta_a,P_rouse,q_tot);
[c_sed,c_b] = sed_concentration(constants,chi,u_mean,R,Hs,Us,z,z_Hb,zeta_a,P_rouse,q_tot,dt);
    %condition so that c_b does not become too high when sediment chi --> 0
    if D>Hs c_b = 0; end

[Hf] = fall_height(constants,z,z_Hb,dz,c_b,c_sed,R,Hs);

[w_s] = norm_fall_vel(constants,w_st,theta,Cd,Hf,D);

[w_i_eff P_fluctu] = eff_impact_vel(constants,u_shear,w_s);

[Ir_tot] = impact_rate_tot_load(c_b,w_s,R);

%[q_b q_sus] = vol_bedload_flux(constants,c_b,q_tot,R,Us,Hs,u_mean,chi);
[q_b,q_sus] = vol_bedload_flux(constants,c_b,q_tot,R,Us,Hs,u_mean,chi,dt);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       NOW COMPUTING ACTUAL EROSION
%       SALTATION MODEL
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Unit of erosion rate: m/s
e_salt = Vi * Ir * Fe;
%conversion to m/a:
e_salt = e_salt * constants.secondsinyear;

%Trying the erosion rate as in equation (24a) of S&D 2004
E1 = (0.08 *constants.r*constants.g*constants.Y)/(constants.k_v*constants.sigma_T^2);
E2 = q_s * (tau/tau_c - 1)^(-1/2);
E3 = 1 - q_s/q_tc;
%Shutdown of erosion is supply exceeds transport
if q_s > q_tc
    E3 = 0;
    disp('Transport capacity exceeded by supply, e_salt = 0')
end

if u_shear > w_f
    E4 = 0;
else
    E4 = (1 - (u_shear/w_f)^2)^(3/2);
end

E = E1 * E2 * E3 * E4;
%Conversion to m/a
E = E * constants.secondsinyear;
%keyboard;

salt_stuff = struct('w_f',w_f,...
                    'w_si',w_si,...
                    'Hs',Hs,...
                    'Ls',Ls,...
                    'Us',Us,...
                    'dz',dz,...
                    'P_rouse',P_rouse,...
                    'chi_int',chi_int_trapz);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       NOW COMPUTING ACTUAL EROSION
%       TOTAL LOAD MODEL
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Computing the total load erosion is done using the composite equation given by
%Lamb et al 2008, eq. (36)

E1_tot = (constants.A1 .* constants.rho_s .* constants.Y) ./ (constants.k_v .* constants.sigma_T.^2);

E2_tot = c_b .* w_i_eff.^3;

if q_tc == 0 || q_b > q_tc
    E3_tot = 0;
else
    %E3_tot = 1 - q_b/q_tc;
    E3_tot = 1 - q_b/(q_tc.*dt);
end

e_tot_load = E1_tot .* E2_tot .* E3_tot;
%e_tot_load = E1_tot .* E2_tot .* Fe;

%converting to m/a
e_tot_load = e_tot_load .* constants.secondsinyear;

tot_load_stuff = struct('Ir_tot',Ir_tot,...
                        'w_i_eff',w_i_eff,...
                        'P_fluctu',P_fluctu,...
                        'w_s',w_s,...
                        'Hf',Hf,...
                        'c_sed',c_sed,...
                        'c_b',c_b,...
                        'Fe_tot',E3_tot,...
                        'chi',chi,...
                        'w_st',w_st,...
                        'z',z,...
                        'dz',dz,...
                        'q_b',q_b,...
                        'q_sus',q_sus,...
                        'P_rouse',P_rouse);
