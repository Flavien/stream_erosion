%Simple m-file to make the grid interpolation
%Maybe should turn that into a function later, but for now
%This is simpler this way
%

if i > 1
    eta_interp = eta_int_ar(i-1,:);
end

%Calculating how far the particles want to travel
travel_dist = Us_ar(i,:) .* dt_temp;

%Calculating the number of cell that the main ones will be split into
cell_per_cell = dx ./ travel_dist;
%rounding that number...
cpc_round = round(cell_per_cell);

%calculating the new normalized dx
new_dx = 1 ./ cpc_round;

x_length = length(travel_dist);

sum_cpc = cumsum(cpc_round);

%Setting the new grid
x_map = zeros(1,sum(cpc_round)+1);

x_map(1) = 0;
x_map(2:cpc_round(1)+1) = [new_dx(1):new_dx(1):1];
for ii = 2:x_length
    x_map(sum_cpc(ii-1)+1:sum_cpc(ii)+1) = [ii-1:new_dx(ii):ii];
end

x_map_avg = (x_map(2:end) + x_map(1:end-1)) ./ 2;

dx_interp = (x_map(2:end) - x_map(1:end-1)) .* dx;


x_reg =linspace(0.5,49.5,50);

if i == 1
    eta_interp = interp1(x_reg,eta_ar(i,:),x_map_avg,'pchip');
end

q_sup_loc_int = interp1(x_reg,q_tot_loc(i,:),x_map_avg,'spline');

%q_tot_interp = eta_interp.*(1-constants.lambda_s)./dt_temp;
q_tot_interp = q_sup_loc_int.*dt + eta_interp.*(1-constants.lambda_s);
%q_tot_interp = interp1(x_reg,q_tot_temp(i,:),x_map_avg,'cubic');
%q_tot_interp = interp1(x_reg,q_tot_temp(i,:),x_map_avg);
%q_tot_interp(isnan(q_tot_interp) == 1) = 0;
q_tc_interp = interp1(x_reg,q_tc_ar(i,:),x_map_avg,'spline');
R_interp = interp1(x_reg,R(i,:),x_map_avg,'spline');

if i == 1
    eta_int_ar = zeros((max(t_slice)+1)*replicate,sum(cpc_round));
end
