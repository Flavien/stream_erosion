%Function to compute the sediment concentration as a function of depth in the water.
%The idea is to compute both c and c_b following Rouse's (1937) equation
%I follow Lamb et al. (2008)
%c from eq.(26) and c_b from eq. (18)

function [c_sed c_b] = sed_concentration(constants,chi,u_mean,R,Hs,Us,z,z_Hb,zeta_a,P_rouse,q_tot,dt)

%Near bed concentration
if (u_mean.*R.*chi + Us.*Hs) == 0 || isnan(u_mean.*R.*chi + Us.*Hs) == 1
    c_b = 0;
else
    %c_b = q_tot ./ (u_mean.*R.*chi + Us.*Hs);
    c_b = q_tot ./ ((u_mean.*R.*chi + Us.*Hs).*dt);
    %c_b = q_tot ./ (u_mean.*R.*chi + Us.*Hs.*dt);
end
%c_b(c_b > 1) = 1;

%sediment concentration throughout the water column
if Hs >= R
    c_sed = z(z_Hb(1):end) .* 0;
else
    %keyboard
    c_sed = c_b.* (zeta_a).^P_rouse;
end
