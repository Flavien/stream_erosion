%Function to compute the fall velocity normal to the bed according the total load model
%again following Lamb et al. 2008
%
%w_s from eq. (30)

function [w_s] = norm_fall_vel(constants,w_st,theta,Cd,Hf,D)

%ws_A = w_st.*cos(theta);
%ws_A = w_st.*cos(atan(theta));
%NOTE it turns out that in Lamb's paper the theta is kept constant for that computation
%if the slope here keeps on changing, then I obtain somewhat periodic impact velocities
%which is not matching with the reproduciton of Lamb's results
ws_A = w_st.*cos(0.0053);
ws_B = 3.* Cd .* constants.rho_w .* Hf;
%ws_C = 2.*constants.rho_s.*D.*cos(theta);
%ws_C = 2.*constants.rho_s.*D.*cos(atan(theta));
ws_C = 2.*constants.rho_s.*D.*cos(0.0053);

ws_A = abs(ws_A);
ws_C = abs(ws_C);

if min(size(Hf)) > 1
    w_s(ws_C == 0) = 0;
    w_s(isnan(ws_C) ==1) = 0;
else
    if ws_C == 0 || isnan(ws_C) == 1
        w_s = 0;
    else
        w_s = ws_A .* sqrt(1- exp((-1).* ws_B./ ws_C));
    end
end
