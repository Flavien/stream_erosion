%Function to compute the Shields stress as originally proposed
%There is therefore no account for steep coarse-bedded streams processes
%Again following the approach of Sklar & Dietrich 2004
%
%Eq. following 2.16 in lit review


function tau = compute_Shields(u_shear,constants,D,tau_b);

tau_f_shear_vel = 1;


if tau_f_shear_vel == 1
    %Note r = specific gravity
    tau = u_shear.^2 ./ (constants.r.*constants.g.*D);
elseif tau_f_shear_vel == 0
    tau = tau_b ./ ((constants.rho_s - constants.rho_w) .* constants.g .* D);
end
