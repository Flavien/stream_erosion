%This script is meant to compute stream erosion following the approach of
%Sklar and Dietrich 2004.
%The difference with the salt_eros.m file is that here the shear stress is computed following what is
%returned from a hydro run.
%
%The reasoning behind this script is based on the literature review on river erosion that I wrote.

function [e_salt E Vi Ir Fe q_tc tau w_f w_si Ls Dmax] = salt_eros_R_channel(constants,tau_c,D,q_s,tau_b,u_shear,u_mean);

%Following Eq. 3.3 in the lit review
%   e_stream = Vi * Ir * Fe

%Call functions to compute all of that

%call [tau] = compute_Shields
tau = compute_Shields(u_shear,constants,D,tau_b);

%call function for transport capacity [q_tc] = Qtc_FLvB
[q_tc] = Qtc_FLv(constants,D,tau,tau_c);

%call [w_si Ls] = impact_norm_vel()
[w_si Ls w_f Us Hs] = impact_norm_vel(constants,tau,tau_c,D,u_shear);

%call Vi = vol_impact
[Vi] = vol_impact(constants,D,w_si);

%call Ir = impact_rate
[Ir] = impact_rate(q_s,constants,D,Ls);

%call Fe = fraction_exposed
[Fe] = fraction_exposed(q_s,q_tc);

[Dmax] = max_moving_D(constants,tau,tau_c,u_shear);

%Unit of erosion rate: m/s
e_salt = Vi * Ir * Fe;
%conversion to m/a:
e_salt = e_salt * constants.secondsinyear;

%Trying the erosion rate as in equation (24a) of S&D 2004
E1 = (0.08 *constants.r*constants.g*constants.Y)/(constants.k_v*constants.sigma_T^2);
E2 = q_s * (tau/tau_c - 1)^(-1/2);
E3 = 1 - q_s/q_tc;
%Shutdown of erosion is supply exceeds transport
if q_s > q_tc
    E3 = 0;
    %disp('Transport capacity exceeded by supply, E = 0')
end

if u_shear > w_f
    E4 = 0;
else
    E4 = (1 - (u_shear/w_f)^2)^(3/2);
end

E = E1 * E2 * E3 * E4;
%Conversion to m/a
E = E * constants.secondsinyear;
