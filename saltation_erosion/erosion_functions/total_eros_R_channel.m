%This script is meant to compute stream erosion following the approach of
%Sklar and Dietrich 2004 at first.
%Then some plugins should be added or modifications done to add Lamb et al 2008
%After that some principles tied to steep streams probably.
%
%The reasoning behind this script is based on the literature review on river erosion that I wrote.
%
%This function computes both the saltation-erosion and the total load erosion. It is suited to be
%applied to what the subglacial hydro run return.

function [e_salt,E,e_tot,e_tot_nd,Vi,Ir,Fe,q_tc,tau,Dmax,tot_load_stuff,salt_stuff] =...
                             total_eros_R_channel(constants,tau_c,D,q_s,tau_b,u_shear,u_mean,R,q_tot,theta,dt)


%Following Eq. 3.3 in the lit review
%   e_stream = Vi * Ir * Fe
%NOTE:
% q_s = q_tot .* constants.rho_s;
% q_tot = q_tot (q_tot_temp(i,j))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       CALLING FUNCTION TO COMPUTE ALL I NEED
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%call [tau] = compute_Shields
[tau] = compute_Shields(u_shear,constants,D,tau_b);

if tau > tau_c
    %call function for transport capacity [q_tc] = Qtc_FLvB
    [q_tc] = Qtc_FLv(constants,D,tau,tau_c);

    %call [w_si Ls] = impact_norm_vel()
    [w_si,Ls,w_f,Us,Hs] = impact_norm_vel(constants,tau,tau_c,D,u_shear,u_mean);

    %And making sure the clasts are not flying out of the water
    %if Hs > R
    %    Hs = R;
    %end
    Hs = min(Hs,R);

    %call Vi = vol_impact
    [Vi] = vol_impact(constants,D,w_si);

    %call Ir = impact_rate
    [Ir] = impact_rate(q_s,constants,D,Ls);

    %call Fe = fraction_exposed
    [Fe] = fraction_exposed(q_s,q_tc.*dt);

    %Now calling functions for the total load erosion model

    [Cd] = drag_coeff(constants,D,w_f);

    %[chi w_st P_rouse z dz] = chi_param(constants,D,Cd,u_mean,Hs,u_shear,R);
    %IT'S GONNA BE A LITTLE MORE COMPLICATED TO VECTORIZE THESE TWO, SO I'LL JUST LOOP FOR THESE TWO FUNCTIONS...
    [chi,w_st,P_rouse,z,z_Hb,dz,zeta_a,chi_int_trapz] = chi_param(constants,D,Cd,u_mean,Hs,u_shear,R);

    [c_sed,c_b] = sed_concentration(constants,chi,u_mean,R,Hs,Us,z,z_Hb,zeta_a,P_rouse,q_tot,dt);
    %condition so that c_b does not become too high when sediment chi --> 0
    if D>Hs c_b = 0; end

    %[Hf] = fall_height(constants,z,dz,c_b,c_sed,R,Hs);
    [Hf] = fall_height(constants,z,z_Hb,dz,c_b,c_sed,R,Hs);
    %PROBABLY HARD TO VECTORIZE TOO...

    [w_s] = norm_fall_vel(constants,w_st,theta,Cd,Hf,D);

    [w_i_eff,P_fluctu] = eff_impact_vel(constants,u_shear,w_s);

    [Ir_tot] = impact_rate_tot_load(c_b,w_s,R);

    [q_b,q_sus] = vol_bedload_flux(constants,c_b,q_tot,R,Us,Hs,u_mean,chi,dt);

    [Dmax] = max_moving_D(constants,tau,tau_c,u_shear);
else
    q_tc = 0;
    w_si = 0; Ls = 0; w_f = 0; Us = 0; Hs = 0;
    Vi = 0;
    Ir = 0; Ir_tot = 0;
    Fe = 0;
    Cd = 0;
    c_b = 0; w_i_eff = 0;
    P_fluctu = 0; w_s = 0;
    Hf = 0; c_sed = 0; chi = 0; z = 0; dz = 0;
    w_st = 0; P_rouse = 0;
    q_b = 0; q_sus = 0;
    Dmax = 0;
end

     


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       NOW COMPUTING ACTUAL EROSION
%       SALTATION MODEL
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Unit of erosion rate: m/s
e_salt = Vi * Ir * Fe;
%conversion to m/a:
e_salt = e_salt * constants.secondsinyear ./dt;

%Trying the erosion rate as in equation (24a) of S&D 2004
E1 = (0.08 *constants.r*constants.g*constants.Y)/(constants.k_v*constants.sigma_T^2) ./ dt;
E2 = q_s .* (tau./tau_c - 1).^(-1/2);
E3 = 1 - q_s/(q_tc.*dt);
%Shutdown of erosion is supply exceeds transport
if q_s > q_tc.*dt
    E3 = 0;
    %disp('Transport capacity exceeded by supply, e_salt = 0')
end

if u_shear > w_f
    E4 = 0;
else
    E4 = (1 - (u_shear/w_f)^2)^(3/2);
end

E = E1 * E2 * E3 * E4;
%Conversion to m/a
E = E * constants.secondsinyear;
%keyboard;

salt_stuff = struct('w_f',w_f,...
                    'w_si',w_si,...
                    'Hs',Hs,...
                    'Ls',Ls,...
                    'Us',Us);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       NOW COMPUTING ACTUAL EROSION
%       TOTAL LOAD MODEL
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Computing the total load erosion is done using the composite equation given by
%Lamb et al 2008, eq. (36)

E1_tot = (constants.A1 .* constants.rho_s .* constants.Y) ./ (constants.k_v .* constants.sigma_T.^2);

E2_tot = c_b .* w_i_eff.^3;

%E3_tot = 1 - q_b/q_tc;
if q_tc == 0 || q_b > q_tc.*dt
    E3_tot = 0;
else
    E3_tot = 1 - q_b/(q_tc.*dt);
end



%e_tot = E1_tot .* E2_tot .* Fe;
e_tot = E1_tot .* E2_tot .* E3_tot;

%Computing the dimensionless total erosion
%nd_fact = constants.sigma_T^2 ./ (constants.rho_s .* constants.Y .* (constants.g .* D).^(3/2));
%e_tot_nd = e_tot .* nd_fact;

End1 = constants.A1./constants.k_v;
End2 = c_b;
End3 = (w_i_eff ./ (constants.g*D).^(1/2)).^3;

e_tot_nd = End1 .* End2 .* End3 .* E3_tot .*constants.secondsinyear;

%converting to m/a
e_tot = e_tot .* constants.secondsinyear;

tot_load_stuff = struct('Ir_tot',Ir_tot,...
                        'w_i_eff',w_i_eff,...
                        'P_fluctu',P_fluctu,...
                        'w_s',w_s,...
                        'Hf',Hf,...
                        'c_sed',c_sed,...
                        'c_b',c_b,...
                        'Fe_tot',E3_tot,...
                        'chi',chi,...
                        'w_st',w_st,...
                        'z',z,...
                        'dz',dz,...
                        'q_b',q_b,...
                        'q_sus',q_sus,...
                        'P_rouse',P_rouse);
