%Function to compute the basal shear stress for flow in an open channel
%and assuming a rectangular channel cross section
%Based on Manning's equation and Sklar & Dietrich 2004 Eq. 25 - 26
%
%This is not for the final code, just to test if I can reproduce
%S. & D. (2004) results.

function [tau_b u_shear u_mean] = open_flow_tau_b(Hw,W,slope,theta,constants,Qw);

%hydraulic radius in a rectangular channel
Rh_f_Qw = 1;
if Rh_f_Qw == 1
    %man_n = Manning n
    Rh_A = Qw * constants.man_n;
    Rh_B = Hw * W * slope^(1/2);

    if Rh_B == 0
        Rh = 0;
    else
        Rh = (Rh_A / Rh_B)^(3/2);
    end

elseif Rh_f_Qw == 0
    Rh_A = Hw * W;
    Rh_B = 2*Hw + W;

    Rh = Rh_A / Rh_B;
end

%basal shear stress
%tau_b = constants.rho_w * constants.g * Rh * slope;
tau_b = constants.rho_w * constants.g * Hw * slope;

%shear velocity
%u_shear = (tau_b / constants.rho_w)^(1/2);
%or as written in Lamb 2008
%u_shear = (constants.g * Hw * sin(theta))^(1/2);
u_shear = (constants.g * Hw * theta)^(1/2);

%u_mean = Rh^(2/3) * slope^(1/2) / constants.man_n;

z_0 = 3.*0.06./30;
z_u = linspace(z_0,Hw,1000);
dz_u = (Hw-z_0)./1000;

u_mean = 1./Hw .* trapz(u_shear./constants.kappa .* log(z_u./z_0)) .* dz_u;
