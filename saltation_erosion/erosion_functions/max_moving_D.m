%Function to compute the size of the maximum moving diameter for given flow conditions
%The idea is simply to invert the Shields stress equation for particle diameter while
%assuming a critical shear stress.
%

function [Dmax] = max_moving_D(constants,tau_b,tau_c,u_shear);

%Dmax = tau_b ./ (tau_c .* (constants.rho_s - constants.rho_w) .* constants.g);

%if you want to compute it with the u_shear
Dmax = (u_shear.^2) ./ (tau_c .* constants.r .* constants.g );
