%function to calculate the transport capacity of a stream based on
%Fernandez-Luque & van Beek 1976 equations
%
%Eq. 3.10 in lit review

function [q_tc] = Qtc_FLv(constants,D,tau,tau_c);

if min(size(tau)) > 1
    %for vector inputs
    tau_sub = tau - tau_c;
    tau_sub(tau_sub < 0) = 0; 
    q_tc = 5.7 .* constants.rho_s .* (constants.r.*constants.g.*D.^3).^(1/2) .* (tau_sub).^(3/2);
else
    %For double values
    if (tau - tau_c) > 0
        q_tc = 5.7 .* constants.rho_s .* (constants.r.*constants.g.*D.^3).^(1/2) .* (tau - tau_c).^(3/2);
    else
        q_tc = tau.*0;
    end
end
%This formula returns the transport capacity per unit width [kg/(m s)]
%To get total transport capacity, multiply by W
