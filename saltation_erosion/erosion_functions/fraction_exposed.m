%Function to compute the fraction of the bedrock exposed
%
%Part of the saltation abrasion model from Sklar and Dietrich 2004
%
%See formula 3.8 in lit. review

function [Fe] = fraction_exposed(q_s,q_tc);

Fe = 1 - q_s./(q_tc);
Fe(Fe < 0) = 0;
