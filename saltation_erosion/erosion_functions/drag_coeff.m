%Function to compute the drag coefficient Cd as in Dietrich 1982.
%Cd from eq. (4)
%
%

function [Cd] = drag_coeff(constants,D,w_f) 

Cd_top = 4 .* (constants.rho_s - constants.rho_w) .* constants.g .* D;
Cd_bot = 3 .* constants.rho_w .* w_f^2;

Cd = Cd_top ./ Cd_bot;
