%Function to compute the effective impact velocity of a particle on the bed in the total load erosion model
%Again this follows Lamb et al 2008
%
%w_i_eff from eq.(35)

function [w_i_eff P_fluctu] = eff_impact_vel(constants,u_shear,w_s)

sigma_w = u_shear;

%set the range over which to integrate velocity fluctuations
w_prime = linspace(-w_s,6*sigma_w,100);
dw = (6.*sigma_w + w_s) ./ 100;

%Probability density function of velocity fluctuations (eq. 33)
P_fluctu = 1./(sqrt(2.*pi).*sigma_w) .* exp( (-1).* w_prime.^2 ./ (2.*sigma_w.^2));

%Effective impact velocity
w_i_eff_int = trapz( (w_prime + w_s).^3 .* P_fluctu ) .* dw;
w_i_eff = w_i_eff_int.^(1/3);
