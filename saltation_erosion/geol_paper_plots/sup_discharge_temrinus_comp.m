%In this file I simply load two discharge profiles to
%from the ENTL and TV runs to show the differences in outlet discharge
%
%

TV_nb = 18;
IG_nb = 11;

%load trans_IS_ENTL_HR_bs3_D256.mat
%IG_load = trans_IS;

%load trans_IS_TR_K_vHR_D256.mat
%TV_load = trans_IS;


fx = 8.9;
fy = 5;

LW1 = 2;
LW2 = 1;

t_plot = [1:1741]./12; %in days

f2 = figure;
f2.Units = 'centimeters';
f2.Position(3) = fx;
f2.Position(4) = fy;
f2.Units = 'normalized';
%%%%%%%%%%%%%%%%%%%%%%

p1 = plot(t_plot,IG_load(IG_nb).Qc(:,end-1));
hold on
p2 = plot(t_plot,TV_load(TV_nb).Qc(:,end-1));

p1.Color = rgb('SlateGrey');
p2.Color = 'k';
p1.LineWidth = LW1;
p2.LineWidth = LW1;

ylabel('Discharge (m^3 s^{-1})');
xlabel('Time during simulation (days)');

ax = gca;

ax.YLim = [0 400];

l1 = legend('Inner gorge scenario','Tunnel valley scenario');
l1.Location = 'NorthWest';
l1.Box = 'off';
