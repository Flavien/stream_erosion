%File to plot the hydrographs of different floods and the
%synthetic season input discharge
%
%

load ref_input
load trans_IS_all_flood_Vx01

Q_bs = sum(b_s_time(1:17400,:)').*1000.*500; %xdx xdy 

figure
for i = 1:6
    %p_hyd{i+1} = plot(trans_IS(11*i+11-i).Qc(:,1));
    kk = 11*i - (i-1)*2;
    %keyboard
    x_max = length(trans_IS(kk).Qc(:,1));
    x_plot = [0:1:x_max-1]./24;
    p_hyd{i+1} = semilogy(x_plot,trans_IS(kk).Qc(:,1)-4.9);
    hold on
end

%p_seas = plot(Q_bs(1:6:end));
x_plot_s = [0:length(Q_bs)/6-1]./24;
p_seas = semilogy(x_plot_s,Q_bs(1:6:end));

ax_hyd = gca;
ax_hyd.YLim(1) = 0.3;
ax_hyd.XLim = [0 120];
