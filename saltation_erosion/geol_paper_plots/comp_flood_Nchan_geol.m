%File to plot a the N-channel carved from different simulations

load_first = 1;

if load_first == 1
    load trans_IS_all_flood.mat
    tIS_all_floods = trans_IS;
    clearvars trans_IS;
    load trans_IS_flood_test
    trans_IS(2) = tIS_all_floods(20);
end

%Runs that I need:
runs_needed = [1 2];
plot_Xsect = 0;
%flag = 0 1 2
%2 for AGU presentation stuff
%0 to get the fig A and B
%1 one for just the clabel
AGU_pres = 2;

%plotting stuffs...
x_plot = [1:min(size(trans_IS(1).q_tc_vol))]-0.5;
x_plot = x_plot .* 500./1000;

%Font btwn 7 and 12
FSlab = 10;
FSax  = 9;
FSax_in  = 9;
FScont = 8;
FS_txt = 9;

text_frac = 0.9;

%Line thickness btw 1 and 2
LW1 = 1;
LW2 = 1.5;
LW3 = 2;

fig1 =1;
fig2 =2;
%fx = 5.9*2; %2 columns out of 3 columns page
fx = 8.9*1; %1 columns out of 2 columns page
fy = 11;
sx1 = 3;
sy1 = 1;

L10 = 0;
L50 = 1;

x_prof_loc_seasA = 60;
x_prof_loc_seasB = 95;

x_prof_loc_fldA = 40;

x_start = 1;

cont_nb = 10;

run_nb = length(runs_needed);

%%%%%%%%%%%
f1 = figure(fig1);
f1.Units = 'centimeters';
f1.Position(3) = fx;
f1.Position(4) = fy;
f1.Units = 'normalized';

%textx1 = 11;
%textx2 = 20;
textx1 = 1.5;
textx2 = 9;
metre_fact = 1e-3;

kk = 1;
S1 = subplot(sx1,sy1,1);
    
    %k = runs_needed(kk);
    k = runs_needed(1);
    vectSA1 = [-120:2:0].*metre_fact;
    vectSA2 = [-10 0].*metre_fact;
    vectSA3 = [-100 0].*metre_fact;

    width_coordA = trans_IS(k).width + max(trans_IS(k).width);
    [CA,hA] = contourf(x_plot(x_start:end),width_coordA,trans_IS(k).valley_topo(:,x_start:end).*metre_fact,vectSA1);
    hold on

    hA.LineColor = 'none';
    [CA3,hA3] = contour(x_plot(x_start:end),width_coordA,trans_IS(k).valley_topo(:,x_start:end).*metre_fact,vectSA3);
    [CA2,hA2] = contour(x_plot(x_start:end),width_coordA,trans_IS(k).valley_topo(:,x_start:end).*metre_fact,vectSA2);
    hA2.LineColor = 'k';
    hA3.LineColor = 'w';
    
    colormap('gray')
    cb = colorbar;
    %cb.Limits = XXXX
    cb.Location = 'south';

if AGU_pres == 2;
    textyA = max(trans_IS(runs_needed(2)).width) .* 0.83;
    %textyA = max(width_coordA) .* 0.9;
    TA1 = text(textx1,textyA,'A');
    %TA2 = text(textx2,textyA,'TR\_IS\_REF');
    %TA2 = text(textx2,textyA,'Season');
    TA1.FontSize = FSax;
    %TA2.FontSize = FSax;
end

if AGU_pres == 2
    S1.YLim = [-25 25];
    S1.YTick = [-25 -15 -5 5 15];
    S1.YTickLabel = {'0','10','20','30','40'};
end

    %t1 = clabel(CA2,'manual');


    %xlabel('Distance from ice divide (km)')
    ylabel('Channel width (m)')
    set(gca,'FontSize',FSax)

    %pAXsec1 = plot(x_prof_loc_seasA./2,[-4:1:4],'-','Color',rgb('Grey'));
    %pAXsec2 = plot(x_prof_loc_seasB./2,[-5:1:5],'Color',rgb('Maroon'));
    pAXsec1 = line([x_prof_loc_seasA./2 x_prof_loc_seasA./2],[-1 5.5],'Color',rgb('DarkSlateGrey'));
    pAXsec2 = line([x_prof_loc_seasB./2 x_prof_loc_seasB./2],[-3 7],'Color',rgb('LightSlateGrey'));

    pAXsec1.LineWidth = LW3;
    pAXsec2.LineWidth = LW3;

    %annotations on graph:
    x0_st = 0.508;
    x0 = [x0_st x0_st+(0.265-0.22)];
    y0_st = 0.9014;
    y0 = [y0_st y0_st-0.05]; 

    %0.5679 0.9232
    x001_st= 0.62;
    x001 = [x001_st x001_st-0.02];
    y001 = y0; 
    

    x01_st = 0.86;
    x01 = [x01_st x01_st+0.01];
    y01 = y0;

    a1 = annotation('textarrow',x0,y0,'String','0');
    a2 = annotation('textarrow',x001,y001,'String','-0.01');
    a3 = annotation('textarrow',x01,y01,'String','-0.1');

    a1.FontSize = FScont;
    a3.FontSize = FScont;
    a2.FontSize = FScont;

kk = 2;
S2 = subplot(sx1,sy1,2);
    
    %k = runs_needed(kk);
    k = runs_needed(2);
    %vectSB1 = [-120:5:0];
    %vectSB2 = [-120:20:0];
    width_coordB = trans_IS(k).width + max(trans_IS(k).width);
    [CB,hB] = contourf(x_plot(x_start:end),width_coordB,trans_IS(k).valley_topo(:,x_start:end).*metre_fact,vectSA1);
    hold on
    [CB2,hB2] = contour(x_plot(x_start:end),width_coordB,trans_IS(k).valley_topo(:,x_start:end).*metre_fact,vectSA2);
    %hB.LineColor = [0.7 0.7 0.7];
    hB2.LineColor = 'k';
    hB.LineColor = 'none';

    %textyB = max(trans_IS(k).width) .* 0.9;
    textyB = max(width_coordB) .* 0.87;

if AGU_pres == 2
    TB1 = text(textx1,textyB,'B');
    %TB2 = text(textx2,textyB,'FLOOD\_Nye\_shape');
    %TB2 = text(textx2,textyB,'20-days flood, V_{water} x10');
    TB1.FontSize = FSax;
    %TB2.FontSize = FSax;
end

    cb2 = colorbar;
    cb2.Location = 'south';
    caxis([-120 0].*metre_fact)
    cb2.Visible = 'off';
    %S2.YAxisLocation = 'right';

    xlabel('Distance from ice divide (km)')
    %ylabel('N-Channel radius (m)')
    set(gca,'FontSize',FSax)
    S2.YTick = [0 10 20 30 40];

    %pBXsec1 = plot(x_prof_loc_fldA./2,[3:1:45],'Color',rgb('Black'));
    pBXsec1 = line([x_prof_loc_fldA./2 x_prof_loc_fldA./2],[0 49.9],'Color',rgb('Black'));

    pBXsec1.LineWidth = LW3;

if plot_Xsect == 1

    inset_axes = axes('Position',[0.16 0.295 0.23 0.2]);

        hold off
        p2a = plot(width_coordB,trans_IS(2).valley_topo(:,x_prof_loc_fldA).*metre_fact);
        hold on
        p2b = plot(width_coordA+22.5,trans_IS(1).valley_topo(:,x_prof_loc_seasA).*metre_fact);
        p2c = plot(width_coordA+22.5,trans_IS(1).valley_topo(:,x_prof_loc_seasB).*metre_fact);

        eros_2mm = (trans_IS(2).valley_topo(:,40).*0 - 2).*metre_fact;
        eros_10mm = eros_2mm *5;
        
        %p2d = plot(width_coordB,eros_2mm,'--');
        %p2e = plot(width_coordB,eros_10mm,':');

        ylabel('Depth (m)','FontSize',FSax_in)
        xlabel('Width (m)','FontSize',FSax_in)

        %l2 = legend('Flood','Annual processes (upstream)','Annual processes (downstream)');
        %l2.FontSize = FSax;
        %l2.Location = 'SouthEast';
        %l2.Box = 'off';

        ax2 = gca;
        
        inset_axes.FontSize = FSax_in;
        inset_axes.YLim = [-120 0].*metre_fact;
        inset_axes.XLim = [10 40];

        p2a.LineWidth = LW3;
        p2b.LineWidth = LW3;
        p2c.LineWidth = LW3;
        p2d.LineWidth = LW2;
        p2e.LineWidth = LW2;

        p2a.Color = 'k';
        p2b.Color = rgb('DarkSlateGrey');
        p2c.Color = rgb('LightSlateGrey');
        p2d.Color = rgb('MediumBlue');
        p2e.Color = rgb('MediumBlue');

        %Make the axis in the inset grey
        grey_fact = 0.4;
        inset_axes.XColor = [1 1 1].*grey_fact;
        inset_axes.YColor = [1 1 1].*grey_fact;
        inset_axes.XLabel.Position(2) = -0.155;
        inset_axes.Color = 'none';
        %inset_axes.XLabel.Color = [1 1 1].*grey_fact;
        %inset_axes.YLabel.Color = [1 1 1].*grey_fact;
end


Nchan_Xlim = [0 50];
S1.XLim = Nchan_Xlim;
S2.XLim = Nchan_Xlim;

Nchan_Xtick = [0:5:50];
S1.XTick = Nchan_Xtick;
S2.XTick = Nchan_Xtick;

Nchan_XTickLab = {'0','','10','','20','','30','','40','','50'};
%S1.XTickLabel = Nchan_XTickLab;
S1.XTickLabel = '';
S2.XTickLabel = Nchan_XTickLab;

S3 = subplot(sx1,sy1,3);

        p2a = plot(width_coordB,trans_IS(2).valley_topo(:,x_prof_loc_fldA).*metre_fact);
        hold on
        p2b = plot(width_coordA+22.5,trans_IS(1).valley_topo(:,x_prof_loc_seasA).*metre_fact);
        p2c = plot(width_coordA+22.5,trans_IS(1).valley_topo(:,x_prof_loc_seasB).*metre_fact);

        eros_2mm = (trans_IS(2).valley_topo(:,40).*0 - 2).*metre_fact;
        eros_10mm = eros_2mm *5;
        
        %p2d = plot(width_coordB,eros_2mm,'--');
        %p2e = plot(width_coordB,eros_10mm,':');

        ylabel('Depth (m)','FontSize',FSax_in)
        xlabel('Width (m)','FontSize',FSax_in)

        %l2 = legend('Flood','Annual processes (upstream)','Annual processes (downstream)');
        %l2.FontSize = FSax;
        %l2.Location = 'SouthEast';
        %l2.Box = 'off';

        inset_axes = gca;
        
        inset_axes.FontSize = FSax_in;
        inset_axes.YLim = [-120 0].*metre_fact;
        inset_axes.XLim = [10 40];

        p2a.LineWidth = LW3;
        p2b.LineWidth = LW3;
        p2c.LineWidth = LW3;
        p2d.LineWidth = LW2;
        p2e.LineWidth = LW2;

        p2a.Color = 'k';
        p2b.Color = rgb('DarkSlateGrey');
        p2c.Color = rgb('LightSlateGrey');
        p2d.Color = rgb('MediumBlue');
        p2e.Color = rgb('MediumBlue');

        %Make the axis in the inset grey
        grey_fact = 0.4;
        %inset_axes.XColor = [1 1 1].*grey_fact;
        %inset_axes.YColor = [1 1 1].*grey_fact;
        %inset_axes.XLabel.Position(2) = -0.155;
        %inset_axes.Color = 'none';
        tC = text(11,-(0.12 - 0.12.*0.85),'C','FontSize',FSax);

%Making everything pretty again...
sub_h = 0.28;
S1.Position(4) = sub_h;
S2.Position(4) = sub_h;
S3.Position(4) = sub_h.*0.65;

sub_x = 0.185;
S1.Position(1) = sub_x; 
S2.Position(1) = sub_x; 
S3.Position(1) = sub_x; 

S1.Position(2) = 0.70;
S2.Position(2) = 0.41;

S1.YLabel.Position(2) = -25;

cb.Position(2) = 0.71;
cb.Position(4) = 0.03;
cb.Label.String = 'Depth (m)';
cb.Label.Position(1) = -0.09;
cb.Label.Position(2)= 2.7;
cb.Ticks = [-120:20:0].*metre_fact;
cb.FontSize = FScont;

clabel(CB2,hB2,'Color','k','FontSize',FScont,'LabelSpacing',1000)

%{
cb.Label.Position(1) = -140.*metre_fact;
cb.Label.Position(2) = 0.9;

cb.FontSize = FSax_in;


if AGU_pres < 2
    clabel(CA3,hA3,'Color','w','FontSize',FScont)
    clabel(CA2,hA2,'Color','k','FontSize',FScont,'LabelSpacing',550)
end

clabel(CB2,hB2,'Color','k','FontSize',FScont,'LabelSpacing',1000)

sub_w = 0.4;
S1.Position(3) = sub_w;
S2.Position(3) = sub_w;

S1.Position(1) = 0.075;
S2.Position(1) = 0.523;


%Now let's put some lines around the inset
subplot(S1)
xp1 = 1;
xp2 = 41;
yp1 = -24.2;
yp2 = -0.2;
l_insax1 = line([xp1 xp2],[yp1 yp1]);
l_insax2 = line([xp1 xp2],[yp2 yp2]);
l_insax3 = line([xp1 xp1],[yp1 yp2]);
l_insax4 = line([xp2 xp2],[yp1 yp2]);
l_insax1.Color = [1 1 1].*grey_fact;
l_insax2.Color = [1 1 1].*grey_fact;
l_insax3.Color = [1 1 1].*grey_fact;
l_insax4.Color = [1 1 1].*grey_fact;

%}
