%File to make a figure that show the details of the inner gorge that is formed.
%This is kindda of an attempt at making a figure for the paper
%
%
%
%It might be good to run the making topo script first to at least compute the
%topo that you might plot

%Load all the different profiles used for the different stages

%load trans_IS_TR_K_vHR_D256.mat
load TV_topo_D256_trueW200.mat
load IG_topo_D256_W100.mat


dx = 500; %m
x_st = 1;
x_end = 541;
x_st_from_div = 820; %km

max_prof_nb = 7625;
%adv_prof_rg = [2:1:16];
%rtr_prof_rg = [18:1:31];
adv_end = 4470;
adv_prof_rg = [1:1:adv_end];
rtr_prof_rg = [adv_end+1:1:max_prof_nb];

%x_t_l = [min(x_tot)-dx x_tot];

ice_dt = 250;

fx = 5.9*2;
fy = 7;
sx = 2;
sy = 2;

LW1 = 2;
LW2 = 1.5;
LWbed = 1;
%FSax = 36;
FSax = 9;
FSlg = 8;

min_xax = 850;
max_xax = 1090;

col_advance = rgb('SkyBlue');
col_retreat = rgb('SteelBlue');
col_max =  rgb('RoyalBlue');

ice_st = 11;
ice_end = 146;
%x_ice = [ice_st-1:ice_end-1].*dx.*4./1e3 + x_st_from_div; %I want that in km and dx 4 times that for eros
x_ice = [0:ice_end-ice_st].*dx.*4./1e3 + x_st_from_div; %I want that in km and dx 4 times that for eros

f1 = figure;
f1.Units = 'centimeters';
f1.Position(3) = fx;
f1.Position(4) = fy;
f1.Units = 'normalized';

%%%%%%%%%%%%%%%%%%%%%%
%   1ST subplot
S1 = subplot(sx,sy,1);

[C,h] = contourf(x_tot_TV,y_tot_TV(1:100:end).*1e3-100,resulting_topo_TV_meters(1:100:end,:),30);
h.LineColor = 'none';
cb = colorbar;
%cb.Location = 'west';
cb.Location = 'NorthOutside';
%cb.Label.String = 'Erosion depth (m)';
cb.Label.String = 'Depth (m)';
cb.Ticks = [-20 -15 -10 -5 0];
axis([min_xax max_xax -100 100]);
S1.XTickLabel = '';

%ylabel({'Bedrock channel';'width (km)'})
ylabel('Half width (m)')
S1.YLabel.Position(1) = 808;

%%%%%%%%%%%%%%%%%%%%%%
%   2ND subplot
S2 = subplot(sx,sy,2);
[C2,h2] = contourf(x_tot_IG,y_tot_IG(1:100:end).*1e3-100,resulting_topo_IG_meters(1:100:end,:),30);
h2.LineColor = 'none';
cb2 = colorbar;
cb2.Location = 'NorthOutside';
max_depth2 = min(min(resulting_topo_IG_meters));
cb2.Limits = [max_depth2 0];
%cb.Label.String = 'Erosion depth (m)';
cb2.Label.String = 'Depth (m)';
axis([5 35 -100 100])

S2.XTickLabel = '';
S2.YTickLabel = '';

%%%%%%%%%%%%%%%%%%%%%%
%   3RD subplot
S3 = subplot(sx,sy,3);

load long_term_sims/cheat_profs_TV_1yr.mat

p1a = plot(x_ice,ice_profs_1yr(1,ice_st:ice_end),'k','LineWidth',LWbed);
hold on
%p1b = plot(x_ice,ice_profs_1yr(2+10*max_prof_nb,ice_st:ice_end),'k','LineWidth',LWbed,'Color',col_max);
p1b = plot(x_ice,ice_profs_1yr(max(adv_prof_rg),ice_st:ice_end),'k','LineWidth',LW1,'Color',col_max);

kk = 1;
for i = 125:250:max(adv_prof_rg)
    p_adv(kk,:) = plot(x_ice,ice_profs_1yr(i,ice_st:ice_end),'LineWidth',LW2,'Color',col_advance);
    kk = kk+1;
end
kk = 1;
for i = max(adv_prof_rg)+250:250:max(rtr_prof_rg)-125
    p_rtr(kk,:) = plot(x_ice,ice_profs_1yr(i,ice_st:ice_end),'LineStyle','--','LineWidth',LW2,'Color',col_retreat);
    kk = kk+1;
end

l1a = legend([p1a p_adv(1) p_rtr(2)],'Bed','Advance','Retreat');
l1a.Orientation = 'horizontal';
l1a.Location = 'NorthOutside';
l1a.FontSize = FSlg;
l1a.Box = 'off';
%l1a.Position(2) = 0.85;
%l1a.Position(1) = 0.75;

xlabel('Distance from ice divide (km)')
ylabel('Elevation (m)')

S3.XLim = [min_xax max_xax];
S3.YLim = [0 2800];

S3.YTickLabelRotation = 35;
S3.YLabel.Position(1) = 0.805e3;



%%%%%%%%%%%%%%%%%%%%%%
%   4TH subplot
S4 = subplot(sx,sy,4);

plot_ice_profiles;
axis([5 35 0 1400])
xlabel('Distance from ice divide (km)')

S4.YTickLabelRotation = 35;


%%%%%%%%%
%making everything neat
S1.FontSize = FSax;
S2.FontSize = FSax;
S3.FontSize = FSax;
S4.FontSize = FSax;

sub_w = 0.39;
S1.Position(3) = sub_w;
S2.Position(3) = sub_w;
S3.Position(3) = sub_w;
S4.Position(3) = sub_w;

sub_h_top = 0.29;
S1.Position(4) = sub_h_top;
S2.Position(4) = sub_h_top;

sub_h_bot = 0.31;
S3.Position(4) = sub_h_top;
S4.Position(4) = sub_h_top;

top_y_align = 0.53;
S1.Position(2) = top_y_align;
S2.Position(2) = top_y_align;

bot_y_align = 0.13;
S3.Position(2) = bot_y_align;
S4.Position(2) = bot_y_align;

left_x_align = 0.11;
S1.Position(1) = left_x_align;
S3.Position(1) = left_x_align;

cb2.Position(2) = 0.84;
cb.Position(2) = cb2.Position(2);
cb.Position(4) = 0.035;
cb2.Position(4) = 0.035;

l1a.Position(1) = 0.24;
l1a.Position(2) = 0.43;

%Tick lenghts
tl1 = 0.012;
tl2 = 0.025;
S1.TickLength = [tl1 tl2];
S2.TickLength = [tl1 tl2];
S3.TickLength = [tl1 tl2];
S4.TickLength = [tl1 tl2];

S1.XMinorTick = 'on';
S1.YMinorTick = 'on';
S2.XMinorTick = 'on';
S2.YMinorTick = 'on';
S3.XMinorTick = 'on';
S3.YMinorTick = 'on';
S4.XMinorTick = 'on';
S4.YMinorTick = 'on';

%Adding text on there
subplot(S1)
%xposfact = 0.05;
xposfact = 0.95;
xposfact2 = 0.96;
%yposfact = 0.85;
yposfact = 0.15;
tA = text(S1.XLim(2) - (S1.XLim(2)-S1.XLim(1)).*xposfact,S1.YLim(2).*yposfact-85,'A');
subplot(S2)
tB = text(S2.XLim(2) - (S2.XLim(2)-S2.XLim(1)).*xposfact,S2.YLim(2).*yposfact-85,'B');
subplot(S3)
tC = text(S3.XLim(2) - (S3.XLim(2)-S3.XLim(1)).*xposfact2,S3.YLim(2).*yposfact,'C');
subplot(S4)
tD = text(S4.XLim(2) - (S4.XLim(2)-S4.XLim(1)).*xposfact2,S4.YLim(2).*yposfact,'D');
