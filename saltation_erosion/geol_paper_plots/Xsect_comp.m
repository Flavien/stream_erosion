%file to plot the cross section of the inner gorge landform that I create
%
%
%

%Profiles needed:
% km 15.1 => 46
%km 19.5 => 66
%km 22.5 => 80
%km 26.14 => 97
%km 28 => 105
%km 29.6 => 113
%km 32.8 => 128
%km 35 => 138

load IG_topo_D256_W100.mat
load TV_topo_D256_W200.mat

fx = 8.9*2;
fy = 14;

f1 = figure;
f1.Units = 'centimeters';
f1.Position(3) = fx;
f1.Position(4) = fy;
f1.Units = 'normalized';

sx_cr = 2;
sy_cr = 4;

LW1 = 2;
FSax = 12;
legTickLen = 0.25;
LWleg = 1.2;

cmap_init = colormap('parula');
cmap_init = flip(cmap_init);
cmap = cmap_init(10:end,:);
cmap_len = length(cmap);



%cut_piece = 7500;
%cut_piece = 5000;
cut_piece = 0;
%Bring the cross section values back in metres
y_tot_cut = y_tot_IG(cut_piece+1:end-cut_piece).*1e3;

k = 1;

section_slices_IG = [32 85 128 138];
section_slices_TV = [137 250 345 470 524 541];

S1 = subplot(2,2,2);
    for i = section_slices_IG
        %p_crossIG{k} = plot3(y_tot_cut.*0 + x_tot_IG(i),y_tot_cut,resulting_topo_IG_meters(cut_piece+1:end-cut_piece,i),'LineWidth',LW1);
        p_crossIG{k} = plot(y_tot_cut,resulting_topo_IG_meters(cut_piece+1:end-cut_piece,i),'LineWidth',LW1);
        hold on
        k = k+1;
        %p_long = plot3(x_tot(section_slices(1):end),x_tot(section_slices(1):end).*0+0.1,resulting_topo(10000,section_slices(1):end));
    end
    IG_ax = gca;
    IG_ax.XLim = [0 200];
    xlabel('Inner gorge width (m)')
    ylabel('Depth of erosion (m)')

    leg_IG_values = round(x_tot_IG(section_slices_IG));
    for tt = 1:length(leg_IG_values)
        leg_IG_text{tt} = strcat(num2str(leg_IG_values(tt)),' km');
    end

    [leg1IG leg2IG] = legend(leg_IG_text);
    leg1IG.Box = 'off';
    
    for i = length(section_slices_IG)+1:2:3.*length(section_slices_IG)
        leg2IG(i).XData(1) = leg2IG(i).XData(2) - legTickLen;
        leg2IG(i).LineWidth = LWleg;
    end

    leg1IG.Box = 'off';
    leg1IG.Position = [0.7506 0.5844 0.1525 0.1675];
    %legendIG.Location = 'SouthEast';

    S1.Title.String = 'Width of erosion 50m; D=256mm';

    %Now let's play with these colors a bit
    pnb = length(p_crossIG);
    for ii = 1:pnb
        if ii ==1
            p_crossIG{ii}.Color = cmap(1,:);
        else
            p_crossIG{ii}.Color = cmap(round(ii/pnb.*cmap_len),:);
        end
    end

k=1;
S2 = subplot(2,2,1);
    for i = section_slices_TV
        %p_crossTV{k} = plot3(y_tot_cut.*0 + x_tot_TV(i),y_tot_cut,resulting_topo_TV_meters(cut_piece+1:end-cut_piece,i),'LineWidth',LW1);
        p_crossTV{k} = plot(y_tot_cut,resulting_topo_TV_meters(cut_piece+1:end-cut_piece,i),'LineWidth',LW1);
        hold on
        k = k+1;
        %p_long = plot3(x_tot(section_slices(1):end),x_tot(section_slices(1):end).*0+0.1,resulting_topo(10000,section_slices(1):end));
    end
    TV_ax = gca;

    TV_ax.XLim = [0 200];
    TV_ax.YLim = IG_ax.YLim;

    %%%%%%%%%%%%%% FOR plot3 %%%%%%%%%%
    %TV_ax.YLim = [0.05 0.15];
    %TV_ax.ZLim = IG_ax.YLim;
    %grid on
    %TV_ax.ZMinorGrid = 'on';

    %xlabel('Distance from ice divide (km)')
    %ylabel('Tunnel valley width (km)')
    %zlabel('Depth of erosion (m)')
    %S2.CameraPosition = [3.628673860771241 -0.000296423252314 0.175610451734947].*1e3;
    %%%%%%%%%%%%%% was FOR plot3 %%%%%%%%%%

    xlabel('Tunnel valley width (m)')
    ylabel('Depth of erosion (m)')

    leg_TV_values = round(x_tot_TV(section_slices_TV));
    for tt = 1:length(leg_TV_values)
        leg_TV_text{tt} = strcat(num2str(leg_TV_values(tt)),' km');
    end
    [leg1TV leg2TV] = legend(leg_TV_text);
    leg1TV.Box = 'off';
    
    for i = length(section_slices_TV)+1:2:3.*length(section_slices_TV)
        leg2TV(i).XData(1) = leg2TV(i).XData(2) - legTickLen;
        leg2TV(i).LineWidth = LWleg;
    end

    leg1TV.Position = [0.3068 0.6083 0.1526 0.1676];
    %legendTV.Location = 'SouthEast';

    S2.Title.String = 'Width of erosion 200m; D=256mm';

    %Now let's play with these colors a bit
    pnb2 = length(p_crossTV);
    for ii = 1:pnb2
        if ii == 1
            p_crossTV{ii}.Color = cmap(1,:);
        else
            p_crossTV{ii}.Color = cmap(round(ii/pnb2.*cmap_len),:);
        end
    end

S3 = subplot(2,2,4);
    IG_image_file = 'Lajeunesse_IG_prof.png';
    IGim_info = imfinfo(IG_image_file);
    IG_image = imread(IG_image_file);

    imIG = imagesc(IG_image);
    axis equal

    xlabel('Inner gorge profile (Lajeunesse, 2014)')
    imIGax = gca;

    %S3.Color = f1.Color;
    S3.Color = 'w';

    %imIGax.XAxis.Color = f1.Color;
    %imIGax.YAxis.Color = f1.Color;
    imIGax.XAxis.Color = 'w';
    imIGax.YAxis.Color = 'w';
    imIGax.XLabel.Color = 'k';
    imIGax.XTickLabel = '';
    imIGax.YTickLabel = '';


S4 = subplot(2,2,3);
    TV_image_file = 'Kehew_TV_gen.png';
    TVim_info = imfinfo(TV_image_file);
    TV_image = imread(TV_image_file);

    imTV = imagesc(TV_image);
    axis equal

    xlabel({'Tunnel valley profile created by different';'generating events (Kehew et al., 2012)'})
    imTVax = gca;

    %S4.Color = f1.Color;
    S4.Color = 'w';

    %imTVax.XAxis.Color = f1.Color;
    %imTVax.YAxis.Color = f1.Color;
    imTVax.XAxis.Color = 'w';
    imTVax.YAxis.Color = 'w';
    imTVax.XLabel.Color = 'k';
    imTVax.XTickLabel = '';
    imTVax.YTickLabel = '';


%MAking the subplot look nice
S4.Position = [0.05 0.03 0.45 0.47];
S3.Position = [0.52 0.03 0.45 0.47];

%Rearanging the labels
S3.XLabel.Position(2) = 1.07e3;
S4.XLabel.Position(2) = 440;

%Adding some figure labelling
Xfact = 0.08;
Yfact = 0.08;

subplot(S1)
spanXA = S1.XLim(2) - S1.XLim(1);
spanYA = S1.YLim(2) - S1.YLim(1);
txtXA = spanXA.*Xfact + S1.XLim(1);
txtYA = spanYA.*Yfact + S1.YLim(1);

TA = text(txtXA,txtYA,'B');

subplot(S2)
spanXB = S2.XLim(2) - S2.XLim(1);
spanYB = S2.YLim(2) - S2.YLim(1);
txtXB = spanXB.*Xfact + S2.XLim(1);
txtYB = spanYB.*Yfact + S2.YLim(1);

TB = text(txtXB,txtYB,'A');

subplot(S3)
spanXC = S3.XLim(2) - S3.XLim(1);
spanYC = S3.YLim(2) - S3.YLim(1);
txtXC = spanXC.*Xfact + S3.XLim(1);
txtYC = spanYC.*Yfact + S3.YLim(1);

TC = text(txtXC,txtYC,'D');

subplot(S4)
spanXD = S4.XLim(2) - S4.XLim(1);
spanYD = S4.YLim(2) - S4.YLim(1);
txtXD = spanXD.*Xfact + S4.XLim(1);
txtYD = spanYD.*Yfact + S4.YLim(1);

TD = text(txtXD,txtYD,'C');

