%File to make a figure that show the details of the inner gorge that is formed.
%This is kindda of an attempt at making a figure for the paper
%
%
%
%It might be good to run the making topo script first to at least compute the
%topo that you might plot

%Load all the different profiles used for the different stages


load trans_IS_TR_K_vHR_D256.mat

dx = 500; %m
x_st = 1;
x_end = 541;
x_st_from_div = 820; %km

max_prof_nb = 7625;
%adv_prof_rg = [2:1:16];
%rtr_prof_rg = [18:1:31];
adv_end = 4470;
adv_prof_rg = [1:1:adv_end];
rtr_prof_rg = [adv_end+1:1:max_prof_nb];

x_t_l = [min(x_tot)-dx x_tot];

total_e_eff = zeros(1,x_end);
total_e_eff_adv = zeros(1,x_end);
total_e_eff_rtr = zeros(1,x_end);
total_E_eff = zeros(1,x_end);
total_E_eff_adv = zeros(1,x_end);
total_E_eff_rtr = zeros(1,x_end);

total_Q_tc = zeros(1,x_end);
Q_tc_int_adv = zeros(1,x_end);
Q_tc_int_rtr = zeros(1,x_end);
sum_Qtc = zeros(length(trans_IS),x_end);

ice_dt = 250;

%for i=1:length(trans_IS)
%    total_e_eff = total_e_eff + trans_IS(i).sum_e_eff.*ice_dt;
%    total_E_eff = total_E_eff + trans_IS(i).sum_E_eff.*ice_dt;

%SORT OUT THE UNITS HERE
%Qtc is in m^3/s
%Here I am computing total volume
%    sum_Qtc(i,:) = trapz(trans_IS(i).Q_tc) .* 600 .* 6;

%    Q_tc_int = Q_tc_int + sum_Qtc(i,:) .* ice_dt;
%end

%Remaking the advance retreat stuff with the cheating stuff
%max advance at 175*25. The rest is retreat.
total_e_eff = sum(e_eff_cheat).*dx;
total_E_eff = sum(E_eff_cheat).*dx;
total_Q_tc = sum(Qtc_cheat).*dx.*trans_IS(1).dt;
avg_E_eff = mean(E_eff_cheat).*dx;

total_e_eff_adv = sum(e_eff_cheat(1:adv_end,:)).*dx;
total_E_eff_adv = sum(E_eff_cheat(1:adv_end,:)).*dx;
Q_tc_int_adv = sum(Qtc_cheat(1:adv_end,:)).*dx.*trans_IS(1).dt;

avg_e_eff_adv = mean(e_eff_cheat(1:adv_end,:)).*dx;
avg_E_eff_adv = mean(E_eff_cheat(1:adv_end,:)).*dx;
Q_tc_avg_adv = mean(Qtc_cheat(1:adv_end,:)).*dx.*trans_IS(1).dt;

total_e_eff_rtr = sum(e_eff_cheat(adv_end+1:end,:)).*dx;
total_E_eff_rtr = sum(E_eff_cheat(adv_end+1:end,:)).*dx;
Q_tc_int_rtr = sum(Qtc_cheat(adv_end+1:end,:)).*dx.*trans_IS(1).dt;

avg_e_eff_rtr = mean(e_eff_cheat(adv_end+1:end,:)).*dx;
avg_E_eff_rtr = mean(E_eff_cheat(adv_end+1:end,:)).*dx;
Q_tc_avg_rtr = mean(Qtc_cheat(adv_end+1:end,:)).*dx.*trans_IS(1).dt;


fx = 8.9*2;
fy = 12;
sx = 3;
sy = 2;

LW1 = 2;
LW2 = 1.5;
LWbed = 1;
%FSax = 36;
FSax = 11;
FSlg = 9;

min_xax = 850;
max_xax = 1090;

col_advance = rgb('SkyBlue');
col_retreat = rgb('SteelBlue');
col_max =  rgb('RoyalBlue');

ice_st = 11;
ice_end = 146;
%x_ice = [ice_st-1:ice_end-1].*dx.*4./1e3 + x_st_from_div; %I want that in km and dx 4 times that for eros
x_ice = [0:ice_end-ice_st].*dx.*4./1e3 + x_st_from_div; %I want that in km and dx 4 times that for eros

f1 = figure;
f1.Units = 'centimeters';
f1.Position(3) = fx;
f1.Position(4) = fy;
f1.Units = 'normalized';
%%%%%%%%%%%%%%%%%%%%%%
%   1ST subplot
S1 = subplot(sx,sy,[1 2]);

load long_term_sims/cheat_profs_TV_1yr.mat

p1a = plot(x_ice,ice_profs_1yr(1,ice_st:ice_end),'k','LineWidth',LWbed);
hold on
%p1b = plot(x_ice,ice_profs_1yr(2+10*max_prof_nb,ice_st:ice_end),'k','LineWidth',LWbed,'Color',col_max);
p1b = plot(x_ice,ice_profs_1yr(max(adv_prof_rg),ice_st:ice_end),'k','LineWidth',LW1,'Color',col_max);

kk = 1;
for i = 125:250:max(adv_prof_rg)
    p_adv(kk,:) = plot(x_ice,ice_profs_1yr(i,ice_st:ice_end),'LineWidth',LW2,'Color',col_advance);
    kk = kk+1;
end
kk = 1;
for i = max(adv_prof_rg)+250:250:max(rtr_prof_rg)-125
    p_rtr(kk,:) = plot(x_ice,ice_profs_1yr(i,ice_st:ice_end),'LineStyle','--','LineWidth',LW2,'Color',col_retreat);
    kk = kk+1;
end

l1a = legend([p1a p_adv(1) p_rtr(2)],'Bed','Advance','Retreat');
l1a.FontSize = FSlg;
l1a.Box = 'off';
l1a.Position(2) = 0.85;
l1a.Position(1) = 0.75;

ylabel('Elevation (m)')
%S1.XTickLabel = '';

%%%%%%%%%%%%%%%%%%%%%%
%   2ND subplot
S2 = subplot(sx,sy,[3 4]);

[C,h] = contourf(x_tot,y_tot(1:100:end),resulting_topo(1:100:end,:)./1000,30);
h.LineColor = 'none';
cb = colorbar;
cb.Location = 'west';
%cb.Label.String = 'Erosion depth (m)';
cb.Label.String = 'Depth (m)';
max_depth = min(min(resulting_topo))/1000;
cb.Limits = [max_depth 0];
cb.Ticks = [-20 -15 -10 -5 0];
axis([min_xax max_xax 0 200]);

%ylabel({'Bedrock channel';'width (km)'})
ylabel('Width (m)')
xlabel('Distance from ice divide (km)')

%%%%%%%%%%%%%%%%%%%%%%
%   3RD subplot
S3 = subplot(sx,sy,5);

%pc1 = plot(x_tot,total_e_eff./1000,'--');
pc2 = plot(x_tot,total_E_eff./1e3);
hold on

pc3 = plot(x_tot,total_E_eff_adv./1e3);
pc4 = plot(x_tot,total_E_eff_rtr./1e3,'--');

%l2 = legend('\int e_{eff} dt','\int E_{eff} dt','\int E_{eff, adv} dt','\int E_{eff, rtr} dt');
l2 = legend('Total','Advance','Retreat');
l2.Location = 'NorthWest';
l2.Box = 'off';
l2.FontSize = FSlg;
%ylabel({'Time integrated erosion';'(m(x m)/m)'})
ylabel({'Volume eroded';'per cell (m^3)'})
xlabel('Distance from ice divide (km)')

pc2.LineWidth = LW1;
pc3.LineWidth = LW2;
pc4.LineWidth = LW2;
pc2.Color = 'k';
pc3.Color = col_advance;
pc4.Color = col_retreat;
max_vol = max(total_E_eff./1e3).*1.1;
axis([min_xax max_xax 0 max_vol])

%%%%%%%%%%%%%%%%%%%%%%
%   4th subplot
S4 = subplot(sx,sy,6);

pd1 = plot(x_tot,Q_tc_int_adv);
hold on
pd2 = plot(x_tot,Q_tc_int_rtr,'--');

l3 = legend('Advance','Retreat');
l3.Location = 'NorthWest';
l3.Box = 'off';
l3.FontSize = FSlg;
ylabel({'Transport';'capacity (m^3)'})
xlabel('Distance from ice divide (km)')

pd1.LineWidth = LW1;
pd2.LineWidth = LW1;
pd1.Color = col_advance;
pd2.Color = col_retreat;

max_tc_adv = max(Q_tc_int_adv);
max_tc_rtr = max(Q_tc_int_adv);
max_tc_ax = max(max_tc_adv,max_tc_rtr) * 1.1;

axis([min_xax max_xax 0 max_tc_ax])

%%%%%%%%%
%making everything neat
S1.XLim = [min_xax max_xax];

S1.FontSize = FSax;
S2.FontSize = FSax;
S3.FontSize = FSax;
S4.FontSize = FSax;

%Arranging layout
S1.XTickLabel = '';

S4.Position(1) = 0.59;

S1.Position(2) = 0.75;
S2.Position(2) = 0.5;
S3.Position(2) = 0.13;
S4.Position(2) = 0.13;

S1.Position(3) = 0.82;
S2.Position(3) = 0.82;
S3.Position(3) = 0.355;
S4.Position(3) = 0.355;

%Tick lenghts
tl1 = 0.012;
tl2 = 0.025;
S1.TickLength = [tl1 tl2];
S2.TickLength = [tl1 tl2];
S3.TickLength = [tl1 tl2];
S4.TickLength = [tl1 tl2];

S1.XMinorTick = 'on';
S1.YMinorTick = 'on';
S2.XMinorTick = 'on';
S2.YMinorTick = 'on';
S3.XMinorTick = 'on';
S3.YMinorTick = 'on';
S4.XMinorTick = 'on';
S4.YMinorTick = 'on';

%Adding text on there
subplot(S1)
xposfact = 0.05;
xposfact2 = 0.96;
yposfact = 0.85;
tA = text(S1.XLim(2) - (S1.XLim(2)-S1.XLim(1)).*xposfact,S1.YLim(2).*yposfact,'A');
subplot(S2)
tB = text(S2.XLim(2) - (S2.XLim(2)-S2.XLim(1)).*xposfact,S2.YLim(2).*yposfact,'B');
subplot(S3)
tC = text(S3.XLim(2) - (S3.XLim(2)-S3.XLim(1)).*xposfact2,S3.YLim(2).*yposfact,'C');
subplot(S4)
tD = text(S4.XLim(2) - (S4.XLim(2)-S4.XLim(1)).*xposfact2,S4.YLim(2).*yposfact,'D');
