%function to plot a contour comparison of all the flood simulations
%that I did
%
%
% If the trans_IS file doesn't exist, go back to paper_plots/load_IS_TR_comp.m to load the right files and make it.

%load trans_IS_all_flood_Vx01
load trans_IS_all_fl_D256
load ref_input
%load trans_IS_all_flood


FSlab = 8;
FSax  = 8;
FScont = 8;
FS_txt = 8;
FS_T = 10;
fx = 8.9;
fig_fact = 1;
%fy = 7.3;
%fx = 5.9;
%fig_fact = 5.9/8.9;
fy = 10;

fig1 = 1;
fig2 = 2;

%first let's make a matrix
for i=1:length(trans_IS)

flood_fact_nb = 11;

    %x = mod(i,11);
    %if x == 0 x = 11; end
    x = mod(i,flood_fact_nb);
    if x == 0 x = flood_fact_nb; end

    if i <= flood_fact_nb y =1;end
    if i >flood_fact_nb && i <= flood_fact_nb.*2 y =2;end
    if i>flood_fact_nb.*2 && i <= flood_fact_nb.*3 y =3;end
    if i>flood_fact_nb.*3 && i <= flood_fact_nb.*4 y =4;end
    if i>flood_fact_nb.*4 && i <= flood_fact_nb.*5 y =5;end
    if i>flood_fact_nb.*5 && i <= flood_fact_nb.*6 y =6;end

    flood_mat_depth(x,y) = max(trans_IS(i).sum_e_eff);
    flood_mat_eros(x,y) = trans_IS(i).result_eros;
    flood_mat_Qout(x,y) = trapz(trans_IS(i).Q_out);
    flood_Qtc_term(x,y) = trapz(trans_IS(i).Q_tc(:,end));
end

metre_fact = 1e-3;

%%%%%%%%%%%
f1 = figure(fig1);
f1.Units = 'centimeters';
f1.Position(3) = fx;
f1.Position(4) = fy;
f1.Units = 'normalized';

S1 = subplot(2,1,1);
%%%%%%%%%%%%%%%
%%%%%%% Sub plot
Q_bs = sum(b_s_time(1:17400,:)').*1000.*500; %xdx xdy 

for i = 1:6
    %p_hyd{i+1} = plot(trans_IS(11*i+11-i).Qc(:,1));
    kk = 11*i - (i-1)*2;
    %keyboard
    x_max = length(trans_IS(kk).Qc(:,1));
    %cheating correction
    x_cheat_fact = (1./(x_max./24/(i*10)));
    x_plot = [0:1:x_max-1]./24;
    x_plot = x_plot.*x_cheat_fact;
    p_hyd{i} = semilogy(x_plot,trans_IS(kk).Qc(:,1)-4.9);
    %p_hyd{i+1} = plot(x_plot,trans_IS(kk).Qc(:,1)-4.9);
    hold on
end

%line patterns
%p_hyd_symb = {'-','--','-.','-','--','-.'};
p_hyd_symb = {'-','-','-','-','-','-'};
p_hyd_LW = 1.5;
gfact1 = 0.1;
gfact2 = 0.45;
%p_hyd_col = {[1 1 1].*gfact1, [1 1 1].*gfact1, [1 1 1].*gfact1, [1 1 1].*gfact2, [1 1 1].*gfact2, [1 1 1].*gfact2};
p_hyd_col = {rgb('DarkRed'),rgb('Crimson'),rgb('DarkMagenta'),rgb('Orchid'),rgb('DodgerBlue'),rgb('DarkTurquoise')};

for i=1:6
    p_hyd{i}.LineStyle = p_hyd_symb{i};
    p_hyd{i}.Color = p_hyd_col{i};
    p_hyd{i}.LineWidth = p_hyd_LW;
end

%hyd_leg = legend('10d, V_{ref} \times 10','20d, V_{ref} \times 8','30d, V_{ref} \times 6',...
            %'40d, V_{ref} \times 4','50d, V_{ref} \times 2','60d, V_{ref} \times 0.1');

%p_seas = plot(Q_bs(1:6:end));
x_plot_s = [0:length(Q_bs)/6-1]./24;
%p_seas = semilogy(x_plot_s,Q_bs(1:6:end));

ax_hyd = gca;

%S1.YAxisLocation = 'right';
ax_hyd.YLim(1) = 0.1;
%ax_hyd.XLim = [0 120];
ax_hyd.XLim = [0 60];
ax_hyd.XTick = [0:10:60];
ax_hyd.YTick = [1 1e1 1e2 1e3 1e4 1e5];

S1.TickLength(1) = 0.013;

ax_hyd.XGrid = 'on';
ax_hyd.YGrid = 'on';
ax_hyd.YMinorGrid = 'off';
ax_hyd.YMinorTick = 'on';

ax_hyd.YLabel.String = {'Discharge';'(m^3 s^{-1})'};

ax_hyd.Position(4) = 0.27;
ax_hyd.Position(2) = 0.705;

ax_hyd.Position(1) = 0.17;
ax_hyd.Position(3) = 0.73*0.95;

S1.YLabel.Position(1) = 75;

S1.FontSize = FSax;

T_hyd = text(54.7,5e3,'A','FontSize',FS_T);

%%%%%%%%%%% END FIGURE
%keyboard
S2 = subplot(2,1,2);

%im = imagesc(flood_mat_depth);
[C,h] = contourf(flood_mat_depth.*metre_fact,15);
imax = gca;

%imax.YDir = 'normal';
imax.YLabel.String = 'Volume factor';
imax.XLabel.String = 'Flood duration (days)';
imax.FontSize = FSax;

cmap = flip(colormap(bone));
colormap(cmap)
cb = colorbar;
%cb.Location = 'West';
cb.Location = 'EastOutside';
% FOR D = 60
%caxis([0 66].*metre_fact)
% FOR D = 256
caxis([0 160].*metre_fact)
%cb.Label.String = {'Max. bedrock';'channel depth (m)'};
cb.Label.String = {'Max. depth (m)'};

%cb.Position(1) = 0.76;

h.LineColor = 'none';

imax.YTick = [1.1 2:1:flood_fact_nb];
imax.YTickLabel = {'0.1','1','2','3','4','5','6','7','8','9','10'};
imax.XTick = [1:1:6];
imax.XTickLabel = {'10','20','30','40','50','60'};
%imax.YLim = [1 11];
imax.YLim = [0.92 11.08];
imax.XLim = [0.95 6.05];

%And now plotting little squares to show where line come from
hold on
MS = 7;
gfact3 = 0.2;
gfact4 = 0.7;
p_M_col = {[1 1 1].*gfact1, [1 1 1].*gfact1, [1 1 1].*gfact1, [1 1 1].*gfact3, [1 1 1].*gfact4, [1 1 1].*gfact4};
for i=1:6
    p_symb{i} = plot(i,11-(i-1)*2,'p','MarkerSize',MS);
    %p_symb{i}.MarkerEdgeColor = p_M_col{i};
    p_symb{i}.MarkerEdgeColor = p_hyd_col{i};
    p_symb{i}.MarkerFaceColor = p_hyd_col{i};
end
%Also plotting location of flood shown
p0 = plot(2,11,'o','MarkerSize',MS,'Color','k','MarkerFaceColor','k');

%Sorting out that mess of a plot...
S2.Position(3) = 0.67 * 0.95;
S2.Position(4) = 0.49;
S2.Position(2) = 0.125;
S2.Position(1) = 0.17;

S2.YLabel.Position(1) = 0.3;

cb.Label.Rotation = 0;

cb.Position(1) = 0.83;
cb.Position(2) = 0.125;
cb.Position(4) = 0.49;

cb.Label.Position(1) = 0; 
% FOR D = 60
cb.Label.Position(2) = 0.0715;
% FOR D = 256
cb.Label.Position(2) = 0.173;


cb.Position(3) = 0.04;


T_im = text(5.6,9.5,'B','FontSize',FS_T);
T_im.BackgroundColor = [1 1 1].*0.9;
%imax.YLabel.Position(1) = 0.55;

%polishing mess after changing pic size
S1.Position(3) = S1.Position(3).*0.9;


