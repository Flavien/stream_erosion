%function to plot a contour comparison of all the flood simulations
%that I did
%
%

load trans_IS_all_flood_Vx01
load ref_input
%load trans_IS_all_flood


FSlab = 9;
FSax  = 9;
FScont = 9;
FS_txt = 9;
FS_T = 10;
fx = 8.9;
fy = 7.3;

fig1 = 1;
fig2 = 2;

%first let's make a matrix
for i=1:length(trans_IS)

flood_fact_nb = 11;

    %x = mod(i,11);
    %if x == 0 x = 11; end
    x = mod(i,flood_fact_nb);
    if x == 0 x = flood_fact_nb; end

    if i <= flood_fact_nb y =1;end
    if i >flood_fact_nb && i <= flood_fact_nb.*2 y =2;end
    if i>flood_fact_nb.*2 && i <= flood_fact_nb.*3 y =3;end
    if i>flood_fact_nb.*3 && i <= flood_fact_nb.*4 y =4;end
    if i>flood_fact_nb.*4 && i <= flood_fact_nb.*5 y =5;end
    if i>flood_fact_nb.*5 && i <= flood_fact_nb.*6 y =6;end

    flood_mat_depth(x,y) = max(trans_IS(i).sum_e_eff);
    flood_mat_eros(x,y) = trans_IS(i).result_eros;
    flood_mat_Qout(x,y) = trapz(trans_IS(i).Q_out);
    flood_Qtc_term(x,y) = trapz(trans_IS(i).Q_tc(:,end));
end

metre_fact = 1e-3;

%%%%%%%%%%%
f1 = figure(fig1);
f1.Units = 'centimeters';
f1.Position(3) = fx;
f1.Position(4) = fy;
f1.Units = 'normalized';

S1 = subplot(2,1,1);
%%%%%%%%%%%%%%%
%%%%%%% Sub plot
Q_bs = sum(b_s_time(1:17400,:)').*1000.*500; %xdx xdy 

for i = 1:6
    %p_hyd{i+1} = plot(trans_IS(11*i+11-i).Qc(:,1));
    kk = 11*i - (i-1)*2;
    %keyboard
    x_max = length(trans_IS(kk).Qc(:,1));
    %cheating correction
    x_cheat_fact = (1./(x_max./24/(i*10)));
    x_plot = [0:1:x_max-1]./24;
    x_plot = x_plot.*x_cheat_fact;
    p_hyd{i} = semilogy(x_plot,trans_IS(kk).Qc(:,1)-4.9);
    %p_hyd{i+1} = plot(x_plot,trans_IS(kk).Qc(:,1)-4.9);
    hold on
end

%line patterns
%p_hyd_symb = {'-','--','-.','-','--','-.'};
p_hyd_symb = {'-','-','-','-','-','-'};
p_hyd_LW = 1.5;
gfact1 = 0.1;
gfact2 = 0.45;
%p_hyd_col = {[1 1 1].*gfact1, [1 1 1].*gfact1, [1 1 1].*gfact1, [1 1 1].*gfact2, [1 1 1].*gfact2, [1 1 1].*gfact2};
p_hyd_col = {rgb('DarkRed'),rgb('Crimson'),rgb('DarkMagenta'),rgb('Orchid'),rgb('DodgerBlue'),rgb('DarkTurquoise')};

for i=1:6
    p_hyd{i}.LineStyle = p_hyd_symb{i};
    p_hyd{i}.Color = p_hyd_col{i};
    p_hyd{i}.LineWidth = p_hyd_LW;
end

%hyd_leg = legend('10d, V_{ref} \times 10','20d, V_{ref} \times 8','30d, V_{ref} \times 6',...
            %'40d, V_{ref} \times 4','50d, V_{ref} \times 2','60d, V_{ref} \times 0.1');

%p_seas = plot(Q_bs(1:6:end));
x_plot_s = [0:length(Q_bs)/6-1]./24;
%p_seas = semilogy(x_plot_s,Q_bs(1:6:end));

ax_hyd = gca;

%S1.YAxisLocation = 'right';
ax_hyd.YLim(1) = 0.1;
%ax_hyd.XLim = [0 120];
ax_hyd.XLim = [0 60];
ax_hyd.XTick = [0:10:60];
ax_hyd.YTick = [1 1e3 1e5];

ax_hyd.XGrid = 'on';
ax_hyd.YGrid = 'on';

ax_hyd.YLabel.String = {'Discharge';'(m^3 s^{-1})'};

ax_hyd.Position(4) = 0.2;
ax_hyd.Position(2) = 0.75;

ax_hyd.Position(1) = 0.12;
ax_hyd.Position(3) = 0.73;

S1.YLabel.Position(1) = 70;

S1.FontSize = FSax;

T_hyd = text(54.7,5e3,'A','FontSize',FS_T);

%%%%%%%%%%% END FIGURE
%keyboard
S2 = subplot(2,1,2);

%im = imagesc(flood_mat_depth);
[C,h] = contourf(flood_mat_depth.*metre_fact,15);
imax = gca;

%imax.YDir = 'normal';
imax.YLabel.String = 'Volume factor';
imax.XLabel.String = 'Flood duration (days)';
imax.FontSize = FSax;

cmap = flip(colormap(bone));
colormap(cmap)
cb = colorbar;
%cb.Location = 'West';
cb.Location = 'EastOutside';
caxis([0 66].*metre_fact)
%cb.Label.String = {'Max. bedrock';'channel depth (m)'};
cb.Label.String = {'Max. depth (m)'};

%cb.Position(1) = 0.76;

h.LineColor = 'none';

imax.YTick = [1.1 2:1:flood_fact_nb];
imax.YTickLabel = {'0.1','1','2','3','4','5','6','7','8','9','10'};
imax.XTick = [1:1:6];
imax.XTickLabel = {'10','20','30','40','50','60'};
%imax.YLim = [1 11];
imax.YLim = [0.92 11.08];
imax.XLim = [0.95 6.05];

%And now plotting little squares to show where line come from
hold on
MS = 7;
gfact3 = 0.2;
gfact4 = 0.7;
p_M_col = {[1 1 1].*gfact1, [1 1 1].*gfact1, [1 1 1].*gfact1, [1 1 1].*gfact3, [1 1 1].*gfact4, [1 1 1].*gfact4};
for i=1:6
    p_symb{i} = plot(i,11-(i-1)*2,'p','MarkerSize',MS);
    %p_symb{i}.MarkerEdgeColor = p_M_col{i};
    p_symb{i}.MarkerEdgeColor = p_hyd_col{i};
    p_symb{i}.MarkerFaceColor = p_hyd_col{i};
end

%Sorting out that mess of a plot...
S2.Position(3) = 0.67;
S2.Position(4) = 0.5;
S2.Position(2) = 0.125;

cb.Label.Rotation = 0;

cb.Position(1) = 0.83;
cb.Position(2) = 0.125;
cb.Position(4) = 0.5;

cb.Label.Position(1) = 0; 
cb.Label.Position(2) = 0.073;

%l1 = line([1 6],[1.5 1.5]);
%l1.LineStyle = '--';
%l1.Color = [1 1 1].*0.5;

%imax.Position(1) = 0.115;
%imax.Position(3) = 0.63;

%imax.Position(1) = 0.22;
%imax.Position(3) = 0.73;
%imax.Position(2) = 0.15;
%imax.Position(4) = 0.5;

cb.Position(3) = 0.04;
%cb.Position(4) = 0.415;


T_im = text(5.6,9.5,'B','FontSize',FS_T);
T_im.BackgroundColor = [1 1 1].*0.9;
%imax.YLabel.Position(1) = 0.55;


%{
%%%%%%%%%%%
f2 = figure(fig2);
f2.Units = 'centimeters';
f2.Position(3) = fx;
f2.Position(4) = fy;
f2.Units = 'normalized';

%im = imagesc(flood_mat_depth);
[C2,h2] = contourf(flood_mat_eros,15);
imax2 = gca;

%imax.YDir = 'normal';
imax2.YLabel.String = 'Volume factor';
imax2.XLabel.String = 'Flood duration (days)';
imax2.FontSize = FSax;

colormap(cmap)
cb2 = colorbar;
caxis([0 0.3])
cb2.Label.String = 'Averaged erosion (mm)';
%cb2.Position(1) = cb.Position(1);

h2.LineColor = 'none';

imax2.YTick = imax.YTick;
imax2.YTickLabel = imax.YTickLabel;
imax2.XTick = [1:1:6];
imax2.XTickLabel = imax.XTickLabel;

%imax2.YLabel.Position(1) = imax.YLabel.Position(1);

%imax2.Position = imax.Position;
%}
