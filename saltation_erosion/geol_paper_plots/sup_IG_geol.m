%file to make some plots are a supplementary material for the
% TV_geol figure
%
%TV_geol has to be run first to make everything nice
%


fx = 8.9;
fy = 12;

f2 = figure;
f2.Units = 'centimeters';
f2.Position(3) = fx;
f2.Position(4) = fy;
f2.Units = 'normalized';

min_xax = 5;
max_xax = 35;

sx2 = 3;
sy2 = 1;
%%%%%%%%%%%%%%%%%%%%%%
%   3RD subplot
S5 = subplot(sx2,sy2,1);

%pc1 = plot(x_tot,avg_e_eff./1000,'--');
pe2 = plot(x_tot,total_E_eff./1e3);
%hold on

%pe3 = plot(x_tot,avg_E_eff_adv./1e3);
%pe4 = plot(x_tot,avg_E_eff_rtr./1e3,'--');

%l2 = legend('\int e_{eff} dt','\int E_{eff} dt','\int E_{eff, adv} dt','\int E_{eff, rtr} dt');
%l4 = legend('Total','Advance','Retreat');
%l4.Location = 'NorthWest';
%l4.Box = 'off';
%l4.FontSize = FSlg;
%ylabel({'Time integrated erosion';'(m(x m)/m)'})
%ylabel({'Volume eroded';'per cell (m^3 a^{-1})'})
ylabel({'Volume eroded';'per cell (m^3)'})
%xlabel('Distance from ice divide (km)')
S5.XTickLabel = '';

pe2.LineWidth = LW1;
%pe3.LineWidth = LW2;
%pe4.LineWidth = LW2;
pe2.Color = 'k';
%pe3.Color = col_advance;
%pe4.Color = col_retreat;
%max_vol = max(avg_E_eff./1e3).*1.2;
max_vol = max(total_E_eff./1e3).*1.2;
axis([min_xax max_xax 0 max_vol])

%%%%%%%%%%%%%%%%%%%%%%
%   4th subplot
S6 = subplot(sx2,sy2,2);

pf1 = plot(x_tot,Q_tc_int);
%pf1 = plot(x_tot,Q_tc_avg_adv);
%hold on
%pf2 = plot(x_tot,Q_tc_avg_rtr,'--');

%l5 = legend('Advance','Retreat');
%l5.Location = 'NorthWest';
%l5.Box = 'off';
%l5.FontSize = FSlg;
%ylabel({'Transport';'capacity (m^3 a^{-1})'})
ylabel({'Transport';'capacity (m^3)'})
%xlabel('Distance from ice divide (km)')
S6.XTickLabel = '';

pf1.LineWidth = LW1;
%pf2.LineWidth = LW1;
%pf1.Color = col_advance;
pf1.Color = 'k';
%pf2.Color = col_retreat;

%max_tc_avg_adv = max(Q_tc_avg_adv);
%max_tc_avg_rtr = max(Q_tc_avg_adv);
%max_tc_avg_ax = max(max_tc_avg_adv,max_tc_avg_rtr) * 1.2;
max_tc_int = max(Q_tc_int) * 1.2;

axis([min_xax max_xax 0 max_tc_int])

%%%%%%%%%%%%%%%%
%Now calculate the timing of channelized conditions and plot it
sz1 = length(trans_IS);
sz2 = length(trans_IS(1).sum_e_eff);
eros_check = zeros(sz1,sz2);
trans_check = zeros(sz1,sz2);

for k =1:sz1
    tp_eros_check = find(trans_IS(k).sum_e_eff > 0);
    tp_qtc_check = find(sum_Qtc(k,:) > 0);

    eros_check(k,tp_eros_check) = 1;
    trans_check(k,tp_qtc_check) = 1;
end

dt_IG = 300; %in years
cum_eros_yrs = sum(eros_check).*dt_IG;
cum_trans_yrs = sum(trans_check).*dt_IG; 

%Now make an ugly figure for now.

S1 = subplot(sx2,sy2,3);

pa1 = plot(x_tot,cum_trans_yrs);
hold on
pa2 = plot(x_tot,cum_eros_yrs);

pa1.Color = rgb('Black');
pa2.Color = rgb('LightSlateGray');

pa1.LineWidth = LW1;
pa2.LineWidth = LW1;

xlabel('Distance from the ice divide (km)')
ylabel({'Number of years';'of activity'})
l1 = legend('Transport occurs','Erosion occurs');
l1.Box = 'off';
l1.Location = 'South';

max_eros_t = max(cum_eros_yrs);
max_trans_t = max(cum_trans_yrs);
max_yrs = max(max_eros_t,max_trans_t) * 1.2;

axis([min_xax max_xax 0 max_yrs])
%%%%%%%%%%%%%
%Making it tidy
S5.FontSize = FSax;
S6.FontSize = FSax;

%Tick lenghts
tl1 = 0.012;
tl2 = 0.025;
S5.TickLength = [tl1 tl2];
S6.TickLength = [tl1 tl2];
S1.TickLength = [tl1 tl2];

S5.XMinorTick = 'on';
S5.YMinorTick = 'on';
S6.XMinorTick = 'on';
S6.YMinorTick = 'on';
S1.XMinorTick = 'on';
S1.YMinorTick = 'on';

%Increasing hight of plots a bit
sub_h = 0.25;
S1_w = 0.72;
S1.Position(3) = S1_w;
S1_x = 0.25;
S1.Position(4) = 0.25;
S5.Position(3:4) = [S1_w sub_h];
S6.Position(3:4) = [S1_w sub_h];
S5.Position(1) = S1_x;
S6.Position(1) = S1_x;
S1.Position(1) = S1_x;

xposfact = 0.05;
xposfact2 = 0.96;
yposfact = 0.85;

subplot(S5)
tE = text(S5.XLim(2) - (S5.XLim(2)-S5.XLim(1)).*xposfact2,S5.YLim(2).*yposfact,'A');
subplot(S6)
tF = text(S6.XLim(2) - (S6.XLim(2)-S6.XLim(1)).*xposfact2,S6.YLim(2).*yposfact,'B');
subplot(S1)
tF = text(S1.XLim(2) - (S1.XLim(2)-S1.XLim(1)).*xposfact2,S1.YLim(2).*yposfact,'C');
