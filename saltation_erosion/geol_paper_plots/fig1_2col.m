%File to plot a the N-channel carved from different simulations

load_first = 1;

if load_first == 1
    % For init geol paper fig
    %load trans_IS_all_flood.mat
    % For fig with D256
    load trans_IS_all_fl_D256.mat
    tIS_all_floods = trans_IS;
    clearvars trans_IS;
    % For init geol paper fig
    %load trans_IS_flood_test
    % For fig with D256
    load trans_IS_REF_D256
    trans_IS(2) = tIS_all_floods(20);

    % Load saved var from REF run
    load ref_sim_var.mat
end

D256 = 1;

%Runs that I need:
runs_needed = [1 2];
plot_Xsect = 0;
%flag = 0 1 2
%2 for AGU presentation stuff
%0 to get the fig A and B
%1 one for just the clabel
AGU_pres = 2;

%plotting stuffs...
x_plot = [1:min(size(trans_IS(1).q_tc_vol))]-0.5;
x_plot = x_plot .* 500./1000;

x_plot2 = [0:max(size(var.H))-1].*500 ./ 1000;

t_plot = [0:1:180.*24]./24; % Time spacing in hours
t_seas_st = 20*24;
t_seas_end = 120*24 + t_seas_st;
t_fl_end = 20.*24;

%Font btwn 7 and 12
FSlab = 10;
FSax  = 8;
FSax_in  = 8;
FScont = 8;
FS_txt = 8;

text_frac = 0.9;

%Line thickness btw 1 and 2
LW1 = 1;
LW2 = 1.5;
LW3 = 2;
LW4 = 3;

fig1 =1;
fig2 =2;
fx = 8.9*2; %1 columns out of 2 columns page
%fy = 11;
fy = 14;
sx1 = 3;
sy1 = 2;

L10 = 0;
L50 = 1;

flood_col = rgb('DarkRed');
seas_col2 = rgb('DarkGreen');
%seas_col2 = rgb('ForestGreen');
seas_col1 = rgb('LightSeaGreen');

if D256 == 1
    x_prof_loc_seasA = 80;
    x_prof_loc_seasB = 93;
    x_prof_loc_fldA = 80;
else
    x_prof_loc_seasA = 60;
    x_prof_loc_seasB = 95;
    x_prof_loc_fldA = 40;
end

% Lateral offset to plot the profiles aligned
if D256 ==1 
    seas_x_offset = 23;
else
    seas_x_offset = 22.5;
end

x_start = 1;

cont_nb = 10;

run_nb = length(runs_needed);

%%%%%%%%%%%
f1 = figure(fig1);
f1.Units = 'centimeters';
f1.Position(3) = fx;
f1.Position(4) = fy;
f1.Units = 'normalized';

%textx1 = 11;
%textx2 = 20;
textx1 = 1.5;
textx2 = 9;
metre_fact = 1e-3;


% PLOTTING THE PROFILE OF THE GLACIER
S4 = subplot(sx1,sy1,1);
    
    p1 = plot(x_plot2,var.H,'b-','LineWidth',LW3);
    hold on
    p2 = plot(x_plot2,var.z_b,'k-','LineWidth',LW3);

    l4 = legend('Ice surface','Bedrock');
    l4.Box = 'off';
    l4.Location = 'SouthWest';

    S4_Xlim = [0 50];
    S4.XLim = S4_Xlim;
    S4_Xtick = [0:10:50];
    S4.XTick = S4_Xtick;
    
    S4_Ytick = [0:250:1e3];
    S4.YTick = S4_Ytick;

    S4.FontSize = FSax;
    S4.XLabel.String = 'Distance from ice divide (km)';
    S4.YLabel.String = 'Elevation (m)';
    

% PLOTTING THE WATER INPUTS
S5 = subplot(sx1,sy1,2);

    p_hyd1 = semilogy(t_plot(t_seas_st:t_seas_end)-20,trans_IS(1).Qc(t_seas_st:t_seas_end,end),'LineWidth',LW2);
    hold on
    p_hyd2 = semilogy(t_plot(1:t_fl_end),trans_IS(2).Qc(1:t_fl_end,end)-5.1,'LineWidth',LW2);

    p_hyd1.Color = seas_col2;
    p_hyd2.Color = flood_col;

    l5 = legend('Reference season','Reference flood');
    l5.Box = 'off';

    S5_Xlim = [0 120];
    S5.XLim = S5_Xlim;
    S5_Xtick = [0:20:120];
    S5.XTick = S5_Xtick;

    S5_Ylim = [0 8e3];
    S5.YLim = S5_Ylim;
    S5_YTick = [1 5 10 50 1e2 5e2 1e3 5e3];
    S5.YTick = S5_YTick;

    S5.YTickLabel = {'1','','10','','10^2','','10^3',''};


    S5.FontSize = FSax;
    S5.XLabel.String = 'Time (day)';
    S5.YLabel.String = 'Water discharge (m^3 s^{-1})';
    

kk = 1;
S1 = subplot(sx1,sy1,3);
    
    %k = runs_needed(kk);
    k = runs_needed(1);
    if D256 == 1
        vectSA1 = [-200:5:0].*metre_fact;
        vectSA2 = [-100 0].*metre_fact;
        vectSA3 = [-150 0].*metre_fact;
    else
        vectSA1 = [-120:2:0].*metre_fact;
        vectSA2 = [-10 0].*metre_fact;
        vectSA3 = [-100 0].*metre_fact;
    end

    width_coordA = trans_IS(k).width + max(trans_IS(k).width);
    [CA,hA] = contourf(x_plot(x_start:end),width_coordA,trans_IS(k).valley_topo(:,x_start:end).*metre_fact,vectSA1);
    hold on

    hA.LineColor = 'none';
    [CA3,hA3] = contour(x_plot(x_start:end),width_coordA,trans_IS(k).valley_topo(:,x_start:end).*metre_fact,vectSA3);
    [CA2,hA2] = contour(x_plot(x_start:end),width_coordA,trans_IS(k).valley_topo(:,x_start:end).*metre_fact,vectSA2);
    hA2.LineColor = 'k';
    hA3.LineColor = 'w';
    
    %colormap('gray')
    colormap('pink')
    cb = colorbar;
    %cb.Limits = XXXX
    cb.Location = 'south';

if AGU_pres == 2;
    textyA = max(trans_IS(runs_needed(2)).width) .* 0.83;
    %textyA = max(width_coordA) .* 0.9;
    %TA2 = text(textx2,textyA,'TR\_IS\_REF');
end

if AGU_pres == 2
    S1.YLim = [-25 25];
    S1.YTick = [-25 -15 -5 5 15];
    S1.YTickLabel = {'0','10','20','30','40'};
end

    %t1 = clabel(CA2,'manual');


    xlabel('Distance from ice divide (km)')
    ylabel('Bedrock channel width (m)')
    set(gca,'FontSize',FSax)


    S1.YLim = [-25 25];
    S1.YTick = [-25 -15 -5 5 15];
    S1.YTickLabel = {'0','10','20','30','40'};
    %S1.YTick = [0 10 20 30 40];

    %pAXsec1 = plot(x_prof_loc_seasA./2,[-4:1:4],'-','Color',rgb('Grey'));
    %pAXsec2 = plot(x_prof_loc_seasB./2,[-5:1:5],'Color',rgb('Maroon'));
    pAXsec1 = line([x_prof_loc_seasA./2 x_prof_loc_seasA./2],[-1 5.5],'Color',seas_col1);
    pAXsec2 = line([x_prof_loc_seasB./2 x_prof_loc_seasB./2],[-3 7],'Color',seas_col2);

    pAXsec1.LineWidth = LW4;
    pAXsec2.LineWidth = LW4;


    if D256 == 1;
        %annotations on graph:
        x0_st = 0.1750;
        x0 = [x0_st x0_st+(0.265-0.22)];
        y0_st = 0.5763;
        y0 = [y0_st y0_st-0.05]; 

        %0.5679 0.9232
        x001_st= 0.3446;
        x001 = [x001_st x001_st+0.02];
        y001 = y0; 
        

        x01_st = 0.4537;
        x01 = [x01_st x01_st+0.01];
        y01 = y0;
        a1 = annotation('textarrow',x0,y0,'String','0');
        a2 = annotation('textarrow',x001,y001,'String','-0.1');
        a3 = annotation('textarrow',x01,y01,'String','-0.15');
    else
        %annotations on graph:
        x0_st = 0.2325;
        x0 = [x0_st x0_st+(0.265-0.22)];
        y0_st = 0.5763;
        y0 = [y0_st y0_st-0.05]; 

        %0.5679 0.9232
        x001_st= 0.3267;
        x001 = [x001_st x001_st-0.02];
        y001 = y0; 
        

        x01_st = 0.4299;
        x01 = [x01_st x01_st+0.01];
        y01 = y0;
        a1 = annotation('textarrow',x0,y0,'String','0');
        a2 = annotation('textarrow',x001,y001,'String','-0.01');
        a3 = annotation('textarrow',x01,y01,'String','-0.1');
    end

    a1.FontSize = FScont;
    a3.FontSize = FScont;
    a2.FontSize = FScont;


kk = 2;
S2 = subplot(sx1,sy1,4);
    
    %k = runs_needed(kk);
    k = runs_needed(2);
    %vectSB1 = [-120:5:0];
    %vectSB2 = [-120:20:0];
    if D256 == 1
        vectSB2 = [-27 0].*metre_fact;
    end
    width_coordB = trans_IS(k).width + max(trans_IS(k).width);
    if D256 == 1
        width_coordB = width_coordB +2.5;
    end
    [CB,hB] = contourf(x_plot(x_start:end),width_coordB,trans_IS(k).valley_topo(:,x_start:end).*metre_fact,vectSA1);
    hold on
    if D256 == 1
        [CB2,hB2] = contour(x_plot(x_start:end),width_coordB,trans_IS(k).valley_topo(:,x_start:end).*metre_fact,vectSB2);
    else
        [CB2,hB2] = contour(x_plot(x_start:end),width_coordB,trans_IS(k).valley_topo(:,x_start:end).*metre_fact,vectSA2);
    end
    %hB.LineColor = [0.7 0.7 0.7];
    hB2.LineColor = 'k';
    hB.LineColor = 'none';

    %textyB = max(trans_IS(k).width) .* 0.9;
    textyB = max(width_coordB) .* 0.87;

if AGU_pres == 2
    %TB1 = text(textx1,textyB,'B');
    %TB2 = text(textx2,textyB,'FLOOD\_Nye\_shape');
    %TB2 = text(textx2,textyB,'20-days flood, V_{water} x10');
    %TB1.FontSize = FSax;
    %TB2.FontSize = FSax;
end

    cb2 = colorbar;
    cb2.Location = 'south';
    if D256 == 1
        caxis([-200 0].*metre_fact)
    else
        caxis([-120 0].*metre_fact)
    end
    cb2.Visible = 'off';
    %S2.YAxisLocation = 'right';

    xlabel('Distance from ice divide (km)')
    %ylabel('N-Channel radius (m)')
    set(gca,'FontSize',FSax)
    S2.YTick = [0 10 20 30 40];

    %pBXsec1 = plot(x_prof_loc_fldA./2,[3:1:45],'Color',rgb('Black'));
    pBXsec1 = line([x_prof_loc_fldA./2 x_prof_loc_fldA./2],[0 49.9],'Color',flood_col);

    pBXsec1.LineWidth = LW4;

if plot_Xsect == 1

    inset_axes = axes('Position',[0.16 0.295 0.23 0.2]);

        hold off
        p2a = plot(width_coordB,trans_IS(2).valley_topo(:,x_prof_loc_fldA).*metre_fact);
        hold on
        p2b = plot(width_coordA+seas_x_offset,trans_IS(1).valley_topo(:,x_prof_loc_seasA).*metre_fact);
        p2c = plot(width_coordA+seas_x_offset,trans_IS(1).valley_topo(:,x_prof_loc_seasB).*metre_fact);

        eros_2mm = (trans_IS(2).valley_topo(:,40).*0 - 2).*metre_fact;
        eros_10mm = eros_2mm *5;
        
        %p2d = plot(width_coordB,eros_2mm,'--');
        %p2e = plot(width_coordB,eros_10mm,':');

        ylabel('Depth (m)','FontSize',FSax_in)
        xlabel('Width (m)','FontSize',FSax_in)

        %l2 = legend('Flood','Annual processes (upstream)','Annual processes (downstream)');
        %l2.FontSize = FSax;
        %l2.Location = 'SouthEast';
        %l2.Box = 'off';

        ax2 = gca;
        
        inset_axes.FontSize = FSax_in;
        if D256 == 1
            inset_axes.YLim = [-200 0].*metre_fact;
        else
            inset_axes.YLim = [-120 0].*metre_fact;
        end
        inset_axes.XLim = [10 40];

        p2a.LineWidth = LW3;
        p2b.LineWidth = LW3;
        p2c.LineWidth = LW3;
        p2d.LineWidth = LW2;
        p2e.LineWidth = LW2;

        p2a.Color = 'k';
        p2b.Color = rgb('DarkSlateGrey');
        p2c.Color = rgb('LightSlateGrey');
        p2d.Color = rgb('MediumBlue');
        p2e.Color = rgb('MediumBlue');

        %Make the axis in the inset grey
        grey_fact = 0.4;
        inset_axes.XColor = [1 1 1].*grey_fact;
        inset_axes.YColor = [1 1 1].*grey_fact;
        inset_axes.XLabel.Position(2) = -0.155;
        inset_axes.Color = 'none';
        %inset_axes.XLabel.Color = [1 1 1].*grey_fact;
        %inset_axes.YLabel.Color = [1 1 1].*grey_fact;
end


if D256 ==1
    Nchan_Xlim = [25 50];
else
    Nchan_Xlim = [0 50];
end
S1.XLim = Nchan_Xlim;
S2.XLim = Nchan_Xlim;

if D256 ==1
    Nchan_Xtick = [25:2.5:50];
else
    Nchan_Xtick = [0:5:50];
end
S1.XTick = Nchan_Xtick;
S2.XTick = Nchan_Xtick;

if D256 ==1
    Nchan_XTickLab = {'25','','30','','25','','40','','45','','50'};
else
    Nchan_XTickLab = {'0','','10','','20','','30','','40','','50'};
end

S1.XTickLabel = Nchan_XTickLab;
%S1.XTickLabel = '';
S2.XTickLabel = Nchan_XTickLab;

S3 = subplot(sx1,sy1,[5 6]);
plot_scale_fact = 10;

        p2a = plot(width_coordB,trans_IS(2).valley_topo(:,x_prof_loc_fldA).*metre_fact.*plot_scale_fact);
        hold on
        p2b = plot(width_coordA+seas_x_offset,trans_IS(1).valley_topo(:,x_prof_loc_seasA).*metre_fact.*plot_scale_fact);
        p2c = plot(width_coordA+seas_x_offset,trans_IS(1).valley_topo(:,x_prof_loc_seasB).*metre_fact.*plot_scale_fact);

        l3 =legend([p2a,p2b,p2c],'Flood-channel','Season-channel upper reach','Season-channel lower reach');
        %l3.Box = 'off';

        eros_2mm = (trans_IS(2).valley_topo(:,40).*0 - 2).*metre_fact.*plot_scale_fact;
        eros_10mm = eros_2mm *5;
        
        %p2d = plot(width_coordB,eros_2mm,'--');
        %p2e = plot(width_coordB,eros_10mm,':');

        ylabel('Depth (\times 10 ^{-1} m)','FontSize',FSax_in)
        xlabel('Width (m)','FontSize',FSax_in)

        %l2 = legend('Flood','Annual processes (upstream)','Annual processes (downstream)');
        %l2.FontSize = FSax;
        %l2.Location = 'SouthEast';
        %l2.Box = 'off';

        inset_axes = gca;
        
        inset_axes.FontSize = FSax_in;
        if D256 == 1
            inset_axes.YLim = [-200 0].*metre_fact.*plot_scale_fact;
        else
            inset_axes.YLim = [-120 0].*metre_fact.*plot_scale_fact;
        end
        if D256 == 1
            inset_axes.XLim = [10 40];
        else
            inset_axes.XLim = [10 40];
        end

        p2a.LineWidth = LW3;
        p2b.LineWidth = LW3;
        p2c.LineWidth = LW3;
        %p2d.LineWidth = LW2;
        %p2e.LineWidth = LW2;

        p2a.Color = flood_col;
        p2b.Color = seas_col1;
        p2c.Color = seas_col2;
        %p2d.Color = rgb('MediumBlue');
        %p2e.Color = rgb('MediumBlue');

        %Make the axis in the inset grey
        grey_fact = 0.4;
        %inset_axes.XColor = [1 1 1].*grey_fact;
        %inset_axes.YColor = [1 1 1].*grey_fact;
        %inset_axes.XLabel.Position(2) = -0.155;
        %inset_axes.Color = 'none';
        %tC = text(11,-(0.12 - 0.12.*0.85).*plot_scale_fact,'C','FontSize',FSax);

%Making everything pretty again...
sub_h1 = 0.33;
sub_h2 = 0.21;
S1.Position(4) = sub_h1;
S2.Position(4) = sub_h1;
S4.Position(4) = sub_h2;
S5.Position(4) = sub_h2;
S3.Position(4) = sub_h2.*0.75;

sub_w1 = 0.40;
S2.Position(3) = sub_w1;
S1.Position(3) = sub_w1;

sub_x1 = S1.Position(1)- 0.04;
S1.Position(1) = sub_x1; 
sub_x2 = S2.Position(1)- 0.04;
S2.Position(1) = sub_x2; 

sub_y2 = S4.Position(2) + 0.04;
S4.Position(2) = sub_y2;
S5.Position(2) = sub_y2;

sub_y1 = S1.Position(2)- 0.065;
S2.Position(2) = sub_y1;
S1.Position(2) = sub_y1;

new_w = S3.Position(3) .* 0.6;
S3.Position(3) = new_w;
new_x = S3.Position(1) + 0.153;
S3.Position(1) = new_x;
new_y = S3.Position(2) - 0.02;
S3.Position(2) = new_y;

l3.Position = [0.5785 0.1285 0.2616 0.0806];

S1.TickLength = [0.02 0.025];
S2.TickLength = [0.02 0.025];
S3.TickLength = [0.02 0.025];
S4.TickLength = [0.02 0.025];
S5.TickLength = [0.02 0.025];

%S1.YLabel.Position(2) = -25;

%S3.YLabel.Position(1) = 5.5;

%cb.Position(2) = 0.71;
%cb.Position(4) = 0.03;
cb.Label.String = 'Depth (m)';
cb.Label.Position(1) = -0.09;
cb.Label.Position(2)= 1.8;
if D256 == 1
    cb.Ticks = [-200:50:0].*metre_fact;
else
    cb.Ticks = [-120:20:0].*metre_fact;
end
cb.FontSize = FScont;

clabel(CB2,hB2,'Color','k','FontSize',FScont,'LabelSpacing',1000)


%xposfact = 0.05;
xposfact = 0.95;
%xposfact2 = 0.96;
yposfact = 0.85;
yposfact2 = 0.15;
subplot(S4)
tA = text(S4.XLim(2) - (S4.XLim(2)-S4.XLim(1)).*xposfact,S4.YLim(2) - (S4.YLim(2)-S4.YLim(1)).*yposfact2,'A');
subplot(S5)
tB = text(S5.XLim(2) - (S5.XLim(2)-S5.XLim(1)).*xposfact,S5.YLim(2) - (S5.YLim(2)-S5.YLim(1)).*yposfact2,'B');
tB.Position(2) = 996.7618; % BECAUSE OF LOG PLOT
subplot(S1)
tC = text(S1.XLim(2) - (S1.XLim(2)-S1.XLim(1)).*xposfact,S1.YLim(2) - (S1.YLim(2)-S1.YLim(1)).*yposfact2,'C');
subplot(S2)
tD = text(S2.XLim(2) - (S2.XLim(2)-S2.XLim(1)).*xposfact,S2.YLim(2) - (S2.YLim(2)-S2.YLim(1)).*yposfact2,'D');
subplot(S3)
tE = text(S3.XLim(2) - (S3.XLim(2)-S3.XLim(1)).*xposfact,S3.YLim(2) - (S3.YLim(2)-S3.YLim(1)).*yposfact2,'E');

tA.FontSize = FSax;
tB.FontSize = FSax;
tC.FontSize = FSax;
tD.FontSize = FSax;
tE.FontSize = FSax;
