%Script to show how the interpolation of erosion is done between two profiles
%
%This is supposed to be run after TV_geol
%
%Making a figure for the sup mat

prof1 = 5;
prof2 = 6;


fx = 12;
fy = 4;

LW1 = 2;
LW2 = 1;

f2 = figure;
f2.Units = 'centimeters';
f2.Position(3) = fx;
f2.Position(4) = fy;
f2.Units = 'normalized';
%%%%%%%%%%%%%%%%%%%%%%

% /1e3 factor on sum_e_eff is to get the value in meters
p1 = plot(x_tot,trans_IS(prof1).sum_e_eff./1e3);
hold on
p2 = plot(x_tot,trans_IS(prof2).sum_e_eff./1e3);

for i=1:250
    p3(i,:) = plot(x_tot,e_eff_cheat(prof1*250-125+i,:)./1e3,'-.','LineWidth',LW2,'Color',rgb('LightSlateGrey'));
end

ax_eros = gca;

p1.LineWidth = LW1;
p2.LineWidth = LW1;
p1.Color = rgb('DarkSlateGrey');
p2.Color = 'k';
%p1.LineStyle = '--';

ax_eros.XLim = [935 980];

xlabel('Distance from ice divide (km)')
ylabel({'Erosion per';'unit width (m a^{-1})'})

l_eros = legend([p1 p2 p3(1,:)],'Erosion @ 1250 years','Erosion @ 1500 years','Interpolated erosion');
l_eros.Location = 'NorthWest';
l_eros.Box = 'off';
