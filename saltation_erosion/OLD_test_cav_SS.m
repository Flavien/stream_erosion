%Script to try to compute the erosion by saltating bedload
%from one the run I did for the 1st paper.
%So far only taking erosion from the channels
%
%

clc

disp('Make sure you have a matlabpool running before doing anything!')
disp('Try something like matlabpool(4) if on the mac, more on desktop machine')
pause

%Call file to load the constants
[constants tau_c D W q_s slope Qw Hw] = init_test;

mac_path = '../hydro/runs/';
ubuntu_path = '../../subglacial_water_flow/runs/';

file_name = 'cav_SS_VG01.mat';

file_to_load = strcat(ubuntu_path,file_name);

t_slice = [100 0];

%Load file from previous run
[q_w RH h p_s tau_b_sub tau_tot u_shear_sub u_mean_sub] = extract_cav_run(file_to_load,constants,t_slice);

%Make an array of q_s to reproduce figure 10 in S&D 2004
Qs_array = [0:0.5:50];
q_s_array = Qs_array./W;

disp('Shields stress computed with u_shear, see compute_Shields')
%disp('Shields stress computed with tau_b, see compute_Shields')

for j=1:min(size(q_w))

        parfor i=1:length(q_w)

        %Set up the good values for this round:
        tau_b = tau_b_sub(i,j);
        %I'm not too sure about the partitionning quite yet...
        %tau_b = tau_tot(i,j);
        u_shear = u_shear_sub(i,j);
        u_mean = u_mean_sub(i,j);
        
        %for k=1:length(q_s_array)
        k=1;

            %q_s = q_s_array(k);

            %Call function to compute saltation erosion 
            [e_salt E Vi Ir Fe q_tc tau w_f w_si Ls Dmax] = salt_eros_R_channel(constants,tau_c,D,q_s,tau_b,u_shear,u_mean);

            %Storing everything in arrays
            e_salt_ar(i,j)    = e_salt;
            E_ar(i,j)         = E;
            Vi_ar(i,j)        = Vi;
            Ir_ar(i,j)        = Ir;
            Fe_ar(i,j)        = Fe;
            q_tc_ar(i,j)      = q_tc;
            tau_ar(i,j)       = tau;
            tau_b_ar(i,j)     = tau_b;
            u_shear_ar(i,j)   = u_shear;
            w_f_ar(i,j)       = w_f;
            w_si_ar(i,j)      = w_si;
            Ls_ar(i,j)        = Ls;
            Dmax_ar(i,j)      = Dmax;
        %end

    end

end
tau_ratio = tau_ar ./ tau_c;
excess_tau = tau_ratio - 1;
