%File to make a plot and some kind of sense out of the equivalent river profile
%
%
%

avg_slope = sum(slope_eq(5250:115*6*24,:)) ./ length(slope_eq(5250:115*6*24,:));

Q_avg = sum(Qc(5250:115*6*24,:)) ./ length(Qc(5250:115*6*24,:));

h1 = 0.5;
h2 = 1;

Cr = 6e-6;

eros_SP = Cr .* Q_avg.^h1 .* avg_slope.^h2;
