%File to show which path to add when running a script
%In the saltation_erosion directory
addpath('erosion_functions/')
addpath('plot_functions/')
addpath('reproducing_results/')
addpath('init_load_extract/')
addpath('paper_plots/')
addpath('long_term_sims/')
addpath('geol_paper_plots/')
addpath('geol_paper_plots/literature_images/')
addpath('hydraulic_potential_check/')
