%Little piece of script to initialize the constants and parameters
%This file is for now generic but will be supposed to be run-specific
%in the future
%


function [constants tau_c D W q_s slope Qw Hw q_tot theta] = init_tot_load();

%   CONSTANTS
%Values from S&D 2004
rho_w   = 1000;
rho_s   = 2650;
g       = 9.81;
man_n   = 0.035;
%man_n   = 0.05;
r       = (rho_s - rho_w)/rho_w;
Y       = 5.0e10; % 5.0e4 MPa
k_v     = 1.0e6; % [m^-1]
%Typo in S & D 2004, where kv = 10e12 while it should be 10e6 (See Lamb 2008 tab.1)
sigma_T = 7e6; % 7MPa
%nu: kinematic viscosity of water
nu      = 10^(-6); %[m2/s]
kappa   = 0.41; % Von Karman's constant
lambda_s  = 0.35; %porosity of bed sediment
secondsinyear = 365.25*24*3600;
%A1 < 1; discussed in paragraph 16, yet no actual number given...
%Alternative solution is to express final erosion as a function of A2 * w_s, yet seems to defete the purpose here
A1      = 0.36;
%A1 accounts for upward lift of particles away from the bed
%Attempt to compute that from the test case and A1*w_i = A2*w_s (A2 = 1/3)
%A1 = A2 * w_s / w_si = 0.3114 
%A1 = A2 * w_s / w_i_eff = 0.3108 
%Actually A1 = A2 = 0.36 from paragraph 43... finally!
constants = struct('rho_w',rho_w,...
                    'rho_s',rho_s,...
                    'g',g,...
                    'man_n',man_n,...
                    'r',r,...
                    'Y',Y,...
                    'k_v',k_v,...
                    'sigma_T',sigma_T,...
                    'nu',nu,...
                    'kappa',kappa,...
                    'lambda_s',lambda_s,...
                    'A1',A1,...
                    'secondsinyear',secondsinyear);

%   NOT ALWAYS CST
%Values from S&D 2004
no_supply = 0;
D60 = 1;
D1 = 0;
Dchoice = 0;

tau_c   = 0.03;
if D60 == 1
    D       = 0.06;
    disp('D = 60 mm')
elseif D1 == 1
    D       = 0.001
    disp('D = 1 mm')
elseif Dchoice == 1
    D       = input('enter the D you want: ');
    %D       = 0.001;
else
    error('You have not specified a grain size!')
end
%D       = 0.10;
W       = 18.0;
%q_s     = 42.6/W; %kg/s

q_tot   = 8.9e-4; %Volumetric sed supply per unit width (Lamb et al. 2008)
q_tot = q_tot .* 4.00; %Necessary for the IS tests...
%test on Us limitation...
%q_tot = 0.005;
%q_tot as of max out of Bench, i.e. q_sup = 60kg/s / 2650 / W_stream (10m) 
%q_tot = 22.64e-4;
%q_tot   = 2.4e-4; %Volumetric sed supply per unit width to match minimum q_tc in ref run
%%Supply for the cavity system
%q_tot   = 8.9e-4./2000;
%q_tot   = 8.9e-4./1000;
%q_tot   = 8.9e-4./1000.*4;
%for the changes in q_tot here what I will use compared to q_tot
% /20 /10 /5 /2 *1.5 *2 *3 *4 *5
%q_tot = q_tot .* 25.00;
%q_tot = q_tot ./ 2;

if no_supply == 1
    q_tot   = 0;
    disp('sediment supply q_tot = 0')
end
q_s     = q_tot * rho_s;
%written in the same units as in Lamb's paper.
slope   = 0.0053;
%slope   = 0.0;
%Qw      = 39.1;
theta   = slope;

%In the paper S & D say:
%Hw      = 1.1;
%But solving numerically gives:
%Hw      = 1.07369036;
%Hw      = 1.07;

%Values for Lamb et al 2008
Hw      = 0.95;
%Hw      = 1.04;
U       = 2.2; 
%Qw      = Hw * W * U;
Qw      = 37.8;

