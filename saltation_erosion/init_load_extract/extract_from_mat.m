%function to extract the variable that I want from a mat file and then
%it is called by the function to load the structure.
%This will allow for a cleaner loading
%


function [E e_salt Fe Vi Ir q_tc tau tau_b tau_t u_shear tau_rat tau_ex Dmax] = extract_from_mat(load_name);

load(load_name);

E       = E_ar(end,:);
e_salt  = e_salt_ar(end,:);
Fe      = Fe_ar(end,:);
Vi      = Vi_ar(end,:);
Ir      = Ir_ar(end,:);
q_tc    = q_tc_ar(end,:);
tau     = tau_ar(end,:);
tau_b   = tau_b_sub(end,:);
tau_t   = tau_tot(end,:);
u_shear = u_shear_sub(end,:);
tau_rat = tau_ratio(end,:);
tau_ex  = excess_tau(end,:);
Dmax    = Dmax_ar(end,:);
