%function to extract the variable that I want from a mat file and then
%it is called by the function to load the structure.
%This will allow for a cleaner loading
%


function  [q_w h E e_tot Fe w_i_eff cb q_tc q_b tau tau_b tau_tot u_shear tau_ratio tau_ex Dmax q_tot] =...
                                         extract_tot_load_cav(load_name);

load(load_name);

q_w     = q_w(end,:);
h       = R(end,:);
E       = E_ar(end,:);
e_tot  = e_tot_ar(end,:);
Fe      = Fe_ar(end,:);
w_i_eff      = w_i_eff_ar(end,:);
cb      = cb_ar(end,:);
q_tc    = q_tc_ar(end,:);
q_b     = q_b_ar(end,:);
tau     = tau_ar(end,:);
tau_b   = tau_b_sub(end,:);
tau_t   = tau_tot(end,:);
u_shear = u_shear_sub(end,:);
tau_ratio = tau_ratio(end,:);
tau_ex  = excess_tau(end,:);
Dmax    = Dmax_ar(end,:);
q_tot   = q_tot_temp(end,:);
