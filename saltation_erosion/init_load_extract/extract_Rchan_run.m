%Function to extract what I need from a run that show subglacial channels.
%The quantities I will need are:
% S Qc RH p_c fr n_ice n_bed R
%
%Look at Clarke 2003 equations for now, then see what happens with cavities and channels together.

function [Q_c RH_int R_int p_c tau_b tau_tot u_shear u_mean slope_eq dt dx] = extract_Rchan_run(file_to_load,constants,t_sub,shear_avg)

load(file_to_load);
t_end = length(Q_c);

dt = cst.dt;
%disp('dt = dt*5 for sed transport to go faster! Change that back later')
%dt = dt.*5;
dx = cst.dx;

t_step = 1;
if t_sub(1) == 99999
    %t_slice = [1 t_end];
    t_slice = [1 6*24*30*5];
    %t_slice = [1 6*24*30*6];
elseif t_sub(1) == 88888
    t_slice = [6*24*30 6*24*30*6];
elseif t_sub(1) == 77777
    %GOTTA TPUT SOMETHING HERE...
    t_slice = [6*24*395 6*24*540];
    t_step = 12;
else
    %For SS simulations
    %t_slice = [t_end-t_sub(1) t_end-t_sub(2)];
    %For flood simulations
    t_slice = [t_sub(1) t_sub(2)];
end
dt = dt * t_step;

%keyboard

%Qc = Q_c;
S = S_sol(t_slice(1):t_step:t_slice(2),:);
S_avg = S_sol_avg(t_slice(1):t_step:t_slice(2),:);
Q_c = Q_c(t_slice(1):t_step:t_slice(2),:);

%TEMPORARY TOO
%disp('If n_ice not defined, come look in extract_Rchan... l20')
n_ice = cst.n_ice;
n_bed = cst.n_bed;
%n_ice = 0.01;
%n_bed = 0.05;

%HAVE TO BE PHI_C - PHI_C_0 BUT GOOD ENOUGH FOR NOW
p_c = phi_c(t_slice(1):t_step:t_slice(2),:);
dphicdx = dphicdx(t_slice(1):t_step:t_slice(2),:);

u_mean = Q_c./S_avg;

%Semi circular channel hence factor 2
R = sqrt(2.*S/pi);

%Trying to find out about an equivalent slope for a river
%Assuming that the cross section of flow would be the same
R_int = (R(:,2:end) + R(:,1:end-1)) ./ 2;

%W_eq = 2.*R;
%H_eq = pi.*R ./ 4;
W_eq = 2.*R_int;
H_eq = pi.*R_int ./ 4;

p_eq = constants.rho_w .* constants.g .* H_eq;
slope_eq = -dphicdx ./ H_eq;


RH = (pi.*R)./(2.*(pi+2));
RH_int = (pi.*R_int)./(2.*(pi+2));

%Try again with the hydraulic radius
H_eq2 = (1./RH_int - 2./W_eq).^(-1);
p_eq2 = constants.rho_w .* constants.g .* H_eq2;
slope_eq2 = -dphicdx ./ H_eq2;

%Froude numbers for ice and bed
Fr_i = (8 * constants.g * n_ice^2) ./ RH_int.^(1/3);
Fr_b = (8 * constants.g * n_bed^2) ./ RH_int.^(1/3);

%Test if averaged Fr matches
fr = (pi.*Fr_i + 2.*Fr_b) ./ (pi+2);

%Compute total shear stress
tau_tot = 1/8 * fr * constants.rho_w .* u_mean .* abs(u_mean);



%Compute shear stress on ice and bed separately
tau_i = 1/8 .* Fr_i .* constants.rho_w .* u_mean .* abs(u_mean);
tau_b = 1/8 .* Fr_b .* constants.rho_w .* u_mean .* abs(u_mean); 

%Test again total shear stress:
tau_test = (pi*R_int.*tau_i + 2*R_int.*tau_b) ./ ((2+pi).*R_int);

%Compute shear velocity
if shear_avg == 0
    u_shear = (tau_b ./ constants.rho_w).^(1/2);
elseif shear_avg == 1
    u_shear = (tau_tot ./ constants.rho_w).^(1/2);
end
