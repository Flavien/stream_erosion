%Function to extract what I need from a run that show subglacial channels.
%The quantities I will need are:
% S Qc RH p_c fr n_ice n_bed R
%
%Look at Clarke 2003 equations for now, then see what happens with cavities and channels together.

function [Qc RH R p_c tau_b tau_tot u_shear u_mean] = extract_sub_channel(file_to_load,constants)

load test_sub_eros_D81.mat
%load test_sub_eros_IS_D.mat

Qc = Qc_plot;
S = conduit_plot(1:3:end,:);
RH = RH_plot;
fr = fr_plot;

p_c = w_pressure_c_plot(1:3:end,:);

u_mean = Qc./S;

R = sqrt(2.*S/pi);

%Compute total shear stress
tau_tot = 1/8 * fr * constants.rho_w .* u_mean .* abs(u_mean);

%Froude numbers for ice and bed
Fr_i = (8 * constants.g * n_ice^2) ./ RH.^(1/3);
Fr_b = (8 * constants.g * n_bed^2) ./ RH.^(1/3);

%Test if averaged Fr matches
Fr_test = (pi.*Fr_i + 2.*Fr_b) ./ (pi+2);

%Compute shear stress on ice and bed separately
tau_i = 1/8 .* Fr_i .* constants.rho_w .* u_mean .* abs(u_mean);
tau_b = 1/8 .* Fr_b .* constants.rho_w .* u_mean .* abs(u_mean); 

%Test again total shear stress:
tau_test = (pi*R.*tau_i + 2*R.*tau_b) ./ ((2+pi).*R);

%Compute shear velocity
%u_shear = (tau_b ./ constants.rho_w).^(1/2);
u_shear = (tau_tot ./ constants.rho_w).^(1/2);
