%Function to extract what I need from a run that show subglacial channels.
%The quantities I will need are:
% S Qc RH p_c fr n_ice n_bed R
%
%Look at Clarke 2003 equations for now, then see what happens with cavities and channels together.

function [q_w RH h_avg p_s tau_b tau_tot u_shear u_mean slope_eq dt dx] = extract_cav_run(file_to_load,constants,t_sub)

load(file_to_load);
t_end = length(q_w);

dt = cst.dt;
dx = cst.dx;

if t_sub(1) == 99999
    %t_slice = [1 t_end];
    t_slice = [1 6*24*30*6];
else
    t_slice = [t_end-t_sub(1) t_end-t_sub(2)];
end

h = h_sol(t_slice(1):t_slice(2),:);
h_avg = h_sol_avg(t_slice(1):t_slice(2),:);
q_w = q_w(t_slice(1):t_slice(2),:);

%HAVE TO BE PHI_C - PHI_C_0 BUT GOOD ENOUGH FOR NOW
p_s = phi(t_slice(1):t_slice(2),:);

u_mean = q_w ./ h_avg;

RH = h_avg ./2;

%Froude numbers for ice and bed
Fr_i = (8 * constants.g * cst.n_ice^2) ./ RH.^(1/3);
Fr_b = (8 * constants.g * cst.n_bed^2) ./ RH.^(1/3);

%Test if averaged Fr matches
fr = (pi.*Fr_i + 2.*Fr_b) ./ (pi+2);

%Compute total shear stress
tau_tot = 1/8 * fr * constants.rho_w .* u_mean .* abs(u_mean);



%Compute shear stress on ice and bed separately
tau_i = 1/8 .* Fr_i .* constants.rho_w .* u_mean .* abs(u_mean);
tau_b = 1/8 .* Fr_b .* constants.rho_w .* u_mean .* abs(u_mean); 

%Test again total shear stress:
tau_test = (tau_i + tau_b) ./ 2;

%Compute shear velocity
u_shear = (tau_b ./ constants.rho_w).^(1/2);
%u_shear = (tau_tot ./ constants.rho_w).^(1/2);

%Computing equivalent slope in the cavities
dphidx = dphidx(t_slice(1):t_slice(2),:);

slope_eq = -dphidx ./ h_avg;
