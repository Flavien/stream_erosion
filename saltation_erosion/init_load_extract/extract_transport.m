%function to extract the variable that I want from a mat file and then
%it is called by the function to load the structure.
%This will allow for a cleaner loading
%


function  [e_tot e_salt eta int_eta_dx Fe w_i_eff cb q_tc tau_b tau_tot u_shear tau_ratio excess_tau Dmax q_tot R] =...
                                         extract_transport(load_name);

load(load_name);

e_tot  = e_tot_ar(end,:)*1e3;
e_salt  = E_ar(end,:)*1e3;
eta     = eta_ar(end,:);
int_eta_dx = int_eta_dx;
Fe      = Fe_ar(end,:);
w_i_eff = w_i_eff_ar(end,:);
cb      = cb_ar(end,:);
q_tc    = q_tc_ar(end,:);
tau_b   = tau_b_sub(end,:);
tau_tot = tau_tot(end,:);
u_shear = u_shear_sub(end,:);
tau_ratio = tau_ratio(end,:);
excess_tau  = excess_tau(end,:);
Dmax    = Dmax_ar(end,:);
q_tot   = q_tot_temp(end,:);
R       = R(end,:);
