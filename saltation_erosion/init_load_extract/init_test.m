%Little piece of script to initialize the constants and parameters
%This file is for now generic but will be supposed to be run-specific
%in the future
%


function [constants tau_c D W q_s slope Qw Hw] = init_test();

%   CONSTANTS
%Values from S&D 2004
rho_w   = 1000;
rho_s   = 2650;
g       = 9.81;
man_n   = 0.035;
%man_n   = 0.05;
r       = (rho_s - rho_w)/rho_w;
Y       = 5.0e10; % 5.0e4 MPa
k_v     = 1.0e6; % [m^-1]
%Typo in S & D 2004, where kv = 10e12 while it should be 10e6 (See Lamb 2008 tab.1)
sigma_T = 7e6; % 7MPa
%nu: kinematic viscosity of water
nu      = 10^(-6); %[m2/s]
kappa   = 0.41; % Von Karman's constant
secondsinyear = 365.25*24*3600;

constants = struct('rho_w',rho_w,...
                    'rho_s',rho_s,...
                    'g',g,...
                    'man_n',man_n,...
                    'r',r,...
                    'Y',Y,...
                    'k_v',k_v,...
                    'sigma_T',sigma_T,...
                    'nu',nu,...
                    'kappa',kappa,...
                    'secondsinyear',secondsinyear);

%   NOT ALWAYS CST
%Values from S&D 2004

tau_c   = 0.03;
D       = 0.06;
%D       = 0.10;
W       = 18.0;
q_s     = 42.6/W; %kg/s
%written in the same units as in Lamb's paper.
slope   = 0.0053;
Qw      = 39.1;

%In the paper S & D say:
%Hw      = 1.1;
%But solving numerically gives:
%Hw      = 1.07369036;
Hw      = 1.07;

%Values for Lamb et al 2008
%Hw      = 0.95;
%U       = 2.2; 
%Qw      = Hw * W * U;
%Qw      = 37.8;
